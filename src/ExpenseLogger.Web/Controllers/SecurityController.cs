﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Mail;
using ExpenseLogger.Models.Security;
using ExpenseLogger.Web.Models.Security;
using Newtonsoft.Json;
using WebExtras.Bootstrap;
using WebExtras.Core;
using WebExtras.FontAwesome;
using WebExtras.Mvc.Bootstrap;
using WebExtras.Mvc.Core;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Controllers
{
  /// <summary>
  ///   Security controller
  /// </summary>
  [Authorize]
  public partial class SecurityController : Controller
  {
    private readonly IUserRepository m_userRepo;
    private readonly IDataRepository m_dataRepo;

    public SecurityController(IUserRepository userRepo, IDataRepository dataRepo)
    {
      m_dataRepo = dataRepo;
      m_userRepo = userRepo;
    }

    // GET: security/error
    [AllowAnonymous]
    public virtual ActionResult Error(string reason = "")
    {
      return View(MVC.Shared.Views.Error, reason);
    }

    // GET: security/register
    [AllowAnonymous]
    public virtual ActionResult Register()
    {
      return View();
    }

    // GET: security/signin
    [AllowAnonymous]
    public virtual ActionResult SignIn()
    {
      return View(new SignInViewModel());
    }

    // POST: security/signin
    [HttpPost]
    [AllowAnonymous]
    public virtual ActionResult SignIn(SignInViewModel model)
    {
      if (!ModelState.IsValid)
        return View(model);

      bool isValid = m_userRepo.ValidateUser(model.Username, model.Password);

      if (!isValid)
      {
        ModelState.AddModelError("", "Username or password incorrect.");
        return View(model);
      }

      FormsAuthentication.RedirectFromLoginPage(model.Username, model.RememberMe);

      this.SaveLastActionMessage("Signin successful", EMessage.Success);

      return RedirectToAction(MVC.Expenses.Index());
    }

    public virtual ActionResult SignOut()
    {
      FormsAuthentication.SignOut();

      this.SaveLastActionMessage("Signout successful", EMessage.Success);
      return RedirectToAction(Actions.SignIn());
    }

    // GET: security/userprofile
    public virtual ActionResult UserProfile()
    {
      User user = m_userRepo.GetUser(User.Identity.Name);

      // unset the secret answer hash so it doesn't show on the page
      user.SecretAnswer = string.Empty;

      UserProfileViewModel model = new UserProfileViewModel
      {
        User = user,
        Settings = user.Settings
      };

      return View(model);
    }

    // POST: security/userprofile
    [HttpPost]
    public virtual ActionResult UserProfile(UserProfileViewModel model)
    {
      ModelState.IgnoreErrorsFor<UserProfileViewModel>(f => f.User.Username, g => g.User.SecretAnswer, h => h.User.SecretQuestion);

      ModelState.Remove("User.Username");
      ModelState.Remove("User.PasswordHash");
      ModelState.Remove("User.SecretAnswer");

      if (!ModelState.IsValid)
        return View(model);

      User user = m_userRepo.GetUser(User.Identity.Name);
      user.FullName = model.User.FullName;
      user.Country = model.User.Country;
      if (user.Email != model.User.Email)
      {
        user.Email = model.User.Email;
        user.IsVerified = false;
      }

      if (!string.IsNullOrEmpty(model.User.PasswordHash))
        user.PasswordHash = MD5Helper.CreateHash(model.User.PasswordHash);

      user.SecretQuestion = model.User.SecretQuestion;

      if (!string.IsNullOrEmpty(model.User.SecretAnswer))
        user.SecretAnswer = MD5Helper.CreateHash(model.User.SecretAnswer);

      user.SettingsJson = JsonConvert.SerializeObject(model.Settings);

      m_userRepo.SaveUser(user);

      this.SaveLastActionMessage("Profile updated", EMessage.Success);

      return RedirectToAction(Actions.UserProfile());
    }

    // GET: security/settings
    public virtual ActionResult Settings()
    {
      Hyperlink link = new Hyperlink("Complete setup", Url.Action(MVC.Security.SmtpSettings(Url.Action(MVC.Expenses.Index()))))
        .AsButton(EBootstrapButton.Default, EBootstrapButton.XSmall);

      EmailSettings emailSettings = m_dataRepo.GetSetting<EmailSettings>(ESystemSetting.Smtp);
      IDictionary<string, string> dict = new Dictionary<string, string>();
      if (emailSettings != null)
        dict = emailSettings.ToDictionary();
      else
        this.SaveUserAlert(new Alert(EMessage.Warning,
          "Reminders setup is not complete. No emails can be sent until this is done. " + link.ToHtmlString(),
          " Warning:", EFontAwesomeIcon.Exclamation_Circle));

      SettingsViewModel model = new SettingsViewModel
      {
        AllCategories = m_dataRepo.GetCategoriesForUser(User.Identity.Name).ToList(),
        DataProviderSettings = m_dataRepo.GetProviderSummary(),
        EmailSettings = dict
      };

      return View(model);
    }

    // GET: security/smtpsettings
    public virtual ActionResult SmtpSettings(string returnUrl)
    {
      EmailSettings settings = m_dataRepo.GetSetting<EmailSettings>(ESystemSetting.Smtp);

      SmtpSettingsViewModel model = new SmtpSettingsViewModel
      {
        ReturnUrl = returnUrl,
        Settings = settings ?? new EmailSettings()
      };

      return View(model);
    }

    // POST: security/smtpsettings
    [HttpPost]
    public virtual ActionResult SmtpSettings(SmtpSettingsViewModel model)
    {
      if (!ModelState.IsValid)
        return View(model);

      try
      {
        SystemSetting setting = new SystemSetting
        {
          Key = ESystemSetting.Smtp,
          RawValue = JsonConvert.SerializeObject(model.Settings)
        };

        m_dataRepo.SaveSetting(setting);
        this.SaveLastActionMessage("SMTP settings saved", EMessage.Success);
      }
      catch (Exception ex)
      {
        this.SaveUserAlert(new Alert(EMessage.Error, "Unable to save SMTP settings. <br/>" + ex.Message, "ERROR:", EFontAwesomeIcon.Times_Circle_O));
        return View(model);
      }

      return Redirect(model.ReturnUrl ?? Url.Action(MVC.Security.Settings()));
    }

    // GET: security/testsmtpconnection
    public virtual ActionResult TestSmtpConnection(string server, int? port, bool useSsl)
    {
      string summary = string.Empty;
      const string successMessage = "Connection successful";

      if (!string.IsNullOrWhiteSpace(server) && port.HasValue && port.Value > 0)
      {
        try
        {
          using (var client = new TcpClient())
          {
            client.Connect(server, port.Value);

            using (var stream = client.GetStream())
            {
              using (var sslStream = useSsl ? (Stream)new SslStream(stream) : stream)
              {
                if (useSsl)
                  ((SslStream)sslStream).AuthenticateAsClient(server);

                using (var writer = new StreamWriter(sslStream))
                using (var reader = new StreamReader(sslStream))
                {
                  writer.WriteLine("EHLO " + server);
                  writer.Flush();

                  // GMail responds with: 220 mx.google.com ESMTP
                  string line = reader.ReadLine();

                  if (!string.IsNullOrWhiteSpace(line) && (line.StartsWith("250") || line.StartsWith("220")))
                    summary = successMessage;
                  else
                    summary = "Connection failed";
                }
              }
            }
          }
        }
        catch (Exception ex)
        {
          summary = ex.Message;
        }
      }
      else if (string.IsNullOrWhiteSpace(server))
        summary = "Server cannot be empty";
      else if (!port.HasValue)
        summary = "You must specify a Port number";
      else if (port.Value <= 0)
        summary = "Port number is invalid. Must be greater than 0";

      var sb = new StringBuilder();
      sb.AppendFormat("<div class=\"alert alert-{0} alert-block\">", summary == successMessage ? "success" : "danger");
      sb.AppendFormat("<h4 class=\"alert-heading\">{0}</h4>", summary == successMessage ? "Success" : "Error");
      sb.Append(summary);
      sb.Append("</div>");

      JsonProcessResult result = new JsonProcessResult(string.IsNullOrEmpty(summary) ? ECode.Ok : ECode.Error, summary, sb.ToString());

      return Json(result, JsonRequestBehavior.AllowGet);
    }
  }
}