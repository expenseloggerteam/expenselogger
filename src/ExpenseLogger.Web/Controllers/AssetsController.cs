﻿using System;
using System.Web.Mvc;
using SquishIt.Framework;

namespace ExpenseLogger.Web.Controllers
{
  /// <summary>
  ///   Controller to handle static content (e.g. CSS and JS) bundles
  ///   via SquishIt
  /// </summary>
  public partial class AssetsController : Controller
  {
    /// <summary>
    ///   Get JS content for website
    /// </summary>
    /// <param name="id">JS Bundle Id</param>
    /// <returns>Minified JS if in production, else individual JS files</returns>
    public virtual ActionResult Js(string id)
    {
      // Set max-age to a year from now
      Response.Cache.SetMaxAge(TimeSpan.FromDays(365));
      return Content(Bundle.JavaScript().RenderCached(id), "text/javascript");
    }

    /// <summary>
    ///   Get CSS content
    /// </summary>
    /// <param name="id">CSS Bundle Id</param>
    /// <returns>Minified CSS if in production, else individual CSS files</returns>
    public virtual ActionResult Css(string id)
    {
      // Set max-age to a year from now
      Response.Cache.SetMaxAge(TimeSpan.FromDays(365));
      return Content(Bundle.Css().RenderCached(id), "text/css");
    }
  }
}