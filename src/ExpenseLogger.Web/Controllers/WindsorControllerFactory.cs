﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Components.DictionaryAdapter;
using Castle.MicroKernel;

namespace ExpenseLogger.Web.Controllers
{
  public class WindsorControllerFactory : DefaultControllerFactory
  {
    private readonly IKernel m_kernel;

    public WindsorControllerFactory(IKernel kernel)
    {
      m_kernel = kernel;
    }

    /// <summary>
    /// Release controller resources
    /// </summary>
    /// <param name="controller">IController object to release memory for</param>
    public override void ReleaseController(IController controller)
    {
      m_kernel.ReleaseComponent(controller);
    }

    /// <summary>
    /// Get an instance for the given controller
    /// </summary>
    /// <param name="requestContext">Current HTTP request context</param>
    /// <param name="controllerType">Type of controller object whose
    /// instance is to be fetched</param>
    /// <returns>An instance of expected controller</returns>
    protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
    {
      if (controllerType == null)
      {
        string msg = string.Format("The controller for path '{0}' could not be found", requestContext.HttpContext.Request.Path);

        // Throw an HTTP exception rather than showing a Code exception
        throw new HttpException(404, msg);
      }
      return (IController)m_kernel.Resolve(controllerType);
    }
  }
}