﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Mail;
using ExpenseLogger.Models.Security;
using ExpenseLogger.Web.Models;
using ExpenseLogger.Web.Models.Expenses;
using ExpenseLogger.Web.Models.Helpers;
using ExpenseLogger.Web.Models.Reminders;
using ExpenseLogger.Web.Models.Security;
using ExpenseLogger.Web.Models.Shared;
using log4net;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using WebExtras.Bootstrap;
using WebExtras.Core;
using WebExtras.FontAwesome;
using WebExtras.Html;
using WebExtras.Mvc.Bootstrap;
using WebExtras.Mvc.Core;
using WebExtras.Mvc.Html;
using SearchViewModel = ExpenseLogger.Web.Models.Expenses.SearchViewModel;

namespace ExpenseLogger.Web.Controllers
{
  /// <summary>
  ///   Expenses controller
  /// </summary>
  [Authorize]
  public partial class ExpensesController : Controller
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof(Controller));
    private readonly IDataRepository m_dataRepo;
    private readonly IExcelFileProcessorFactory m_excelFileProcessorFactory;
    private IExcelFileProcessor<Expense> m_excelFileProcessor;
    private readonly IUserRepository m_userRepo;

    /// <summary>
    ///   The key to be used to get name of the user uploaded data file
    /// </summary>
    private const string UploadedFileTempDataKey = "expenselogger-uploaded-filename-expenses";

    // ctor
    public ExpensesController(IUserRepository userRepo, IDataRepository dataRepo, IExcelFileProcessorFactory excelFileProcessorFactory)
    {
      m_dataRepo = dataRepo;
      m_excelFileProcessorFactory = excelFileProcessorFactory;
      m_userRepo = userRepo;
    }

    /// <inheritdoc />
    protected override void OnActionExecuting(ActionExecutingContext filterContext)
    {
      if (m_excelFileProcessor == null)
        m_excelFileProcessor = m_excelFileProcessorFactory.GetInstance<Expense>(User.Identity.Name);

      base.OnActionExecuting(filterContext);
    }

    #region Index

    // GET: expenses
    public virtual ActionResult Index()
    {
      Expense[] monthlyExpenses = m_dataRepo.GetExpensesForUser(User.Identity.Name)
        .Where(e => e.Date.Month == DateTime.Now.Month && e.Date.Year == DateTime.Now.Year)
        .ToArray();

      EmailSettings settings = m_dataRepo.GetSetting<EmailSettings>(ESystemSetting.Smtp);
      if (settings == null)
      {
        string suffixText = "Please contact your system administrator.";

        Hyperlink link = new Hyperlink("Complete setup", Url.Action(MVC.Security.SmtpSettings(Url.Action(MVC.Expenses.Index())))).AsButton(EBootstrapButton.Default, EBootstrapButton.XSmall);
        if (User.Identity.Name == AppConstants.AdminUsername)
          suffixText = link.ToHtmlString();

        this.SaveUserAlert(new Alert(EMessage.Warning,
          "Reminders setup is not complete. No emails can be sent until this is done. " + suffixText,
          " Warning:", EFontAwesomeIcon.Exclamation_Circle));
      }

      Func<IEnumerable<Reminder>, IEnumerable<Reminder>> whereClause = f => { return f.Where(x => x.Date.Date >= DateTime.Now.AddDays(-1).Date && x.Date.Date <= DateTime.Now.AddDays(1).Date); };

      List<Reminder> reminders = m_dataRepo.GetByUser(User.Identity.Name, whereClause).ToList();

      Dictionary<EReminderLookupKey, List<Reminder>> remindersLookup = new Dictionary<EReminderLookupKey, List<Reminder>>
      {
        {EReminderLookupKey.Yesterday, reminders.Where(r => r.Date.Date == DateTime.Now.AddDays(-1).Date).ToList()},
        {EReminderLookupKey.Today, reminders.Where(r => r.Date.Date == DateTime.Now.Date).ToList()},
        {EReminderLookupKey.Tomorrow, reminders.Where(r => r.Date.Date == DateTime.Now.AddDays(1).Date).ToList()}
      };

      IndexViewModel model = new IndexViewModel
      {
        MonthlyExpenses = monthlyExpenses,
        Summary = m_dataRepo.GetExpenseSummary(User.Identity.Name) ?? new ExpenseSummary(),
        DailyTrendGraphData = m_dataRepo.GetExpenseTrendForUser(User.Identity.Name, DateTime.Now).ToArray(),
        RemindersLookup = remindersLookup
      };

      return View(model);
    }

    #endregion Index

    #region Add

    // GET: expenses/add
    public virtual ActionResult Add()
    {
      User u = m_userRepo.GetUser(User.Identity.Name);

      AddOrEditExpenseViewModel model = new AddOrEditExpenseViewModel
      {
        AllUserCategories = CreateUserCategoryOptions(),
        AllPaymentTypes = CreateEPaymentOptions(u.Settings.PaymentType)
      };

      return View(Views.AddOrEdit, model);
    }

    // POST: expenses/add
    [HttpPost]
    public virtual ActionResult Add(AddOrEditExpenseViewModel model, bool saveAddAnotherEnabled)
    {
      // we don't want to validate Id
      ModelState.Remove("Expense.Id");

      if (!ModelState.IsValid)
      {
        model.AllUserCategories = CreateUserCategoryOptions();
        model.AllPaymentTypes = CreateEPaymentOptions(model.Expense.PaymentType);
        return View(Views.AddOrEdit, model);
      }

      User u = m_userRepo.GetUser(User.Identity.Name);

      model.Expense.UserId = u.Id;

      try
      {
        m_dataRepo.Save(model.Expense);
      }
      catch (Exception e)
      {
        ModelState.AddModelError(string.Empty, e.Message);
        model.AllUserCategories = CreateUserCategoryOptions();
        return View(Views.AddOrEdit, model);
      }

      this.SaveLastActionMessage("Expense added successfully", EMessage.Success);

      if (saveAddAnotherEnabled)
        return RedirectToAction(Actions.Add());

      return RedirectToAction(Actions.Index());
    }

    #endregion Add

    #region Edit

    // GET: expenses/edit
    public virtual ActionResult Edit(int id)
    {
      Expense e = m_dataRepo.GetById<Expense>(id);

      if (e == null)
        return RedirectToAction(MVC.Security.Error("No such expense found"));

      AddOrEditExpenseViewModel model = new AddOrEditExpenseViewModel
      {
        AllUserCategories = CreateUserCategoryOptions(e.CategoryId),
        AllPaymentTypes = CreateEPaymentOptions(e.PaymentType),
        Expense = e
      };

      return View(Views.AddOrEdit, model);
    }

    // POST: expenses/edit
    [HttpPost]
    public virtual ActionResult Edit(AddOrEditExpenseViewModel model)
    {
      if (!ModelState.IsValid)
      {
        model.AllUserCategories = CreateUserCategoryOptions(model.Expense.CategoryId);
        model.AllPaymentTypes = CreateEPaymentOptions(model.Expense.PaymentType);
        return View(Views.AddOrEdit, model);
      }

      User u = m_userRepo.GetUser(User.Identity.Name);

      model.Expense.UserId = u.Id;

      try
      {
        m_dataRepo.Save(model.Expense);
      }
      catch (Exception e)
      {
        ModelState.AddModelError(string.Empty, e.Message);
        model.AllUserCategories = CreateUserCategoryOptions(model.Expense.CategoryId);
        return View(Views.AddOrEdit, model);
      }

      this.SaveLastActionMessage("Expense edited successfully", EMessage.Success);

      return RedirectToAction(Actions.MonthlyDetails(model.Expense.Date.Ticks));
    }

    #endregion Edit

    #region Delete/ConfirmDelete

    // GET: expenses/confirmdelete
    public virtual ActionResult ConfirmDelete(int id, string url)
    {
      Expense e = m_dataRepo.GetById<Expense>(id);

      if (e == null)
        return RedirectToAction(MVC.Security.Error("No such expense found"));

      Button yes = new Button(EButton.Regular, "Yes").AsButton(EBootstrapButton.Danger, EBootstrapButton.Small);
      yes.Attributes["id"] = "yes";
      yes.Attributes["data-id"] = id.ToString();
      yes.Attributes["data-url"] = url;

      Button no = new Button(EButton.Regular, "No").AsButton(EBootstrapButton.Default, EBootstrapButton.Small);
      no.Attributes["data-dismiss"] = "modal";

      ModalContentJsonViewModel model = new ModalContentJsonViewModel
      {
        Title = "Confirm delete",
        Content = this.GetRenderedPartialView(MVC.Expenses.Views._DeleteConfirm, e).ToHtmlString(),
        Buttons = yes.ToHtmlString() + " " + no.ToHtmlString()
      };

      return new JsonNetResult(model, JsonRequestBehavior.AllowGet);
    }

    // POST: expense/delete
    [HttpPost]
    public virtual ActionResult Delete(int id)
    {
      Expense e = m_dataRepo.GetById<Expense>(id);

      if (e == null)
        return RedirectToAction(MVC.Security.Error("Whoa there fella, no such expense found. Are you trying to hack the mainframe?"));

      try
      {
        m_dataRepo.Delete(e);
        this.SaveLastActionMessage("Expense was deleted", EMessage.Success);
      }
      catch (Exception ex)
      {
        this.SaveUserAlert(new Alert(EMessage.Error, ex.Message));
        Log.Error("Unable to delete expense", ex);
      }

      return Json(true, JsonRequestBehavior.DenyGet);
    }

    #endregion Delete/ConfirmDelete

    #region Daily/Monthly/Yearly Details

    // GET: expenses/showdailydetails
    public virtual ActionResult ShowDailyDetails(DateTime date)
    {
      IEnumerable<Expense> dataSource = m_dataRepo.GetExpensesForUser(User.Identity.Name)
        .Where(e => e.Date.Date == date.Date)
        .ToList();

      var columns = new[]
      {
        new BootstrapRowTableCell {Content = "Category", ColSMWidth = 4},
        new BootstrapRowTableCell {Content = "Description", ColSMWidth = 4},
        new BootstrapRowTableCell {CssClasses = "text-right", Content = "Amount", ColSMWidth = 4}
      };

      CustomPrincipal currentUser = User as CustomPrincipal;
      HtmlComponent currencyIcon = new HtmlComponent(EHtmlTag.I);
      currencyIcon.CssClasses.Add("fa " + (currentUser == null ? ECurrency.Money.GetStringValue() : currentUser.Currency.GetStringValue()));

      Func<Expense, string> amtContentCreator = e =>
      {
        string result = string.Empty;

        if (e.PaymentType != EPayment.Unknown)
          result = e.PaymentType.GetStringValue() + " | ";

        result += currencyIcon.ToHtml() + " " + e.Amount.ToString("F02");

        return result;
      };

      var data = dataSource
        .OrderBy(e => e.Category.Name)
        .Select(e => new[]
        {
          new BootstrapRowTableCell {ColSMWidth = 4, Content = e.Category.Name},
          new BootstrapRowTableCell {ColSMWidth = 4, Content = e.Description},
          new BootstrapRowTableCell
          {
            ColSMWidth = 4,
            CssClasses = "text-right",
            Content = amtContentCreator(e)
          }
        })
        .ToArray();

      ModalContentJsonViewModel model = new ModalContentJsonViewModel
      {
        Title = date.ToString(AppConstants.DisplayDateTimeFormat),
        Content = this.GetRenderedPartialView(MVC.Expenses.Views._ViewDailyDetails, new BootstrapRowTable(columns, data)).ToHtmlString()
      };

      return new JsonNetResult(model, JsonRequestBehavior.AllowGet);
    }

    // GET: expenses/monthlydetails
    public virtual ActionResult MonthlyDetails(long ticks)
    {
      DateTime dt = new DateTime(ticks, DateTimeKind.Local);

      var data = m_dataRepo.GetExpensesForUser(User.Identity.Name)
        .Where(e => e.Date.Month == dt.Month && e.Date.Year == dt.Year);

      ViewDetailsViewModel model = CreateDefaultViewDetailsModel(dt.ToString("MMMM yyyy"), data);

      model.PreviousLink = new BootstrapIconlink(EFontAwesomeIcon.Chevron_Left, Url.Action(MVC.Expenses.MonthlyDetails(dt.AddMonths(-1).Ticks)))
        .SetAttribute("title", "View details for previous month");
      model.NextLink = new BootstrapIconlink(EFontAwesomeIcon.Chevron_Right, Url.Action(MVC.Expenses.MonthlyDetails(dt.AddMonths(1).Ticks)))
        .SetAttribute("title", "View details for next month");

      return View(MVC.Shared.Views.ViewDetails, model);
    }

    // GET: expenses/yearlydetails
    public virtual ActionResult YearlyDetails(long ticks)
    {
      DateTime dt = new DateTime(ticks, DateTimeKind.Local);

      var columns = new[]
      {
        new BootstrapRowTableCell {ColSMWidth = 9, Content = "Month"},
        new BootstrapRowTableCell {ColSMWidth = 2, CssClasses = "text-right", Content = "Amount"},
        new BootstrapRowTableCell {ColSMWidth = 1, CssClasses = "text-center", Content = "<i class='fa fa-info'></i>"}
      };

      CustomPrincipal currentUser = User as CustomPrincipal;
      HtmlComponent currencyIcon = new HtmlComponent(EHtmlTag.I);
      currencyIcon.CssClasses.Add("fa " + (currentUser == null ? ECurrency.Money.GetStringValue() : currentUser.Currency.GetStringValue()));

      var data = m_dataRepo.GetExpensesForUser(User.Identity.Name)
        .Where(e => e.Date.Year == dt.Year)
        .OrderByDescending(e => e.Date)
        .GroupBy(e => e.Date.Month)
        .Select(g => new[]
        {
          new BootstrapRowTableCell {ColSMWidth = 9, Content = new DateTime(dt.Year, g.Key, 1).ToString("MMMM")},
          new BootstrapRowTableCell {ColSMWidth = 2, CssClasses = "text-right", Content = currencyIcon.ToHtml() + " " + g.Sum(e => e.Amount).ToString("F02")},
          new BootstrapRowTableCell
          {
            ColSMWidth = 1,
            CssClasses = "text-center",
            Content = string.Format("<a href='{0}' class='btn btn-xs btn-primary'>View</a>", Url.Action(Actions.MonthlyDetails(new DateTime(dt.Year, g.Key, 1).Ticks)))
          }
        })
        .ToArray();

      ViewDetailsViewModel model = new ExpensesViewDetailsViewModel
      {
        PreviousLink = new BootstrapIconlink(EFontAwesomeIcon.Chevron_Left, Url.Action(MVC.Expenses.YearlyDetails(dt.AddYears(-1).Ticks)))
          .SetAttribute("title", "View details for previous year")
          .SetAttribute("data-toggle", "tooltip"),
        NextLink = new BootstrapIconlink(EFontAwesomeIcon.Chevron_Right, Url.Action(MVC.Expenses.YearlyDetails(dt.AddYears(1).Ticks)))
          .SetAttribute("title", "View details for next year")
          .SetAttribute("data-toggle", "tooltip"),
        Title = "Year " + dt.Year,
        Table = new BootstrapRowTable(columns, data)
      };

      return View(MVC.Shared.Views.ViewDetails, model);
    }

    #endregion Daily/Monthly/Yearly Details

    #region Search

    // GET: expenses/search
    public virtual ActionResult Search()
    {
      SearchViewModel model = new SearchViewModel
      {
        AllUserCategories = CreateUserCategoryOptions().Skip(1),
        AllPaymentTypes = CreateEPaymentOptions()
      };

      return View(model);
    }

    // POST: expenses/search
    [HttpPost]
    public virtual ActionResult Search(SearchViewModel model)
    {
      if (!ModelState.IsValid)
      {
        model.AllUserCategories = CreateUserCategoryOptions().Skip(1);
        model.AllPaymentTypes = CreateEPaymentOptions();

        return View(model);
      }

      var expenses = m_dataRepo.GetExpensesForUser(User.Identity.Name)
        .Where(f => f.Date >= model.DateFrom && f.Date <= model.DateTo);

      string[] selectedCategories = new string[0];

      if (model.Category != null && model.Category.Length > 0)
      {
        expenses = expenses.Where(f => model.Category.Contains(f.CategoryId));
        selectedCategories = m_dataRepo.GetCategoriesForUser(User.Identity.Name)
          .Where(f => model.Category.Contains(f.Id))
          .Select(f => f.Name)
          .ToArray();
      }

      if (!string.IsNullOrWhiteSpace(model.Description))
        expenses = expenses.Where(f => f.Description.ToLowerInvariant().Contains(model.Description.ToLowerInvariant()));

      if (model.AmountFrom.HasValue)
        expenses = expenses.Where(f => f.Amount >= model.AmountFrom);

      if (model.AmountTo.HasValue)
        expenses = expenses.Where(f => f.Amount <= model.AmountTo);

      EPayment[] selectedPaymentTypes = new EPayment[0];
      if (model.PaymentType != null && model.PaymentType.Length > 0)
      {
        selectedPaymentTypes = model.PaymentType.Select(f => (EPayment)Enum.Parse(typeof(EPayment), f)).ToArray();
        expenses = expenses.Where(f => selectedPaymentTypes.Contains(f.PaymentType));
      }

      Models.Expenses.SearchResultsViewModel resultModel = new Models.Expenses.SearchResultsViewModel
      {
        Inputs = model,
        SelectedCategories = selectedCategories,
        SelectedPaymentTypes = selectedPaymentTypes,
        Results = CreateDefaultViewDetailsModel("Search results", expenses)
      };

      return View(Views.SearchResults, resultModel);
    }

    #endregion Search

    #region Upload

    // GET: /expenses/upload
    public virtual ActionResult Upload()
    {
      return View();
    }

    // POST: /expenses/upload
    [HttpPost]
    public virtual ActionResult Upload(HttpPostedFileBase dataFile)
    {
      try
      {
        if (dataFile.ContentLength <= 0)
          throw new Exception("Invalid file size (" + dataFile.ContentLength + ")");

        string filename = Path.GetFileName(dataFile.FileName);
        string cacheFilePath = AppConstants.CacheDirectory + "\\" + filename;

        dataFile.SaveAs(cacheFilePath);

        TempData[UploadedFileTempDataKey] = cacheFilePath;

        return RedirectToAction(Actions.UploadResult());
      }
      catch (Exception ex)
      {
        this.SaveUserAlert(new Alert(EMessage.Error, "File upload failed: " + ex.Message, "ERROR:"));
        return View();
      }
    }

    // GET: /expenses/uploadresult
    public virtual ActionResult UploadResult()
    {
      IExcelFileProcessor<Expense> processor = new ExpensesExcelFileProcessor(User.Identity.Name, m_userRepo, m_dataRepo);
      string cacheFile = TempData[UploadedFileTempDataKey].ToString();

      ProcessResult result;

      ProcessResult<string[][]> rawData = processor.SanityCheck(cacheFile);
      if (rawData.Success)
      {
        ProcessResult<Expense[]> expenses = processor.ProcessData(rawData.Data);

        result = expenses.Success ? processor.UpdateStore(expenses.Data) : new ProcessResult(rawData.Code, rawData.Summary, rawData.Details);
      }
      else
      {
        result = new ProcessResult(rawData.Code, rawData.Summary, rawData.Details);
      }

      if (!result.Success)
      {
        this.SaveUserAlert(new Alert(EMessage.Error, result.Summary, "ERROR:", EFontAwesomeIcon.Times_Circle_O));
        return RedirectToAction(Actions.Index());
      }

      return this.RedirectToAction(Actions.Index(), rawData.Data.Length + " expenses imported successfully");
    }

    // GET: /expenses/exceltemplatefile
    public virtual FileResult ExcelTemplateFile()
    {
      byte[] templateFileBytes = m_excelFileProcessor.GetTemplateFileAsBytes();

      return File(templateFileBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "expenses-external-data.xlsx");
    }

    #endregion Upload

    #region Helper methods

    /// <summary>
    ///   Get select list items for all categories for current user
    /// </summary>
    /// <param name="selectedCategoryId">Id of the selected category</param>
    /// <returns>A list of <see cref="SelectListItem" /></returns>
    private List<SelectListItem> CreateUserCategoryOptions(int selectedCategoryId = 0)
    {
      var userCategories = m_dataRepo.GetCategoriesForUser(User.Identity.Name)
        .OrderBy(c => c.Name)
        .Select(c => new SelectListItem {Text = c.Name, Value = c.Id.ToString(), Selected = c.Id == selectedCategoryId});

      userCategories = new[] {new SelectListItem {Text = "- Select One -", Value = "0", Selected = selectedCategoryId == 0}}.Concat(userCategories);

      return userCategories.ToList();
    }

    /// <summary>
    ///   Get a list of all <see cref="EPayment" /> types available
    /// </summary>
    /// <param name="selectedPaymentType">A payment type already selected</param>
    /// <returns>A list of <see cref="SelectListItem" /></returns>
    private static IEnumerable<SelectListItem> CreateEPaymentOptions(EPayment? selectedPaymentType = null)
    {
      var enumOptions = EnumUtil.GetValues<EPayment>().Select(f => new SelectListItem
      {
        Text = f.ToString().SplitCamelCase(),
        Value = f.ToString(),
        Selected = selectedPaymentType.HasValue && f == selectedPaymentType.Value
      });

      return enumOptions.ToList();
    }

    /// <summary>
    ///   Create a default view details table data
    /// </summary>
    /// <param name="title">Table title</param>
    /// <param name="expenses">Expenses to create table from</param>
    /// <returns>View details view model</returns>
    private ViewDetailsViewModel CreateDefaultViewDetailsModel(string title, IEnumerable<Expense> expenses)
    {
      var columns = new[]
      {
        new BootstrapRowTableCell {CssClasses = "text-center", Content = "Date", ColSMWidth = 2},
        new BootstrapRowTableCell {Content = "Category", ColSMWidth = 3},
        new BootstrapRowTableCell {Content = "Description", ColSMWidth = 3},
        new BootstrapRowTableCell {CssClasses = "text-right", Content = "Amount", ColSMWidth = 2},
        new BootstrapRowTableCell {CssClasses = "text-center", Content = "<i class='fa fa-wrench'></i>", ColSMWidth = 2}
      };

      Func<int, string> createEditDeleteLinks = id =>
      {
        Hyperlink editLink = new Hyperlink("Edit", Url.Action(MVC.Expenses.Edit(id)))
          .AddCssClass("edit-link")
          .AsButton(EBootstrapButton.Default, EBootstrapButton.XSmall);

        Hyperlink deleteLink = new Hyperlink("Delete", "#")
          .AddCssClass("delete-link")
          .AsButton(EBootstrapButton.Danger, EBootstrapButton.XSmall);

        deleteLink.Attributes["data-id"] = id.ToString();
        deleteLink.Attributes["data-toggle"] = "modal";
        deleteLink.Attributes["data-target"] = "#page-modal";

        return editLink.ToHtmlString() + "&nbsp;&nbsp;&nbsp;" + deleteLink.ToHtmlString();
      };

      CustomPrincipal currentUser = User as CustomPrincipal;
      HtmlComponent currencyIcon = new HtmlComponent(EHtmlTag.I);
      currencyIcon.CssClasses.Add("fa " + (currentUser == null ? ECurrency.Money.GetStringValue() : currentUser.Currency.GetStringValue()));

      IEnumerable<Expense> dataSource = (expenses ?? new List<Expense>()).ToList();

      Func<Expense, string> amtContentCreator = e =>
      {
        string result = string.Empty;

        if (e.PaymentType != EPayment.Unknown)
          result = e.PaymentType.GetStringValue() + " | ";

        result += currencyIcon.ToHtml() + " " + e.Amount.ToString("F02");

        return result;
      };

      var data = dataSource
        .OrderByDescending(e => e.Date)
        .Select(e => new[]
        {
          new BootstrapRowTableCell {ColSMWidth = 2, CssClasses = "text-center", Content = e.Date.ToString(AppConstants.DisplayDateTimeFormat)},
          new BootstrapRowTableCell {ColSMWidth = 3, Content = e.Category.Name},
          new BootstrapRowTableCell {ColSMWidth = 3, Content = e.Description},
          new BootstrapRowTableCell
          {
            ColSMWidth = 2,
            CssClasses = "text-right",
            Content = amtContentCreator(e)
          },
          new BootstrapRowTableCell {ColSMWidth = 2, CssClasses = "text-center", Content = createEditDeleteLinks(e.Id)}
        })
        .ToArray();

      var footer = new[]
      {
        new BootstrapRowTableCell {ColXSWidth = 6, ColSMWidth = 8, Content = "Total"},
        new BootstrapRowTableCell {ColXSWidth = 6, ColSMWidth = 2, Content = currencyIcon.ToHtml() + " " + dataSource.Sum(f => f.Amount).ToString("F02")}
      };

      ViewDetailsViewModel model = new ExpensesViewDetailsViewModel
      {
        Title = title,
        Table = new BootstrapRowTable(columns, data, footer)
      };

      return model;
    }

    #endregion Helper methods
  }
}