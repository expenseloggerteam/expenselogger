﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Mail;
using ExpenseLogger.Models.Security;
using ExpenseLogger.Web.Models.Helpers;
using ExpenseLogger.Web.Models.Reminders;
using ExpenseLogger.Web.Models.Shared;
using log4net;
using WebExtras.Bootstrap;
using WebExtras.Core;
using WebExtras.FontAwesome;
using WebExtras.Html;
using WebExtras.Mvc.Bootstrap;
using WebExtras.Mvc.Core;
using WebExtras.Mvc.Html;
using System.Collections.Generic;
using ExpenseLogger.Web.Models.Security;
using ExpenseLogger.Web.Models;

namespace ExpenseLogger.Web.Controllers
{
  [Authorize]
  public partial class RemindersController : Controller
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof(Controller));
    private readonly IDataRepository m_dataRepo;
    private readonly IUserRepository m_userRepo;
    private readonly IEmailService m_emailSvc;

    public RemindersController(IUserRepository userRepo, IDataRepository dataRepo, IEmailService emailSvc)
    {
      m_dataRepo = dataRepo;
      m_userRepo = userRepo;
      m_emailSvc = emailSvc;
    }

    // GET: Reminders
    public virtual ActionResult Index()
    {
      return View();
    }

    // GET: reminders/add
    public virtual ActionResult Add()
    {
      AddOrEditReminderViewModel model = new AddOrEditReminderViewModel();

      return View(Views.AddOrEdit, model);
    }

    // POST: reminders/add
    [HttpPost]
    public virtual ActionResult Add(AddOrEditReminderViewModel model)
    {
      return AddOrEdit(model);
    }

    // GET: reminders/edit
    public virtual ActionResult Edit(int id)
    {
      Reminder reminder = m_dataRepo.GetById<Reminder>(id);

      if (reminder == null)
        return RedirectToAction(MVC.Security.Error("No such reminder found"));

      AddOrEditReminderViewModel model = new AddOrEditReminderViewModel
      {
        Reminder = reminder
      };

      return View(Views.AddOrEdit, model);
    }

    // POST: reminders/edit
    [HttpPost]
    public virtual ActionResult Edit(AddOrEditReminderViewModel model)
    {
      return AddOrEdit(model);
    }

    [NonAction]
    private ActionResult AddOrEdit(AddOrEditReminderViewModel model)
    {
      if (!ModelState.IsValid)
        return View(Views.AddOrEdit, model);

      User u = m_userRepo.GetUser(User.Identity.Name);
      model.Reminder.UserId = u.Id;

      try
      {
        m_dataRepo.Save(model.Reminder);
      }
      catch (Exception e)
      {
        ModelState.AddModelError(string.Empty, e.Message);
        return View(Views.AddOrEdit, model);
      }

      this.SaveLastActionMessage("Reminder saved", EMessage.Success);
      return RedirectToAction(MVC.Expenses.Index());
    }

    // GET: reminders/confirmdelete
    public virtual ActionResult ConfirmDelete(int id, string url)
    {
      Reminder e = m_dataRepo.GetById<Reminder>(id);

      if (e == null)
        return RedirectToAction(MVC.Security.Error("No such reminder found"));

      Button yes = new Button(EButton.Regular, "Yes").AsButton(EBootstrapButton.Danger, EBootstrapButton.Small);
      yes.Attributes["id"] = "yes";
      yes.Attributes["data-id"] = id.ToString();
      yes.Attributes["data-url"] = url;

      Button no = new Button(EButton.Regular, "No").AsButton(EBootstrapButton.Default, EBootstrapButton.Small);
      no.Attributes["data-dismiss"] = "modal";

      ModalContentJsonViewModel model = new ModalContentJsonViewModel
      {
        Title = "Confirm delete",
        Content = this.GetRenderedPartialView(MVC.Reminders.Views._DeleteConfirm, e).ToHtmlString(),
        Buttons = yes.ToHtmlString() + " " + no.ToHtmlString()
      };

      return new JsonNetResult(model, JsonRequestBehavior.AllowGet);
    }

    // POST: reminders/delete
    [HttpPost]
    public virtual ActionResult Delete(int id)
    {
      Reminder r = m_dataRepo.GetById<Reminder>(id);

      if (r == null)
        return RedirectToAction(MVC.Security.Error("Whoa there fella, no such reminder found. Are you trying to hack the mainframe?"));

      try
      {
        m_dataRepo.Delete(r);
        this.SaveLastActionMessage("Reminder was deleted", EMessage.Success);
      }
      catch (Exception ex)
      {
        this.SaveUserAlert(new Alert(EMessage.Error, ex.Message));
        Log.Error("Unable to delete reminder", ex);
      }

      return Json(true, JsonRequestBehavior.DenyGet);
    }

    // GET: reminders/sendemailstart
    public virtual ActionResult SendEmailStart(int id, string returnUrl = "")
    {
      Reminder r = m_dataRepo.GetById<Reminder>(id);

      if (r != null)
        return View(new SendEmailStartViewModel {ReminderId = id, ReturnUrl = returnUrl});

      this.SaveUserAlert(new Alert(EMessage.Error, "Invalid reminder id"));
      return Redirect(string.IsNullOrWhiteSpace(returnUrl) ? Url.Action(MVC.Expenses.Index()) : returnUrl);
    }

    // POST: reminders/sendemailasync
    [HttpPost]
    public virtual async Task<JsonNetResult> SendEmailAsync(int reminderId, string returnUrl = "")
    {
      ProcessResult result;
      try
      {
        Reminder reminder = m_dataRepo.GetById<Reminder>(reminderId);

        if (reminder == null)
          throw new Exception("No such reminder found");

        User u = m_userRepo.GetUser(User.Identity.Name);

        ReminderManualEmail email = new ReminderManualEmail(reminder);
        result = await m_emailSvc.SendMailAsync(u.Email, email);
      }
      catch (Exception e)
      {
        result = new ProcessResult(ECode.Error, e);
      }

      if (result.Code != ECode.Ok)
      {
        Hyperlink tryAgainLink = new Hyperlink("Try again", Url.Action(Actions.SendEmailStart(reminderId, returnUrl))).AsButton(EBootstrapButton.Default, EBootstrapButton.XSmall);
        Alert alert = new Alert(EMessage.Error, result.Summary + Environment.NewLine + result.Details + " " + tryAgainLink.ToHtmlString());
        this.SaveUserAlert(alert);
      }
      else
        this.SaveLastActionMessage("Reminder email sent", EMessage.Success);

      JsonProcessResult jsonResult = new JsonProcessResult(result);

      return new JsonNetResult(jsonResult, JsonRequestBehavior.DenyGet);
    }

    // GET: reminders/monthlydetails
    public virtual ActionResult MonthlyDetails(long ticks)
    {
      DateTime dt = new DateTime(ticks, DateTimeKind.Local);
      string title = dt.ToString("MMMM yyyy");

      var reminders = m_dataRepo.GetRemindersForUser(User.Identity.Name)
        .Where(r => r.Date.Month == dt.Month);

      ViewDetailsViewModel model = CreateDefaultViewDetailsModel(title, reminders);

      model.PreviousLink = new BootstrapIconlink(EFontAwesomeIcon.Chevron_Left, Url.Action(MVC.Reminders.MonthlyDetails(dt.AddMonths(-1).Ticks)))
        .SetAttribute("title", "View reminders for previous month");
      model.NextLink = new BootstrapIconlink(EFontAwesomeIcon.Chevron_Right, Url.Action(MVC.Reminders.MonthlyDetails(dt.AddMonths(1).Ticks)))
        .SetAttribute("title", "View reminders for next month");

      return View(MVC.Shared.Views.ViewDetails, model);
    }

    // GET: reminders/search
    public virtual ActionResult Search()
    {
      SearchViewModel model = new SearchViewModel();

      return View(model);
    }

    // POST: reminders/search
    [HttpPost]
    public virtual ActionResult Search(SearchViewModel model)
    {
      if (!ModelState.IsValid)
      {
        return View(model);
      }

      var reminders = m_dataRepo.GetRemindersForUser(User.Identity.Name)
        .Where(f => f.Date >= model.DateFrom && f.Date <= model.DateTo);

      if (!string.IsNullOrWhiteSpace(model.Description))
        reminders = reminders.Where(f => f.Description.ToLowerInvariant().Contains(model.Description.ToLowerInvariant()));

      SearchResultsViewModel resultModel = new SearchResultsViewModel
      {
        Inputs = model,
        Results = CreateDefaultViewDetailsModel("Search results", reminders)
      };

      return View(Views.SearchResults, resultModel);
    }

    /// <summary>
    ///   Create a default view details table data
    /// </summary>
    /// <param name="title">Table title</param>
    /// <param name="reminders">Reminders to create table from</param>
    /// <returns>View details view model</returns>
    private ViewDetailsViewModel CreateDefaultViewDetailsModel(string title, IEnumerable<Reminder> reminders)
    {
            var columns = new[]
      {
        new BootstrapRowTableCell {CssClasses = "text-center", Content = "Date", ColSMWidth = 2},
        new BootstrapRowTableCell {Content = "Description", ColSMWidth = 6},
        new BootstrapRowTableCell {CssClasses = "text-center", Content = "Send email", ColSMWidth = 2},
        new BootstrapRowTableCell {CssClasses = "text-center", Content = "<i class='fa fa-wrench'></i>", ColSMWidth = 2}
      };

      Func<int, string> createEditDeleteLinks = id =>
      {
        Hyperlink editLink = new Hyperlink("Edit", Url.Action(MVC.Reminders.Edit(id)))
          .AddCssClass("edit-link")
          .AsButton(EBootstrapButton.Default, EBootstrapButton.XSmall);

        //HtmlComponent editIcon = new HtmlComponent(EHtmlTag.I);
        //editIcon.CssClasses.Add("fa " + EFontAwesomeIcon.Edit.GetStringValue());
        //editLink.Component.AppendTags.Add(editIcon);

        Hyperlink deleteLink = new Hyperlink("Delete", "#")
          .AddCssClass("delete-link")
          .AsButton(EBootstrapButton.Danger, EBootstrapButton.XSmall);

        deleteLink.Attributes["data-id"] = id.ToString();
        deleteLink.Attributes["data-toggle"] = "modal";
        deleteLink.Attributes["data-target"] = "#page-modal";

        //HtmlComponent deleteIcon = new HtmlComponent(EHtmlTag.I);
        //deleteIcon.CssClasses.Add("fa " + EFontAwesomeIcon.Trash_O.GetStringValue() + " color-red");
        //deleteLink.Component.AppendTags.Add(deleteIcon);

        return editLink.ToHtmlString() + "&nbsp;&nbsp;&nbsp;" + deleteLink.ToHtmlString();
      };

      var data = reminders
        .OrderBy(f => f.Date)
        .Select(r => new[]
        {
          new BootstrapRowTableCell {ColSMWidth = 2, CssClasses = "text-center", Content = r.Date.ToString(AppConstants.DisplayDateTimeFormat)},
          new BootstrapRowTableCell {ColSMWidth = 6, Content = r.Description},
          new BootstrapRowTableCell {ColSMWidth = 2, CssClasses = "text-center", Content = r.SendEmail ? "Yes" : "No"},
          new BootstrapRowTableCell {ColSMWidth = 2, CssClasses = "text-center", Content = createEditDeleteLinks(r.Id)}
        })
        .ToArray();

      ViewDetailsViewModel model = new RemindersViewDetailsViewModel
      {
        Title = title,
        Table = new BootstrapRowTable(columns, data)
      };

      return model;
    }
  }
}