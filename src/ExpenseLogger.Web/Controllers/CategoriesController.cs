﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Web.Mvc;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Security;
using ExpenseLogger.Web.Models.Categories;

namespace ExpenseLogger.Web.Controllers
{
  [Authorize]
  public partial class CategoriesController : Controller
  {
    private readonly IUserRepository m_userRepo;
    private readonly IDataRepository m_dataRepo;

    // ctor
    public CategoriesController(IUserRepository userRepo, IDataRepository dataRepo)
    {
      m_userRepo = userRepo;
      m_dataRepo = dataRepo;
    }

    // GET: categories/add
    public virtual ActionResult Add()
    {
      return View(Views.AddOrEdit, new AddOrEditCategoryViewModel());
    }

    // POST: categories/add
    [HttpPost]
    public virtual ActionResult Add(AddOrEditCategoryViewModel model)
    {
      if (!ModelState.IsValid)
        return View(Views.AddOrEdit, model);

      User u = m_userRepo.GetUser(User.Identity.Name);

      model.Category.UserId = u.Id;

      try
      {
        m_dataRepo.Save(model.Category);
      }
      catch (Exception e)
      {
        ModelState.AddModelError(string.Empty, e.Message);
      }

      return RedirectToAction(MVC.Security.Settings());
    }

    // GET: categories/edit
    public virtual ActionResult Edit(int id)
    {
      Category c = m_dataRepo.GetById<Category>(id);

      if (c == null)
        return RedirectToAction(MVC.Security.Error("Unknown category specified"));

      User currentUser = m_userRepo.GetUser(User.Identity.Name);
      User adminUser = m_userRepo.GetUser(AppConstants.AdminUsername);

      if (c.UserId == adminUser.Id && !currentUser.IsAdmin)
        return RedirectToAction(MVC.Security.Error("You are not authorized to edit this category"));

      AddOrEditCategoryViewModel model = new AddOrEditCategoryViewModel {
        Category = c
      };

      return RedirectToAction(Views.AddOrEdit, model);
    }
  }
}