﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Models.Mail;
using FluentValidation;
using FluentValidation.Attributes;

namespace ExpenseLogger.Web.Models.Security
{
  /// <summary>
  ///   View model for SMTP settings
  /// </summary>
  [Validator(typeof(SmtpSettingsViewModelValidator))]
  public class SmtpSettingsViewModel
  {
    public string ReturnUrl { get; set; }
    public EmailSettings Settings { get; set; }
  }

  /// <summary>
  ///   A fluent validator for <see cref="SmtpSettingsViewModel" />
  /// </summary>
  public class SmtpSettingsViewModelValidator : AbstractValidator<SmtpSettingsViewModel>
  {
    public SmtpSettingsViewModelValidator()
    {
      RuleFor(f => f.Settings).SetValidator(new EmailSettingsValidator());
    }
  }
}