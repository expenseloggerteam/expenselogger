﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using FluentValidation;
using FluentValidation.Attributes;
using WebExtras.Core;

namespace ExpenseLogger.Web.Models.Security
{
  /// <summary>
  ///   User settings view model
  /// </summary>
  [Validator(typeof(UserProfileViewModelValidator))]
  public class UserProfileViewModel
  {
    private SelectListItem[] m_allCountries;
    private SelectListItem[] m_allCurrencies;
    private SelectListItem[] m_allPaymentTypes;
    private SelectListItem[] m_allReminderDays;

    public IEnumerable<SelectListItem> AllCountries
    {
      get
      {
        if (m_allCountries != null)
          return m_allCountries;

        m_allCountries = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
          .Select(c => new RegionInfo(c.LCID).EnglishName)
          .Distinct()
          .OrderBy(g => g)
          .Select(f => new SelectListItem {Text = f, Value = f, Selected = f == User.Country})
          .ToArray();

        return m_allCountries;
      }
    }

    public IEnumerable<SelectListItem> AllCurrencies
    {
      get
      {
        if (m_allCurrencies != null)
          return m_allCurrencies;

        m_allCurrencies = Enum.GetValues(typeof(ECurrency))
          .Cast<ECurrency>()
          .Select(f => new SelectListItem { Text = f.ToString().SplitCamelCase(), Value = f.ToString(), Selected = f == Settings.Currency })
          .ToArray(); ;

        return m_allCurrencies;
      }
    }

    public IEnumerable<SelectListItem> AllPaymentTypes
    {
      get
      {
        if (m_allPaymentTypes != null)
          return m_allPaymentTypes;

        m_allPaymentTypes = Enum.GetValues(typeof(EPayment))
          .Cast<EPayment>()
          .Select(f => new SelectListItem {Text = f.ToString().SplitCamelCase(), Value = f.ToString(), Selected = f == Settings.PaymentType})
          .ToArray();

        return m_allPaymentTypes;
      }
    }

    public IEnumerable<SelectListItem> AllReminderDays
    {
      get
      {
        if (m_allReminderDays != null)
          return m_allReminderDays;

        m_allReminderDays = Enum.GetValues(typeof(EReminderDays))
          .Cast<EReminderDays>()
          .Select(f => new SelectListItem {Text = f.GetStringValue(), Value = ((int)f).ToString(), Selected = f == Settings.ReminderDays})
          .ToArray();

        return m_allReminderDays;
      }
    }

    public User User { get; set; }

    public UserSetting Settings { get; set; }
  }

  /// <summary>
  ///   <see cref="UserProfileViewModel" /> fluent validator
  /// </summary>
  public class UserProfileViewModelValidator : AbstractValidator<UserProfileViewModel>
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    public UserProfileViewModelValidator()
    {
      RuleFor(f => f.User).SetValidator(new UserValidator());
      RuleFor(f => f.Settings).SetValidator(new UserSettingValidator());
    }
  }
}