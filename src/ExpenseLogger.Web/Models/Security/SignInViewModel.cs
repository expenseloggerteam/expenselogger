﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FluentValidation;

namespace ExpenseLogger.Web.Models.Security
{
  /// <summary>
  ///   Sign in view model
  /// </summary>
  public class SignInViewModel
  {
    [Required]
    public string Username { get; set; }

    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; }

    [DisplayName("Remember me")]
    public bool RememberMe { get; set; }
  }

  /// <summary>
  /// A fluent validator for <see cref="SignInViewModel"/>
  /// </summary>
  public class SignInViewModelValidator : AbstractValidator<SignInViewModel>
  {
    /// <summary>
    /// Constructor
    /// </summary>
    public SignInViewModelValidator()
    {
      // nothing to do here yet...
    }
  }
}