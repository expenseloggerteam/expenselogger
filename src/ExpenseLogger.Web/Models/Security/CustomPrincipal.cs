﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Security.Principal;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using WebExtras.Core;
using WebExtras.Html;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models.Security
{
  /// <summary>
  ///   Custom principal
  /// </summary>
  public class CustomPrincipal : IPrincipal
  {
    #region Implementation of IPrincipal

    /// <summary>
    ///   Check whether the current user is in requested role
    /// </summary>
    /// <param name="role">Role to be checked</param>
    /// <returns>True if user is in role, else false</returns>
    public bool IsInRole(string role)
    {
      throw new NotImplementedException("Role manager has been disabled.");
    }

    /// <summary>
    ///   Current user identity
    /// </summary>
    public IIdentity Identity { get; private set; }

    /// <summary>
    ///   Whether the current user is an admin
    /// </summary>
    public bool IsAdmin { get; private set; }

    /// <summary>
    ///   User currency
    /// </summary>
    public ECurrency Currency { get; private set; }

    /// <summary>
    ///   User's currency icon
    /// </summary>
    public IExtendedHtmlString CurrencyIcon
    {
      get
      {
        HtmlComponent i = new HtmlComponent(EHtmlTag.I);
        i.CssClasses.Add("fa " + Currency.GetStringValue());

        return new ExtendedHtmlString(i);
      }
    }

    #endregion Implementation of IPrincipal

    /// <summary>
    ///   Constructor
    /// </summary>
    private CustomPrincipal()
    {
      IsAdmin = false;
      Currency = ECurrency.Money;
    }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="username">Username</param>
    /// <param name="user">User profile</param>
    public CustomPrincipal(string username, User user)
      : this()
    {
      Identity = new GenericIdentity(username);

      if (user == null)
        return;

      IsAdmin = user.IsAdmin;
      Currency = user.Settings.Currency;
    }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="identity">Current user identity</param>
    /// <param name="user">User profile</param>
    public CustomPrincipal(IIdentity identity, User user)
      : this()
    {
      Identity = identity;

      if (user == null)
        return;

      IsAdmin = user.IsAdmin;
      Currency = user.Settings.Currency;
    }
  }
}