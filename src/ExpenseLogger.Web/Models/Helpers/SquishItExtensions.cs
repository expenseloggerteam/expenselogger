﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.Components.DictionaryAdapter;
using SquishIt.Framework.CSS;
using SquishIt.Framework.JavaScript;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  /// <see cref="SquishIt.Framework.Bundle"/> extensions
  /// </summary>
  public static class SquishItExtensions
  {
    /// <summary>
    ///   Save current CSS bundle as a cached resource on server
    /// </summary>
    /// <param name="cssBundle">Current CSS bundle</param>
    /// <param name="name">Name with which to store current bundle</param>
    /// <returns>Cached bundle access HTML tag</returns>
    public static string AsCached(this CSSBundle cssBundle, ContentBundle name)
    {
      return cssBundle.AsCached(name.ToString(), "~/assets/css/" + name);
    }

    /// <summary>
    ///   Save current JavaScript bundle as a cached resource on server
    /// </summary>
    /// <param name="jsBundle">Current JavaScript bundle</param>
    /// <param name="name">Name with which to store current bundle</param>
    /// <returns>Cached bundle access HTML tag</returns>
    public static string AsCached(this JavaScriptBundle jsBundle, ContentBundle name)
    {
      return jsBundle.AsCached(name.ToString(), "~/assets/js/" + name);
    }
  }
}