﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using WebExtras.Core;
using WebExtras.Html;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   A component which renders Boostrap rows as a table
  /// </summary>
  public class BootstrapRowTable : HtmlComponent
  {
    private const string FooterRowCssClass = "wb-bs-row-table-footer";
    private const string HeaderRowCssClass = "wb-bs-row-table-header";

    private bool m_preRenderCalled;
    private readonly BootstrapRowTableCell[] m_header;
    private readonly BootstrapRowTableCell[][] m_data;
    private readonly BootstrapRowTableCell[] m_footer;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="data">Table data. Note: The first row is considered as the header row</param>
    /// <param name="footer">Footer row</param>
    public BootstrapRowTable(BootstrapRowTableCell[][] data, BootstrapRowTableCell[] footer = null)
      : base(EHtmlTag.Div)
    {
      if (data == null)
        throw new ArgumentNullException(nameof(data));

      if (data.Length > 0)
      {
        m_header = data[0];
        m_data = data.Skip(1).ToArray();
      }
      else
        m_data = data;

      m_footer = footer ?? new BootstrapRowTableCell[0];
    }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="header">Header row</param>
    /// <param name="data">Table data</param>
    /// <param name="footer">Footer row</param>
    public BootstrapRowTable(BootstrapRowTableCell[] header, BootstrapRowTableCell[][] data, BootstrapRowTableCell[] footer = null)
      : base(EHtmlTag.Div)
    {
      if (header == null)
        throw new ArgumentNullException(nameof(header));

      m_header = header;
      m_data = data ?? new List<BootstrapRowTableCell[]>().ToArray();
      m_footer = footer ?? new BootstrapRowTableCell[0];
    }

    /// <inheritdoc />
    protected override void PreRender()
    {
      if (m_preRenderCalled)
        return;

      CssClasses.Add("wb-bs-row-table");

      bool headerAndDataLengthOk = m_data.All(f => f.Length == m_header.Length);
      if (!headerAndDataLengthOk)
        throw new DataMisalignedException("Header and each data row must be of same length");

      if (m_data.Any())
      {
        int maxNumCols = m_data.Max(f => f.Length);
        bool dataRowsOfSameLength = m_data.All(f => f.Length == maxNumCols);
        if (!dataRowsOfSameLength)
          throw new DataMisalignedException("All data rows must be of same length");

        HtmlComponent summaryDataRow = new HtmlComponent(EHtmlTag.Label);
        summaryDataRow.CssClasses.Add("col-xs-12 text-center");
        summaryDataRow.InnerHtml = "Summary";

        HtmlComponent summaryRow = new HtmlComponent(EHtmlTag.Div);
        summaryRow.CssClasses.Add("row visible-xs " + HeaderRowCssClass);
        summaryRow.AppendTags.Add(summaryDataRow);
        AppendTags.Add(summaryRow);

        HtmlComponent headerRow = new HtmlComponent(EHtmlTag.Div);
        headerRow.CssClasses.Add("row hidden-xs " + HeaderRowCssClass);
        headerRow.AppendTags.AddRange(m_header.Select(f => f.ToComponent(EHtmlTag.Label)));
        AppendTags.Add(headerRow);

        foreach (var row in m_data)
        {
          HtmlComponent dataRow = new HtmlComponent(EHtmlTag.Div);
          dataRow.CssClasses.Add("row");
          dataRow.AppendTags.AddRange(row.Select(f => f.ToComponent()));
          AppendTags.Add(dataRow);
        }
      }
      else
      {
        HtmlComponent emptyRowData = new HtmlComponent(EHtmlTag.Div);
        emptyRowData.CssClasses.Add("col-xs-12 text-center");
        emptyRowData.InnerHtml = "<h3>No data found!</h3>";

        HtmlComponent emptyRow = new HtmlComponent(EHtmlTag.Div);
        emptyRow.CssClasses.Add("row");
        emptyRow.AppendTags.Add(emptyRowData);

        AppendTags.Add(emptyRow);
      }

      if (m_data.Any() && m_footer.Any())
      {
        HtmlComponent footerRow = new HtmlComponent(EHtmlTag.Div);
        footerRow.CssClasses.Add("row " + FooterRowCssClass);
        footerRow.AppendTags.AddRange(m_footer.Select(f => f.ToComponent()));
        AppendTags.Add(footerRow);
      }

      m_preRenderCalled = true;
    }

    /// <summary>
    /// Adds given CSS classes to the table container
    /// </summary>
    /// <param name="cssClass">CSS class(es) to be added</param>
    /// <returns>Updated component</returns>
    public BootstrapRowTable AddCssClass(string cssClass)
    {
      CssClasses.Add(cssClass);
      return this;
    }

    /// <summary>
    /// Renders the table as striped
    /// </summary>
    /// <returns>Updated component</returns>
    public BootstrapRowTable AsStriped()
    {
      if (!m_preRenderCalled)
        PreRender();

      CssClasses.Add("wb-bs-row-table-striped");

      bool odd = true;
      foreach (var row in AppendTags)
      {
        if (row.CssClasses.Contains(HeaderRowCssClass) || row.CssClasses.Contains(FooterRowCssClass))
          continue;
        
        if (odd)
        {
          row.CssClasses.Add("odd");
          odd = false;
        }
        else
        {
          row.CssClasses.Add("even");
          odd = true;
        }
      }

      return this;
    }
  }
}