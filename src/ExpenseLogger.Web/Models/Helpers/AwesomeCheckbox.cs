﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Web;
using WebExtras.Core;
using WebExtras.Html;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Denotes an awesome checkbox generated using https://github.com/flatlogic/awesome-bootstrap-checkbox javascript plugin
  /// </summary>
  public class AwesomeCheckbox : HtmlComponent, IHtmlString
  {
    /// <summary>
    ///   Associated input tag
    /// </summary>
    public IHtmlComponent Input { get; }

    /// <summary>
    ///   Associated label tag
    /// </summary>
    public HtmlLabel Label { get; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="input">Associated input tag</param>
    /// <param name="label">Associated label tag</param>
    public AwesomeCheckbox(IHtmlComponent input, HtmlLabel label)
      : base(EHtmlTag.Div)
    {
      if (input.Tag != EHtmlTag.Input)
        throw new ArgumentException("Invalid tag type. Must be a HTML INPUT.", nameof(input));

      Input = input;
      Label = label;
    }

    /// <summary>
    ///   Styles the checkbox with bootstrap primary colors
    /// </summary>
    /// <returns>Updated checkbox</returns>
    public AwesomeCheckbox AsPrimary()
    {
      CssClasses.Add("checkbox-primary");
      return this;
    }

    /// <summary>
    ///   Hook into the rendering pipeline just before the render operation starts
    /// </summary>
    protected override void PreRender()
    {
      AppendTags.Add(Input);
      AppendTags.Add(Label);

      base.PreRender();
    }

    #region Implementation of IHtmlString

    /// <summary>Returns an HTML-encoded string.</summary>
    /// <returns>An HTML-encoded string.</returns>
    public string ToHtmlString()
    {
      return ToHtml();
    }

    #endregion Implementation of IHtmlString
  }
}