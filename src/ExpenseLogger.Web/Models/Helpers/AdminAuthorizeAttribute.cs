﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Web;
using System.Web.Mvc;
using ExpenseLogger.Models.Security;
using ExpenseLogger.Web.Models.Security;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Admin authorize attribute which when decorated with only allows actions to be performed by admin users
  /// </summary>
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
  public class AdminAuthorizeAttribute : AuthorizeAttribute
  {
    /// <summary>
    ///   Authorise request
    /// </summary>
    /// <param name="httpContext">Current HTTP context</param>
    /// <returns>True if success, else false</returns>
    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {
      CustomPrincipal user = httpContext.User as CustomPrincipal;

      return user != null && user.IsAdmin;
    }

    /// <summary>
    ///   Unauthorized request handler
    /// </summary>
    /// <param name="filterContext">Current authorization context</param>
    protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
    {
      filterContext.Result = new RedirectToRouteResult(MVC.Security.Error("You are unauthorized to do this action").GetRouteValueDictionary());
    }
  }
}