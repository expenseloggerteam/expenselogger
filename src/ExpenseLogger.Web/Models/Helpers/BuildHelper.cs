﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ExpenseLogger.Core;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Application build helpers
  /// </summary>
  public static class BuildHelper
  {
    /// <summary>
    ///   Current assembly details
    /// </summary>
    private static AssemblyName ASSEMBLY_NAME;

    /// <summary>
    ///   Current assembly details
    /// </summary>
    private static AssemblyName ExpenseLoggerAssembly => ASSEMBLY_NAME ?? (ASSEMBLY_NAME = Assembly.GetExecutingAssembly().GetName());

    /// <summary>
    ///   Build date
    /// </summary>
    private static string BUILD_DATE;

    /// <summary>
    ///   Get build date
    /// </summary>
    private static readonly Func<string> GetAppBuildDate = () =>
    {
      if (!string.IsNullOrWhiteSpace(BUILD_DATE))
        return BUILD_DATE;

      FileInfo f = new FileInfo(Assembly.GetExecutingAssembly().Location);

      BUILD_DATE = f.CreationTime.ToString(AppConstants.DisplayDateTimeFormat);

      return BUILD_DATE;
    };

    /// <summary>
    ///   Retrieves system version number
    /// </summary>
    /// <param name="html">Current html helper</param>
    /// <returns>System version number</returns>
    public static IHtmlString GetVersion(this HtmlHelper html)
    {
      Version v = ExpenseLoggerAssembly.Version;
      
      string version = string.Format("v{0}.{1}.{2}-{3}-{4}", v.Major, v.Minor, v.Build, v.Revision, ExpenseLoggerAssembly.ProcessorArchitecture.ToString().ToLowerInvariant());

      return MvcHtmlString.Create(version);
    }

    /// <summary>
    ///   Retrieves system build date
    /// </summary>
    /// <param name="html">Current html helper</param>
    /// <returns>System build date</returns>
    public static IHtmlString GetBuildDate(this HtmlHelper html)
    {
      string date = GetAppBuildDate();

      return MvcHtmlString.Create(date);
    }
  }
}