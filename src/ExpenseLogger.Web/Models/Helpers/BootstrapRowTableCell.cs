﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using WebExtras.Core;
using WebExtras.Html;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Denotes a table cell for a <see cref="T:BootstrapRowTable" />
  /// </summary>
  public class BootstrapRowTableCell
  {
    public int ColXSWidth { get; set; }
    public int ColSMWidth { get; set; }
    public int ColMDWidth { get; set; }
    public int ColLGWidth { get; set; }
    public string CssClasses { get; set; }
    public string Content { get; set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    public BootstrapRowTableCell()
    {
      ColXSWidth = 12;
    }

    /// <summary>
    ///   Converts this cell to a <see cref="IHtmlComponent" />
    /// </summary>
    /// <param name="enclosingTag">The tag to enclose the converted component in</param>
    /// <returns>Converted component</returns>
    public IHtmlComponent ToComponent(EHtmlTag enclosingTag = EHtmlTag.Div)
    {
      if (ColXSWidth < 1 || ColXSWidth > 12)
        throw new InvalidUsageException("ColXSWidth must be between 1 and 12");

      //if ((ColSMWidth < 1 || ColSMWidth > 12) && ColXSWidth != 0)
      //  throw new InvalidUsageException("ColSMWidth must be between 1 and 12");

      //if (ColMDWidth < 1 || ColMDWidth > 12)
      //  throw new InvalidUsageException("ColMDWidth must be between 1 and 12");

      //if (ColLGWidth < 1 || ColLGWidth > 12)
      //  throw new InvalidUsageException("ColLGWidth must be between 1 and 12");

      HtmlComponent component = new HtmlComponent(enclosingTag);
      component.CssClasses.Add(CssClasses);
      component.CssClasses.Add("col-xs-" + ColXSWidth);

      if (ColSMWidth > 0)
        component.CssClasses.Add("col-sm-" + ColSMWidth);

      if (ColMDWidth > 0)
        component.CssClasses.Add("col-md-" + ColMDWidth);

      if (ColLGWidth > 0)
        component.CssClasses.Add("col-lg-" + ColLGWidth);

      component.InnerHtml = Content;

      return component;
    }
  }
}