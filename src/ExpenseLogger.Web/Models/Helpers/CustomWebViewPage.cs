﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Security.Principal;
using System.Web.Mvc;
using ExpenseLogger.Models.Security;
using ExpenseLogger.Web.Models.Security;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Custom <see cref="WebViewPage" /> so we have access to <see cref="CustomPrincipal" /> implementation
  ///   of <see cref="IPrincipal" />
  /// </summary>
  public abstract class CustomWebViewPage : WebViewPage
  {
    /// <summary>
    ///   Current user
    /// </summary>
    public new CustomPrincipal User => (base.User is CustomPrincipal) ? (CustomPrincipal)base.User : new CustomPrincipal(base.User.Identity, null);
  }

  /// <summary>
  ///   Custom <see cref="WebViewPage{TModel}" /> so we have access to <see cref="CustomPrincipal" /> implementation
  ///   of <see cref="IPrincipal" />
  /// </summary>
  public abstract class CustomWebViewPage<TModel> : WebViewPage<TModel>
  {
    /// <summary>
    ///   Current user
    /// </summary>
    public new CustomPrincipal User => (base.User is CustomPrincipal) ? (CustomPrincipal)base.User : new CustomPrincipal(base.User.Identity, null);
  }
}