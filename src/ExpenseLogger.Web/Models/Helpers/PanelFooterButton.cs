﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using WebExtras.Core;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Denotes a panel footer button
  /// </summary>
  [Serializable]
  public class PanelFooterButton : Button
  {
    /// <summary>
    ///   Flag indicating whether the button should be appended to the panel after the default buttons
    /// </summary>
    public bool IsAppend { get; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="type">Button type</param>
    /// <param name="text">Button text</param>
    /// <param name="append">
    ///   [Optional] Flag indicating whether to append or prepend the button to the footer. Defaults to
    ///   append.
    /// </param>
    /// <param name="htmlAttributes">[Optional] Extra HTML attributes</param>
    public PanelFooterButton(EButton type, string text, bool append = true, object htmlAttributes = null)
      : base(type, text, htmlAttributes)
    {
      IsAppend = append;
    }
  }
}