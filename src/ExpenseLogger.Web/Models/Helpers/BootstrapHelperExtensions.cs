﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using WebExtras.Bootstrap;
using WebExtras.Core;
using WebExtras.Html;
using WebExtras.Mvc.Core;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Bootstrap helper methods
  /// </summary>
  public static class BootstrapHelperExtensions
  {
    #region Panel extensions

    /// <summary>
    ///   Renders a bootstrap panel title
    /// </summary>
    /// <param name="helper">Current <see cref="HtmlHelper" /> instance</param>
    /// <param name="title">Panel title</param>
    /// <param name="htmlAttributes">[Optional] Extra HTML attributes</param>
    public static IExtendedHtmlString PanelHeading(this HtmlHelper helper, string title, object htmlAttributes = null)
    {
      HtmlDiv divTitle = new HtmlDiv { InnerHtml = title };
      divTitle.CssClasses.Add("panel-title");

      HtmlDiv divHeading = new HtmlDiv(htmlAttributes);
      divHeading.CssClasses.Add("panel-heading");
      divHeading.AppendTags.Add(divTitle);

      return new ExtendedHtmlString(divHeading);
    }

    /// <summary>
    ///   Renders a bootstrap panel footer with Submit/Reset buttons
    /// </summary>
    /// <param name="helper">Current <see cref="HtmlHelper" /> instance</param>
    /// <param name="submitButtonText">Submit button text</param>
    /// <param name="resetButtonText">
    ///   [Optional] Reset button text. If set to null or empty string, the reset button is not
    ///   rendered. Defaults to empty string
    /// </param>
    public static PanelFooter PanelFooter(this HtmlHelper helper, string submitButtonText, string resetButtonText = "")
    {
      PanelFooter divFooter = new PanelFooter(submitButtonText, resetButtonText);

      return divFooter;
    }

    /// <summary>
    ///   Renders a bootstrap panel footer with Submit/Reset buttons
    /// </summary>
    /// <param name="helper">Current <see cref="HtmlHelper" /> instance</param>
    /// <param name="submitButtonText">Submit button text</param>
    /// <param name="resetButtonText">
    ///   [Optional] Reset button text. If set to null or empty string, the reset button is not
    ///   rendered. Defaults to empty string
    /// </param>
    /// <param name="customButtons">Any custom buttons to be added to panel footer</param>
    public static PanelFooter PanelFooter(this HtmlHelper helper, string submitButtonText, string resetButtonText = "", params PanelFooterButton[] customButtons)
    {
      PanelFooter divFooter = new PanelFooter(submitButtonText, resetButtonText, customButtons);

      return divFooter;
    }

    #endregion Panel extensions

    #region BootstrapSwitchCheckboxFor extensions

    /// <summary>
    ///   Renders a bootstrap switch library compatible checkbox. <see cref="http://www.bootstrap-switch.org/" />. Note: The
    ///   label text for the checkbox will be retrieved from the <see cref="DisplayNameAttribute" /> if available, else just
    ///   the property name will be used
    /// </summary>
    /// <typeparam name="TModel">Model type</typeparam>
    /// <typeparam name="TValue">Property type</typeparam>
    /// <param name="helper">Current HTML helper instance</param>
    /// <param name="expression">Property selector</param>
    /// <param name="htmlAttributes">[Optional] Extra HTML attributes</param>
    /// <returns>Boostrap switch enabled checkbox</returns>
    public static IExtendedHtmlString BootstrapSwitchCheckboxFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
    {
      string name = helper.GetDisplayName(expression, true);

      return BootstrapSwitchCheckboxFor(helper, expression, name, htmlAttributes);
    }

    /// <summary>
    ///   Renders a bootstrap switch library compatible checkbox. <see cref="http://www.bootstrap-switch.org/" />
    /// </summary>
    /// <typeparam name="TModel">Model type</typeparam>
    /// <typeparam name="TValue">Property type</typeparam>
    /// <param name="helper">Current HTML helper instance</param>
    /// <param name="expression">Property selector</param>
    /// <param name="labelText">Label text</param>
    /// <param name="htmlAttributes">[Optional] Extra HTML attributes</param>
    /// <returns>Boostrap switch enabled checkbox</returns>
    public static IExtendedHtmlString BootstrapSwitchCheckboxFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string labelText,
      object htmlAttributes = null)
    {
      var result = helper.ViewData.Model != null ? expression.Compile().DynamicInvoke(helper.ViewData.Model) : false;

      if (!(result is bool))
        throw new InvalidUsageException("BootstrapSwitchCheckboxFor<> can only be used for boolean properties");

      bool isChecked = (bool)result;

      MemberExpression exp = expression.Body as MemberExpression;

      string id = WebExtrasUtil.GetFieldIdFromExpression(exp);
      string name = WebExtrasUtil.GetFieldNameFromExpression(exp);

      HtmlElement input = new HtmlElement(EHtmlTag.Input, htmlAttributes)
      {
        ["id"] = id,
        ["name"] = name,
        ["type"] = "checkbox",
        ["class"] = "form-control",
        ["data-label-text"] = labelText
      };

      if (isChecked)
      {
        input["checked"] = "true";
        input["value"] = "true";
      }
      else
      {
        input["value"] = "false";
      }

      return new ExtendedHtmlString(input.Component);
    }

    #endregion BootstrapSwitchCheckboxFor extensions

    #region AwesomeBootstrapCheckboxFor extensions

    /// <summary>
    ///   Renders a bootstrap checkbox using awesome-bootstrap-checkbox library.
    ///   <see cref="https://github.com/flatlogic/awesome-bootstrap-checkbox" />
    /// </summary>
    /// <typeparam name="TModel">Model type</typeparam>
    /// <typeparam name="TValue">Property type</typeparam>
    /// <param name="helper">Current HTML helper instance</param>
    /// <param name="expression">Property selector</param>
    /// <param name="htmlAttributes">[Optional] Extra HTML attributes</param>
    /// <returns>Boostrap checkbox</returns>
    public static AwesomeCheckbox AwesomeBootstrapCheckboxFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
    {
      string name = helper.GetDisplayName(expression, true);

      return AwesomeBootstrapCheckboxFor(helper, expression, name, htmlAttributes);
    }

    /// <summary>
    ///   Renders a bootstrap checkbox using awesome-bootstrap-checkbox library.
    ///   <see cref="https://github.com/flatlogic/awesome-bootstrap-checkbox" />
    /// </summary>
    /// <typeparam name="TModel">Model type</typeparam>
    /// <typeparam name="TValue">Property type</typeparam>
    /// <param name="helper">Current HTML helper instance</param>
    /// <param name="expression">Property selector</param>
    /// <param name="labelText">Label text</param>
    /// <param name="htmlAttributes">[Optional] Extra HTML attributes</param>
    /// <returns>Boostrap checkbox</returns>
    public static AwesomeCheckbox AwesomeBootstrapCheckboxFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string labelText,
      object htmlAttributes = null)
    {
      var result = helper.ViewData.Model != null ? expression.Compile().DynamicInvoke(helper.ViewData.Model) : false;

      if (!(result is bool))
        throw new InvalidUsageException("AwesomeBootstrapCheckboxFor<> can only be used for boolean properties");

      bool isChecked = (bool)result;
      MemberExpression exp = expression.Body as MemberExpression;

      string id = WebExtrasUtil.GetFieldIdFromExpression(exp);
      string name = WebExtrasUtil.GetFieldNameFromExpression(exp);

      HtmlComponent input = new HtmlComponent(EHtmlTag.Input, htmlAttributes);
      input.Attributes.Add("type", "checkbox");
      input.Attributes.Add("id", id);
      input.Attributes.Add("name", name);
      input.CssClasses.Add("styled");

      if (isChecked)
        input.Attributes.Add("checked", string.Empty);

      HtmlLabel label = new HtmlLabel(labelText);
      label.Attributes.Add("for", id);

      AwesomeCheckbox chkBoxDiv = new AwesomeCheckbox(input, label);
      chkBoxDiv.CssClasses.Add("checkbox");

      return chkBoxDiv;
    }

    #endregion AwesomeBootstrapCheckboxFor extensions

    #region ValidationBootstrap extensions

    /// <summary>
    ///   Returns an unordered list (ul element) of validation messages that utilizes bootstrap markup and styling.
    /// </summary>
    /// <param name="htmlHelper"></param>
    /// <param name="alertType">The alert type styling rule to apply to the summary element.</param>
    /// <param name="heading">The optional value for the heading of the summary element.</param>
    public static IHtmlString ValidationBootstrap(this HtmlHelper htmlHelper, string alertType = "danger",
      string heading = "Error:")
    {
      if (htmlHelper.ViewData.ModelState.IsValid)
        return new HtmlString(string.Empty);

      var sb = new StringBuilder();

      sb.AppendFormat("<div class=\"alert alert-{0} alert-block\">", alertType);

      if (!string.IsNullOrWhiteSpace(heading))
        sb.AppendFormat("<h4 class=\"alert-heading\">{0}</h4>", heading);

      sb.Append(htmlHelper.ValidationSummary());
      sb.Append("</div>");

      return new HtmlString(sb.ToString());
    }

    #endregion ValidationBootstrap extensions

    #region MultiSelectControlFor extensions

    /// <summary>
    ///   Create a bootstrap select form group control
    /// </summary>
    /// <typeparam name="TModel">Type to be scanned</typeparam>
    /// <typeparam name="TValue">Property to be scanned</typeparam>
    /// <param name="html">Current HTML helper object</param>
    /// <param name="expression">Member expression</param>
    /// <param name="options">Select list options</param>
    /// <param name="htmlAttributes">[Optional] Extra HTML attributes</param>
    /// <returns>The created form control</returns>
    public static IFormControl<TModel, TValue> MultiSelectControlFor<TModel, TValue>(this HtmlHelper<TModel> html,
      Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> options, object htmlAttributes = null)
    {
      if (options == null)
        throw new ArgumentNullException(nameof(options), "Select list options cannot be null");

      var newOptions = options.Select(f => new HtmlSelectListOption(f.Text, f.Value, f.Selected));
      BootstrapFormComponent<TModel, TValue> bfc = new BootstrapFormComponent<TModel, TValue>(expression, newOptions, htmlAttributes);

      bfc.Input.Attributes["multiple"] = "yes";
      bfc.Input.Attributes["data-selected-text-format"] = "count > 3";

      if (html.ViewData.Model == null)
        return new FormControl<TModel, TValue>(bfc);

      object result = string.Empty;

      try
      {
        result = expression.Compile().DynamicInvoke(html.ViewData.Model);
      }
      catch (Exception)
      {
        // ignore
      }

      bfc.SetValue(result?.ToString() ?? string.Empty);

      return new FormControl<TModel, TValue>(bfc);
    }

    #endregion MultiSelectControlFor extensions

    #region RenderUserAlerts extensions

    /// <summary>
    ///   Renders any saved user alerts of given type
    /// </summary>
    /// <param name="html">Current <see cref="HtmlHelper" /></param>
    /// <param name="type">[Optional] Alert message type</param>
    /// <returns>Saved user alerts</returns>
    public static IHtmlString RenderUserAlerts(this HtmlHelper html, EMessage type = EMessage.Information)
    {
      IExtendedHtmlString[] alerts = html.GetUserAlerts(type);

      if (!alerts.Any())
        return MvcHtmlString.Empty;

      var sb = new StringBuilder();

      sb.AppendFormat("<div class=\"alert alert-{0} alert-block\">", type.GetStringValue());

      sb.AppendFormat("<h4 class=\"alert-heading\">{0}</h4>", type);

      sb.Append("<ul>");

      Array.ForEach(alerts, a => sb.Append("<li>" + a.ToHtmlString() + "</li>"));

      sb.Append("</ul>");

      sb.Append("</div>");

      return MvcHtmlString.Create(sb.ToString());
    }

    #endregion RenderUserAlerts extensions
  }
}