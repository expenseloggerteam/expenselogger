﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Html helper extensions
  /// </summary>
  public static class HtmlHelperExtensions
  {
    /// <summary>
    ///   Renders given partial view if the current user is an authenticated user
    /// </summary>
    /// <param name="html">Current <see cref="HtmlHelper" /></param>
    /// <param name="partialViewName">Partial view name</param>
    /// <returns>HTML encoded partial view</returns>
    public static IHtmlString AuthPartial(this HtmlHelper html, string partialViewName)
    {
      if (html.ViewContext.HttpContext.User.Identity.IsAuthenticated)
        return html.Partial(partialViewName);

      return MvcHtmlString.Empty;
    }
  }
}