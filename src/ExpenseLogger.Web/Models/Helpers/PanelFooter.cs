﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Web;
using WebExtras.Core;
using WebExtras.Html;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models.Helpers
{
  /// <summary>
  ///   Denotes a bootstrap panel footer
  /// </summary>
  [Serializable]
  public class PanelFooter : HtmlDiv, IHtmlString
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="submitBtnText">Submit button text</param>
    /// <param name="customButtons">Any custom buttons</param>
    public PanelFooter(string submitBtnText, params PanelFooterButton[] customButtons)
      : this(submitBtnText, null, customButtons)
    {
      // nothing to do here
    }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="submitBtnText">Submit button text</param>
    /// <param name="resetBtnText">Reset button text</param>
    /// <param name="customButtons">Any custom buttons</param>
    public PanelFooter(string submitBtnText, string resetBtnText, params PanelFooterButton[] customButtons)
      : base()
    {
      Attributes["class"] = "panel-footer";
      CustomButtons = customButtons ?? new PanelFooterButton[0];

      SubmitBtn = new Button(EButton.Submit, submitBtnText).AddCssClass("btn btn-inverse");

      if (!string.IsNullOrEmpty(resetBtnText))
        ResetBtn = new Button(EButton.Reset, resetBtnText).AddCssClass("btn btn-default");
    }

    /// <summary>
    ///   Submit button
    /// </summary>
    public Button SubmitBtn { get; }

    /// <summary>
    ///   Reset button
    /// </summary>
    public Button ResetBtn { get; }

    /// <summary>
    ///   Any custom buttons for the panel footer
    /// </summary>
    public PanelFooterButton[] CustomButtons { get; }

    /// <summary>
    ///   Hook into the rendering pipeline just before the render operation starts
    /// </summary>
    protected override void PreRender()
    {
      AppendTags.Add(SubmitBtn);

      if (ResetBtn != null)
        PrependTags.Add(ResetBtn);

      Array.ForEach(CustomButtons, btn =>
      {
        if (btn.IsAppend)
          AppendTags.Add(btn);
        else
          PrependTags.Add(btn);
      });

      base.PreRender();
    }

    #region Implementation of IHtmlString

    /// <summary>Returns an HTML-encoded string.</summary>
    /// <returns>An HTML-encoded string.</returns>
    public string ToHtmlString()
    {
      return ToHtml();
    }

    #endregion Implementation of IHtmlString
  }
}