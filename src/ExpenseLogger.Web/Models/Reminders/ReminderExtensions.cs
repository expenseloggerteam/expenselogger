﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Text;
using System.Web;
using System.Web.Mvc;
using ExpenseLogger.Models.Core;
using WebExtras.Core;
using WebExtras.FontAwesome;
using WebExtras.Html;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models.Reminders
{
  /// <summary>
  ///   Extension methods for <see cref="Reminder" />
  /// </summary>
  public static class ReminderExtensions
  {
    /// <summary>
    ///   Renders the current reminder as a Bootstrap pop over link
    /// </summary>
    /// <param name="reminder">The reminder to be converted</param>
    /// <returns>Reminder popover link</returns>
    public static IExtendedHtmlString ToPopoverLink(this Reminder reminder)
    {
      UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

      Hyperlink emailBtn = new Hyperlink(" Send email", urlHelper.Action(MVC.Reminders.SendEmailStart(reminder.Id, urlHelper.Action(MVC.Expenses.Index()))))
        .SetAttribute("data-id", reminder.Id.ToString())
        .SetAttribute("class", "btn btn-default btn-xs");

      HtmlComponent emailIcon = new HtmlComponent(EHtmlTag.I);
      emailIcon.Attributes.Add("class", "fa " + EFontAwesomeIcon.Envelope_Square.GetStringValue());

      emailBtn.PrependTags.Add(emailIcon);

      Hyperlink editBtn = new Hyperlink(" Edit", urlHelper.Action(MVC.Reminders.Edit(reminder.Id)))
        .SetAttribute("data-id", reminder.Id.ToString())
        .SetAttribute("class", "btn btn-default btn-xs");

      HtmlComponent editIcon = new HtmlComponent(EHtmlTag.I);
      editIcon.Attributes.Add("class", "fa " + EFontAwesomeIcon.Edit.GetStringValue());

      editBtn.PrependTags.Add(editIcon);

      StringBuilder sb = new StringBuilder();
      sb.Append(emailBtn.ToHtmlString());
      sb.Append(" ");
      sb.Append(editBtn.ToHtmlString());

      Hyperlink link = new Hyperlink(reminder.Description, "#")
        .SetAttribute("data-container", "body")
        .SetAttribute("data-toggle", "popover")
        .SetAttribute("data-animation", "true")
        .SetAttribute("data-content", sb.ToString().Replace('"', '\''))
        .SetAttribute("data-html", "true")
        .SetAttribute("data-placement", "bottom");

      return link;
    }
  }
}