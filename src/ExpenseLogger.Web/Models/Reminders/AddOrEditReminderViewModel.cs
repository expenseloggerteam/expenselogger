﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Web.Models.Reminders
{
  /// <summary>
  ///   A view model for add/edit reminder view
  /// </summary>
  public class AddOrEditReminderViewModel
  {
    /// <summary>
    ///   View title. Note: This title is based on the <see cref="M:Reminder" /> property.
    /// </summary>
    public string Title => Reminder.Id == 0 ? "Add reminder" : "Edit reminder";

    /// <summary>
    ///   Reminder to be added/edited
    /// </summary>
    public Reminder Reminder { get; set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    public AddOrEditReminderViewModel()
    {
      Reminder = new Reminder();
    }
  }
}