﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExpenseLogger.Web.Models.Reminders
{
  /// <summary>
  /// Search action view model
  /// </summary>
  public class SearchViewModel
  {
    public DateTime DateFrom { get; set; }
    public DateTime DateTo { get; set; }
    public string Description { get; set; }

    public SearchViewModel()
    {
      DateFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Local);
      DateTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month), 0, 0, 0, DateTimeKind.Local);
    }
  }

  /// <summary>
  /// A fluent validator for <see cref="SearchViewModel"/>
  /// </summary>
  public class SearchViewModelValidator : AbstractValidator<SearchViewModel>
  {
    /// <summary>
    /// Constructor
    /// </summary>
    public SearchViewModelValidator()
    {
      RuleFor(f => f.DateFrom)
        .NotEmpty()
        .WithMessage("You must select a From date");

      RuleFor(f => f.DateTo)
        .NotEmpty()
        .WithMessage("You must select a To date");

      RuleFor(f => f.DateTo)
        .GreaterThan(f => f.DateFrom)
        .WithMessage("From date must be greater than To date...duhh");
    }
  }
}