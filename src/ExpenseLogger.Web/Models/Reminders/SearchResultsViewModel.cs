﻿using ExpenseLogger.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExpenseLogger.Web.Models.Reminders
{
  /// <summary>
  /// A view model for results
  /// </summary>
  public class SearchResultsViewModel
  {
    public SearchViewModel Inputs { get; set; }

    public ViewDetailsViewModel Results { get; set; }

  }
}