﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Web.Models.Helpers;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models.Shared
{
  public abstract class ViewDetailsViewModel
  {
    public bool ShowPrevNextLinks => PreviousLink != null && NextLink != null;
    public Hyperlink PreviousLink { get; set; }
    public Hyperlink NextLink { get; set; }
    public string Title { get; set; }
    public BootstrapRowTable Table { get; set; }
    public abstract string ConfirmDeleteUrl { get; protected set; }
    public abstract string DeleteUrl { get; protected set; }

    protected ViewDetailsViewModel()
    {
      Table = new BootstrapRowTable(new BootstrapRowTableCell[][] { new BootstrapRowTableCell[0] });
    }
  }
}