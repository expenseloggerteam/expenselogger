﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using ExpenseLogger.Web.Models.Helpers;
using Links;
using WebExtras.Html;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models.Home
{
  /// <summary>
  ///   A view model for the credits page
  /// </summary>
  public class CreditsViewModel
  {
    public BootstrapRowTable Table
    {
      get
      {
        Dictionary<string, string> libraries = new Dictionary<string, string>
        {
          {
            new Hyperlink("jQuery", "http://jquery.com").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("Modernizer", "https://modernizr.com/").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("Respond.js", "https://github.com/scottjehl/Respond").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("Moment.js", "http://momentjs.com").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("Bootstrap", "http://getbootstrap.com/").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("Bootstrap Switch", "http://www.bootstrap-switch.org").ToHtmlString(),
            new Hyperlink("Apache License, Version 2.0", "https://opensource.org/licenses/Apache-2.0").ToHtmlString()
          },
          {
            new Hyperlink("Bootstrap Select", "https://silviomoreto.github.io/bootstrap-select/").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("Bootstrap DateTime Picker", "http://eonasdan.github.io/bootstrap-datetimepicker/").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("HighCharts", "http://www.highcharts.com").ToHtmlString(),
            new Hyperlink("CC BY-NC 3.0", "https://creativecommons.org/licenses/by-nc/3.0/").ToHtmlString()
          },
          {
            new Hyperlink("DataTables", "https://datatables.net").ToHtmlString(),
            new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          },
          {
            new Hyperlink("Font Awesome", "http://fontawesome.io").ToHtmlString(),
            "Dual licensed under " + new Hyperlink("SIL OFL 1.1", "http://scripts.sil.org/OFL").ToHtmlString() + " and " + new Hyperlink("MIT License", "https://opensource.org/licenses/MIT").ToHtmlString()
          }
        };

        string coinStackSvg = new Image(Content.images.coin_stack_svg).AddCssClass("flaticon flaticon-2x flaticon-coin-stack").ToHtmlString();
        const string coinStackSvgReference = "Icon made by <a href=\"http://www.freepik.com\" title=\"Freepik\">Freepik</a> from <a href=\"http://www.flaticon.com\" title=\"Flaticon\">www.flaticon.com</a> and is licensed by <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\" target=\"_blank\">CC 3.0 BY</a>";

        libraries.Add(coinStackSvg, coinStackSvgReference);

        List<BootstrapRowTableCell[]> data = new List<BootstrapRowTableCell[]>();
        foreach (var kv in libraries)
        {
          data.Add(new[]
          {
            new BootstrapRowTableCell {Content = kv.Key, ColSMWidth = 5},
            new BootstrapRowTableCell {Content = kv.Value, ColSMWidth = 7}
          });
        }

        BootstrapRowTableCell[] header =
        {
          new BootstrapRowTableCell {Content = "Library", ColSMWidth = 5},
          new BootstrapRowTableCell {Content = "License/Description", ColSMWidth = 7}
        };

        BootstrapRowTable tbl = new BootstrapRowTable(header, data.ToArray());

        return tbl;
      }
    }
  }
}