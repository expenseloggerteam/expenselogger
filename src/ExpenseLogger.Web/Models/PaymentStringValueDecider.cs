﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Models.Core;
using Links;
using WebExtras.Core;
using WebExtras.Html;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models
{
  /// <summary>
  ///   A <see cref="IStringValueDecider{T}" /> for <see cref="EPayment" />
  /// </summary>
  public class PaymentStringValueDecider : IStringValueDecider<EPayment>
  {
    #region Implementation of IStringValueDecider<EPayment>

    /// <inheritdoc />
    public string Decide(StringValueDeciderArgs<EPayment> args)
    {
      switch (args.Value)
      {
        case EPayment.Cash:
          Image coinImg = new Image(Content.images.coin_stack_svg)
            .SetAttribute("class", "flaticon flaticon-coin-stack")
            .SetAttribute("title", "Cash");
          return coinImg.ToHtmlString();

        case EPayment.BankTransfer:
          Image transferImg = new Image(Content.images.transfer_money_svg)
            .SetAttribute("class", "flaticon flaticon-transfer-money")
            .SetAttribute("title", "Bank transfer");
          return transferImg.ToHtmlString();

        case EPayment.CreditCard:
        case EPayment.DebitCard:
          HtmlComponent icon = new HtmlComponent(EHtmlTag.I);
          icon.Attributes["class"] = "fa " + (args.Value == EPayment.CreditCard ? "fa-credit-card-alt text-info" : "fa-credit-card");
          icon.Attributes["title"] = args.Value.ToString().SplitCamelCase();
          return icon.ToHtml();

        default:
          return string.Empty;
      }
    }

    #endregion Implementation of IStringValueDecider<EPayment>
  }
}