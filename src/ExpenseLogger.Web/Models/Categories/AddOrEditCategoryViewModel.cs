﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Web.Models.Categories
{
  public class AddOrEditCategoryViewModel
  {
    public Category Category { get; set; }
  }
}