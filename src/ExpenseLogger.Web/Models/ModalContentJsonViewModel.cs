﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Web;
using WebExtras.Mvc.Html;

namespace ExpenseLogger.Web.Models
{
  /// <summary>
  ///   A generic bootstrap modal view model
  /// </summary>
  public class ModalContentJsonViewModel
  {
    public string Title { get; set; }
    public string Content { get; set; }
    public string Buttons { get; set; }
  }
}