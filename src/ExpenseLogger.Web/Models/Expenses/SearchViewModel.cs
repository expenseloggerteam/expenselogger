﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ExpenseLogger.Models.Core;
using FluentValidation;
using FluentValidation.Attributes;

namespace ExpenseLogger.Web.Models.Expenses
{
  /// <summary>
  /// A view model for search expenses
  /// </summary>
  [Validator(typeof(SearchViewModelValidator))]
  public class SearchViewModel
  {
    public DateTime DateFrom { get; set; }
    public DateTime DateTo { get; set; }
    public string Description { get; set; }
    public int[] Category { get; set; }
    public IEnumerable<SelectListItem> AllUserCategories { get; set; }
    public double? AmountFrom { get; set; }
    public double? AmountTo { get; set; }
    public string[] PaymentType { get; set; }
    public IEnumerable<SelectListItem> AllPaymentTypes { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public SearchViewModel()
    {
      DateFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Local);
      DateTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month), 0, 0, 0, DateTimeKind.Local);
    }
  }

  /// <summary>
  /// A fluent validator for <see cref="SearchViewModel"/>
  /// </summary>
  public class SearchViewModelValidator : AbstractValidator<SearchViewModel>
  {
    /// <summary>
    /// Constructor
    /// </summary>
    public SearchViewModelValidator()
    {
      RuleFor(f => f.DateFrom)
        .NotEmpty()
        .WithMessage("You must select a From date");

      RuleFor(f => f.DateTo)
        .NotEmpty()
        .WithMessage("You must select a To date");

      RuleFor(f => f.DateTo)
        .GreaterThan(f => f.DateFrom)
        .WithMessage("From date must be greater than To date...duhh");

      RuleFor(f => f.AmountFrom)
        .GreaterThan(0)
        .WithMessage("I don't support negative amounts yet");

      RuleFor(f => f.AmountTo)
        .GreaterThan(0)
        .WithMessage("I don't support negative amounts yet");

      RuleFor(f => f.AmountTo)
        .GreaterThanOrEqualTo(f => f.AmountFrom)
        .When(f => f.AmountFrom > 0)
        .WithMessage("That's wierd, Amount To was less than Amount From");
    }
  }
}