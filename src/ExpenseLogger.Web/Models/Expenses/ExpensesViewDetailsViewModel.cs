﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Web;
using System.Web.Mvc;
using ExpenseLogger.Web.Models.Shared;

namespace ExpenseLogger.Web.Models.Expenses
{
  public class ExpensesViewDetailsViewModel : ViewDetailsViewModel
  {
    #region Overrides of ViewDetailsViewModel

    /// <inheritdoc />
    public override string ConfirmDeleteUrl { get; protected set; }

    /// <inheritdoc />
    public override string DeleteUrl { get; protected set; }

    #endregion Overrides of ViewDetailsViewModel

    public ExpensesViewDetailsViewModel()
    {
      UrlHelper u = new UrlHelper(HttpContext.Current.Request.RequestContext);
      ConfirmDeleteUrl = u.Action(MVC.Expenses.ConfirmDelete());
      DeleteUrl = u.Action(MVC.Expenses.Delete());
    }
  }
}