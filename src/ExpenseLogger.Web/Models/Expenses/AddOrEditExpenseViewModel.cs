﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ExpenseLogger.Models.Core;
using FluentValidation;
using FluentValidation.Attributes;

namespace ExpenseLogger.Web.Models.Expenses
{
  /// <summary>
  ///   A view model for the AddOrEdit page
  /// </summary>
  [Validator(typeof(AddOrEditExpenseViewModelValidator))]
  public class AddOrEditExpenseViewModel
  {
    /// <summary>
    /// Flag indicating whether we are adding a new expense
    /// </summary>
    public bool IsNewExpense
    {
      get
      {
        if (Expense == null)
          return true;

        if (Expense.Id == 0)
          return true;

        return false;
      }
    }

    /// <summary>
    ///   Expense to be added/edited
    /// </summary>
    public Expense Expense { get; set; }

    /// <summary>
    ///   All categories available to user
    /// </summary>
    public List<SelectListItem> AllUserCategories { get; set; }

    public IEnumerable<SelectListItem> AllPaymentTypes { get; set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    public AddOrEditExpenseViewModel()
    {
      Expense = new Expense
      {
        Date = DateTime.Now
      };
    }
  }

  /// <summary>
  ///   <see cref="AddOrEditExpenseViewModel" /> fluent validator
  /// </summary>
  public class AddOrEditExpenseViewModelValidator : AbstractValidator<AddOrEditExpenseViewModel>
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    public AddOrEditExpenseViewModelValidator()
    {
      RuleFor(f => f.Expense.Amount)
        .GreaterThan(0.0)
        .WithMessage("If your expense amount is 0 why are you even putting this expense?");

      RuleFor(f => f.Expense.CategoryId)
        .GreaterThan(0)
        .WithMessage("You must select an expense category");

      RuleFor(f => f.Expense.Date)
        .LessThanOrEqualTo(DateTime.Now)
        .WithMessage("You can not enter future expenses yet...");
    }
  }
}