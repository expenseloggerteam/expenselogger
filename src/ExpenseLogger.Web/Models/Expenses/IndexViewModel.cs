﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Web.Models.Reminders;
using WebExtras.Core;
using WebExtras.FontAwesome;
using WebExtras.Html;

namespace ExpenseLogger.Web.Models.Expenses
{
  /// <summary>
  ///   Index page view model
  /// </summary>
  public class IndexViewModel
  {
    private double[] m_dailySpends;

    public ExpenseSummary Summary { get; set; }

    public string PanelColor
    {
      get
      {
        string color = "panel-green";
        double percentOver = Summary.MonthActual / Summary.MonthBudget * 100;

        if (percentOver > 100)
          color = "panel-orange";

        if (percentOver > 150)
          color = "panel-red";

        return color;
      }
    }

    public IHtmlString PanelIcon
    {
      get
      {
        Random rand = new Random(DateTime.Now.Millisecond);
        EFontAwesomeIcon icon = rand.Next(1, 3) == 1 ? EFontAwesomeIcon.Smile_O : EFontAwesomeIcon.Thumbs_O_Up;
        string title = "Like a champ!";

        double percentOver = Summary.MonthActual / Summary.MonthBudget * 100;

        if (percentOver > 100)
        {
          icon = EFontAwesomeIcon.Meh_O;
          title = "Sometimes it's OK to be a bit over!";
        }

        if (percentOver > 150)
        {
          title = "Do you even care how much you spend?";
          switch (rand.Next(1, 4))
          {
            case 1:
              icon = EFontAwesomeIcon.Hand_Stop_O;
              break;

            case 2:
              icon = EFontAwesomeIcon.Thumbs_O_Down;
              break;

            default:
              icon = EFontAwesomeIcon.Frown_O;
              break;
          }
        }

        HtmlComponent i = new HtmlComponent(EHtmlTag.I)
        {
          Attributes =
          {
            ["title"] = title,
            ["class"] = "fa fa-5x " + icon.GetStringValue()
          }
        };

        return MvcHtmlString.Create(i.ToHtml());
      }
    }

    public double[] DailySpendGraphData
    {
      get
      {
        if (m_dailySpends != null)
          return m_dailySpends;

        Dictionary<int, double> sums = MonthlyExpenses
          .GroupBy(k => k.Date.Day)
          .ToDictionary(k => k.Key, v => v.Sum(e => e.Amount));

        int count = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        double[] dailySpends = new double[count];

        for (int i = 0; i < count; i++)
          dailySpends[i] = sums.ContainsKey(i + 1) ? sums[i + 1] : 0;

        m_dailySpends = dailySpends;

        return dailySpends;
      }
    }

    public double DailyAverage => DailySpendGraphData.Take(DateTime.Now.Day).Sum() / DateTime.Now.Day;

    public double[] DailyTrendGraphData { get; set; }

    public Expense[] MonthlyExpenses { get; set; }

    public Expense[] ExpensesToday
    {
      get
      {
        if (MonthlyExpenses == null)
          return new Expense[0];

        return MonthlyExpenses.Where(f => f.Date.Day == DateTime.Now.Day).ToArray();
      }
    }

    public bool HaveAnyReminders
    {
      get
      {
        if (RemindersLookup == null)
          return false;

        return RemindersLookup.Values.SelectMany(f => f).Any();
      }
    }

    /// <summary>
    ///   Stores 3 days of reminders (yesterday, today and tomorrow)
    /// </summary>
    public IDictionary<EReminderLookupKey, List<Reminder>> RemindersLookup { get; set; }

    public IndexViewModel()
    {
      //RemindersLookup = new Dictionary<EReminderLookupKey, List<Reminder>> {
      //  { EReminderLookupKey.Yesterday, new List<Reminder> { new Reminder { Description = "yesterday's reminder" } } },
      //  { EReminderLookupKey.Today, new List<Reminder> { new Reminder { Description = "today's reminder" } } },
      //  { EReminderLookupKey.Tomorrow, new List<Reminder> { new Reminder { Description = "tomorrow's reminder" } } }
      //};

      RemindersLookup = new Dictionary<EReminderLookupKey, List<Reminder>>();
    }
  }
}