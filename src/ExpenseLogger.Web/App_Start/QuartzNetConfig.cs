﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Web.Hosting;
using Castle.Facilities.QuartzIntegration;
using Castle.Windsor;
using ExpenseLogger.Models.Jobs;
using Quartz;

namespace ExpenseLogger.Web
{
  /// <summary>
  ///   Quartz.Net scheduler configuration
  /// </summary>
  public static class QuartzNetConfig
  {
    /// <summary>
    ///   Configures the Quartz.Net scheduler with jobs and starts it
    /// </summary>
    /// <param name="container">Windsor container to resolve required components</param>
    public static void Configure(IWindsorContainer container)
    {
      string quartzJobsFile = HostingEnvironment.ApplicationPhysicalPath + @"\etc\quartz-jobs.config";

      IDictionary<string, string> properties = new Dictionary<string, string>
      {
        {"quartz.scheduler.instanceName", "XmlConfiguredInstance"},
        {"quartz.threadPool.type", "Quartz.Simpl.SimpleThreadPool, Quartz"},
        {"quartz.threadPool.threadCount", "4"},
        {"quartz.threadPool.threadPriority", "Normal"},
        {"quartz.plugin.xml.type", "Quartz.Plugin.Xml.XMLSchedulingDataProcessorPlugin, Quartz"},
        {"quartz.plugin.xml.scanInterval", "10"},
        {"quartz.plugin.xml.fileNames", quartzJobsFile}
      };

      WindsorJobFactory factory = new WindsorJobFactory(container.Kernel);
      IScheduler scheduler = new QuartzNetScheduler(properties, factory, container.Kernel);

      JobsRegistrar.RegisterWith(scheduler);

      scheduler.StartDelayed(TimeSpan.FromMinutes(1));
    }
  }
}