﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using ExpenseLogger.Core.Castle;
using ExpenseLogger.Models.Conf;
using ExpenseLogger.Web.Controllers;

namespace ExpenseLogger.Web
{
  /// <summary>
  ///   Castle windsor configuration
  /// </summary>
  public static class CastleWindsorConfig
  {
    /// <summary>
    ///   Registers components with the castle windsor container
    /// </summary>
    /// <param name="container">Windsor container to register components with</param>
    /// <returns>Configuration properties</returns>
    public static ConfigProperty Configure(IWindsorContainer container)
    {
      container.AddFacility(new AppConstantsInitFacility());

      ConfigProperty configProps = CastleWindsorModelsConfig.Configure(container);

      container.Register(Classes.FromThisAssembly().BasedOn<Controller>().LifestylePerWebRequest());

      WindsorControllerFactory controllerFactory = new WindsorControllerFactory(container.Kernel);
      ControllerBuilder.Current.SetControllerFactory(controllerFactory);

      return configProps;
    }
  }
}