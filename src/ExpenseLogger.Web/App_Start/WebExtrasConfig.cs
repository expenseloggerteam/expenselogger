﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Core.Json;
using ExpenseLogger.Web.Models;
using WebExtras.Bootstrap;
using WebExtras.Bootstrap.v3;
using WebExtras.Core;
using WebExtras.FontAwesome;

namespace ExpenseLogger.Web
{
  /// <summary>
  ///   WebExtras library configurator
  /// </summary>
  public static class WebExtrasConfig
  {
    /// <summary>
    ///   Configure WebExtras library options
    /// </summary>
    public static void Configure()
    {
      WebExtrasSettings.BootstrapVersion = EBootstrapVersion.V3;
      WebExtrasSettings.CssFramework = ECssFramework.Bootstrap;
      WebExtrasSettings.FontAwesomeVersion = EFontAwesomeVersion.V4;
      WebExtrasSettings.FormControlBehavior = EFormControlBehavior.Default;
      WebExtrasSettings.JsonSerializerSettings.ContractResolver = new LowerFirstCharacterContractResolver();
      WebExtrasSettings.AddStringValueDecider(new PaymentStringValueDecider());

      BootstrapSettings.DateTimePickerOptions.format = "DD MMM YYYY";
    }
  }
}