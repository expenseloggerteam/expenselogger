﻿namespace ExpenseLogger.Web
{
  /// <summary>
  /// All available system roles
  /// </summary>
  public static class SystemRole
  {
    /// <summary>
    /// Administrator role
    /// </summary>
    public const string Admin = "SystemAdministrator";
  }
}