﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Data.Entity;
using Castle.Windsor;
using ExpenseLogger.Models.Data;
using log4net;

namespace ExpenseLogger.Web
{
  /// <summary>
  ///   Database configuration
  /// </summary>
  public static class DatabaseConfig
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(DatabaseConfig));

    /// <summary>
    ///   Configures the membership and role providers for the application
    /// </summary>
    /// <param name="container">Current castle windsor container</param>
    public static void Configure(IWindsorContainer container)
    {
      using (IDataContext ctx = container.Resolve<IDataContextFactory>().GetInstance())
      {
        if (ctx == null)
          throw new InvalidOperationException("No data context registered with the IoC container");
        
        ctx.SeedData();
      }

      Logger.Info("Database initialised");
    }
  }
}