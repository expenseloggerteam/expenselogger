﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Web.Models.Helpers;
using SquishIt.Framework;

namespace ExpenseLogger.Web
{
  /// <summary>
  /// Bundle config
  /// </summary>
  public static class BundleConfig
  {
    /// <summary>
    /// Register bundles
    /// </summary>
    public static void RegisterBundles()
    {
      // register main css bundle
      Bundle.Css()
        .AddMinified(Links.Content.css.bootstrap.bootstrap_min_css)
        .AddMinified(Links.Content.css.bootstrap.bootstrap_theme_min_css)
        .Add(Links.Content.css.bootstrap.awesome_bootstrap_checkbox_css)
        //.Add(Links.Content.css.bootstrap.bootstrap_switch_css)
        .Add(Links.Content.css.bootstrap.bootstrap3_datetimepicker_4_7_14_css)
        .AddMinified(Links.Content.css.bootstrap.bootstrap_select_min_css)
        .AddMinified(Links.Content.css.fontawesome.font_awesome_min_css)
        .Add(Links.Content.css.dataTables.dataTables_css)
        .Add(Links.Content.css.dataTables.dataTables_responsive_css)
        .Add(Links.Content.css.dataTables.dataTables_bootstrap_css)
        .Add(Links.Content.css.dataTables.dataTables_fontAwesome_css)
        .AddMinified(Links.Content.css.sb_admin_2.sb_admin_2_min_css)
        //.Add(Links.Content.css.pace.pace_css)
        .AddMinified(Links.Content.css.style_bootstrap3_min_css)
        .AddMinified(Links.Content.css.style_dataTables_min_css)
        .AddMinified(Links.Content.css.style_min_css)
        .AsCached(ContentBundle.CssMain);

      // register main js bundle
      Bundle.JavaScript()
        .AddMinified(Links.Scripts.jquery.jquery_1_9_1_min_js)
        .Add(Links.Scripts.modernizer.modernizr_2_8_3_js)
        //.AddMinified(Links.Scripts.respond.respond_min_js)
        .AsCached(ContentBundle.JsMain);

      // register libraries js bundle
      Bundle.JavaScript()
        .Add(Links.Scripts.moment.moment_js)
        .AddMinified(Links.Scripts.bootstrap.bootstrap_min_js)
        //.AddMinified(Links.Scripts.bootstrap.bootstrap_switch_min_js)
        .AddMinified(Links.Scripts.bootstrap.bootstrap_select_min_js)
        .Add(Links.Scripts.bootstrap.bootstrap3_datetimepicker_4_0_0_js)
        .Add(Links.Scripts.charts.highcharts_4_1_4_js)
        .AddMinified(Links.Scripts.dataTables.jquery_dataTables_min_js)
        .AddMinified(Links.Scripts.dataTables.dataTables_bootstrap_min_js)
        .Add(Links.Scripts.dataTables.dataTables_responsive_js)
        //.Add(Links.Scripts.dataTables.dataTables_scrollingPagination_js)
        //.AddMinified(Links.Scripts.pace.pace_min_js)
        .AsCached(ContentBundle.JsLib);
    }
  }
}