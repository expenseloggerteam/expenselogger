﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.MicroKernel;
using Castle.Windsor;
using Castle.Windsor.Extensions;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Conf;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Security;
using ExpenseLogger.Web.Models.Security;
using FluentValidation.Mvc;
using Newtonsoft.Json;
using WebExtras.Core;

namespace ExpenseLogger.Web
{
  /// <summary>
  ///   Main MVC application
  /// </summary>
  public class MvcApplication : HttpApplication
  {
    /// <summary>
    ///   Current castle windsor kernel
    /// </summary>
    private static IKernel CASTLE_KERNEL;

    /// <summary>
    ///   Application startup
    /// </summary>
    protected void Application_Start()
    {
      string logFile = HostingEnvironment.ApplicationPhysicalPath + @"\logs\expenselogger.log";
      AppConstants.CacheDirectory = HostingEnvironment.ApplicationPhysicalPath + @"\cache";

      Log4NetConfig.Configure(logFile);

      string configFile = HostingEnvironment.ApplicationPhysicalPath + @"\etc\expenselogger.config";

      IWindsorContainer container = new PropertyResolvingWindsorContainer(configFile);
      ConfigProperty conf = CastleWindsorConfig.Configure(container);

      //////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////
      AppConstants.DemoFlag = conf.DemoFlag;
      AppConstants.SeedDataFlag = conf.SeedDataFlag;
      //////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////////////

      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles();

      DatabaseConfig.Configure(container);
      WebExtrasConfig.Configure();
      QuartzNetConfig.Configure(container);

      JsonConvert.DefaultSettings = () => WebExtrasSettings.JsonSerializerSettings;

      FluentValidationModelValidatorProvider.Configure();

      CASTLE_KERNEL = container.Kernel;
    }

    /// <summary>
    ///   Application authorise
    /// </summary>
    protected void Application_AuthorizeRequest(object source, EventArgs e)
    {
      if (!User.Identity.IsAuthenticated)
        return;

      IUserRepository userRepo = CASTLE_KERNEL.Resolve<IUserRepository>();

      User u = userRepo.GetUser(User.Identity.Name);

      Context.User = new CustomPrincipal(User.Identity, u);
    }
  }
}