﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Diagnostics;
using System.IO;
using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Extensions;
using Castle.Windsor.Extensions.Registration;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.DbMigrator.Core;
using ExpenseLogger.DbMigrator.Data;
using log4net;
using log4net.Appender;
using log4net.Config;

namespace ExpenseLogger.DbMigrator
{
  /// <summary>
  ///   Main program
  /// </summary>
  class Program
  {
    /// <summary>
    /// Main method
    /// </summary>
    /// <param name="args">Command line arguments</param>
    /// <returns>0 if all went well, else 1</returns>
    static int Main(string[] args)
    {
      GlobalContext.Properties["pid"] = Process.GetCurrentProcess().Id;

      string logFile = Path.GetFullPath(@"..\logs\dbmigrator.log");

      IAppender appender = Log4NetHelper.GetRollingFileAppender(logFile);
      BasicConfigurator.Configure(appender);

      ILog logger = LogManager.GetLogger(typeof(Program));
      logger.EmptyLine();
      logger.ToLoggerAndConsole("Initializing ...");

      // We always want to run in demo mode so the database gets created
      AppConstants.DemoFlag = true;

      // We don't want to seed any default data
      AppConstants.SeedDataFlag = false;

      IWindsorContainer container = new PropertyResolvingWindsorContainer(@"..\etc\dbmigrator.config");

      container.Register(Component.For<IKernel>().Named("kernel").Instance(container.Kernel).OnlyNewServices());
      container.Register(Classes.FromThisAssembly().BasedOn<IDataMigrator>().WithServiceBase().LifestyleTransient());

      IRegistration configRegistration = PropertyResolvingComponent.For<Config>()
        .DependsOnProperties(
          new ResolvableProperty("oldDbType"),
          new ResolvableProperty("oldDbConnectionString"),
          new ResolvableProperty("newDbType"),
          new ResolvableProperty("newDbConnectionString"))
        .WithLifestyle(LifestyleType.Transient);

      container.Register(configRegistration);

      Config conf = container.Resolve<Config>();

      if (conf.OldDbType.ToString() == conf.NewDbType.ToString())
      {
        logger.ToLoggerAndConsole(ELogLevel.Error, "Migrating between same schemas is quite stupid don't you think");
        return 1;
      }

      DefaultDataMigratorFactory factory = new DefaultDataMigratorFactory(container.Kernel);

      logger.ToLoggerAndConsole("Requesting migrator instance");

      string migratorKey = DataMigratorKeyGenerator.Generate(conf.OldDbType, conf.NewDbType);

      IDataMigrator migrator = factory.GetInstance(migratorKey);

      if (migrator == null)
      {
        logger.ToLoggerAndConsole(ELogLevel.Error, "No migrator found that can migrate your data :(");
        return 1;
      }

      migrator.Migrate();

      logger.EmptyLine();
      logger.ToLoggerAndConsole("Migration complete");

#if DEBUG
      Console.WriteLine("\nPress any key to exit...");
      Console.ReadKey();
#endif

      return 0;
    }
  }
}