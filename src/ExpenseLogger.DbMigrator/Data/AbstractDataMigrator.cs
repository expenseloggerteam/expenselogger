﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Castle.MicroKernel;
using ExpenseLogger.Core.Helpers;
using log4net;

namespace ExpenseLogger.DbMigrator.Data
{
  /// <summary>
  ///   A abstract data migrator
  /// </summary>
  public abstract class AbstractDataMigrator : IDataMigrator
  {
    /// <summary>
    ///   Current log4net logger
    /// </summary>
    protected static readonly ILog Logger = LogManager.GetLogger(typeof(AbstractDataMigrator));

    /// <summary>
    ///   Castle kernel to resolve components
    /// </summary>
    protected IKernel Kernel { get; private set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="kernel">Current Castle kernel</param>
    protected AbstractDataMigrator(IKernel kernel)
    {
      Kernel = kernel;
    }

    /// <summary>
    /// Logs given message to the same line on console
    /// </summary>
    /// <param name="message">Log message</param>
    protected void LogToConsoleSameLine(string message)
    {
      Console.Write("\r" + DateTime.Now.ToString(Log4NetHelper.LogDateTimeFormat) + " - " + message);
    }

    #region Implementation of IDataMigrator

    /// <inheritdoc />
    public abstract bool CanMigrate(string migratorKey);

    /// <inheritdoc />
    public abstract void Migrate();

    #endregion Implementation of IDataMigrator
  }
}