﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.DbMigrator.Core;

namespace ExpenseLogger.DbMigrator.Data
{
  /// <summary>
  ///   A utility class to generate data migrator keys
  /// </summary>
  public static class DataMigratorKeyGenerator
  {
    /// <summary>
    ///   Generates data migrator key based on the migration path given
    /// </summary>
    /// <param name="from">Data persistence provider to migrate from</param>
    /// <param name="to">Data persistence provider to migrate to</param>
    /// <returns>Data migrator key</returns>
    public static string Generate(EMigrateFrom from, EMigrateTo to)
    {
      return from + "_" + to;
    }
  }
}