﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Castle.MicroKernel;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.DbMigrator.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Legacy.Version2;
using Newtonsoft.Json;
// old models
using OldUser = ExpenseLogger.Models.Legacy.Version2.User;
using OldUserSetting = ExpenseLogger.Models.Legacy.Version2.UserSetting;
using OldCategory = ExpenseLogger.Models.Legacy.Version2.Category;
using OldExpense = ExpenseLogger.Models.Legacy.Version2.Expense;
// new models
using NewUser = ExpenseLogger.Models.Core.User;
using NewUserSetting = ExpenseLogger.Models.Core.UserSetting;
using NewCategory = ExpenseLogger.Models.Core.Category;
using NewExpense = ExpenseLogger.Models.Core.Expense;

namespace ExpenseLogger.DbMigrator.Data
{
  /// <summary>
  ///   A generic data migrator which can migrate data from a V2 schema to a V3 schema
  /// </summary>
  public class GenerictV2ToV3DataMigrator : AbstractDataMigrator
  {
    /// <summary>
    ///   Username of the test user
    /// </summary>
    private const string TestUsername = "test";

    /// <summary>
    ///   Built in currencies for ExpenseLogger v2
    /// </summary>
    private static readonly IDictionary<string, ECurrency> BuiltInCurrencies = new Dictionary<string, ECurrency>
    {
      {"&euro;", ECurrency.Euro},
      {"$", ECurrency.Dollar},
      {"&pound;", ECurrency.Pound},
      {"&yen;", ECurrency.Yen},
      {"Rs.", ECurrency.Rupees}
    };

    /// <summary>
    ///   All known migrator keys that this migrator can handle
    /// </summary>
    private static readonly string[] KnownMigratorKeys =
    {
      DataMigratorKeyGenerator.Generate(EMigrateFrom.SQLServerV2Schema, EMigrateTo.SQLiteV3Schema),
      DataMigratorKeyGenerator.Generate(EMigrateFrom.SQLServerV2Schema, EMigrateTo.SQLServerV3Schema)
    };

    /// <inheritdoc />
    public GenerictV2ToV3DataMigrator(IKernel kernel)
      : base(kernel)
    {
      // nothing to do 
    }

    /// <inheritdoc />
    public override bool CanMigrate(string migratorKey)
    {
      return KnownMigratorKeys.Contains(migratorKey);
    }

    /// <inheritdoc />
    public override void Migrate()
    {
      Logger.ToLoggerAndConsole("Migrating from: SQL Server based ExpenseLogger v2 schema");
      Logger.ToLoggerAndConsole("Migrating to: SQLite based ExpenseLogger v3 schema");

      Config conf = Kernel.Resolve<Config>();

      SQLServerDataContextV2 oldCtx = new SQLServerDataContextV2(conf.OldDbConnectionString);

      AbstractDataContext newCtx = null;

      switch (conf.NewDbType)
      {
        case EMigrateTo.SQLiteV3Schema:
          newCtx = new SQLiteDataContext(conf.NewDbConnectionString);
          break;

        case EMigrateTo.SQLServerV3Schema:
          newCtx = new SQLServerDataContext(conf.NewDbConnectionString);
          break;
      }

      if (newCtx == null)
        throw new DataException("Unable to create database context for new database");

      Hashtable oldToNewUserIdHash = new Hashtable();
      Hashtable oldToNewCategoryIdHash = new Hashtable();

      // migrate users
      foreach (OldUser ou in oldCtx.Users)
      {
        Logger.EmptyLine();
        Logger.ToLoggerAndConsole(string.Format("Migrating user: {0}({1})", ou.Username, ou.Name));

        // atm only currency can be migrated
        // TODO: Implement migration of all settings
        OldUserSetting ous = oldCtx.UserSettings.FirstOrDefault(f => f.UserID == ou.Id);
        if (ous == null)
          throw new DataException(string.Format("No settings found for old user: {0}({1})", ou.Username, ou.Name));

        NewUserSetting nus = new NewUserSetting();

        string[] split = ous.Options.Split(':');
        switch (split.Length)
        {
          case 1:
            break;

          case 2:
            break;

          case 3:
            break;

          case 4:
            nus.Currency = BuiltInCurrencies.ContainsKey(split[2]) ? BuiltInCurrencies[split[2]] : ECurrency.Money;
            break;

          default:
            nus.Currency = ECurrency.Money;
            break;
        }

        NewUser nu = new NewUser
        {
          Username = ou.Username,
          PasswordHash = MD5Helper.CreateHash(ou.Password),
          FullName = ou.Name,
          Email = ou.Username == TestUsername ? "test@example.com" : ou.Email,
          Country = "India",
          SecretQuestion = ou.SecretQuestion,
          SecretAnswer = MD5Helper.CreateHash(ou.SecretAnswer),
          MemberSince = ou.MemberSince,
          LastLoginDate = ou.MemberSince,
          IsAdmin = ou.Username == AppConstants.AdminUsername,
          IsVerified = ou.Verified,
          SettingsJson = JsonConvert.SerializeObject(nus)
        };

        newCtx.Users.Add(nu);
        newCtx.SaveChanges();

        oldToNewUserIdHash.Add(ou.Id, nu.Id);

        // migrate categories
        Logger.EmptyLine();
        Logger.ToLoggerAndConsole("Migrating user categories");

        IEnumerable<OldCategory> ocs = oldCtx.Categories.Where(f => f.UserId == ou.Id);

        if (!ocs.Any())
          Logger.ToLoggerAndConsole("User has no custom categories");

        foreach (var oc in ocs)
        {
          Logger.ToLoggerAndConsole("Category: " + oc.Description);
          NewCategory nc = newCtx.Categories.FirstOrDefault(f => f.UserId == nu.Id && f.Name == oc.Description);

          if (nc == null)
          {
            nc = new NewCategory
            {
              Name = oc.Description,
              UserId = nu.Id
            };

            newCtx.Categories.Add(nc);
            newCtx.SaveChanges();
          }

          oldToNewCategoryIdHash.Add(oc.Id, nc.Id);
        }

        Logger.ToLoggerAndConsole("Total " + ocs.Count() + " categories migrated");

        // migrate expenses
        Logger.EmptyLine();
        Logger.ToLoggerAndConsole("Migrating user expenses");

        IEnumerable<OldExpense> oes = oldCtx.Expenses.Where(f => f.UserId == ou.Id);

        long count = oes.LongCount();

        if (count == 0)
          Logger.ToLoggerAndConsole("User has no expenses");

        string expenseMsgTemplate = "Expense: {0}/" + count;
        long i = 1;

        foreach (var oe in oes)
        {
          LogToConsoleSameLine(string.Format(expenseMsgTemplate, i));
          NewExpense ne = new NewExpense
          {
            Date = oe.ExpenseDate.Date,
            CategoryId = Convert.ToInt32(oldToNewCategoryIdHash[oe.CategoryId]),
            Description = oe.Description,
            Amount = oe.Amount,
            UserId = Convert.ToInt32(oldToNewUserIdHash[oe.UserId])
          };

          newCtx.Expenses.Add(ne);

          i++;
        }

        if (count > 0)
          Console.WriteLine();

        Logger.ToLoggerAndConsole("Total " + count + " expenses migrated");

        newCtx.SaveChanges();
      }

      Logger.EmptyLine();
      Logger.ToLoggerAndConsole("Total " + oldCtx.Users.Count() + " users migrated");
    }
  }
}