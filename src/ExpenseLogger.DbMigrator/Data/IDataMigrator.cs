﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Data;

namespace ExpenseLogger.DbMigrator.Data
{
  /// <summary>
  ///   A generic interface implemented by all migrators
  /// </summary>
  public interface IDataMigrator
  {
    /// <summary>
    ///   Whether this migrate can migrate data based on given migrator key
    /// </summary>
    /// <param name="migratorKey">Migrator unique key</param>
    bool CanMigrate(string migratorKey);

    /// <summary>
    ///   Migrate data
    /// </summary>
    /// <exception cref="DataException">Thrown if migration fails due to missing/corrupt data in old database</exception>
    void Migrate();
  }
}