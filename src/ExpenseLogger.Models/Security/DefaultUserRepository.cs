﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;

namespace ExpenseLogger.Models.Security
{
  /// <summary>
  ///   Default user repository
  /// </summary>
  public class DefaultUserRepository : IUserRepository
  {
    private readonly IDataContext m_ctx;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="contextFactory">Data context factory to get <see cref="IDataContext"/> instance(s) from</param>
    public DefaultUserRepository(IDataContextFactory contextFactory)
    {
      m_ctx = contextFactory.GetInstance();
    }

    #region Implementation of IUserRepository

    /// <inheritdoc />
    public void SaveUser(User user)
    {
      m_ctx.SaveChanges(user);

      UpdateSummaries(user);
    }

    /// <inheritdoc />
    public User GetUser(string username)
    {
      return m_ctx.Users.FirstOrDefault(u => u.Username == username);
    }

    /// <inheritdoc />
    public IEnumerable<User> GetAllUsers()
    {
      return m_ctx.Users;
    }

    /// <inheritdoc />
    public bool ValidateUser(string username, string password)
    {
      string hash = MD5Helper.CreateHash(password);

      return m_ctx.Users.FirstOrDefault(u => u.Username == username && u.PasswordHash == hash) != null;
    }

    #endregion Implementation of IUserRepository

    #region Helper methods

    /// <summary>
    ///   Updates the expense summaries for given user
    /// </summary>
    /// <param name="user">User for which to update expense summaries</param>
    private void UpdateSummaries(User user)
    {
      ExpenseSummary summary = m_ctx.ExpenseSummaries.FirstOrDefault(f =>
        f.UserId == user.Id &&
        f.MonthAndYear.Year == DateTime.Now.Year &&
        f.MonthAndYear.Month == DateTime.Now.Month);

      if (summary == null)
      {
        summary = new ExpenseSummary
        {
          UserId = user.Id,
          MonthAndYear = DateTime.Now.Date,
          MonthBudget = user.Settings.MonthlyBudget
        };
      }
      else
        summary.MonthBudget = user.Settings.MonthlyBudget;

      m_ctx.SaveChanges(summary);
    }

    #endregion Helper methods

    #region Implementation of IDisposable

    /// <inheritdoc />
    public void Dispose()
    {
      m_ctx.Dispose();
    }

    #endregion
  }
}