﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Specialized;
using System.Configuration;
using System.Web.Security;

namespace ExpenseLogger.Models.Security
{
  /// <summary>
  ///   Membership provider wrapper
  /// </summary>
  public class MembershipProviderWrapper : MembershipProvider
  {
    private MembershipProvider m_provider;

    /// <summary>
    ///   Provider key used in the <see cref="M:Membership.Providers" /> collection
    /// </summary>
    public const string ProviderKey = "MembershipProviderWrapper";

    /// <summary>
    ///   Checks whether the internal membership provider is set
    /// </summary>
    /// <exception cref="System.Configuration.ConfigurationErrorsException">
    ///   Thrown if the internal membership provider is not
    ///   set
    /// </exception>
    private void CheckInternalProvider()
    {
      if (m_provider == null)
        throw new ConfigurationErrorsException("Internal membership provider not set. Please set this using the MembershipProviderWrapper.SetInternalProvider(MembershipProvider) method.");
    }

    /// <summary>
    ///   Set the internal provider implementation
    /// </summary>
    /// <param name="provider">Membership provider</param>
    public void SetInternalProvider(MembershipProvider provider)
    {
      m_provider = provider;
      m_provider.Initialize(ProviderKey, new NameValueCollection());
    }

    #region Overrides of MembershipProvider

    /// <summary>
    ///   Initialises the provider.
    /// </summary>
    /// <param name="name">Provider name</param>
    /// <param name="config">Configuration</param>
    public override void Initialize(string name, NameValueCollection config)
    {
      if (string.IsNullOrEmpty(name))
        name = ProviderKey;

      config["description"] =
        "A simple wrapper for membership provider. Delegates all operations to the internal provider " +
        "set by using the MembershipProvierWrapper.SetInternalProvider(MembershipProvider) method.";

      base.Initialize(name, config);
    }

    /// <summary>
    ///   Adds a new membership user to the data source.
    /// </summary>
    /// <param name="username">The user name for the new user.</param>
    /// <param name="password">The password for the new user.</param>
    /// <param name="email">The e-mail address for the new user.</param>
    /// <param name="passwordQuestion">The password question for the new user.</param>
    /// <param name="passwordAnswer">The password answer for the new user</param>
    /// <param name="isApproved">Whether or not the new user is approved to be validated.</param>
    /// <param name="providerUserKey">The unique identifier from the membership data source for the user.</param>
    /// <param name="status">
    ///   A <see cref="T:System.Web.Security.MembershipCreateStatus" /> enumeration value indicating whether
    ///   the user was created successfully.
    /// </param>
    /// <returns>
    ///   A <see cref="T:System.Web.Security.MembershipUser" /> object populated with the information for the newly created
    ///   user.
    /// </returns>
    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey,
      out MembershipCreateStatus status)
    {
      CheckInternalProvider();

      return m_provider.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, providerUserKey, out status);
    }

    /// <summary>
    ///   Processes a request to update the password question and answer for a membership user.
    /// </summary>
    /// <param name="username">The user to change the password question and answer for.</param>
    /// <param name="password">The password for the specified user.</param>
    /// <param name="newPasswordQuestion">The new password question for the specified user.</param>
    /// <param name="newPasswordAnswer">The new password answer for the specified user.</param>
    /// <returns>
    ///   true if the password question and answer are updated successfully; otherwise, false.
    /// </returns>
    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
      CheckInternalProvider();

      return m_provider.ChangePasswordQuestionAndAnswer(username, password, newPasswordQuestion, newPasswordAnswer);
    }

    /// <summary>
    ///   Gets the password for the specified user name from the data source.
    /// </summary>
    /// <param name="username">The user to retrieve the password for.</param>
    /// <param name="answer">The password answer for the user.</param>
    /// <returns>
    ///   The password for the specified user name.
    /// </returns>
    public override string GetPassword(string username, string answer)
    {
      CheckInternalProvider();

      return m_provider.GetPassword(username, answer);
    }

    /// <summary>
    ///   Processes a request to update the password for a membership user.
    /// </summary>
    /// <param name="username">The user to update the password for.</param>
    /// <param name="oldPassword">The current password for the specified user.</param>
    /// <param name="newPassword">The new password for the specified user.</param>
    /// <returns>
    ///   true if the password was updated successfully; otherwise, false.
    /// </returns>
    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
      CheckInternalProvider();

      return m_provider.ChangePassword(username, oldPassword, newPassword);
    }

    /// <summary>
    ///   Resets a user's password to a new, automatically generated password.
    /// </summary>
    /// <param name="username">The user to reset the password for.</param>
    /// <param name="answer">The password answer for the specified user.</param>
    /// <returns>The new password for the specified user.</returns>
    /// <exception cref="T:System.Configuration.Provider.ProviderException">
    ///   username is not found in the membership database.- or -The
    ///   change password action was canceled by a subscriber to the System.Web.Security.Membership.ValidatePassword
    ///   event and the <see cref="P:System.Web.Security.ValidatePasswordEventArgs.FailureInformation"></see> property was
    ///   null.- or -An
    ///   error occurred while retrieving the password from the database.
    /// </exception>
    /// <exception cref="T:System.NotSupportedException">
    ///   <see cref="P:System.Web.Security.SqlMembershipProvider.EnablePasswordReset"></see>
    ///   is set to false.
    /// </exception>
    /// <exception cref="T:System.ArgumentException">
    ///   username is an empty string (""), contains a comma, or is longer than 256 characters.
    ///   - or -passwordAnswer is an empty string or is longer than 128 characters and
    ///   <see cref="P:System.Web.Security.SqlMembershipProvider.RequiresQuestionAndAnswer"></see> is true.- or -passwordAnswer
    ///   is longer
    ///   than 128 characters after encoding.
    /// </exception>
    /// <exception cref="T:System.ArgumentNullException">
    ///   username is null.- or -passwordAnswer is null and
    ///   <see cref="P:System.Web.Security.SqlMembershipProvider.RequiresQuestionAndAnswer"></see> is true.
    /// </exception>
    /// <exception cref="T:System.Web.Security.MembershipPasswordException">
    ///   passwordAnswer is invalid. - or -The user account
    ///   is currently locked out.
    /// </exception>
    public override string ResetPassword(string username, string answer)
    {
      CheckInternalProvider();

      return m_provider.ResetPassword(username, answer);
    }

    /// <summary>
    ///   Updates information about a user in the data source.
    /// </summary>
    /// <param name="user">
    ///   A <see cref="T:System.Web.Security.MembershipUser" /> object that represents the user to update and
    ///   the updated information for the user.
    /// </param>
    public override void UpdateUser(MembershipUser user)
    {
      CheckInternalProvider();

      m_provider.UpdateUser(user);
    }

    /// <summary>
    ///   Verifies that the specified user name and password exist in the data source.
    /// </summary>
    /// <param name="username">The name of the user to validate.</param>
    /// <param name="password">The password for the specified user.</param>
    /// <returns>
    ///   true if the specified username and password are valid; otherwise, false.
    /// </returns>
    public override bool ValidateUser(string username, string password)
    {
      CheckInternalProvider();

      return m_provider.ValidateUser(username, password);
    }

    /// <summary>
    ///   Unlocks the user.
    /// </summary>
    /// <param name="userName">The username.</param>
    /// <returns>Returns true if user was unlocked; otherwise returns false.</returns>
    public override bool UnlockUser(string userName)
    {
      CheckInternalProvider();

      return m_provider.UnlockUser(userName);
    }

    /// <summary>
    ///   Gets user information from the data source based on the unique identifier for the membership user. Provides an option
    ///   to update the last-activity date/time stamp for the user.
    /// </summary>
    /// <param name="providerUserKey">The unique identifier for the membership user to get information for.</param>
    /// <param name="userIsOnline">
    ///   true to update the last-activity date/time stamp for the user; false to return user
    ///   information without updating the last-activity date/time stamp for the user.
    /// </param>
    /// <returns>
    ///   A <see cref="T:System.Web.Security.MembershipUser" /> object populated with the specified user's information from the
    ///   data source.
    /// </returns>
    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
      CheckInternalProvider();

      return m_provider.GetUser(providerUserKey, userIsOnline);
    }

    /// <summary>
    ///   Gets information from the data source for a user. Provides an option to update the last-activity date/time stamp for
    ///   the user.
    /// </summary>
    /// <param name="username">The name of the user to get information for.</param>
    /// <param name="userIsOnline">
    ///   true to update the last-activity date/time stamp for the user; false to return user
    ///   information without updating the last-activity date/time stamp for the user.
    /// </param>
    /// <returns>
    ///   A <see cref="T:System.Web.Security.MembershipUser" /> object populated with the specified user's information from the
    ///   data source.
    /// </returns>
    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
      CheckInternalProvider();

      return m_provider.GetUser(username, userIsOnline);
    }

    /// <summary>
    ///   Gets the user name associated with the specified e-mail address.
    /// </summary>
    /// <param name="email">The e-mail address to search for.</param>
    /// <returns>
    ///   The user name associated with the specified e-mail address. If no match is found, return null.
    /// </returns>
    public override string GetUserNameByEmail(string email)
    {
      CheckInternalProvider();

      return m_provider.GetUserNameByEmail(email);
    }

    /// <summary>
    ///   Removes a user from the membership data source.
    /// </summary>
    /// <param name="username">The name of the user to delete.</param>
    /// <param name="deleteAllRelatedData">
    ///   true to delete data related to the user from the database; false to leave data
    ///   related to the user in the database.
    /// </param>
    /// <returns>
    ///   true if the user was successfully deleted; otherwise, false.
    /// </returns>
    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
      CheckInternalProvider();

      return m_provider.DeleteUser(username, deleteAllRelatedData);
    }

    /// <summary>
    ///   Gets a collection of all the users in the data source in pages of data.
    /// </summary>
    /// <param name="pageIndex">The index of the page of results to return. <paramref name="pageIndex" /> is zero-based.</param>
    /// <param name="pageSize">The size of the page of results to return.</param>
    /// <param name="totalRecords">The total number of matched users.</param>
    /// <returns>
    ///   A <see cref="T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of
    ///   <paramref name="pageSize" /><see cref="T:System.Web.Security.MembershipUser" /> objects beginning at the page
    ///   specified by <paramref name="pageIndex" />.
    /// </returns>
    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
      CheckInternalProvider();

      return m_provider.GetAllUsers(pageIndex, pageSize, out totalRecords);
    }

    /// <summary>
    ///   Gets the number of users currently accessing the application.
    /// </summary>
    /// <returns>
    ///   The number of users currently accessing the application.
    /// </returns>
    public override int GetNumberOfUsersOnline()
    {
      CheckInternalProvider();

      return m_provider.GetNumberOfUsersOnline();
    }

    /// <summary>
    ///   Gets a collection of membership users where the user name contains the specified user name to match.
    /// </summary>
    /// <param name="usernameToMatch">The user name to search for.</param>
    /// <param name="pageIndex">The index of the page of results to return. <paramref name="pageIndex" /> is zero-based.</param>
    /// <param name="pageSize">The size of the page of results to return.</param>
    /// <param name="totalRecords">The total number of matched users.</param>
    /// <returns>
    ///   A <see cref="T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of
    ///   <paramref name="pageSize" /><see cref="T:System.Web.Security.MembershipUser" /> objects beginning at the page
    ///   specified by <paramref name="pageIndex" />.
    /// </returns>
    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
      CheckInternalProvider();

      return m_provider.FindUsersByName(usernameToMatch, pageIndex, pageSize, out totalRecords);
    }

    /// <summary>
    ///   Gets a collection of membership users where the e-mail address contains the specified e-mail address to match.
    /// </summary>
    /// <param name="emailToMatch">The e-mail address to search for.</param>
    /// <param name="pageIndex">The index of the page of results to return. <paramref name="pageIndex" /> is zero-based.</param>
    /// <param name="pageSize">The size of the page of results to return.</param>
    /// <param name="totalRecords">The total number of matched users.</param>
    /// <returns>
    ///   A <see cref="T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of
    ///   <paramref name="pageSize" /><see cref="T:System.Web.Security.MembershipUser" /> objects beginning at the page
    ///   specified by <paramref name="pageIndex" />.
    /// </returns>
    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
      CheckInternalProvider();

      return m_provider.FindUsersByEmail(emailToMatch, pageIndex, pageSize, out totalRecords);
    }

    /// <summary>
    ///   Indicates whether the membership provider is configured to allow users to retrieve their passwords.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   true if the membership provider is configured to support password retrieval; otherwise, false. The default is false.
    /// </returns>
    public override bool EnablePasswordRetrieval
    {
      get
      {
        CheckInternalProvider();
        return m_provider.EnablePasswordRetrieval;
      }
    }

    /// <summary>
    ///   Indicates whether the membership provider is configured to allow users to reset their passwords.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   true if the membership provider supports password reset; otherwise, false. The default is true.
    /// </returns>
    public override bool EnablePasswordReset
    {
      get
      {
        CheckInternalProvider();
        return m_provider.EnablePasswordReset;
      }
    }

    /// <summary>
    ///   Gets a value indicating whether the membership provider is configured to require the user to answer a password
    ///   question for password reset and retrieval.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   true if a password answer is required for password reset and retrieval; otherwise, false. The default is true.
    /// </returns>
    public override bool RequiresQuestionAndAnswer
    {
      get
      {
        CheckInternalProvider();
        return m_provider.RequiresQuestionAndAnswer;
      }
    }

    /// <summary>
    ///   The name of the application using the custom membership provider.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The name of the application using the custom membership provider.
    /// </returns>
    public override string ApplicationName
    {
      get
      {
        CheckInternalProvider();
        return m_provider.ApplicationName;
      }
      set
      {
        CheckInternalProvider();
        m_provider.ApplicationName = value;
      }
    }

    /// <summary>
    ///   Gets the number of invalid password or password-answer attempts allowed before the membership user is locked out.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The number of invalid password or password-answer attempts allowed before the membership user is locked out.
    /// </returns>
    public override int MaxInvalidPasswordAttempts
    {
      get
      {
        CheckInternalProvider();
        return m_provider.MaxInvalidPasswordAttempts;
      }
    }

    /// <summary>
    ///   Gets the number of minutes in which a maximum number of invalid password or password-answer attempts are allowed
    ///   before the membership user is locked out.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The number of minutes in which a maximum number of invalid password or password-answer attempts are allowed before
    ///   the membership user is locked out.
    /// </returns>
    public override int PasswordAttemptWindow
    {
      get
      {
        CheckInternalProvider();
        return m_provider.PasswordAttemptWindow;
      }
    }

    /// <summary>
    ///   Gets a value indicating whether the membership provider is configured to require a unique e-mail address for each
    ///   user name.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   true if the membership provider requires a unique e-mail address; otherwise, false. The default is true.
    /// </returns>
    public override bool RequiresUniqueEmail
    {
      get
      {
        CheckInternalProvider();
        return m_provider.RequiresUniqueEmail;
      }
    }

    /// <summary>
    ///   Gets a value indicating the format for storing passwords in the membership data store.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   One of the <see cref="T:System.Web.Security.MembershipPasswordFormat" /> values indicating the format for storing
    ///   passwords in the data store.
    /// </returns>
    public override MembershipPasswordFormat PasswordFormat
    {
      get
      {
        CheckInternalProvider();
        return m_provider.PasswordFormat;
      }
    }

    /// <summary>
    ///   Gets the minimum length required for a password.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The minimum length required for a password.
    /// </returns>
    public override int MinRequiredPasswordLength
    {
      get
      {
        CheckInternalProvider();
        return m_provider.MinRequiredPasswordLength;
      }
    }

    /// <summary>
    ///   Gets the minimum number of special characters that must be present in a valid password.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The minimum number of special characters that must be present in a valid password.
    /// </returns>
    public override int MinRequiredNonAlphanumericCharacters
    {
      get
      {
        CheckInternalProvider();
        return m_provider.MinRequiredNonAlphanumericCharacters;
      }
    }

    /// <summary>
    ///   Gets the regular expression used to evaluate a password.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   A regular expression used to evaluate a password.
    /// </returns>
    public override string PasswordStrengthRegularExpression
    {
      get
      {
        CheckInternalProvider();
        return m_provider.PasswordStrengthRegularExpression;
      }
    }

    #endregion Overrides of MembershipProvider
  }
}