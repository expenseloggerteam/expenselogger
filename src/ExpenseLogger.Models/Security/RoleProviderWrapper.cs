﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Specialized;
using System.Configuration;
using System.Web.Security;

namespace ExpenseLogger.Models.Security
{
  /// <summary>
  ///   Role provider wrapper
  /// </summary>
  public class RoleProviderWrapper : RoleProvider
  {
    private RoleProvider m_provider;

    /// <summary>
    ///   Provider key used in the <see cref="M:Roles.Providers" /> collection
    /// </summary>
    public const string ProviderKey = "RoleProviderWrapper";

    /// <summary>
    ///   Checks whether the internal role provider is set
    /// </summary>
    /// <exception cref="System.Configuration.ConfigurationErrorsException">
    ///   Thrown if the internal role provider is not
    ///   set
    /// </exception>
    private void CheckInternalProvider()
    {
      if (m_provider == null)
        throw new ConfigurationErrorsException("Internal role provider not set. Please set this using the RoleProviderWrapper.SetInternalProvider(RoleProvider) method.");
    }

    /// <summary>
    ///   Set the internal provider implementation
    /// </summary>
    /// <param name="provider">Role provider</param>
    public void SetInternalProvider(RoleProvider provider)
    {
      m_provider = provider;
      m_provider.Initialize(ProviderKey, new NameValueCollection());
    }

    #region Overrides of RoleProvider

    /// <summary>
    ///   Initializes the provider.
    /// </summary>
    /// <param name="name">The friendly name of the provider.</param>
    /// <param name="config">
    ///   A collection of the name/value pairs representing the provider-specific attributes specified in
    ///   the configuration for this provider.
    /// </param>
    /// <exception cref="T:System.ArgumentNullException">
    ///   The name of the provider is null.
    /// </exception>
    /// <exception cref="T:System.ArgumentException">
    ///   The name of the provider has a length of zero.
    /// </exception>
    /// <exception cref="T:System.InvalidOperationException">
    ///   An attempt is made to call
    ///   <see
    ///     cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)" />
    ///   on a provider after the provider has already been initialized.
    /// </exception>
    public override void Initialize(string name, NameValueCollection config)
    {
      if (string.IsNullOrEmpty(name))
        name = ProviderKey;

      config["description"] =
        "A simple wrapper for role provider. Delegates all operations to the internal provider " +
        "set by using the RoleProviderWrapper.SetInternalProvider(RoleProvider) method.";

      // init the abstract base class.
      base.Initialize(name, config);
    }

    /// <summary>
    ///   Gets a value indicating whether the specified user is in the specified role for the configured applicationName.
    /// </summary>
    /// <param name="username">The user name to search for.</param>
    /// <param name="roleName">The role to search in.</param>
    /// <returns>
    ///   true if the specified user is in the specified role for the configured applicationName; otherwise, false.
    /// </returns>
    public override bool IsUserInRole(string username, string roleName)
    {
      CheckInternalProvider();

      return m_provider.IsUserInRole(username, roleName);
    }

    /// <summary>
    ///   Gets a list of the roles that a specified user is in for the configured applicationName.
    /// </summary>
    /// <param name="username">The user to return a list of roles for.</param>
    /// <returns>
    ///   A string array containing the names of all the roles that the specified user is in for the configured
    ///   applicationName.
    /// </returns>
    public override string[] GetRolesForUser(string username)
    {
      CheckInternalProvider();

      return m_provider.GetRolesForUser(username);
    }

    /// <summary>
    ///   Adds a new role to the data source for the configured applicationName.
    /// </summary>
    /// <param name="roleName">The name of the role to create.</param>
    public override void CreateRole(string roleName)
    {
      CheckInternalProvider();

      m_provider.CreateRole(roleName);
    }

    /// <summary>
    ///   Removes a role from the data source for the configured applicationName.
    /// </summary>
    /// <param name="roleName">The name of the role to delete.</param>
    /// <param name="throwOnPopulatedRole">
    ///   If true, throw an exception if <paramref name="roleName" /> has one or more members
    ///   and do not delete <paramref name="roleName" />.
    /// </param>
    /// <returns>
    ///   true if the role was successfully deleted; otherwise, false.
    /// </returns>
    public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    {
      CheckInternalProvider();

      return m_provider.DeleteRole(roleName, throwOnPopulatedRole);
    }

    /// <summary>
    ///   Gets a value indicating whether the specified role name already exists in the role data source for the configured
    ///   applicationName.
    /// </summary>
    /// <param name="roleName">The name of the role to search for in the data source.</param>
    /// <returns>
    ///   true if the role name already exists in the data source for the configured applicationName; otherwise, false.
    /// </returns>
    public override bool RoleExists(string roleName)
    {
      CheckInternalProvider();

      return m_provider.RoleExists(roleName);
    }

    /// <summary>
    ///   Adds the specified user names to the specified roles for the configured applicationName.
    /// </summary>
    /// <param name="usernames">A string array of user names to be added to the specified roles.</param>
    /// <param name="roleNames">A string array of the role names to add the specified user names to.</param>
    public override void AddUsersToRoles(string[] usernames, string[] roleNames)
    {
      CheckInternalProvider();

      m_provider.AddUsersToRoles(usernames, roleNames);
    }

    /// <summary>
    ///   Removes the specified user names from the specified roles for the configured applicationName.
    /// </summary>
    /// <param name="usernames">A string array of user names to be removed from the specified roles.</param>
    /// <param name="roleNames">A string array of role names to remove the specified user names from.</param>
    public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    {
      CheckInternalProvider();

      m_provider.RemoveUsersFromRoles(usernames, roleNames);
    }

    /// <summary>
    ///   Gets the users in role.
    /// </summary>
    /// <param name="roleName">Name of the role.</param>
    /// <returns>Returns the users in role.</returns>
    public override string[] GetUsersInRole(string roleName)
    {
      CheckInternalProvider();

      return m_provider.GetUsersInRole(roleName);
    }

    /// <summary>
    ///   Gets a list of all the roles for the configured applicationName.
    /// </summary>
    /// <returns>
    ///   A string array containing the names of all the roles stored in the data source for the configured applicationName.
    /// </returns>
    public override string[] GetAllRoles()
    {
      CheckInternalProvider();

      return m_provider.GetAllRoles();
    }

    /// <summary>
    ///   Gets an array of user names in a role where the user name contains the specified user name to match.
    /// </summary>
    /// <param name="roleName">The role to search in.</param>
    /// <param name="usernameToMatch">The user name to search for.</param>
    /// <returns>
    ///   A string array containing the names of all the users where the user name matches <paramref name="usernameToMatch" />
    ///   and the user is a member of the specified role.
    /// </returns>
    public override string[] FindUsersInRole(string roleName, string usernameToMatch)
    {
      CheckInternalProvider();

      return m_provider.FindUsersInRole(roleName, usernameToMatch);
    }

    /// <summary>
    ///   Gets or sets the name of the application to store and retrieve role information for.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The name of the application to store and retrieve role information for.
    /// </returns>
    public override string ApplicationName
    {
      get
      {
        CheckInternalProvider();
        return m_provider.ApplicationName;
      }
      set
      {
        CheckInternalProvider();
        m_provider.ApplicationName = value;
      }
    }

    #endregion
  }
}