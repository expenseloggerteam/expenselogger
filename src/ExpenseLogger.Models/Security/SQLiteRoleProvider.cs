﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Data;
using System.Data.SQLite;
using System.Web;
using System.Web.Security;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Security
{
  /// <summary>
  ///   SQLite based ASP.Net role provider
  /// </summary>
  public class SQLiteRoleProvider : RoleProvider
  {
    #region Private Fields

    private const string HTTPTransactionId = "SQLiteTran";
    private const string AppTableName = "[aspnet_Applications]";
    private const string RoleTableName = "[aspnet_Roles]";
    private const string UserTableName = "[aspnet_Users]";
    private const string UsersInRolesTableName = "[aspnet_UsersInRoles]";
    private const int MaxUsernameLength = 256;
    private const int MaxRolenameLength = 256;
    private const int MaxApplicationNameLength = 256;

    private static string APPLICATION_ID;
    private static string APPLICATION_NAME;

    private static string MEMBERSHIP_APPLICATION_ID;
    private static string MEMBERSHIP_APPLICATION_NAME;

    private static string CONNECTION_STRING;

    #endregion Private Fields

    /// <summary>
    ///   The provider key used in the <see cref="M:Roles.Providers" /> collection
    /// </summary>
    public const string ProviderKey = "SQLiteRoleProvider";

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="connString">Database connection string</param>
    public SQLiteRoleProvider(string connString)
    {
      CONNECTION_STRING = connString;
    }

    #region Public Properties

    /// <summary>
    ///   Gets or sets the name of the application to store and retrieve role information for.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The name of the application to store and retrieve role information for.
    /// </returns>
    public override string ApplicationName
    {
      get { return APPLICATION_NAME; }
      set
      {
        if (value.Length > MaxApplicationNameLength)
          throw new ProviderException(string.Format("SQLiteRoleProvider error: applicationName must be less than or equal to {0} characters.", MaxApplicationNameLength));

        APPLICATION_NAME = value;
        APPLICATION_ID = GetApplicationId(APPLICATION_NAME);
      }
    }

    /// <summary>
    ///   Gets or sets the name of the application used by the Membership provider.
    /// </summary>
    /// <value></value>
    /// <returns>
    ///   The name of the application used by the Membership provider.
    /// </returns>
    public static string MembershipApplicationName
    {
      get { return MEMBERSHIP_APPLICATION_NAME; }
      set
      {
        if (value.Length > MaxApplicationNameLength)
          throw new ProviderException(string.Format("SQLiteRoleProvider error: membershipApplicationName must be less than or equal to {0} characters.", MaxApplicationNameLength));

        MEMBERSHIP_APPLICATION_NAME = value;
        MEMBERSHIP_APPLICATION_ID = ((APPLICATION_NAME == MEMBERSHIP_APPLICATION_NAME) ? APPLICATION_ID : GetApplicationId(MEMBERSHIP_APPLICATION_NAME));
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    ///   Initializes the provider.
    /// </summary>
    /// <param name="name">The friendly name of the provider.</param>
    /// <param name="config">
    ///   A collection of the name/value pairs representing the provider-specific attributes specified in
    ///   the configuration for this provider.
    /// </param>
    /// <exception cref="T:System.ArgumentNullException">
    ///   The name of the provider is null.
    /// </exception>
    /// <exception cref="T:System.ArgumentException">
    ///   The name of the provider has a length of zero.
    /// </exception>
    /// <exception cref="T:System.InvalidOperationException">
    ///   An attempt is made to call
    ///   <see
    ///     cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)" />
    ///   on a provider after the provider has already been initialized.
    /// </exception>
    public override void Initialize(string name, NameValueCollection config)
    {
      if (string.IsNullOrEmpty(name))
        name = ProviderKey;

      config["description"] = "SQLite powered role provider";

      // init the abstract base class.
      base.Initialize(name, config);

      ApplicationName = AppConstants.ApplicationName;
      MembershipApplicationName = ApplicationName;

      // Verify a record exists in the application table.
      VerifyApplication();
    }

    /// <summary>
    ///   Adds the specified user names to the specified roles for the configured applicationName.
    /// </summary>
    /// <param name="usernames">A string array of user names to be added to the specified roles.</param>
    /// <param name="roleNames">A string array of the role names to add the specified user names to.</param>
    public override void AddUsersToRoles(string[] usernames, string[] roleNames)
    {
      foreach (string roleName in roleNames)
      {
        if (!RoleExists(roleName))
        {
          throw new ProviderException("Role name not found.");
        }
      }

      foreach (string username in usernames)
      {
        if (username.IndexOf(',') > 0)
        {
          throw new ArgumentException("User names cannot contain commas.");
        }

        foreach (string roleName in roleNames)
        {
          if (IsUserInRole(username, roleName))
          {
            throw new ProviderException("User is already in role.");
          }
        }
      }

      SQLiteTransaction tran = null;
      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        if (cn.State == ConnectionState.Closed)
          cn.Open();

        if (!IsTransactionInProgress())
          tran = cn.BeginTransaction();

        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "INSERT INTO " + UsersInRolesTableName
                            + " (UserId, RoleId)"
                            + " SELECT u.UserId, r.RoleId"
                            + " FROM " + UserTableName + " u, " + RoleTableName + " r"
                            + " WHERE (u.LoweredUsername = $Username) AND (u.ApplicationId = $MembershipApplicationId)"
                            + " AND (r.LoweredRoleName = $RoleName) AND (r.ApplicationId = $ApplicationId)";

          SQLiteParameter userParm = cmd.Parameters.Add("$Username", DbType.String, MaxUsernameLength);
          SQLiteParameter roleParm = cmd.Parameters.Add("$RoleName", DbType.String, MaxRolenameLength);
          cmd.Parameters.AddWithValue("$MembershipApplicationId", MEMBERSHIP_APPLICATION_ID);
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          foreach (string username in usernames)
          {
            foreach (string roleName in roleNames)
            {
              userParm.Value = username.ToLowerInvariant();
              roleParm.Value = roleName.ToLowerInvariant();
              cmd.ExecuteNonQuery();
            }
          }

          // Commit the transaction if it's the one we created in this method.
          if (tran != null)
            tran.Commit();
        }
      }
      catch
      {
        if (tran != null)
        {
          try
          {
            tran.Rollback();
          }
          catch (SQLiteException)
          {
          }
        }
        throw;
      }
      finally
      {
        if (tran != null)
          tran.Dispose();

        if (!IsTransactionInProgress())
          cn.Dispose();
      }
    }

    /// <summary>
    ///   Adds a new role to the data source for the configured applicationName.
    /// </summary>
    /// <param name="roleName">The name of the role to create.</param>
    public override void CreateRole(string roleName)
    {
      if (roleName.IndexOf(',') > 0)
      {
        throw new ArgumentException("Role names cannot contain commas.");
      }

      if (RoleExists(roleName))
      {
        throw new ProviderException("Role name already exists.");
      }

      if (!SecurityUtil.ValidateParameter(ref roleName, true, true, false, MaxRolenameLength))
      {
        throw new ProviderException(string.Format("The role name is too long: it must not exceed {0} chars in length.", MaxRolenameLength));
      }

      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "INSERT INTO " + RoleTableName
                            + " (RoleId, RoleName, LoweredRoleName, ApplicationId) "
                            + " Values ($RoleId, $RoleName, $LoweredRoleName, $ApplicationId)";

          cmd.Parameters.AddWithValue("$RoleId", Guid.NewGuid().ToString());
          cmd.Parameters.AddWithValue("$RoleName", roleName);
          cmd.Parameters.AddWithValue("$LoweredRoleName", roleName.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          cmd.ExecuteNonQuery();
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }
    }

    /// <summary>
    ///   Removes a role from the data source for the configured applicationName.
    /// </summary>
    /// <param name="roleName">The name of the role to delete.</param>
    /// <param name="throwOnPopulatedRole">
    ///   If true, throw an exception if <paramref name="roleName" /> has one or more members
    ///   and do not delete <paramref name="roleName" />.
    /// </param>
    /// <returns>
    ///   true if the role was successfully deleted; otherwise, false.
    /// </returns>
    public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    {
      if (!RoleExists(roleName))
      {
        throw new ProviderException("Role does not exist.");
      }

      if (throwOnPopulatedRole && GetUsersInRole(roleName).Length > 0)
      {
        throw new ProviderException("Cannot delete a populated role.");
      }

      SQLiteTransaction tran = null;
      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        if (cn.State == ConnectionState.Closed)
          cn.Open();

        if (!IsTransactionInProgress())
          tran = cn.BeginTransaction();

        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "DELETE FROM " + UsersInRolesTableName + " WHERE (RoleId IN"
                            + " (SELECT RoleId FROM " + RoleTableName + " WHERE LoweredRoleName = $RoleName))";

          cmd.Parameters.AddWithValue("$RoleName", roleName.ToLowerInvariant());

          cmd.ExecuteNonQuery();
        }

        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "DELETE FROM " + RoleTableName + " WHERE LoweredRoleName = $RoleName AND ApplicationId = $ApplicationId";

          cmd.Parameters.AddWithValue("$RoleName", roleName.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          cmd.ExecuteNonQuery();
        }

        // Commit the transaction if it's the one we created in this method.
        if (tran != null)
          tran.Commit();
      }
      catch
      {
        if (tran != null)
        {
          try
          {
            tran.Rollback();
          }
          catch (SQLiteException)
          {
          }
        }

        throw;
      }
      finally
      {
        if (tran != null)
          tran.Dispose();

        if (!IsTransactionInProgress())
          cn.Dispose();
      }

      return true;
    }

    /// <summary>
    ///   Gets a list of all the roles for the configured applicationName.
    /// </summary>
    /// <returns>
    ///   A string array containing the names of all the roles stored in the data source for the configured applicationName.
    /// </returns>
    public override string[] GetAllRoles()
    {
      string tmpRoleNames = string.Empty;

      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "SELECT RoleName FROM " + RoleTableName + " WHERE ApplicationId = $ApplicationId";
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          using (SQLiteDataReader dr = cmd.ExecuteReader())
          {
            while (dr.Read())
            {
              tmpRoleNames += dr.GetString(0) + ",";
            }
          }
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }

      if (tmpRoleNames.Length > 0)
      {
        // Remove trailing comma.
        tmpRoleNames = tmpRoleNames.Substring(0, tmpRoleNames.Length - 1);
        return tmpRoleNames.Split(',');
      }

      return new string[0];
    }

    /// <summary>
    ///   Gets a list of the roles that a specified user is in for the configured applicationName.
    /// </summary>
    /// <param name="username">The user to return a list of roles for.</param>
    /// <returns>
    ///   A string array containing the names of all the roles that the specified user is in for the configured
    ///   applicationName.
    /// </returns>
    public override string[] GetRolesForUser(string username)
    {
      string tmpRoleNames = string.Empty;

      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "SELECT r.RoleName FROM " + RoleTableName + " r INNER JOIN " + UsersInRolesTableName
                            + " uir ON r.RoleId = uir.RoleId INNER JOIN " + UserTableName + " u ON uir.UserId = u.UserId"
                            + " WHERE (u.LoweredUsername = $Username) AND (u.ApplicationId = $MembershipApplicationId)";

          cmd.Parameters.AddWithValue("$Username", username.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$MembershipApplicationId", MEMBERSHIP_APPLICATION_ID);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          using (SQLiteDataReader dr = cmd.ExecuteReader())
          {
            while (dr.Read())
            {
              tmpRoleNames += dr.GetString(0) + ",";
            }
          }
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }

      if (tmpRoleNames.Length > 0)
      {
        // Remove trailing comma.
        tmpRoleNames = tmpRoleNames.Substring(0, tmpRoleNames.Length - 1);
        return tmpRoleNames.Split(',');
      }

      return new string[0];
    }

    /// <summary>
    ///   Gets the users in role.
    /// </summary>
    /// <param name="roleName">Name of the role.</param>
    /// <returns>Returns the users in role.</returns>
    public override string[] GetUsersInRole(string roleName)
    {
      string tmpUserNames = string.Empty;

      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "SELECT u.Username FROM " + UserTableName + " u INNER JOIN " + UsersInRolesTableName
                            + " uir ON u.UserId = uir.UserId INNER JOIN " + RoleTableName + " r ON uir.RoleId = r.RoleId"
                            + " WHERE (r.LoweredRoleName = $RoleName) AND (r.ApplicationId = $ApplicationId)";

          cmd.Parameters.AddWithValue("$RoleName", roleName.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          using (SQLiteDataReader dr = cmd.ExecuteReader())
          {
            while (dr.Read())
            {
              tmpUserNames += dr.GetString(0) + ",";
            }
          }
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }

      if (tmpUserNames.Length > 0)
      {
        // Remove trailing comma.
        tmpUserNames = tmpUserNames.Substring(0, tmpUserNames.Length - 1);
        return tmpUserNames.Split(',');
      }

      return new string[0];
    }

    /// <summary>
    ///   Gets a value indicating whether the specified user is in the specified role for the configured applicationName.
    /// </summary>
    /// <param name="username">The user name to search for.</param>
    /// <param name="roleName">The role to search in.</param>
    /// <returns>
    ///   true if the specified user is in the specified role for the configured applicationName; otherwise, false.
    /// </returns>
    public override bool IsUserInRole(string username, string roleName)
    {
      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "SELECT COUNT(*) FROM " + UsersInRolesTableName + " uir INNER JOIN "
                            + UserTableName + " u ON uir.UserId = u.UserId INNER JOIN " + RoleTableName + " r ON uir.RoleId = r.RoleId "
                            + " WHERE u.LoweredUsername = $Username AND u.ApplicationId = $MembershipApplicationId"
                            + " AND r.LoweredRoleName = $RoleName AND r.ApplicationId = $ApplicationId";

          cmd.Parameters.AddWithValue("$Username", username.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$RoleName", roleName.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$MembershipApplicationId", MEMBERSHIP_APPLICATION_ID);
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          return (Convert.ToInt64(cmd.ExecuteScalar()) > 0);
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }
    }

    /// <summary>
    ///   Removes the specified user names from the specified roles for the configured applicationName.
    /// </summary>
    /// <param name="usernames">A string array of user names to be removed from the specified roles.</param>
    /// <param name="roleNames">A string array of role names to remove the specified user names from.</param>
    public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    {
      foreach (string roleName in roleNames)
      {
        if (!RoleExists(roleName))
        {
          throw new ProviderException("Role name not found.");
        }
      }

      foreach (string username in usernames)
      {
        foreach (string roleName in roleNames)
        {
          if (!IsUserInRole(username, roleName))
          {
            throw new ProviderException("User is not in role.");
          }
        }
      }

      SQLiteTransaction tran = null;
      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        if (cn.State == ConnectionState.Closed)
          cn.Open();

        if (!IsTransactionInProgress())
          tran = cn.BeginTransaction();

        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "DELETE FROM " + UsersInRolesTableName
                            + " WHERE UserId = (SELECT UserId FROM " + UserTableName + " WHERE LoweredUsername = $Username AND ApplicationId = $MembershipApplicationId)"
                            + " AND RoleId = (SELECT RoleId FROM " + RoleTableName + " WHERE LoweredRoleName = $RoleName AND ApplicationId = $ApplicationId)";

          SQLiteParameter userParm = cmd.Parameters.Add("$Username", DbType.String, MaxUsernameLength);
          SQLiteParameter roleParm = cmd.Parameters.Add("$RoleName", DbType.String, MaxRolenameLength);
          cmd.Parameters.AddWithValue("$MembershipApplicationId", MEMBERSHIP_APPLICATION_ID);
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          foreach (string username in usernames)
          {
            foreach (string roleName in roleNames)
            {
              userParm.Value = username.ToLowerInvariant();
              roleParm.Value = roleName.ToLowerInvariant();
              cmd.ExecuteNonQuery();
            }
          }

          // Commit the transaction if it's the one we created in this method.
          if (tran != null)
            tran.Commit();
        }
      }
      catch
      {
        if (tran != null)
        {
          try
          {
            tran.Rollback();
          }
          catch (SQLiteException)
          {
          }
        }

        throw;
      }
      finally
      {
        if (tran != null)
          tran.Dispose();

        if (!IsTransactionInProgress())
          cn.Dispose();
      }
    }

    /// <summary>
    ///   Gets a value indicating whether the specified role name already exists in the role data source for the configured
    ///   applicationName.
    /// </summary>
    /// <param name="roleName">The name of the role to search for in the data source.</param>
    /// <returns>
    ///   true if the role name already exists in the data source for the configured applicationName; otherwise, false.
    /// </returns>
    public override bool RoleExists(string roleName)
    {
      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "SELECT COUNT(*) FROM " + RoleTableName +
                            " WHERE LoweredRoleName = $RoleName AND ApplicationId = $ApplicationId";

          cmd.Parameters.AddWithValue("$RoleName", roleName.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          return (Convert.ToInt64(cmd.ExecuteScalar()) > 0);
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }
    }

    /// <summary>
    ///   Gets an array of user names in a role where the user name contains the specified user name to match.
    /// </summary>
    /// <param name="roleName">The role to search in.</param>
    /// <param name="usernameToMatch">The user name to search for.</param>
    /// <returns>
    ///   A string array containing the names of all the users where the user name matches <paramref name="usernameToMatch" />
    ///   and the user is a member of the specified role.
    /// </returns>
    public override string[] FindUsersInRole(string roleName, string usernameToMatch)
    {
      string tmpUserNames = string.Empty;

      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "SELECT u.Username FROM " + UsersInRolesTableName + " uir INNER JOIN " + UserTableName
                            + " u ON uir.UserId = u.UserId INNER JOIN " + RoleTableName + " r ON r.RoleId = uir.RoleId"
                            + " WHERE u.LoweredUsername LIKE $UsernameSearch AND r.LoweredRoleName = $RoleName AND u.ApplicationId = $MembershipApplicationId"
                            + " AND r.ApplicationId = $ApplicationId";

          cmd.Parameters.AddWithValue("$UsernameSearch", usernameToMatch);
          cmd.Parameters.AddWithValue("$RoleName", roleName.ToLowerInvariant());
          cmd.Parameters.AddWithValue("$MembershipApplicationId", MEMBERSHIP_APPLICATION_ID);
          cmd.Parameters.AddWithValue("$ApplicationId", APPLICATION_ID);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          using (SQLiteDataReader dr = cmd.ExecuteReader())
          {
            while (dr.Read())
            {
              tmpUserNames += dr.GetString(0) + ",";
            }
          }
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }

      if (tmpUserNames.Length > 0)
      {
        // Remove trailing comma.
        tmpUserNames = tmpUserNames.Substring(0, tmpUserNames.Length - 1);
        return tmpUserNames.Split(',');
      }

      return new string[0];
    }

    #endregion

    #region Private Methods

    private static string GetApplicationId(string appName)
    {
      SQLiteConnection cn = GetDbConnectionForRole();
      try
      {
        using (SQLiteCommand cmd = cn.CreateCommand())
        {
          cmd.CommandText = "SELECT ApplicationId FROM aspnet_Applications WHERE ApplicationName = $AppName";
          cmd.Parameters.AddWithValue("$AppName", appName);

          if (cn.State == ConnectionState.Closed)
            cn.Open();

          return cmd.ExecuteScalar() as string;
        }
      }
      finally
      {
        if (!IsTransactionInProgress())
          cn.Dispose();
      }
    }

    private void VerifyApplication()
    {
      // Verify a record exists in the application table.
      if (string.IsNullOrEmpty(APPLICATION_ID) || string.IsNullOrEmpty(MEMBERSHIP_APPLICATION_NAME))
      {
        // No record exists in the application table for either the role application and/or the membership application. Create it.
        SQLiteConnection cn = GetDbConnectionForRole();
        try
        {
          using (SQLiteCommand cmd = cn.CreateCommand())
          {
            cmd.CommandText = "INSERT INTO " + AppTableName + " (ApplicationId, ApplicationName, Description) VALUES ($ApplicationId, $ApplicationName, $Description)";

            string roleApplicationId = Guid.NewGuid().ToString();

            cmd.Parameters.AddWithValue("$ApplicationId", roleApplicationId);
            cmd.Parameters.AddWithValue("$ApplicationName", APPLICATION_NAME);
            cmd.Parameters.AddWithValue("$Description", string.Empty);

            if (cn.State == ConnectionState.Closed)
              cn.Open();

            // Insert record for the role application.
            if (string.IsNullOrEmpty(APPLICATION_ID))
            {
              cmd.ExecuteNonQuery();

              APPLICATION_ID = roleApplicationId;
            }

            if (string.IsNullOrEmpty(MEMBERSHIP_APPLICATION_ID))
            {
              if (APPLICATION_NAME == MEMBERSHIP_APPLICATION_NAME)
              {
                // Use the app name for the membership app name.
                MembershipApplicationName = ApplicationName;
              }
              else
              {
                // Need to insert record for the membership application.
                MEMBERSHIP_APPLICATION_ID = Guid.NewGuid().ToString();

                cmd.Parameters["$ApplicationId"].Value = MEMBERSHIP_APPLICATION_ID;
                cmd.Parameters["$ApplicationName"].Value = MEMBERSHIP_APPLICATION_NAME;

                cmd.ExecuteNonQuery();
              }
            }
          }
        }
        finally
        {
          if (!IsTransactionInProgress())
            cn.Dispose();
        }
      }
    }

    /// <summary>
    ///   Get a reference to the database connection used for Role. If a transaction is currently in progress, and the
    ///   connection string of the transaction connection is the same as the connection string for the Role provider,
    ///   then the connection associated with the transaction is returned, and it will already be open. If no transaction is in
    ///   progress,
    ///   a new <see cref="SQLiteConnection" /> is created and returned. It will be closed and must be opened by the caller
    ///   before using.
    /// </summary>
    /// <returns>A <see cref="SQLiteConnection" /> object.</returns>
    /// <remarks>
    ///   The transaction is stored in <see cref="System.Web.HttpContext.Current" />. That means transaction support is limited
    ///   to web applications. For other types of applications, there is no transaction support unless this code is modified.
    /// </remarks>
    private static SQLiteConnection GetDbConnectionForRole()
    {
      // Look in the HTTP context bag for a previously created connection and transaction. Return if found and its connection
      // string matches that of the Role connection string; otherwise return a fresh connection.
      if (HttpContext.Current != null)
      {
        SQLiteTransaction tran = (SQLiteTransaction)HttpContext.Current.Items[HTTPTransactionId];
        if ((tran != null) && (string.Equals(tran.Connection.ConnectionString, CONNECTION_STRING)))
          return tran.Connection;
      }

      return new SQLiteConnection(CONNECTION_STRING, true);
    }

    /// <summary>
    ///   Determines whether a database transaction is in progress for the Role provider.
    /// </summary>
    /// <returns>
    ///   <c>true</c> if a database transaction is in progress; otherwise, <c>false</c>.
    /// </returns>
    /// <remarks>
    ///   A transaction is considered in progress if an instance of <see cref="SQLiteTransaction" /> is found in the
    ///   <see cref="System.Web.HttpContext.Current" /> Items property and its connection string is equal to the Role
    ///   provider's connection string. Note that this implementation of
    ///   <see cref="SQLiteRoleProvider" /> never adds a
    ///   <see cref="SQLiteTransaction" /> to <see cref="System.Web.HttpContext.Current" />, but it is possible that
    ///   another data provider in this application does. This may be because other data is also stored in this SQLite
    ///   database,
    ///   and the application author wants to provide transaction support across the individual providers. If an instance of
    ///   <see cref="System.Web.HttpContext.Current" /> does not exist (for example, if the calling application is not a web
    ///   application),
    ///   this method always returns false.
    /// </remarks>
    private static bool IsTransactionInProgress()
    {
      if (HttpContext.Current == null)
        return false;

      SQLiteTransaction tran = (SQLiteTransaction)HttpContext.Current.Items[HTTPTransactionId];

      if ((tran != null) && (string.Equals(tran.Connection.ConnectionString, CONNECTION_STRING)))
        return true;
      return false;
    }

    #endregion
  }
}