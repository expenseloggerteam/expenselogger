﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Security
{
  /// <summary>
  ///   Generic user profile service interface
  /// </summary>
  public interface IUserRepository : IDisposable
  {
    /// <summary>
    ///   Save a user
    /// </summary>
    /// <param name="user">User to be saved</param>
    void SaveUser(User user);

    /// <summary>
    ///   Get user
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <returns>User if found, else null</returns>
    User GetUser(string username);

    /// <summary>
    /// Get all users
    /// </summary>
    /// <returns>All users in system</returns>
    IEnumerable<User> GetAllUsers();

    /// <summary>
    ///   Validate a user
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <param name="password">Password of user</param>
    /// <returns>True if user is valid, else false</returns>
    bool ValidateUser(string username, string password);
  }
}