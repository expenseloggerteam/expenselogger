﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;

namespace ExpenseLogger.Models.Threading.Tasks
{
  /// <summary>
  ///   A task for <see cref="IExcelFileProcessor{TEntity}" /> to process an <see cref="Expense" /> file
  /// </summary>
  public class ExpensesExcelFileProcessorTask : AbstractTask<string>
  {
    private readonly string m_file;
    private readonly IExcelFileProcessor<Expense> m_processor;
    private readonly IProgress<string> m_progressIndicator;
    private readonly Task<bool> m_task;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="file">File to be processed</param>
    /// <param name="processor">Excel file processor</param>
    /// <param name="progressIndicator">Progress indicator</param>
    public ExpensesExcelFileProcessorTask(string file, IExcelFileProcessor<Expense> processor, IProgress<string> progressIndicator)
    {
      m_file = file;
      m_processor = processor;
      m_progressIndicator = progressIndicator;
      m_task = new Task<bool>(RunTask);
    }

    /// <summary>
    ///   Run the internal task
    /// </summary>
    /// <returns>True if success, else false</returns>
    private bool RunTask()
    {
      ProcessResult<string[][]> rawData = m_processor.SanityCheck(m_file);

      ProcessResult<Expense[]> expenses = m_processor.ProcessData(rawData.Data);

      ProcessResult result = m_processor.UpdateStore(expenses.Data);

      return result.Success;
    }

    /// <inheritdoc />
    public override void Start()
    {
      m_task.Start();
    }

    /// <inheritdoc />
    public override void Stop()
    {
      m_task.Dispose();
    }
  }
}