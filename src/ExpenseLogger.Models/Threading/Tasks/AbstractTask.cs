﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Threading.Tasks
{
  /// <summary>
  ///   An abstract task
  /// </summary>
  /// <typeparam name="TMessageType">Progress message type</typeparam>
  public abstract class AbstractTask<TMessageType> : Disposable, ITask
  {
    private readonly IProgress<TMessageType> m_progressIndicator;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="progressIndicator">[Optional] Progress indicator implementation</param>
    protected AbstractTask(IProgress<TMessageType> progressIndicator = null)
    {
      m_progressIndicator = progressIndicator ?? new NullProgress<TMessageType>();
    }

    #region Implementation of ITask

    /// <inheritdoc />
    public abstract void Start();

    /// <inheritdoc />
    public abstract void Stop();

    #endregion Implementation of ITask
  }
}