﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Security;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using OfficeOpenXml.Style;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   An <see cref="IExcelFileProcessor{TEntity}" /> for Expense data files
  /// </summary>
  public class ExpensesExcelFileProcessor : IExcelFileProcessor<Expense>
  {
    private readonly string m_username;
    private readonly IUserRepository m_userRepo;
    private readonly IDataRepository m_dataRepo;


    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="username">Username for whom to process data</param>
    /// <param name="userRepo">Users repository</param>
    /// <param name="dataRepo">Data repository</param>
    public ExpensesExcelFileProcessor(string username, IUserRepository userRepo, IDataRepository dataRepo)
    {
      m_username = username;
      m_userRepo = userRepo;
      m_dataRepo = dataRepo;
    }

    #region Implementation of IExcelFileProcessor

    /// <summary>
    ///   Returns the Excel file template for the associated user
    /// </summary>
    /// <returns>Excel file template as a memory stream</returns>
    public byte[] GetTemplateFileAsBytes()
    {
      User u = m_userRepo.GetUser(m_username);

      List<Category> userCategories = m_dataRepo.GetCategoriesForUser(m_username).ToList();

      string md5Salt = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(u.Username + "_" + u.MemberSince.ToString(AppConstants.IsoDateTimeFormat))));

      ExcelPackage excelTemplate = new ExcelPackage();
      ExcelWorksheet dataSheet = excelTemplate.Workbook.Worksheets.Add("Expenses");
      dataSheet.DefaultColWidth = 30;
      dataSheet.DefaultRowHeight = 20;
      dataSheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous; // align date column to center
      dataSheet.Column(1).Style.Numberformat.Format = "dd MMM yyyy";

      dataSheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right; // align amount column to right

      // write header row for user data fields
      int rowOffset = 1;
      dataSheet.Cells["A" + rowOffset].Value = "Enter data";
      dataSheet.Cells["A" + rowOffset].Style.Font.Bold = true;
      dataSheet.Cells["A" + rowOffset].Style.Font.UnderLine = true;
      dataSheet.Cells["A" + rowOffset].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

      rowOffset++;
      dataSheet.Cells["A" + rowOffset].Value = "Date";
      dataSheet.Cells["A" + rowOffset].Style.Font.Bold = true;
      dataSheet.Cells["B" + rowOffset].Value = "Category";
      dataSheet.Cells["B" + rowOffset].Style.Font.Bold = true;
      dataSheet.Cells["C" + rowOffset].Value = "Description";
      dataSheet.Cells["C" + rowOffset].Style.Font.Bold = true;
      dataSheet.Cells["D" + rowOffset].Value = "Amount";
      dataSheet.Cells["D" + rowOffset].Style.Font.Bold = true;
      dataSheet.Cells["E" + rowOffset].Value = "Payment type";
      dataSheet.Cells["E" + rowOffset].Style.Font.Bold = true;

      const int rowLimit = 1000;
      rowOffset++;

      // data validation for expense date column
      var dateValidation = dataSheet.DataValidations.AddDateTimeValidation("A" + rowOffset + ":A" + (rowOffset + rowLimit));
      dateValidation.ShowErrorMessage = true;
      dateValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
      dateValidation.ErrorTitle = "Invalid value entered";
      dateValidation.Error = "Value must be a date and greater than 31 Dec 1999";
      dateValidation.Formula.Value = new DateTime(1999, 12, 31, 0, 0, 0, DateTimeKind.Utc);
      dateValidation.ShowInputMessage = true;
      dateValidation.AllowBlank = false;
      dateValidation.PromptTitle = "Enter date of expense";

      // data validation for category name column
      var categoryNameValidation = dataSheet.DataValidations.AddListValidation("B" + rowOffset + ":B" + (rowOffset + rowLimit));
      categoryNameValidation.ShowErrorMessage = true;
      categoryNameValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
      categoryNameValidation.ErrorTitle = "Invalid value entered";
      categoryNameValidation.Error = "You must select one of the given options";
      userCategories.ForEach(c => categoryNameValidation.Formula.Values.Add(c.Name));
      categoryNameValidation.ShowInputMessage = true;
      categoryNameValidation.AllowBlank = false;
      categoryNameValidation.PromptTitle = "Select expense category";

      // data validation for amount column
      var amountValidation = dataSheet.DataValidations.AddDecimalValidation("D" + rowOffset + ":D" + (rowOffset + rowLimit));
      amountValidation.ShowErrorMessage = true;
      amountValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
      amountValidation.ErrorTitle = "Invalid value entered";
      amountValidation.Error = "Value must be greater than 0.01";
      amountValidation.Formula.Value = 0.01;
      amountValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
      amountValidation.ShowInputMessage = true;
      amountValidation.AllowBlank = false;
      amountValidation.PromptTitle = "Enter expense amount";

      // data validation for payment type column
      var paymentTypeValidation = dataSheet.DataValidations.AddListValidation("E" + rowOffset + ":E" + (rowOffset + rowLimit));
      paymentTypeValidation.ShowErrorMessage = true;
      paymentTypeValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
      paymentTypeValidation.ErrorTitle = "Invalid value entered";
      paymentTypeValidation.Error = "You must select one of the given options";
      Array.ForEach(Enum.GetNames(typeof(EPayment)), t => paymentTypeValidation.Formula.Values.Add(t));
      paymentTypeValidation.ShowInputMessage = true;
      paymentTypeValidation.AllowBlank = false;
      paymentTypeValidation.PromptTitle = "Select payment type";

      excelTemplate.Compression = CompressionLevel.BestCompression;
      excelTemplate.Workbook.Properties.Author = "ExpenseLogger";
      excelTemplate.Workbook.Properties.Comments = md5Salt;
      excelTemplate.Workbook.Properties.Title = "External data import template for user: " + m_username;

      MemoryStream ms = new MemoryStream();
      excelTemplate.SaveAs(ms);

      return ms.ToArray();
    }

    /// <inheritdoc />
    public ProcessResult<string[][]> SanityCheck(string file)
    {
      ProcessResult<string[][]> result;

      try
      {
        User u = m_userRepo.GetUser(m_username);

        Exception invalidDataException = new InvalidDataException("Data file is not an ExpenseLogger data template for: " + u.Username);

        string md5Salt = BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(u.Username + "_" + u.MemberSince.ToString(AppConstants.IsoDateTimeFormat))));
        string expectedMeta = md5Salt + ", External data import template for user: " + u.Username;

        ExcelPackage pkg = new ExcelPackage(new FileInfo(file));
        string actualMeta = pkg.Workbook.Properties.Comments
                            + ", " + pkg.Workbook.Properties.Title;

        // check whether the file is legit i.e a template file generated by ExpenseLogger app
        if (expectedMeta != actualMeta)
          throw invalidDataException;

        // 2 header rows + 1 = first data row
        int rowOffset = 3;

        List<string[]> data = new List<string[]>();

        ExcelWorksheet sheet = pkg.Workbook.Worksheets.FirstOrDefault();
        if (sheet == null)
          throw invalidDataException;

        while (true)
        {
          // read the rows
          string firstValue = sheet.Cells["a" + rowOffset].GetValue<string>();
          if (string.IsNullOrWhiteSpace(firstValue))
            break;

          string category = sheet.Cells["b" + rowOffset].GetValue<string>();
          string description = sheet.Cells["c" + rowOffset].GetValue<string>();
          string amount = sheet.Cells["d" + rowOffset].GetValue<string>();
          string paymentType = sheet.Cells["e" + rowOffset].GetValue<string>();

          data.Add(new[] { firstValue, category, description, amount, paymentType });
          rowOffset++;
        }

        pkg.Dispose();

        result = new ProcessResult<string[][]>(data: data.ToArray());

        // clean up the uploaded file from cache
        File.Delete(file);
      }
      catch (Exception e)
      {
        result = new ProcessResult<string[][]>(ECode.Error, e);
      }

      return result;
    }

    /// <inheritdoc />
    public ProcessResult<Expense[]> ProcessData(string[][] rawData)
    {
      ProcessResult<Expense[]> result;
      DateTime minDate = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
      const int dataProcessLimitRows = 1000;

      try
      {
        // get user and all his all categories
        User u = m_userRepo.GetUser(m_username);
        List<Category> categories = m_dataRepo.GetCategoriesForUser(m_username).ToList();
        List<Expense> expenses = new List<Expense>();
        
        for (int i = 0; i < System.Math.Min(rawData.Length, dataProcessLimitRows); i++)
        {
          string[] row = rawData[i];
          int rowNum = i + 1;

          // parse expense date
          double dblDate;
          bool parseOk = double.TryParse(row[0], out dblDate);
          if (!parseOk)
            throw new InvalidDataException("Data row: " + rowNum + ", Unable to parse expense date");

          DateTime date = DateTime.FromOADate(dblDate);
          if (date < minDate)
            throw new InvalidDataException("Data row: " + rowNum + ", Expense date must be greater than 2000-01-01");

          // parse expense category
          // comparing user id as well handles the condition where uploaded data references categories not owned by the user
          Category category = categories.FirstOrDefault(c => c.Name.Equals(row[1], StringComparison.OrdinalIgnoreCase) && (c.UserId == u.Id || c.User.Username == AppConstants.AdminUsername));
          if (category == null)
            throw new InvalidDataException("Data row: " + rowNum + ", Unknown category with name: " + row[1] + ". Either the category doesn't exist or you don't have access to it");

          string description = row[2];

          double amount;
          parseOk = double.TryParse(row[3], out amount);
          if (!parseOk)
            throw new InvalidDataException("Data row: " + rowNum + ", " + row[3] + " is not a valid amount");
          if (amount < 0.01)
            throw new InvalidDataException("Data row: " + rowNum + ", Expense amount must be greater than or equal to 0.01");

          EPayment paymentType = (EPayment)Enum.Parse(typeof(EPayment), row[4], true);

          Expense e = new Expense
          {
            Date = date,
            CategoryId = category.Id,
            Description = description,
            Amount = amount,
            UserId = u.Id,
            PaymentType = paymentType
          };

          expenses.Add(e);
        }

        result = new ProcessResult<Expense[]>(data: expenses.ToArray());
      }
      catch (Exception e)
      {
        result = new ProcessResult<Expense[]>(ECode.Error, e);
      }

      return result;
    }

    /// <inheritdoc />
    public ProcessResult UpdateStore(IEnumerable<Expense> entities)
    {
      ProcessResult result = new ProcessResult();

      try
      {
        m_dataRepo.Save(entities.Cast<AbstractEntity>().ToList());
      }
      catch (Exception e)
      {
        result = new ProcessResult(ECode.Error, e);
      }

      return result;
    }

    #endregion Implementation of IExcelFileProcessor
  }
}