﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Data.Entity;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Data;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A <see cref="DbContext" /> for MySql database
  /// </summary>
  [DbConfigurationType(typeof(DatabaseConfiguration))]
  public class MySqlDataContext : AbstractDataContext
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="connString">Database connection string</param>
    public MySqlDataContext(string connString)
      : base(DatabaseConnectionFactory.CreateConnection(EPersistenceType.MySql, connString))
    {
      // nothing to do here
    }

    /// <inheritdoc />
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      if (AppConstants.DemoFlag)
        Database.SetInitializer(new DropCreateDatabaseAlways<MySqlDataContext>());
      else
        Database.SetInitializer(new CreateDatabaseIfNotExists<MySqlDataContext>());
    }

    #region Overrides of AbstractDataContext

    /// <inheritdoc />
    protected override EPersistenceType Type
    {
      get { return EPersistenceType.MySql; }
    }

    /// <inheritdoc />
    public override IDictionary<EProviderDetailKey, string> GetProviderSummary()
    {
      IDictionary<EProviderDetailKey, string> dict = base.GetProviderSummary();

      return dict;
    }

    #endregion Overrides of AbstractDataContext
  }
}