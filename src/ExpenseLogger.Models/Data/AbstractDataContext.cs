﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using log4net;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A default <see cref="DbContext" />
  /// </summary>
  public abstract class AbstractDataContext : DbContext, IDataContext
  {
    /// <summary>
    ///   log4net logger
    /// </summary>
    protected static ILog Logger = LogManager.GetLogger(typeof(AbstractDataContext));

    /// <summary>
    ///   Persistence type of the underlying data context
    /// </summary>
    protected abstract EPersistenceType Type { get; }

    /// <summary>
    ///   Connection string to the underlying data source
    /// </summary>
    public string ConnString { get; private set; }

    /// <summary>
    /// Flag indicating whether this data context has already been disposed
    /// </summary>
    public bool IsDisposed { get; private set; }

    /// <inheritdoc />
    public virtual IDictionary<EProviderDetailKey, string> GetProviderSummary()
    {
      Dictionary<EProviderDetailKey, string> dict = new Dictionary<EProviderDetailKey, string>();

      dict[EProviderDetailKey.ProviderName] = Type.ToString();

      if (Database.Connection.State != ConnectionState.Open)
      {
        Database.Connection.Close();
        Database.Connection.Open();

        dict[EProviderDetailKey.Version] = Database.Connection.ServerVersion;
        dict[EProviderDetailKey.DataSource] = string.IsNullOrWhiteSpace(Database.Connection.Database) ? Database.Connection.DataSource : Database.Connection.Database;

        Database.Connection.Close();
      }

      return dict;
    }

    /// <summary>
    ///   <see cref="Expense" /> table
    /// </summary>
    public IDbSet<Expense> Expenses { get; set; }

    /// <summary>
    ///   <see cref="Category" /> table
    /// </summary>
    public IDbSet<Category> Categories { get; set; }

    /// <summary>
    ///   <see cref="User" /> table
    /// </summary>
    public IDbSet<User> Users { get; set; }

    /// <summary>
    ///   <see cref="Reminder" /> table
    /// </summary>
    public IDbSet<Reminder> Reminders { get; set; }

    /// <summary>
    ///   <see cref="RecurringExpense" /> table
    /// </summary>
    public IDbSet<RecurringExpense> RecurringExpenses { get; set; }

    /// <summary>
    ///   <see cref="ExpenseSummary" /> table
    /// </summary>
    public IDbSet<ExpenseSummary> ExpenseSummaries { get; set; }

    /// <summary>
    ///   <see cref="SystemSettings" /> table
    /// </summary>
    public IDbSet<SystemSetting> SystemSettings { get; set; }

    /// <summary>
    ///   An event to hook into just after the <see cref="OnModelCreating(DbModelBuilder)" /> starts but before
    ///   any code is actually run.
    /// </summary>
    public event EventHandler OnModelCreatingStarting;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="connection">Database connection</param>
    protected AbstractDataContext(DbConnection connection)
      : base(connection, true)
    {
      ConnString = connection.ConnectionString;
    }

    /// <summary>
    ///   Fire the message processed event
    ///   http://stackoverflow.com/questions/4742214/c-sharp-accessing-parent-classs-events-not-possible
    /// </summary>
    protected void FireOnModelCreatingStartingEvent()
    {
      if (OnModelCreatingStarting != null)
        OnModelCreatingStarting(this, new EventArgs());
    }

    /// <inheritdoc />
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      FireOnModelCreatingStartingEvent();

      modelBuilder.Entity<Expense>()
        .HasRequired(f => f.User)
        .WithMany()
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<Expense>()
        .HasRequired(f => f.Category)
        .WithMany()
        .WillCascadeOnDelete(false);
    }

    /// <summary>
    ///   Seed data to the database
    /// </summary>
    public virtual void SeedData()
    {
      StackTrace trace = new StackTrace();
      StackFrame[] stackFrames = trace.GetFrames();

      if (stackFrames != null)
        if (stackFrames.Any(frame => frame.GetMethod().Name == "OnModelCreating"))
          throw new InvalidOperationException("You can not call SeedData() method from the OnModelCreating() method.");

      Database.Initialize(false);
      User u = this.CreateAdminUser();

      if (AppConstants.DemoFlag && AppConstants.SeedDataFlag)
        this.InsertSampleData(u);
      else if (!AppConstants.DemoFlag && AppConstants.SeedDataFlag)
        this.CreateDefaultCategories(u);

      SaveChanges();
    }

    /// <summary>
    ///   Save changes to the given entity to the underlying data store
    /// </summary>
    /// <typeparam name="TEntity">Type of entity to be saved. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <param name="entity">Entity to be saved</param>
    /// <returns>Number of changes saved</returns>
    public int SaveChanges<TEntity>(TEntity entity) where TEntity : AbstractEntity
    {
      Entry(entity).State = entity.Id == 0 ? EntityState.Added : EntityState.Modified;
      return SaveChanges();
    }

    /// <inheritdoc />
    public int SaveChanges<TEntity>(ICollection<TEntity> entities) where TEntity : AbstractEntity
    {
      if (entities == null)
        throw new ArgumentNullException(nameof(entities));

      foreach(TEntity entity in entities)
        Entry(entity).State = entity.Id == 0 ? EntityState.Added : EntityState.Modified;

      return SaveChanges();
    }

    /// <summary>
    ///   Loads a reference navigation property for the given entity
    /// </summary>
    /// <typeparam name="TEntity">Type of entity. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <typeparam name="TProperty">Type of the navigation property</typeparam>
    /// <param name="entity">Entity to update</param>
    /// <param name="navigationProperty">Navigation property</param>
    public void LoadNavigationProperty<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> navigationProperty) where TEntity : AbstractEntity where TProperty : class
    {
      Entry(entity).Reference(navigationProperty).Load();
    }

    /// <inheritdoc />
    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);

      IsDisposed = true;
    }
  }
}