﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A generic interface for an ExpenseLogger data repository
  /// </summary>
  public interface IDataRepository : IDisposable
  {
    /// <summary>
    ///   Gets the summary details of the underlying persistence provider
    /// </summary>
    /// <returns>Persistence provider summary data</returns>
    IDictionary<EProviderDetailKey, string> GetProviderSummary();

    /// <summary>
    ///   Get expense summaries for current month for given user
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <returns>Expense summaries</returns>
    ExpenseSummary GetExpenseSummary(string username);

    /// <summary>
    ///   Get all expenses for given user
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <param name="fnFilter">
    ///   [Optional] Any filters to be applied. Using this is better since the filters then get applied at
    ///   a lower level (i.e at database level rather than code level)
    /// </param>
    /// <returns>User expenses</returns>
    IEnumerable<Expense> GetExpensesForUser(string username, Func<IEnumerable<Expense>, IEnumerable<Expense>> fnFilter = null);

    /// <summary>
    ///   Gets an expense trend for given user and month
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <param name="monthAndYear">Month and year for which to generate a trend</param>
    /// <returns>Trended values till end of the month</returns>
    IEnumerable<double> GetExpenseTrendForUser(string username, DateTime monthAndYear);

    /// <summary>
    ///   Get all categories available to the user. This includes categories created by system administrator and categories
    ///   created by the user
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <returns>User categories</returns>
    IEnumerable<Category> GetCategoriesForUser(string username);

    /// <summary>
    ///   Get reminders for user
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <param name="fnFilter">
    ///   [Optional] Any filters to be applied. Using this is better since the filters then get applied at
    ///   a lower level (i.e at database level rather than code level)
    /// </param>
    /// <returns>User reminders</returns>
    IEnumerable<Reminder> GetRemindersForUser(string username, Func<IEnumerable<Reminder>, IEnumerable<Reminder>> fnFilter = null);

    /// <summary>
    ///   Save or update an existing entity in the repository
    /// </summary>
    /// <param name="entity">Entity to be saved/updated</param>
    void Save(AbstractEntity entity);

    /// <summary>
    ///   Save or update a collection of entities in the repository
    /// </summary>
    /// <param name="entities">Entities to be saved/updated</param>
    void Save(ICollection<AbstractEntity> entities);

    /// <summary>
    ///   Get an entity from the repository
    /// </summary>
    /// <typeparam name="T">Entity type. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <param name="id">Entity id</param>
    /// <returns>Entity if found, else null</returns>
    T GetById<T>(int id) where T : AbstractEntity;

    /// <summary>
    ///   Get entities by username from the repository
    /// </summary>
    /// <typeparam name="T">Entity type. This must be an <see cref="IUserReference" /></typeparam>
    /// <param name="username">Username for which to get data</param>
    /// <param name="fnFilter">[Optional] Any filters to be applied on the data retrieved</param>
    /// <returns>Entity data if found, else emtpy collection</returns>
    IEnumerable<T> GetByUser<T>(string username, Func<IEnumerable<T>, IEnumerable<T>> fnFilter = null) where T : class, IUserReference;

    /// <summary>
    ///   Delete an entity from the repository
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    /// <param name="entity">Entity</param>
    void Delete<T>(T entity) where T : AbstractEntity;

    /// <summary>
    ///   Get setting from repository
    /// </summary>
    /// <typeparam name="T">Type of setting</typeparam>
    /// <param name="setting">Setting to be retrieved</param>
    /// <returns>Setting value</returns>
    T GetSetting<T>(ESystemSetting setting);

    /// <summary>
    ///   Save setting to repository
    /// </summary>
    /// <param name="setting">Setting to be saved</param>
    void SaveSetting(SystemSetting setting);
  }
}