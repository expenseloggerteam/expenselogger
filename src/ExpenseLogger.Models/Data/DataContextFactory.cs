﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Core;
using ExpenseLogger.Models.Conf;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   The default data context factory
  /// </summary>
  public sealed class DataContextFactory : IDataContextFactory
  {
    private static AbstractDataContext CONTEXT;
    private readonly ConfigProperty m_conf;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="conf">Configuration properties</param>
    public DataContextFactory(ConfigProperty conf)
    {
      m_conf = conf;
    }

    /// <summary>
    ///   Get a data context instance
    /// </summary>
    public IDataContext GetInstance()
    {
      // handle special case of InMemory db type (used for unit testing)
      if (m_conf.DbType == EPersistenceType.InMemory)
        return new InMemoryDataContext();

      if (CONTEXT != null && !CONTEXT.IsDisposed)
        return CONTEXT;

      switch (m_conf.DbType)
      {
        case EPersistenceType.SQLite:
          CONTEXT = new SQLiteDataContext(m_conf.ConnectionString);
          break;
      }

      return CONTEXT;
    }
  }
}