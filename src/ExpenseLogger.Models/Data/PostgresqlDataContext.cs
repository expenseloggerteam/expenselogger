﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Data.Entity;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Data;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A <see cref="DbContext" /> for Postgresql database
  /// </summary>
  [DbConfigurationType(typeof(DatabaseConfiguration))]
  public class PostgresqlDataContext : AbstractDataContext
  {
    /// <inheritdoc />
    /// <param name="connString">Database connection string</param>
    public PostgresqlDataContext(string connString)
      : base(DatabaseConnectionFactory.CreateConnection(EPersistenceType.Postgresql, connString))
    {
      // nothing to do here
    }

    /// <inheritdoc />
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      modelBuilder.HasDefaultSchema("public");
       
      if (AppConstants.DemoFlag)
        Database.SetInitializer(new DropCreateDatabaseAlways<PostgresqlDataContext>());
      else
        Database.SetInitializer(new CreateDatabaseIfNotExists<PostgresqlDataContext>());
    }

    #region Overrides of AbstractDataContext

    /// <inheritdoc />
    protected override EPersistenceType Type
    {
      get { return EPersistenceType.Postgresql; }
    }

    /// <inheritdoc />
    public override IDictionary<EProviderDetailKey, string> GetProviderSummary()
    {
      IDictionary<EProviderDetailKey, string> dict = base.GetProviderSummary();

      return dict;
    }

    #endregion Overrides of AbstractDataContext
  }
}