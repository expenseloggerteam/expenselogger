﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A fake <see cref="DbContext" />. Note: This facilitates unit testing.
  /// </summary>
  public class InMemoryDataContext : Disposable, IDataContext
  {
    private int m_changeCounter;

    /// <summary>
    ///   Constructor
    /// </summary>
    public InMemoryDataContext()
    {
      m_changeCounter = 0;
      ConnString = "fake-context";

      var users = new InMemoryDbSet<User>();
      users.OnSetChanged += OnSetChangedHandler;
      Users = users;

      var categories = new InMemoryDbSet<Category>();
      categories.OnSetChanged += OnSetChangedHandler;
      Categories = categories;

      var expenses = new InMemoryDbSet<Expense>();
      expenses.OnSetChanged += OnSetChangedHandler;
      Expenses = expenses;

      var expenseSummaries = new InMemoryDbSet<ExpenseSummary>();
      expenseSummaries.OnSetChanged += OnSetChangedHandler;
      ExpenseSummaries = expenseSummaries;
    }

    /// <summary>
    ///   The handler for <see cref="InMemoryDbSet{TEntity}.OnSetChanged" /> event
    /// </summary>
    /// <param name="sender">The <see cref="InMemoryDbSet{TEntity}" /> that generated this event</param>
    /// <param name="e">Event arguments</param>
    private void OnSetChangedHandler(object sender, EventArgs e)
    {
      m_changeCounter++;
    }

    #region Implementation of IDataContext

    /// <summary>
    ///   Connection string to the underlying data source
    /// </summary>
    public string ConnString { get; private set; }

    /// <summary>
    ///   <see cref="Expense" /> table
    /// </summary>
    public IDbSet<Expense> Expenses { get; set; }

    /// <summary>
    ///   <see cref="Category" /> table
    /// </summary>
    public IDbSet<Category> Categories { get; set; }

    /// <summary>
    ///   <see cref="User" /> table
    /// </summary>
    public IDbSet<User> Users { get; set; }

    /// <summary>
    ///   <see cref="Reminder" /> table
    /// </summary>
    public IDbSet<Reminder> Reminders { get; set; }

    /// <summary>
    ///   <see cref="RecurringExpense" /> table
    /// </summary>
    public IDbSet<RecurringExpense> RecurringExpenses { get; set; }

    /// <summary>
    ///   <see cref="ExpenseSummary" /> table
    /// </summary>
    public IDbSet<ExpenseSummary> ExpenseSummaries { get; set; }

    /// <inheritdoc />
    public IDbSet<SystemSetting> SystemSettings { get; set; }

    /// <inheritdoc />
    public IDictionary<EProviderDetailKey, string> GetProviderSummary()
    {
      return new Dictionary<EProviderDetailKey, string>
      {
        {EProviderDetailKey.ProviderName, "InMemory"},
        {EProviderDetailKey.Version, "1"},
        {EProviderDetailKey.LibraryVersion, Assembly.GetExecutingAssembly().GetName().Version.ToString()}
      };
    }

    /// <summary>
    ///   Seed data to the database
    /// </summary>
    public void SeedData()
    {
      // nothing to do here
    }

    /// <summary>
    ///   Save changes to the underlying data store
    /// </summary>
    /// <returns>Number of changes saved</returns>
    public int SaveChanges()
    {
      // this is pretty much a dummy method so that we can tap into DbContext.SaveChanges()

      int val = m_changeCounter;
      m_changeCounter = 0;

      return val;
    }

    /// <summary>
    ///   Save changes to the given entity to the underlying data store
    /// </summary>
    /// <typeparam name="TEntity">Type of entity to be saved. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <param name="entity">Entity to be saved</param>
    /// <returns>Number of changes saved</returns>
    public int SaveChanges<TEntity>(TEntity entity) where TEntity : AbstractEntity
    {
      var collection = GetCollection<TEntity>();
      collection.Add(entity);

      return SaveChanges();
    }

    /// <inheritdoc />
    public int SaveChanges<TEntity>(ICollection<TEntity> entities) where TEntity : AbstractEntity
    {
      if (entities == null)
        throw new ArgumentNullException(nameof(entities));

      foreach (TEntity entity in entities)
        SaveChanges(entity);

      return SaveChanges();
    }

    /// <summary>
    ///   Loads a reference navigation property for the given entity
    /// </summary>
    /// <typeparam name="TEntity">Type of entity. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <typeparam name="TProperty">Type of the navigation property</typeparam>
    /// <param name="entity">Entity to update</param>
    /// <param name="navigationProperty">Navigation property</param>
    public void LoadNavigationProperty<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> navigationProperty) where TEntity : AbstractEntity where TProperty : class
    {
      Type entityType = typeof(TEntity);

      PropertyInfo[] props = entityType.GetProperties();

      PropertyInfo navProp = props.FirstOrDefault(f => f.Name == ((MemberExpression)navigationProperty.Body).Member.Name);

      ForeignKeyAttribute fKey = navProp.GetCustomAttribute<ForeignKeyAttribute>();

      PropertyInfo refProp = props.FirstOrDefault(f => f.Name == fKey.Name);

      IDbSet<AbstractEntity> navPropCollection = GetCollection(navProp.ReflectedType);

      int val = (int)refProp.GetValue(entity);

      object navPropVal = navPropCollection.FirstOrDefault(f => f.Id == val);

      navProp.SetValue(entity, navPropVal);
    }

    #endregion Implementation of IDataContext

    /// <summary>
    ///   Get local collection of entities based on it's type
    /// </summary>
    /// <typeparam name="TEntity">Entity type. This must be a <see cref="AbstractEntity" /></typeparam>
    /// <returns>Local collection to use/lookup</returns>
    private IDbSet<TEntity> GetCollection<TEntity>() where TEntity : AbstractEntity
    {
      object result = null;

      if (typeof(TEntity) == typeof(User))
        result = Users;
      else if (typeof(TEntity) == typeof(Category))
        result = Categories;
      else if (typeof(TEntity) == typeof(Expense))
        result = Expenses;
      else if (typeof(TEntity) == typeof(ExpenseSummary))
        result = ExpenseSummaries;

      return (IDbSet<TEntity>)result;
    }

    /// <summary>
    ///   Get local collection of entities based on it's type
    /// </summary>
    /// <param name="type">Entity type. This must be a <see cref="AbstractEntity" /></param>
    /// <returns>Local collection to use/lookup</returns>
    private IDbSet<AbstractEntity> GetCollection(Type type)
    {
      object result = null;

      if (type == typeof(User))
        result = Users;
      else if (type == typeof(Category))
        result = Categories;
      else if (type == typeof(Expense))
        result = Expenses;
      else if (type == typeof(ExpenseSummary))
        result = ExpenseSummaries;

      return (IDbSet<AbstractEntity>)result;
    }
  }
}