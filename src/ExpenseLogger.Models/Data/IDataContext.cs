﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Expressions;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A generic data context interface
  /// </summary>
  public interface IDataContext : IDisposable
  {
    /// <summary>
    ///   Connection string to the underlying data source
    /// </summary>
    string ConnString { get; }

    /// <summary>
    ///   <see cref="Expense" /> table
    /// </summary>
    IDbSet<Expense> Expenses { get; set; }

    /// <summary>
    ///   <see cref="Category" /> table
    /// </summary>
    IDbSet<Category> Categories { get; set; }

    /// <summary>
    ///   <see cref="User" /> table
    /// </summary>
    IDbSet<User> Users { get; set; }

    /// <summary>
    ///   <see cref="Reminder" /> table
    /// </summary>
    IDbSet<Reminder> Reminders { get; set; }

    /// <summary>
    ///   <see cref="RecurringExpense" /> table
    /// </summary>
    IDbSet<RecurringExpense> RecurringExpenses { get; set; }

    /// <summary>
    ///   <see cref="ExpenseSummary" /> table
    /// </summary>
    IDbSet<ExpenseSummary> ExpenseSummaries { get; set; }

    /// <summary>
    ///   <see cref="SystemSettings" /> table
    /// </summary>
    IDbSet<SystemSetting> SystemSettings { get; set; }

    /// <inheritdoc />
    IDictionary<EProviderDetailKey, string> GetProviderSummary();

    /// <summary>
    ///   Seed data to the database
    /// </summary>
    void SeedData();

    /// <summary>
    ///   Save changes to the underlying data store
    /// </summary>
    /// <returns>Number of changes saved</returns>
    int SaveChanges();

    /// <summary>
    ///   Save changes to the given entity to the underlying data store
    /// </summary>
    /// <typeparam name="TEntity">Type of entity to be saved. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <param name="entity">Entity to be saved</param>
    /// <returns>Number of changes saved</returns>
    int SaveChanges<TEntity>(TEntity entity) where TEntity : AbstractEntity;

    /// <summary>
    ///   Save changes to the given entity to the underlying data store
    /// </summary>
    /// <typeparam name="TEntity">Type of entity to be saved. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <param name="entities">Entities to be saved</param>
    /// <returns>Number of changes saved</returns>
    int SaveChanges<TEntity>(ICollection<TEntity> entities) where TEntity : AbstractEntity;

    /// <summary>
    ///   Loads a reference navigation property for the given entity
    /// </summary>
    /// <typeparam name="TEntity">Type of entity. This must be an <see cref="AbstractEntity" /></typeparam>
    /// <typeparam name="TProperty">Type of the navigation property</typeparam>
    /// <param name="entity">Entity to update</param>
    /// <param name="navigationProperty">Navigation property</param>
    void LoadNavigationProperty<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> navigationProperty) where TEntity : AbstractEntity where TProperty : class;
  }
}