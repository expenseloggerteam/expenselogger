﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Mail;
using Newtonsoft.Json;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A data seed class to generate seed data for database
  /// </summary>
  public static class AbstractDataContextExtensions
  {
    /// <summary>
    /// Gets the default categories
    /// </summary>
    /// <param name="user">Owner of the categories</param>
    /// <returns>A collection of <see cref="Category"/></returns>
    private static List<Category> GetDefaultCategories(User user)
    {
      List<Category> categories = new List<Category>
      {
        new Category {UserId = user.Id, Name = "Food and Dining"},
        new Category {UserId = user.Id, Name = "Rent / Mortgage"},
        new Category {UserId = user.Id, Name = "Gym"},
        new Category {UserId = user.Id, Name = "Grocery"},
        new Category {UserId = user.Id, Name = "Money transfer"},
        new Category {UserId = user.Id, Name = "Personal vehicle"},
        new Category {UserId = user.Id, Name = "Public transport"},
        new Category {UserId = user.Id, Name = "Other"},
        new Category {UserId = user.Id, Name = "Shopping"},
        new Category {UserId = user.Id, Name = "Entertainment"},
        new Category {UserId = user.Id, Name = "Bills"},
        new Category {UserId = user.Id, Name = "Medicine"}
      };

      return categories;
    }

    /// <summary>
    ///   Creates the admin user
    /// </summary>
    /// <param name="context">Database data context</param>
    /// <returns>Created admin user</returns>
    public static User CreateAdminUser(this AbstractDataContext context)
    {
      User admin = context.Users.FirstOrDefault(u => u.Username == AppConstants.AdminUsername);

      if (admin != null)
        return admin;

      admin = new User
      {
        Username = AppConstants.AdminUsername,
        PasswordHash = MD5Helper.CreateHash("admin@123"),
        FullName = "System Administrator",
        Email = "admin@example.com",
        Country = "India",
        SecretQuestion = "no question",
        SecretAnswer = MD5Helper.CreateHash("no answer"),
        MemberSince = AppConstants.MinDate,
        LastLoginDate = DateTime.Now,
        SettingsJson = JsonConvert.SerializeObject(new { Currency = ECurrency.Rupees.ToString() }),
        IsAdmin = true
      };

      context.Users.Add(admin);

      return admin;
    }

    /// <summary>
    ///   Create default categories using the admin user
    /// </summary>
    /// <param name="context">Database data context</param>
    /// <param name="user">
    ///   User for which to insert default categories. Important: This must be the 'admin' user. See also
    ///   <see cref="M:AppConstants.AdminUsername" />
    /// </param>
    public static void CreateDefaultCategories(this AbstractDataContext context, User user)
    {
      if (user.Username != AppConstants.AdminUsername || !user.IsAdmin)
        throw new InvalidOperationException(
          "Default categories can only be seeded by the system administrator user. Use " +
          "AppConstants.AdminUsername property to set this user's username, set the IsAdmin " +
          "flag to true and try again");

      List<Category> categories = GetDefaultCategories(user);

      categories.ForEach(c =>
      {
        Category existing = context.Categories.FirstOrDefault(f => f.Name == c.Name);

        if (existing == null)
          context.Categories.Add(c);
      });
    }

    /// <summary>
    ///   Inserts sample data. Creates new categories and inserts random expenses for these categories. Note: This method
    ///   inserts all data and does not perform any check whether the data already exists. You have been warned!!!
    /// </summary>
    /// <param name="context">Database data context</param>
    /// <param name="user">User for which to insert sample data</param>
    public static void InsertSampleData(this AbstractDataContext context, User user)
    {
      var categories = GetDefaultCategories(user);
      categories.ForEach(c => context.Categories.Add(c));

      Random rand = new Random(DateTime.Now.Millisecond);

      // add expenses
      List<Expense> expenses = Enumerable.Range(0, 180)
        .Select(f => new Expense
        {
          CategoryId = rand.Next(1, categories.Count),
          UserId = user.Id,
          Date = DateTime.Now.Date.AddDays(-f),
          Amount = rand.Next(1, 100) / 10.0,
          Description = "fake expense " + f,
          PaymentType = (EPayment)rand.Next(1, 5)
        })
        .ToList();

      expenses.ForEach(f => context.Expenses.Add(f));

      // generate expense summary
      double monthActual = expenses.Where(e => e.Date.Month == DateTime.Now.Month).Sum(s => s.Amount);
      double monthProjected = expenses.Average(e => e.Amount) * DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

      double yearActual = expenses.Where(e => e.Date.Year == DateTime.Now.Year).Sum(s => s.Amount);
      double yearProjected = expenses.Average(e => e.Amount) * 365;

      ExpenseSummary summary = new ExpenseSummary
      {
        UserId = user.Id,
        MonthAndYear = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Local),
        MonthActual = monthActual,
        MonthProjected = monthProjected,
        YearActual = yearActual,
        YearProjected = yearProjected,
        MonthBudget = 1800
      };

      context.ExpenseSummaries.Add(summary);

      // add reminders
      List<Reminder> reminders = new List<Reminder> {
        new Reminder {UserId = user.Id, Date=DateTime.Now.Date.AddDays(-1), Description="fake reminder " + Guid.NewGuid().ToString(), SendEmail=false  },
        new Reminder {UserId = user.Id, Date=DateTime.Now.Date, Description="fake reminder " + Guid.NewGuid().ToString(), SendEmail=false  },
        new Reminder {UserId = user.Id, Date=DateTime.Now.Date.AddDays(1), Description="fake reminder " + Guid.NewGuid().ToString(), SendEmail=true  }
      };

      reminders.ForEach(r => context.Reminders.Add(r));

      // creating sample email settings
      EmailSettings emailSettings = new EmailSettings
      {
        Host = "smtp.example.com",
        Port = 21,
        Username = "test",
        Password = "test",
        UseSsl = true,
        SystemEmail = "admin@example.com",
        WebmasterName = "ExpenseLogger Admin"
      };

      context.SystemSettings.Add(new SystemSetting
      {
        Key = ESystemSetting.Smtp,
        RawValue = JsonConvert.SerializeObject(emailSettings)
      });
    }
  }
}