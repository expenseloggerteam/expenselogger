﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SQLite;
using System.Reflection;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Data;
using SQLite.CodeFirst;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A <see cref="DbContext" /> for SQLite database
  /// </summary>
  [DbConfigurationType(typeof(DatabaseConfiguration))]
  public class SQLiteDataContext : AbstractDataContext
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="connString">Connection string</param>
    public SQLiteDataContext(string connString)
      : base(DatabaseConnectionFactory.CreateConnection(EPersistenceType.SQLite, connString))
    {
      // nothing to do here
    }

    /// <inheritdoc />
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      IDatabaseInitializer<SQLiteDataContext> initializer = new SqliteCreateDatabaseIfNotExists<SQLiteDataContext>(modelBuilder);

      if (AppConstants.DemoFlag)
        initializer = new SqliteDropCreateDatabaseAlways<SQLiteDataContext>(modelBuilder);

      Database.SetInitializer(initializer);
    }

    #region Overrides of AbstractDataContext

    /// <inheritdoc />
    protected override EPersistenceType Type
    {
      get { return EPersistenceType.SQLite; }
    }

    /// <inheritdoc />
    public override IDictionary<EProviderDetailKey, string> GetProviderSummary()
    {
      IDictionary<EProviderDetailKey, string> dict = base.GetProviderSummary();

      Assembly a = Assembly.GetAssembly(typeof(SQLiteFactory));
      Version v = a.GetName().Version;

      dict[EProviderDetailKey.LibraryVersion] = v.ToString();
      dict[EProviderDetailKey.DataSource] = Database.Connection.DataSource;
      
      return dict;
    }

    #endregion Overrides of AbstractDataContext

  }
}