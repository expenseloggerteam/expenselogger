﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Math;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   Data repository
  /// </summary>
  public class DefaultDataRepository : IDataRepository
  {
    private readonly IDataContext m_ctx;
    private static readonly IRawValueConverter[] KnownConverters;
    private readonly MonthSummaryGenerator m_monthSummaryGenerator;

    /// <summary>
    ///   Static constructor to initialize stuff once
    /// </summary>
    static DefaultDataRepository()
    {
      List<IRawValueConverter> converters = new List<IRawValueConverter>
      {
        new EmailSettingsRawValueConverter(),
        new StringRawValueConverter()
      };

      KnownConverters = converters.ToArray();
    }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="contextFactory">Data context factory to get <see cref="IDataContext" /> instance(s) from</param>
    public DefaultDataRepository(IDataContextFactory contextFactory)
    {
      m_ctx = contextFactory.GetInstance();

      m_monthSummaryGenerator = new MonthSummaryGenerator(this);
    }

    #region Implementation of IDataRepository

    /// <inheritdoc />
    public IDictionary<EProviderDetailKey, string> GetProviderSummary()
    {
      return m_ctx.GetProviderSummary();
    }

    /// <inheritdoc />
    public ExpenseSummary GetExpenseSummary(string username)
    {
      DateTime checkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Local);

      return m_ctx.ExpenseSummaries.FirstOrDefault(f => f.User.Username == username && f.MonthAndYear == checkDate);
    }

    /// <inheritdoc />
    public IEnumerable<Expense> GetExpensesForUser(string username, Func<IEnumerable<Expense>, IEnumerable<Expense>> fnFilter = null)
    {
      var data = m_ctx.Expenses.Where(e => e.User.Username == username);

      if (data.Any() && fnFilter != null)
        data = fnFilter(data).AsQueryable();

      return data;
    }

    /// <summary>
    ///   Gets an expense trend for given user and month
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <param name="monthAndYear">Month and year for which to generate a trend</param>
    /// <returns>Trended values till end of the month</returns>
    public IEnumerable<double> GetExpenseTrendForUser(string username, DateTime monthAndYear)
    {
      int daysInMonth = DateTime.DaysInMonth(monthAndYear.Year, monthAndYear.Month);
      DateTime startOfMonth = new DateTime(monthAndYear.Year, monthAndYear.Month, 1, 0, 0, 0, DateTimeKind.Local);
      DateTime lowerBound = startOfMonth.AddDays(-AppConstants.NumDaysInQuarter);
      DateTime upperBound = new DateTime(monthAndYear.Year, monthAndYear.Month, daysInMonth, 0, 0, 0, DateTimeKind.Local);
      Expense[] baseCheckExpenses = GetExpensesForUser(username)
        .Where(e => (e.Date >= lowerBound) && (e.Date <= upperBound))
        .ToArray();

      List<double> result = new List<double>();
      MonthSummary prevSummary = null;
      List<Expense> projectedExpenses = new List<Expense>();
      for (DateTime dt = startOfMonth; dt <= upperBound; dt = dt.AddDays(1))
      {
        List<Expense> refExpenses = baseCheckExpenses
          .Where(e => e.Date.Date > dt.AddDays(-AppConstants.NumDaysInQuarter) && e.Date.Date <= dt)
          .ToList();

        double thisDaySum = refExpenses.Where(e => e.Date.Date == dt.Date).Sum(a => a.Amount);
        double futureSum = baseCheckExpenses.Where(e => e.Date.Date > dt.Date).Sum(a => a.Amount);

        if (thisDaySum == 0 && futureSum == 0 && prevSummary != null)
        {
          // use the daily average from previous summary
          projectedExpenses.Add(new Expense
          {
            Date = dt,
            Amount = prevSummary.DailyAverage
          });

          refExpenses.AddRange(projectedExpenses);
        }

        MonthSummary summary = m_monthSummaryGenerator.Generate(refExpenses, AppConstants.NumDaysInQuarter, dt);
        result.Add(summary.EndOfMonthProjection);

        prevSummary = summary;
      }

      //DateTime lowerBound = monthAndYear.AddMonths(-3);
      //DateTime upperBound = new DateTime(monthAndYear.Year, monthAndYear.Month, daysInMonth, 0, 0, 0, DateTimeKind.Local);

      //var pastExpenses = GetExpensesForUser(username)
      //  .Where(e => (e.Date >= lowerBound) && (e.Date <= upperBound))
      //  .OrderBy(e => e.Date)
      //  .GroupBy(e => e.Date)
      //  .Select(e => new KeyValuePair<DateTime, double>(e.Key, e.Sum(f => f.Amount)))
      //  .ToDictionary(k => k.Key, v => v.Value);

      //List<double> points = pastExpenses.Where(f => f.Key < startOfMonth).Select(f => (double)f.Key.Ticks).ToList();
      //List<double> values = pastExpenses.Where(f => f.Key < startOfMonth).Select(f => f.Value).ToList();

      //Quartile q = values.InterquartileRange();

      //if (q != null && q.Range > 0)
      //{
      //  // remove outliers from the data and just add them as static values in the final projection
      //  double thirdIqr = (q.Range * 3) + q.Third;

      //  for (int i = 0; i < points.Count; i++)
      //  {
      //    if (values[i] <= thirdIqr)
      //      continue;

      //    points.RemoveAt(i);
      //    values.RemoveAt(i);
      //  }
      //}

      //for (DateTime dt = startOfMonth; dt <= upperBound; dt = dt.AddDays(1))
      //{
      //  double avg = values.Any() ? values.Average() : 0;

      //  result.Add(avg < 0 ? 0 : avg);

      //  // add the expense for 'dt' if found
      //  if (pastExpenses.ContainsKey(dt))
      //  {
      //    points.Add(dt.Ticks);
      //    values.Add(pastExpenses[dt]);
      //  }
      //  else
      //  {
      //    // Check if we have any future expenses till the end of expense's month
      //    // If we do then it means that we are adding a past expense and that we don't need to
      //    // project.
      //    bool haveFutureExpenses = pastExpenses.Any(f => f.Key > dt && f.Key <= upperBound);

      //    if (haveFutureExpenses)
      //      continue;

      //    points.Add(dt.Ticks);
      //    values.Add(avg);
      //  }
      //}

      return result;
    }

    /// <inheritdoc />
    public IEnumerable<Category> GetCategoriesForUser(string username)
    {
      if (username == AppConstants.AdminUsername)
        return m_ctx.Categories;

      return m_ctx.Categories.Where(c => c.User.Username == username || c.User.Username == AppConstants.AdminUsername);
    }

    /// <inheritdoc />
    public IEnumerable<Reminder> GetRemindersForUser(string username, Func<IEnumerable<Reminder>, IEnumerable<Reminder>> fnFilter = null)
    {
      var data = m_ctx.Reminders.Where(f => f.User.Username == username);

      if (data.Any() && fnFilter != null)
        data = (IQueryable<Reminder>)fnFilter(data);

      return data;
    }

    /// <summary>
    ///   Save or update an existing entity in the repository
    /// </summary>
    /// <param name="entity">Entity to be saved</param>
    public void Save(AbstractEntity entity)
    {
      m_ctx.SaveChanges(entity);
    }

    /// <inheritdoc />
    public void Save(ICollection<AbstractEntity> entities)
    {
      if (entities == null)
        throw new ArgumentNullException(nameof(entities));

      m_ctx.SaveChanges(entities);
    }

    /// <summary>
    ///   Get an entity from the repository
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    /// <param name="id">Entity id</param>
    /// <returns>Entity if found, else null</returns>
    public T GetById<T>(int id) where T : AbstractEntity
    {
      IEnumerable<T> collection = GetInternalCollection<T>();

      T entity = collection.FirstOrDefault(f => f.Id == id);

      return entity;
    }

    /// <inheritdoc />
    public IEnumerable<T> GetByUser<T>(string username, Func<IEnumerable<T>, IEnumerable<T>> fnFilter = null) where T : class, IUserReference
    {
      IEnumerable<T> collection = GetInternalCollection<T>();

      collection = collection.Where(f => f.User.Username == username).ToList();

      if (!collection.Any())
        return new List<T>();

      if (fnFilter != null)
        collection = fnFilter(collection);

      return collection;
    }

    /// <inheritdoc />
    public void Delete<T>(T entity) where T : AbstractEntity
    {
      IDbSet<T> collection = GetInternalCollection<T>();

      collection.Remove(entity);
      m_ctx.SaveChanges();

      Expense e = entity as Expense;
      if (e == null)
        return;

      User u = m_ctx.Users.First(f => f.Id == e.UserId);
      ExpenseSummary summary = GetExpenseSummary(u.Username);
      summary.MonthActual -= e.Amount;
      summary.YearActual -= e.Amount;

      m_ctx.SaveChanges(summary);
    }

    /// <inheritdoc />
    public T GetSetting<T>(ESystemSetting setting)
    {
      SystemSetting dbVal = m_ctx.SystemSettings.FirstOrDefault(f => f.Key == setting);

      if (dbVal == null)
        return default(T);

      Type ttype = typeof(T);

      if (!KnownConverters.Any(f => f.CanConvert(ttype)))
        throw new InvalidOperationException("No raw value converter registered for setting: " + setting + " which can retrieve the value as a " + typeof(T).FullName);

      T actualValue = (T)KnownConverters.First(f => f.CanConvert(ttype)).Convert(dbVal.RawValue);
      return actualValue;
    }

    /// <inheritdoc />
    public void SaveSetting(SystemSetting setting)
    {
      SystemSetting existing = m_ctx.SystemSettings.FirstOrDefault(f => f.Key == setting.Key);

      if (existing != null)
      {
        existing.RawValue = setting.RawValue;

        m_ctx.SaveChanges(existing);
      }
      else
        m_ctx.SaveChanges(setting);
    }

    #endregion Implementation of IDataRepository

    #region Helper methods

    /// <summary>
    ///   Get the internal lookup collection to manipulate based on the entity type
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    /// <returns>Internal entity collection</returns>
    private IDbSet<T> GetInternalCollection<T>() where T : class
    {
      Type entityType = typeof(T);
      IDbSet<T> collection = null;

      if (entityType.FullName == EntityName.Expense)
        collection = (IDbSet<T>)m_ctx.Expenses;
      else if (entityType.FullName == EntityName.Category)
        collection = (IDbSet<T>)m_ctx.Categories;
      else if (entityType.FullName == EntityName.Reminder)
        collection = (IDbSet<T>)m_ctx.Reminders;

      if (collection == null)
        throw new InvalidOperationException("Invalid entity type specified. Entity type: " + entityType.FullName + " is unknown.");

      return collection;
    }

    #endregion Helper methods

    #region Implementation of IDisposable

    /// <inheritdoc />
    public void Dispose()
    {
      m_ctx.Dispose();
    }

    #endregion
  }
}