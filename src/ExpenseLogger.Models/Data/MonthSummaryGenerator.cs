﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   A month summary generator
  /// </summary>
  public class MonthSummaryGenerator
  {
    private IDataRepository m_dataRepo;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="dataRepo">Data repository</param>
    public MonthSummaryGenerator(IDataRepository dataRepo)
    {
      m_dataRepo = dataRepo;
    }

    /// <summary>
    ///   Generate a month summary
    /// </summary>
    /// <param name="refData">Reference expense data to use to generate summary</param>
    /// <param name="refDataSpanDays">
    ///   No. of days over which reference data spans. Note: This is the data query days range
    ///   which resulted in the <see cref="M:refData" />
    /// </param>
    /// <param name="day">Day of the month for which to generate summary</param>
    /// <returns>Generated summary</returns>
    public MonthSummary Generate(ICollection<Expense> refData, int refDataSpanDays, DateTime day)
    {
      double dailyAverage = refData.Sum(f => f.Amount) / refDataSpanDays;
      int numDaysLeft = DateTime.DaysInMonth(day.Year, day.Month) - day.Day;

      IEnumerable<Expense> thisMonthExpenses = refData.Where(f => f.Date.Date >= new DateTime(day.Year, day.Month, 1, 0, 0, 0, DateTimeKind.Local));
      double thisMonthExpensesSum = thisMonthExpenses.Sum(f => f.Amount);
      double endOfMonthProjection = thisMonthExpensesSum + dailyAverage * numDaysLeft;

      return new MonthSummary
      {
        Day = day,
        DailyAverage = dailyAverage,
        TotalAmountTillEndOfDay = thisMonthExpensesSum,
        EndOfMonthProjection = endOfMonthProjection
      };
    }
  }
}