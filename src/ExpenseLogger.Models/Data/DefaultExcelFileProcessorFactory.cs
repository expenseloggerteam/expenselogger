﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Castle.MicroKernel;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Security;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   Default implementation of <see cref="IExcelFileProcessorFactory" />
  /// </summary>
  public class DefaultExcelFileProcessorFactory : IExcelFileProcessorFactory
  {
    private readonly IKernel m_kernel;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="kernel">Castle kernel</param>
    public DefaultExcelFileProcessorFactory(IKernel kernel)
    {
      m_kernel = kernel;
    }

    /// <inheritdoc />
    public IExcelFileProcessor<TEntity> GetInstance<TEntity>(string username) where TEntity : AbstractEntity
    {
      string entity = typeof(TEntity).FullName;
      IExcelFileProcessor<TEntity> processor;

      IUserRepository userRepo = m_kernel.Resolve<IUserRepository>();
      IDataRepository dataRepo = m_kernel.Resolve<IDataRepository>();

      if (entity == EntityName.Expense)
        processor = (IExcelFileProcessor<TEntity>)new ExpensesExcelFileProcessor(username, userRepo, dataRepo);
      else
        throw new InvalidOperationException("Requested file processor not available");

      return processor;
    }
  }
}