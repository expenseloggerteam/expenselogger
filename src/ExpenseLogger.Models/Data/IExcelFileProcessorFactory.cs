﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Core;

namespace ExpenseLogger.Models.Data
{
  /// <summary>
  ///   Generic interface for an Excel file processor factory
  /// </summary>
  public interface IExcelFileProcessorFactory
  {
    /// <summary>
    ///   Get an instance of a <see cref="IExcelFileProcessor{TEntity}" />
    /// </summary>
    /// <typeparam name="TEntity">An <see cref="AbstractEntity" /></typeparam>
    /// <param name="username">Username for which to processing needs to be done</param>
    /// <returns>An Excel file processor</returns>
    IExcelFileProcessor<TEntity> GetInstance<TEntity>(string username) where TEntity : AbstractEntity;
  }
}