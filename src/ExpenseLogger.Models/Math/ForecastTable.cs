﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Data;

namespace ExpenseLogger.Models.Math
{
  /// <summary>
  ///   Denotes a Forecast table
  /// </summary>
  public class ForecastTable : DataTable
  {
    /// <summary>
    ///   Default constructor
    /// </summary>
    public ForecastTable()
    {
      Columns.Add("Instance", typeof(int)); // The position in which this value occurred in the time-series
      Columns.Add("Value", typeof(double)); // The value which actually occurred
      Columns.Add("Forecast", typeof(double)); // The forecasted value for this instance
      Columns.Add("Holdout", typeof(bool)); // Identifies a holdout actual value row, for testing after err is calculated

      //E(t) = D(t) - F(t)
      Columns.Add("Error", typeof(double), "Forecast-Value");

      //Absolute Error = |E(t)|
      Columns.Add("AbsoluteError", typeof(double), "IIF(Error>=0, Error, Error * -1)");

      //Percent Error = E(t) / D(t)
      Columns.Add("PercentError", typeof(double), "IIF(Value<>0, Error / Value, Null)");

      //Absolute Percent Error = |E(t)| / D(t)
      Columns.Add("AbsolutePercentError", typeof(double), "IIF(Value <> 0, AbsoluteError / Value, Null)");
    }

    /// <summary>
    ///   Get an empty row for the forecast table
    /// </summary>
    /// <returns>Returns an empty but initialised row</returns>
    public DataRow GetEmptyRow()
    {
      DataRow row = NewRow();
      row["Value"] = double.MinValue;
      row["Forecast"] = double.MinValue;
      return row;
    }
  }
}