﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Linq;

namespace ExpenseLogger.Models.Math
{
  /// <summary>
  ///   Extension methods for <see cref="IEnumerable{T}" />
  /// </summary>
  public static class EnumerableDoubleExtensions
  {
    /// <summary>
    ///   Computes the interquartile range for the given data set
    /// </summary>
    /// <param name="data">Data range to compute interquartile for</param>
    /// <returns>Interquartile value</returns>
    public static Quartile InterquartileRange(this IEnumerable<double> data)
    {
      if (data == null)
        return null;

      double[] dataArr = data.ToArray();

      if (dataArr.Length == 0)
        return new Quartile(dataArr, 0, 0);

      return new Quartile(dataArr, QuantileInplace(dataArr, 0.75), QuantileInplace(dataArr, 0.25));
    }

    /// <summary>
    ///   Estimates the tau-th quantile from the unsorted data array.
    ///   The tau-th quantile is the data value where the cumulative distribution
    ///   function crosses tau.
    ///   Approximately median-unbiased regardless of the sample distribution (R8).
    ///   WARNING: Works inplace and can thus causes the data array to be reordered.
    /// </summary>
    /// <param name="data">Sample array, no sorting is assumed. Will be reordered.</param>
    /// <param name="tau">Quantile selector, between 0.0 and 1.0 (inclusive).</param>
    /// <remarks>
    ///   R-8, SciPy-(1/3,1/3):
    ///   Linear interpolation of the approximate medians for order statistics.
    ///   When tau &lt; (2/3) / (N + 1/3), use x1. When tau &gt;= (N - 1/3) / (N + 1/3), use xN.
    /// </remarks>
    private static double QuantileInplace(double[] data, double tau)
    {
      if (tau < 0.0 || tau > 1.0 || data.Length == 0)
        return double.NaN;
      double num1 = (data.Length + 1.0 / 3.0) * tau + 1.0 / 3.0;
      int rank = (int)num1;
      if (rank <= 0 || tau == 0.0)
        return data.Min();
      if (rank >= data.Length || tau == 1.0)
        return data.Max();
      double num2 = SelectInplace(data, rank - 1);
      double num3 = SelectInplace(data, rank);
      return num2 + (num1 - rank) * (num3 - num2);
    }

    private static double SelectInplace(double[] workingData, int rank)
    {
      if (rank <= 0)
        return workingData.Min();
      if (rank >= workingData.Length - 1)
        return workingData.Max();
      double[] numArray = workingData;
      int index1 = 0;
      int index2 = numArray.Length - 1;
      while (index2 > index1 + 1)
      {
        int index3 = index1 + index2 >> 1;
        double num1 = numArray[index3];
        numArray[index3] = numArray[index1 + 1];
        numArray[index1 + 1] = num1;
        if (numArray[index1] > numArray[index2])
        {
          double num2 = numArray[index1];
          numArray[index1] = numArray[index2];
          numArray[index2] = num2;
        }
        if (numArray[index1 + 1] > numArray[index2])
        {
          double num2 = numArray[index1 + 1];
          numArray[index1 + 1] = numArray[index2];
          numArray[index2] = num2;
        }
        if (numArray[index1] > numArray[index1 + 1])
        {
          double num2 = numArray[index1];
          numArray[index1] = numArray[index1 + 1];
          numArray[index1 + 1] = num2;
        }
        int index4 = index1 + 1;
        int index5 = index2;
        double num3 = numArray[index4];
        while (true)
        {
          do
          {
            ++index4;
          } while (numArray[index4] < num3);
          do
          {
            --index5;
          } while (numArray[index5] > num3);
          if (index5 >= index4)
          {
            double num2 = numArray[index4];
            numArray[index4] = numArray[index5];
            numArray[index5] = num2;
          }
          else
            break;
        }
        numArray[index1 + 1] = numArray[index5];
        numArray[index5] = num3;
        if (index5 >= rank)
          index2 = index5 - 1;
        if (index5 <= rank)
          index1 = index4;
      }
      if (index2 == index1 + 1 && numArray[index2] < numArray[index1])
      {
        double num = numArray[index1];
        numArray[index1] = numArray[index2];
        numArray[index2] = num;
      }
      return numArray[rank];
    }
  }
}