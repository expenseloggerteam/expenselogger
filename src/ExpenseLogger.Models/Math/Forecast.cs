﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Data;
using System.Linq;

namespace ExpenseLogger.Models.Math
{
  /// <summary>
  ///   Forecasting techniques available
  /// </summary>
  public class Forecast
  {
    /// <summary>
    ///   Naive Bayes
    /// </summary>
    /// <param name="values">Existing/Observed values</param>
    /// <param name="extension">
    ///   Number of periods in the furture to produce forecasts.
    ///   The higher the value the more probability of errors
    /// </param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <returns>The forecasted values</returns>
    public static ForecastTable NaiveBayes(double[] values, int extension, int holdout)
    {
      ForecastTable dt = new ForecastTable();

      for (int i = 0; i < (values.Length + extension); i++)
      {
        // insert a row for each value in set
        DataRow row = dt.NewRow();
        dt.Rows.Add(row);

        row.BeginEdit();
        // assign its sequence number
        row["Instance"] = i;

        // if 'i' is in the holdout range of values
        row["holdout"] = (i > (values.Length - 1 - holdout)) && (i < values.Length);

        if (i < values.Length)
        {
          // processing values which actually occurred
          // assign the actual value to the DataRow
          row["Value"] = values[i];

          // if first row value gets itself, else value value of the prior row
          row["Forecast"] = (i == 0) ? values[i] : values[i - 1];
        }
        else
        {
          // extension rows
          row["Forecast"] = values[values.Length - 1];
        }
        row.EndEdit();
      }
      dt.AcceptChanges();
      return dt;
    }

    /// <summary>
    ///   Simple moving average
    /// </summary>
    /// <param name="values">Existing/Observed values</param>
    /// <param name="extension">
    ///   Number of periods in the furture to produce forecasts.
    ///   The higher the value the more probability of errors
    /// </param>
    /// <param name="periods">
    ///   Number of last periods to use for calculating the moving
    ///   average of values
    /// </param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <returns>The forecasted values</returns>
    public static ForecastTable SimpleMovingAverage(double[] values, int extension, int periods, int holdout)
    {
      //
      //            ( Dt + D(t-1) + D(t-2) + ... + D(t-n+1) )
      //  F(t+1) =  -----------------------------------------
      //                              n
      ForecastTable dt = new ForecastTable();

      for (int i = 0; i < values.Length + extension; i++)
      {
        // insert a row for each value in set
        DataRow row = dt.NewRow();
        dt.Rows.Add(row);

        row.BeginEdit();

        // assign its sequence number
        row["Instance"] = i;
        if (i < values.Length)
        {
          // processing values which actually occurred
          row["Value"] = values[i];
        }

        // indicate if this is a holdout row
        row["holdout"] = (i > (values.Length - holdout)) && (i < values.Length);

        if (i == 0)
        {
          // initialize first row with its own value
          row["Forecast"] = values[i];
        }
        else if (i <= values.Length - holdout)
        {
          // processing values which actually occurred, but not in holdout set
          double avg = 0;
          DataRow[] rows = dt.Select("Instance>=" + (i - periods) + " AND Instance < " + i, "Instance");

          Array.ForEach(rows, f => avg += (double)f["Value"]);

          avg /= rows.Length;

          row["Forecast"] = avg;
        }
        else
        {
          // must be in the holdout set or the extension
          double avg = 0;

          // get the Periods-prior rows and calculate an average actual value
          DataRow[] rows = dt.Select("Instance>=" + (i - periods) + " AND Instance < " + i, "Instance");

          // for each row, if we are in test/holdout set take the prior value,
          // else we are in extension area take the prior forecast value since we don't have an actual value
          Array.ForEach(rows, f => avg += ((int)f["Instance"] < values.Length) ? (double)f["Value"] : (double)f["Forecast"]);

          avg /= rows.Length;

          // set the forecasted value
          row["Forecast"] = avg;
        }
        row.EndEdit();
      }

      dt.AcceptChanges();
      return dt;
    }

    /// <summary>
    ///   Weighted moving average
    /// </summary>
    /// <param name="values">Existing/Observed values</param>
    /// <param name="extension">
    ///   Number of periods in the furture to produce forecasts.
    ///   The higher the value the more probability of errors
    /// </param>
    /// <param name="weights">
    ///   Weights to be assigned to existing/observed values when calculating the
    ///   moving average
    /// </param>
    /// <returns>The forcasted values</returns>
    public static ForecastTable WeightedMovingAverage(double[] values, int extension, params double[] weights)
    {
      //            
      //  F(t+1) =  (Weight1 * D(t)) + (Weight2 * D(t-1)) + (Weight3 * D(t-2)) + ... + (WeightN * D(t-n+1))
      //          
      // PeriodWeight[].Length is used to determine the number of periods over which to average
      // PeriodWeight[x] is used to apply a weight to the prior period's value

      // make sure PeriodWeight values add up to 100%
      double test = weights.Sum();
      if (test != 1.0)
        throw new ArgumentException("Period weights must add up to 1.0");

      ForecastTable dt = new ForecastTable();

      for (int i = 0; i < values.Length + extension; i++)
      {
        // insert a row for each value in set
        DataRow row = dt.NewRow();
        dt.Rows.Add(row);

        row.BeginEdit();

        // assign its sequence number
        row["Instance"] = i;

        if (i < values.Length)
        {
          // we're in the test set
          row["Value"] = values[i];
        }

        if (i == 0)
        {
          // initialize forecast with first row's value
          row["Forecast"] = values[i];
        }
        else if ((i < values.Length) && (i < weights.Length))
        {
          // get the datarows representing the values within the WMA length
          DataRow[] rows = dt.Select("Instance>=" + (i - weights.Length) + " AND Instance < " + i, "Instance");

          // processing one of the first rows, before we've advanced enough to properly weight past rows
          // apply an initial, uniform weight (1 / rows.Length) to the initial rows
          row["Forecast"] = rows.Select(f => (double)f["Value"] * (1 / rows.Length)).Sum();
        }
        else if ((i < values.Length) && (i >= weights.Length))
        {
          // get the rows within the weight range just prior to the current row
          DataRow[] rows = dt.Select("Instance>=" + (i - weights.Length) + " AND Instance < " + i, "Instance");

          // apply the appropriate period's weight to the value
          row["Forecast"] = rows.Select((f, idx) => (double)f["Value"] * weights[idx]).Sum();
        }
        else
        {
          // get the rows within the weight range just prior to the current row
          DataRow[] rows = dt.Select("Instance>=" + (i - weights.Length) + " AND Instance < " + i, "Instance");

          // with no actual values to weight, use the previous rows' forecast instead
          row["Forecast"] = rows.Select((f, idx) => (double)f["Forecast"] * weights[idx]).Sum();
        }

        row.EndEdit();
      }

      dt.AcceptChanges();
      return dt;
    }

    /// <summary>
    ///   Exponential smoothing
    /// </summary>
    /// <param name="values">Existing/Observed values</param>
    /// <param name="extension">
    ///   Number of periods in the furture to produce forecasts.
    ///   The higher the value the more probability of errors
    /// </param>
    /// <param name="alpha">The smooting factor</param>
    /// <returns>The forecasted values</returns>
    public static ForecastTable ExponentialSmoothing(double[] values, int extension, double alpha)
    {
      //
      //  F(t+1) =    ( Alpha * D(t) ) + (1 - Alpha) * F(t)
      //
      ForecastTable dt = new ForecastTable();

      for (int i = 0; i < (values.Length + extension); i++)
      {
        // insert a row for each value in set
        DataRow row = dt.NewRow();
        dt.Rows.Add(row);

        row.BeginEdit();

        // assign its sequence number
        row["Instance"] = i;
        if (i < values.Length)
        {
          // test set
          row["Value"] = values[i];
          if (i == 0)
          {
            // initialize first value
            row["Forecast"] = values[i];
          }
          else
          {
            // main area of forecasting
            DataRow priorRow = dt.Select("Instance=" + (i - 1))[0];
            double priorForecast = (double)priorRow["Forecast"];
            double priorValue = (double)priorRow["Value"];

            row["Forecast"] = priorForecast + (alpha * (priorValue - priorForecast));
          }
        }
        else
        {
          // extension has to use prior forecast instead of prior value
          DataRow priorRow = dt.Select("Instance=" + (i - 1))[0];
          double priorForecast = (double)priorRow["Forecast"];
          double priorValue = (double)priorRow["Forecast"];

          row["Forecast"] = priorForecast + (alpha * (priorValue - priorForecast));
        }
        row.EndEdit();
      }

      dt.AcceptChanges();

      return dt;
    }

    /// <summary>
    ///   Adaptive rate smoothing
    /// </summary>
    /// <param name="values">Existing/Observed values</param>
    /// <param name="extension">
    ///   Number of periods in the furture to produce forecasts.
    ///   The higher the value the more probability of errors
    /// </param>
    /// <param name="minGamma">Lower bound for the smoothing factor</param>
    /// <param name="maxGamma">Upper bound for the smoothing factor</param>
    /// <returns>The forecasted values</returns>
    public static ForecastTable AdaptiveRateSmoothing(double[] values, int extension, double minGamma, double maxGamma)
    {
      ForecastTable dt = new ForecastTable();

      for (int i = 0; i < (values.Length + extension); i++)
      {
        // insert a row for each value in set
        DataRow row = dt.NewRow();
        dt.Rows.Add(row);

        row.BeginEdit();

        // assign its sequence number
        row["Instance"] = i;

        // update our game value
        double gamma = System.Math.Abs(TrackingSignal(dt, false, 3));
        if (gamma < minGamma)
          gamma = minGamma;
        else if (gamma > maxGamma)
          gamma = maxGamma;

        // get prior row, if i==0 then get current row
        DataRow priorRow = (i == 0) ? dt.GetEmptyRow() : dt.Select("Instance=" + (i - 1)).First();
        double priorForecast = (double)priorRow["Forecast"];
        double priorValue = (i < values.Length) ? (double)priorRow["Value"] : priorForecast;

        if (i == 0) // first row
        {
          row["Value"] = values[i];
          row["Forecast"] = values[i];
        }
        else if (i < values.Length) // we're still in observed set
        {
          row["Value"] = values[i];
          row["Forecast"] = priorForecast + (gamma * (priorValue - priorForecast));
        }
        else
        {
          // we're in extensions set
          row["Forecast"] = priorForecast + (gamma * (priorValue - priorForecast));
        }

        row.EndEdit();
      }

      dt.AcceptChanges();

      return dt;
    }

    #region Forecast Performance Measures

    /// <summary>
    ///   Mean signed error
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <returns>The calculated mean signed error</returns>
    public static double MeanSignedError(ForecastTable dt, bool holdout, int ignoreInitial)
    {
      // 
      // MeanSignedError = Sum(E(t)) / n
      // 
      string filter = "Error Is Not Null AND Instance > " + ignoreInitial;
      if (holdout)
        filter += " AND holdout=True";

      return (dt.Select(filter).Length == 0) ? 0 : (double)dt.Compute("Avg(Error)", filter);
    }

    /// <summary>
    ///   Mean absolute error
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <returns>The calculated mean absolute error</returns>
    public static double MeanAbsoluteError(ForecastTable dt, bool holdout, int ignoreInitial)
    {
      //
      // MeanAbsoluteError = Sum(|E(t)|) / n
      //
      string filter = "AbsoluteError Is Not Null AND Instance > " + ignoreInitial;
      if (holdout)
        filter += " AND holdout=True";

      return (dt.Select(filter).Length == 0) ? 0 : (double)dt.Compute("Avg(AbsoluteError)", filter);
    }

    /// <summary>
    ///   Mean percent error
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <returns>The calculated mean percent error</returns>
    public static double MeanPercentError(ForecastTable dt, bool holdout, int ignoreInitial)
    {
      //
      // MeanPercentError = Sum( PercentError ) / n
      //
      string filter = "PercentError Is Not Null AND Instance > " + ignoreInitial;
      if (holdout)
        filter += " AND holdout=True";

      return (dt.Select(filter).Length == 0) ? 0 : (double)dt.Compute("Avg(PercentError)", filter);
    }

    /// <summary>
    ///   Mean absolute percent error
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <returns>The calculated mean absolute percent error</returns>
    public static double MeanAbsolutePercentError(ForecastTable dt, bool holdout, int ignoreInitial)
    {
      //
      // MeanAbsolutePercentError = Sum( |PercentError| ) / n
      //
      string filter = "AbsolutePercentError Is Not Null AND Instance > " + ignoreInitial;
      if (holdout)
        filter += " AND holdout=True";

      return (dt.Select(filter).Length == 0) ? 1 : (double)dt.Compute("AVG(AbsolutePercentError)", filter);
    }

    /// <summary>
    ///   Tracking signal
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <returns>The calculated tracking signal</returns>
    public static double TrackingSignal(ForecastTable dt, bool holdout, int ignoreInitial)
    {
      //
      // Tracking signal = MeanSignedError / MeanAbsoluteError
      //
      double MAE = MeanAbsoluteError(dt, holdout, ignoreInitial);

      return (MAE == 0) ? 0 : MeanSignedError(dt, holdout, ignoreInitial) / MAE;
    }

    /// <summary>
    ///   Mean squared error
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <param name="degreesOfFreedom">Number of values to ignore when calculating the error</param>
    /// <returns>The calculated mean squared error</returns>
    public static double MeanSquaredError(ForecastTable dt, bool holdout, int ignoreInitial, int degreesOfFreedom)
    {
      //
      // MSE = Sum( E(t)^2 ) / n
      //
      string filter = "Error Is Not Null AND Instance > " + ignoreInitial;
      if (holdout)
        filter += " AND holdout=True";

      DataRow[] rows = dt.Select(filter);
      if (rows.Length == 0)
        return 0;

      double sumSquareError = rows.Select(f => System.Math.Pow(double.Parse(f["Error"].ToString()), 2.0)).Sum();

      return sumSquareError / (dt.Rows.Count - degreesOfFreedom);
    }

    /// <summary>
    ///   Cumulative signed error
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <returns>The calculated cumulative signed error</returns>
    public static double CumulativeSignedError(ForecastTable dt, bool holdout, int ignoreInitial)
    {
      //
      // CumulativeSignedError = Sum( E(t) )
      //
      string filter = "Error Is Not Null AND Instance > " + ignoreInitial;
      if (holdout)
        filter += " AND holdout=True";

      return (dt.Select(filter).Length == 0) ? 0 : (double)dt.Compute("SUM(Error)", filter);
    }

    /// <summary>
    ///   Cumulative absolute error
    /// </summary>
    /// <param name="dt">Forecast table to calculate  the error for</param>
    /// <param name="holdout">
    ///   An integer number of periods to withhold from the testable
    ///   set of observed values (from the end). The functions calculate forecasts for
    ///   these values without looking at the observed values until after the forecast
    ///   is generated. In this way, forecasts for a number of periods may be verified
    ///   against observed values without the inconvenience of having to wait for future
    ///   periods to occur
    /// </param>
    /// <param name="ignoreInitial">Number of initial values to ignore</param>
    /// <returns>The calculated cumulative absolute error</returns>
    public static double CumulativeAbsoluteError(ForecastTable dt, bool holdout, int ignoreInitial)
    {
      //
      // CumulativeAbsoluteError = Sum( |E(t)| )
      //
      string filter = "AbsoluteError Is Not Null AND Instance > " + ignoreInitial;
      if (holdout)
        filter += " AND holdout=True";

      return (dt.Select(filter).Length == 0) ? 0 : (double)dt.Compute("SUM(AbsoluteError)", filter);
    }

    #endregion Forecast Performance Measures
  }
}