﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;

namespace ExpenseLogger.Models.Math
{
  /// <summary>
  ///   Denotes an Quartile for a collection
  /// </summary>
  public class Quartile
  {
    /// <summary>
    ///   Data on which the quartile was calculated
    /// </summary>
    public IEnumerable<double> Data { get; private set; }

    /// <summary>
    ///   1st quartile
    /// </summary>
    public double First { get; private set; }

    /// <summary>
    ///   3rd quartile
    /// </summary>
    public double Third { get; private set; }

    /// <summary>
    ///   Interquartile range
    /// </summary>
    public double Range
    {
      get { return Third - First; }
    }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="data">Data on which the quartile was calculated</param>
    /// <param name="firstQuartile">1st quartile value of the collection</param>
    /// <param name="thirdQuartile">3rd quartile value of the collection</param>
    public Quartile(IEnumerable<double> data, double firstQuartile, double thirdQuartile)
    {
      Data = data;
      First = firstQuartile;
      Third = thirdQuartile;
    }
  }
}