﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Security;
using Quartz;

namespace ExpenseLogger.Models.Jobs
{
  /// <summary>
  ///   A job that creates monthly and yearly forecasts for all users
  /// </summary>
  [DisallowConcurrentExecution]
  public class ExpensesForecasterJob : AbstractRunOnceDailyJob
  {
    private const int NumDaysOfYear = 365;
    private const int NumDaysInQuarter = 120;

    private readonly IDataRepository m_dataRepo;
    private readonly IUserRepository m_userRepo;

    private ProcessResult m_result;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="isEnabled">Flag indicating whether this task is enabled</param>
    /// <param name="dataRepo">Expenses repository</param>
    /// <param name="userRepo">User profile repository</param>
    public ExpensesForecasterJob(bool isEnabled, IDataRepository dataRepo, IUserRepository userRepo)
      : base(isEnabled)
    {
      m_dataRepo = dataRepo;
      m_userRepo = userRepo;
    }

    #region Overrides of AbstractRunOnceDailyJob

    /// <inheritdoc />
    public override bool ShouldRunNow()
    {
      if (Enabled)
        return true;

      m_result = new ProcessResult(ECode.NotAllowed, "Job has been disabled");
      Log.ToLoggerAndConsole("Job has been disabled");
      return false;
    }

    /// <inheritdoc />
    public override ProcessResult ExecuteJob(IJobExecutionContext context)
    {
      m_result = new ProcessResult();

      // get all users
      IEnumerable<User> allUsers = m_userRepo.GetAllUsers();

      foreach (User u in allUsers)
      {
        ExpenseSummary summary = m_dataRepo.GetExpenseSummary(u.Username) ?? new ExpenseSummary
        {
          UserId = u.Id,
          User = u,
          MonthAndYear = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Local),
          MonthBudget = u.Settings.MonthlyBudget
        };

        List<Expense> expensesToUse = m_dataRepo.GetExpensesForUser(u.Username, e => { return e.Where(f => f.Date.Date >= DateTime.Now.Date.AddDays(-NumDaysOfYear)); }).ToList();

        UpdateYearlyStats(summary, expensesToUse);

        expensesToUse = expensesToUse.Where(f => f.Date.Date >= DateTime.Now.Date.AddDays(-NumDaysInQuarter)).ToList();

        UpdateMonthlyStats(summary, expensesToUse);

        m_dataRepo.Save(summary);

        Log.ToLoggerAndConsole("Updated monthly and yearly forecasts for user: " + u.Username);
      }

      return m_result;
    }

    #endregion Overrides of AbstractRunOnceDailyJob

    /// <summary>
    /// Update the given expense summary with yearly actuals and projection
    /// </summary>
    /// <param name="summary">Summary to update</param>
    /// <param name="expensesToUse">Expenses to use for calculation</param>
    private static void UpdateYearlyStats(ExpenseSummary summary, List<Expense> expensesToUse)
    {
      double dailyAverage = expensesToUse.Sum(f => f.Amount) / NumDaysOfYear;
      int numDaysLeft = NumDaysOfYear - DateTime.Now.DayOfYear;

      IEnumerable<Expense> thisYearExpenses = expensesToUse.Where(f => f.Date.Date > new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0, DateTimeKind.Local));

      double thisYearExpensesSum = thisYearExpenses.Sum(f => f.Amount);
      double endOfYearProjection = thisYearExpensesSum + dailyAverage * numDaysLeft;

      summary.YearActual = thisYearExpensesSum;
      summary.YearProjected = endOfYearProjection;
    }

    /// <summary>
    /// Update the given expense summary with monthly actuals and projection
    /// </summary>
    /// <param name="summary">Summary to update</param>
    /// <param name="expensesToUse">Expenses to use for calculation</param>
    private static void UpdateMonthlyStats(ExpenseSummary summary, List<Expense> expensesToUse)
    {
      double dailyAverage = expensesToUse.Sum(f => f.Amount) / NumDaysInQuarter;
      int numDaysLeft = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day;

      IEnumerable<Expense> thisMonthExpenses = expensesToUse.Where(f => f.Date.Date > new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Local));

      double thisMonthExpensesSum = thisMonthExpenses.Sum(f => f.Amount);
      double endOfMonthProjection = thisMonthExpensesSum + dailyAverage * numDaysLeft;

      summary.MonthActual = thisMonthExpensesSum;
      summary.MonthProjected = endOfMonthProjection;
    }
  }
}