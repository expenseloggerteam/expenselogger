// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Reflection;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using log4net;
using Quartz;
using WebExtras.Core;

namespace ExpenseLogger.Models.Jobs
{
  /// <summary>
  ///   An abstract job which runs daily once. This job also handle running again when there is a failure of some kind in running it.
  /// </summary>
  public abstract class AbstractRunOnceDailyJob : IExpenseLoggerJob
  {
    private string m_stateFile;
    private ProcessResult m_result;

    /// <summary>
    ///   Logger instance
    /// </summary>
    protected static ILog Log;

    /// <summary>
    ///   Flag indicating whether this task is enabled
    /// </summary>
    public bool Enabled { get; private set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="isEnabled">Whether this task is enabled</param>
    protected AbstractRunOnceDailyJob(bool isEnabled)
    {
      Enabled = isEnabled;
      m_result = new ProcessResult();

      if (!Directory.Exists(AppConstants.CacheDirectory))
        Directory.CreateDirectory(AppConstants.CacheDirectory);
    }

    /// <summary>
    ///   Whether the task should run now. By default just checks the
    ///   Enabled flag and returns appropriate result
    /// </summary>
    /// <returns>True if task should run, else false</returns>
    public virtual bool ShouldRunNow()
    {
      if (!Enabled)
      {
        m_result = new ProcessResult(ECode.NotAllowed, "Job has been disabled");
        Log.ToLoggerAndConsole("Job has been disabled");
        return false;
      }

      if (string.IsNullOrEmpty(m_stateFile) || !File.Exists(m_stateFile))
        return true;

      DateTime lastRunTime = DateTime.Parse(File.ReadAllText(m_stateFile)).AsLocal();

      Log.ToLoggerAndConsole("Job last ran on: " + lastRunTime.ToString(AppConstants.IsoDateTimeFormat));

      // if the last run time is greater than midnight today then don't run the task
      // since we only want to run this task once every day
      return lastRunTime < DateTime.Now.Date;
    }

    /// <summary>
    ///   Called by the <see cref="T:Quartz.IScheduler" /> when a <see cref="T:Quartz.ITrigger" />
    ///   fires that is associated with the <see cref="T:Quartz.IJob" />.
    /// </summary>
    /// <remarks>
    ///   The implementation may wish to set a  result object on the
    ///   JobExecutionContext before this method exits.  The result itself
    ///   is meaningless to Quartz, but may be informative to
    ///   <see cref="T:Quartz.IJobListener" />s or
    ///   <see cref="T:Quartz.ITriggerListener" />s that are watching the job's
    ///   execution.
    /// </remarks>
    /// <param name="context">The execution context.</param>
    public void Execute(IJobExecutionContext context)
    {
      Log = Log ?? LogManager.GetLogger(context.JobDetail.Key.Name);
      Log.EmptyLine();
      Log.ToLoggerAndConsole(AppConstants.LineBreak());
      Log.ToLoggerAndConsole("JOB: " + context.JobDetail.Key.Name);

      if (string.IsNullOrEmpty(m_stateFile))
        m_stateFile = Path.GetFullPath(Path.Combine(AppConstants.CacheDirectory, context.JobDetail.Key.Name + ".ts"));

      try
      {

        if (ShouldRunNow())
        {
          //PreExecute(context);
          m_result = ExecuteJob(context);
          //PostExecute(context);
        }
        else
        {
          const string msg = "Job was not run since it has run once today";
          m_result = new ProcessResult(ECode.NotAllowed, msg);
          Log.ToLoggerAndConsole(msg);
        }

        if (m_result.Code == ECode.Ok)
          File.WriteAllText(m_stateFile, DateTime.Now.ToString(AppConstants.IsoDateTimeFormat));

        context.Result = m_result;
      }
      catch (Exception e)
      {
        Log.ToLoggerAndConsole("Unable to run job", e);
      }
      Log.ToLoggerAndConsole(AppConstants.LineBreak());
      Log.EmptyLine();
    }

    ///// <summary>
    /////   Operations to be performed before the job gets executed
    ///// </summary>
    ///// <param name="context">Job execution context</param>
    //public virtual void PreExecute(IJobExecutionContext context)
    //{
    //  // nothing to do here
    //}

    /// <summary>
    ///   Execute job
    /// </summary>
    /// <param name="context">Job execution context</param>
    /// <returns>Result of the execution</returns>
    public abstract ProcessResult ExecuteJob(IJobExecutionContext context);

    ///// <summary>
    /////   Operations to be performed after the job execution
    ///// </summary>
    ///// <param name="context">Job execution context</param>
    //public virtual void PostExecute(IJobExecutionContext context)
    //{
    //  // nothing to do here
    //}
  }
}