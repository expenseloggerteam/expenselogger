﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Security;
using Quartz;

namespace ExpenseLogger.Models.Jobs
{
  /// <summary>
  ///   This job generates and saves the summary data for ongoing month
  /// </summary>
  [DisallowConcurrentExecution]
  public class CurrentMonthForecasterJob : AbstractRunOnceDailyJob
  {
    private readonly IDataRepository m_dataRepo;
    private readonly IUserRepository m_userRepo;

    private ProcessResult m_result;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="isEnabled">Flag indicating whether this task is enabled</param>
    /// <param name="dataRepo">Expenses repository</param>
    /// <param name="userRepo">User profile repository</param>
    public CurrentMonthForecasterJob(bool isEnabled, IDataRepository dataRepo, IUserRepository userRepo)
      : base(isEnabled)
    {
      m_dataRepo = dataRepo;
      m_userRepo = userRepo;
    }

    #region Overrides of AbstractRunOnceDailyJob

    /// <inheritdoc />
    public override bool ShouldRunNow()
    {
      if (Enabled)
        return true;

      m_result = new ProcessResult(ECode.NotAllowed, "Job has been disabled");
      Log.ToLoggerAndConsole("Job has been disabled");
      return false;
    }

    /// <inheritdoc />
    public override ProcessResult ExecuteJob(IJobExecutionContext context)
    {
      m_result = new ProcessResult();

      MonthSummaryGenerator m_monthSummaryGenerator = new MonthSummaryGenerator(m_dataRepo);

      // get all users
      IEnumerable<User> users = m_userRepo.GetAllUsers();

      foreach (User u in users)
      {
        try
        {
          DateTime checkDate = DateTime.Now;
          IEnumerable<Expense> previousExpenses = m_dataRepo.GetExpensesForUser(u.Username, e => { return e.Where(f => f.Date.Date >= checkDate.Date.AddDays(-AppConstants.NumDaysInQuarter)).ToList(); }).ToList();
          //int daysInMonth = DateTime.DaysInMonth(checkDate.Year, checkDate.Month);

          MonthSummary monthSummary = m_monthSummaryGenerator.Generate(previousExpenses.ToArray(), AppConstants.NumDaysInQuarter, checkDate);

          //double dailyAverage = previousExpenses.Sum(f => f.Amount) / AppConstants.NumDaysInQuarter;
          //int numDaysLeft = daysInMonth - checkDate.Day;

          //IEnumerable<Expense> thisMonthExpenses = previousExpenses.Where(f => f.Date.Date > new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Local));

          //double thisMonthExpensesSum = thisMonthExpenses.Sum(f => f.Amount);
          //double endOfMonthProjection = thisMonthExpensesSum + dailyAverage * numDaysLeft;

          ExpenseSummary summary = m_dataRepo.GetExpenseSummary(u.Username) ?? new ExpenseSummary
          {
            UserId = u.Id,
            User = u,
            MonthAndYear = DateTime.Now,
            MonthBudget = u.Settings.MonthlyBudget
          };

          summary.MonthActual = monthSummary.TotalAmountTillEndOfDay;
          summary.MonthProjected = monthSummary.EndOfMonthProjection;

          m_dataRepo.Save(summary);

          Log.ToLoggerAndConsole("Updated monthly forecasts for user: " + u.Username);

        }
        catch (Exception ex)
        {
          Log.ToLoggerAndConsole("Unable to update month summary for: " + u.Username, ex);
          m_result = new ProcessResult(ECode.Error, ex);
        }
      }

      return m_result;
    }

    #endregion Overrides of AbstractRunOnceDailyJob
  }
}