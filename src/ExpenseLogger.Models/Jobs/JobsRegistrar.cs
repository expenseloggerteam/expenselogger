﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Quartz;

namespace ExpenseLogger.Models.Jobs
{
  /// <summary>
  ///   A job registrar for registering jobs with the Quartz scheduler
  /// </summary>
  public static class JobsRegistrar
  {
    private static int JOB_COUNT = 1;

    /// <summary>
    ///   Schedules all known jobs with the given Quartz scheduler
    /// </summary>
    /// <param name="scheduler">Quartz scheduler to register jobs with</param>
    public static void RegisterWith(IScheduler scheduler)
    {
      Type[] jobTypes = {
        typeof(ExpensesForecasterJob)
      };

      Array.ForEach(jobTypes, type =>
      {
        IJobDetail job = JobBuilder.Create(type).WithIdentity(type.Name).Build();

        ITrigger trigger = CreateTriggerForGroup();

        scheduler.ScheduleJob(job, trigger);
      });
    }

    /// <summary>
    ///   Creates an hourly trigger for given group
    /// </summary>
    /// <returns>Created trigger</returns>
    private static ITrigger CreateTriggerForGroup()
    {
      var trigger = TriggerBuilder.Create()
        .WithIdentity("hourly-trigger-" + Guid.NewGuid())
        .StartAt(DateTimeOffset.Now.AddMinutes(JOB_COUNT))
        .WithSimpleSchedule(f => f.WithIntervalInHours(1).RepeatForever())
        .Build();

      JOB_COUNT++;

      return trigger;
    }
  }
}