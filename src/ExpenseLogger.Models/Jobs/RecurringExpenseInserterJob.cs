﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Mail;
using ExpenseLogger.Models.Security;
using Quartz;

namespace ExpenseLogger.Models.Jobs
{
  /// <summary>
  ///   A job to insert recurring expenses
  /// </summary>
  [DisallowConcurrentExecution]
  public class RecurringExpenseInserterJob : AbstractRunOnceDailyJob
  {
    #region Attributes & Ctor

    private readonly IDataRepository m_expensesRepo;
    private readonly IEmailService m_emailer;
    private readonly IUserRepository m_userProfileRepo;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="isEnabled">Flag indicating whether this task is enabled</param>
    /// <param name="repository">Expenses repository</param>
    /// <param name="userProfileRepo">User profile repository</param>
    /// <param name="emailer">Emailer to send emails</param>
    public RecurringExpenseInserterJob(bool isEnabled, IDataRepository repository, IUserRepository userProfileRepo, IEmailService emailer)
      : base(isEnabled)
    {
      m_expensesRepo = repository;
      m_emailer = emailer;
      m_userProfileRepo = userProfileRepo;
    }

    #endregion Attributes & Ctor

    #region Overrides of AbstractRunOnceDailyJob

    /// <inheritdoc />
    public override ProcessResult ExecuteJob(IJobExecutionContext context)
    {
      // TODO:
      // nothing to do yet since the functionality is not yet available in main application
      Log.ToLoggerAndConsole("Nothing to do yet");

      return new ProcessResult();
    }

    #endregion Overrides of AbstractRunOnceDailyJob
  }
}