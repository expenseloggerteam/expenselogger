// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Mail;
using ExpenseLogger.Models.Security;
using Quartz;

namespace ExpenseLogger.Models.Jobs
{
  /// <summary>
  ///   This job generates and sends the reminder emails to users
  /// </summary>
  [DisallowConcurrentExecution]
  public class ReminderEmailerJob : AbstractRunOnceDailyJob
  {
    #region Attributes & Ctor

    private readonly IDataRepository m_expensesRepo;
    private readonly IEmailService m_emailer;
    private readonly IUserRepository m_userProfileRepo;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="isEnabled">Flag indicating whether this task is enabled</param>
    /// <param name="repository">Expenses repository</param>
    /// <param name="userProfileRepo">User profile repository</param>
    /// <param name="emailer">Emailer to send emails</param>
    public ReminderEmailerJob(bool isEnabled, IDataRepository repository, IUserRepository userProfileRepo, IEmailService emailer)
      : base(isEnabled)
    {
      m_expensesRepo = repository;
      m_emailer = emailer;
      m_userProfileRepo = userProfileRepo;
    }

    #endregion Attributes & Ctor

    #region IJob members

    /// <inheritdoc />
    public override ProcessResult ExecuteJob(IJobExecutionContext context)
    {
      ProcessResult result = new ProcessResult();

      // get all users
      IEnumerable<User> users = m_userProfileRepo.GetAllUsers();
      List<Reminder> reminders = new List<Reminder>();

      foreach (User user in users)
      {
        DateTime limit = DateTime.Now.Date;

        // always add today's reminders by default
        reminders.AddRange(m_expensesRepo.GetByUser<Reminder>(user.Username).Where(r => r.Date.Date == limit));

        UserSetting settings = user.Settings;
        limit = limit.AddDays((int)settings.ReminderDays);
        reminders.AddRange(m_expensesRepo.GetByUser<Reminder>(user.Username).Where(r => r.Date.Date == limit));

        // send reminder emails
        foreach (Reminder r in reminders)
        {
          try
          {
            if (!r.SendEmail)
              continue;

            AbstractEmail email = new ReminderAutoEmail(r);
            m_emailer.SendMail(user.Email, email);

            Log.ToLoggerAndConsole("Email sent successfully. Reminder id: " + r.Id + ", User email: " + user.Email + ".");
          }
          catch (Exception ex)
          {
            string error = "Unable to send email. Reminder id: " + r.Id + ", User email: " + user.Email + ".";
            Log.ToLoggerAndConsole(error, ex);
            result = new ProcessResult(ECode.Error, ex);
          }
        }

        if (!reminders.Any())
          Log.ToLoggerAndConsole("No reminders for user: " + user.Username);
      }

      return result;
    }

    #endregion IJob members
  }
}