﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ExpenseLogger.Core;
using Newtonsoft.Json;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   Denotes a category
  /// </summary>
  [Table("Categories")]
  public class Category : AbstractEntity, IUserReference
  {
#pragma warning disable 1591
    //private ICollection<Expense> m_expenses;

    public int UserId { get; set; }

    [Required(AllowEmptyStrings = false)]
    public string Name { get; set; }

    //public virtual ICollection<Expense> Expenses
    //{
    //  get { return m_expenses ?? new HashSet<Expense>(); }
    //  set { m_expenses = value; }
    //}

    [JsonIgnore]
    [ForeignKey("UserId")]
    public virtual User User { get; set; }
#pragma warning restore 1591
  }
}