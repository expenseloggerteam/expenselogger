﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using ExpenseLogger.Core;
using FluentValidation;
using Newtonsoft.Json;
using SQLite.CodeFirst;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   Represents a user profile
  /// </summary>
  [Table("Users")]
  public class User : AbstractEntity
  {
    private UserSetting m_userSettings;

#pragma warning disable 1591
    [Required]
    [Unique(OnConflictAction.Fail)]
    public string Username { get; set; }

    [Required(ErrorMessage = "The Password field is required")]
    [DataType(DataType.Password)]
    public string PasswordHash { get; set; }

    [Required]
    public string FullName { get; set; }

    [Required]
    [Unique(OnConflictAction.Fail)]
    public string Email { get; set; }

    [Required(ErrorMessage = "You must select a country")]
    public string Country { get; set; }

    [Required]
    [DisplayName("Secret Question")]
    public string SecretQuestion { get; set; }

    [Required]
    [DisplayName("Secret Answer")]
    public string SecretAnswer { get; set; }

    public DateTime MemberSince { get; set; }

    [DisplayName("Last Logged In")]
    public DateTime LastLoginDate { get; set; }

    public string SettingsJson { get; set; }

    public bool IsAdmin { get; set; }

    public bool IsVerified { get; set; }

    [NotMapped]
    public UserSetting Settings
    {
      get
      {
        if (m_userSettings != null)
          return m_userSettings;

        try
        {
          m_userSettings = JsonConvert.DeserializeObject<UserSetting>(SettingsJson);
        }
        catch (Exception)
        {
          m_userSettings = UserSetting.DefaultUserSettings;
        }

        return m_userSettings;
      }
    }

#pragma warning restore 1591
  }

  /// <summary>
  ///   <see cref="User" /> fluent validator
  /// </summary>
  public class UserValidator : AbstractValidator<User>
  {
    private const int MinPasswordLength = 8;

    /// <summary>
    ///   Constructor
    /// </summary>
    public UserValidator()
    {
      RuleFor(f => f.PasswordHash)
        .Cascade(CascadeMode.StopOnFirstFailure)
        .Must(ShouldBeGreaterThanMinLength)
        .WithMessage("Password must be atleast 8 characters")
        .Must(HaveAtleastOneNonAlphanumeric)
        .WithMessage("Password must have atleast 1 non-alphanumeric character");

      RuleFor(f => f.Email)
        .EmailAddress()
        .WithMessage("Invalid email address given");
    }

    /// <summary>
    ///   Password string must have atleast one non alphanumeric character
    /// </summary>
    /// <param name="pwd"></param>
    private bool HaveAtleastOneNonAlphanumeric(string pwd)
    {
      if (string.IsNullOrEmpty(pwd))
        return true;

      int numNonAlphaNumericChars = pwd.Where((t, i) => !char.IsLetterOrDigit(pwd, i)).Count();

      return numNonAlphaNumericChars >= 1;
    }

    /// <summary>
    ///   Password string should be greater than <see cref="MinPasswordLength" /> characters
    /// </summary>
    /// <param name="pwd"></param>
    private bool ShouldBeGreaterThanMinLength(string pwd)
    {
      if (string.IsNullOrEmpty(pwd))
        return true;

      return pwd.Length >= MinPasswordLength;
    }
  }
}