﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ExpenseLogger.Core;
using Newtonsoft.Json;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   Denotes a reminder
  /// </summary>
  [Table("Reminders")]
  public class Reminder : AbstractEntity, IUserReference
  {
    /// <summary>
    ///   Reminder date
    /// </summary>
    [Required(ErrorMessage = "You really need to set a date for your reminder!")]
    public DateTime Date { get; set; }

    /// <summary>
    ///   Reminder description
    /// </summary>
    [Required(AllowEmptyStrings = false, ErrorMessage = "A reminder without any description just doesn't make any sense.")]
    public string Description { get; set; }

    /// <summary>
    ///   Whether to send an email alert for this reminder
    /// </summary>
    [DisplayName("Receive email?")]
    public bool SendEmail { get; set; }

    /// <summary>
    ///   Id of user whose reminder this is
    /// </summary>
    public int UserId { get; set; }

    /// <summary>
    ///   Associated user profile
    /// </summary>
    [JsonIgnore]
    [ForeignKey("UserId")]
    public virtual User User { get; set; }
  }
}