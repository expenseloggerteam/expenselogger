﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   A generic disposable
  /// </summary>
  public class Disposable : IDisposable
  {
    /// <summary>
    ///   Whether the object is already disposed
    /// </summary>
    public bool IsDisposed { get; private set; }

    #region Implementation of IDisposable

    /// <summary>
    ///   Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      if (IsDisposed)
        throw new ObjectDisposedException(string.Empty, "Object has already been disposed by runtime");

      IsDisposed = true;
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    #endregion

    /// <summary>
    ///   Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    /// <param name="disposing">
    ///   True if we are disposing resources due to an explicit call to
    ///   <see cref="M:IDisposable.Dispose" />, False if the method was called by the garbage collector
    /// </param>
    protected virtual void Dispose(bool disposing)
    {
      // nothing to do here
    }
  }
}