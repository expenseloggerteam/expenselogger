﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using WebExtras.Core;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   Available reminders days options
  /// </summary>
  public enum EReminderDays : byte
  {
#pragma warning disable 1591
    [StringValue("Same day")]
    SameDay = 0,

    [StringValue("4 days prior & on same day")]
    SameDayAnd4DaysPrior = 4,

    [StringValue("7 days prior & on same day")]
    SameDayAnd7DaysPrior = 7
#pragma warning restore 1591
  }
}