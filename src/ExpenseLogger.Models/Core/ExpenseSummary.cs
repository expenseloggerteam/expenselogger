﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ExpenseLogger.Core;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   Denotes expense summaries
  /// </summary>
  [Table("ExpenseSummaries")]
  public class ExpenseSummary : AbstractEntity
  {
#pragma warning disable 1591
    public int UserId { get; set; }

    public DateTime MonthAndYear { get; set; }

    public int MonthBudget { get; set; }

    public double MonthActual { get; set; }

    public double MonthProjected { get; set; }

    public double YearActual { get; set; }

    public double YearProjected { get; set; }

    [ForeignKey("UserId")]
    public virtual User User { get; set; }

    public ExpenseSummary()
    {
      MonthBudget = AppConstants.DefaultInt32NotSetValue;
    }

#pragma warning restore 1591
  }
}