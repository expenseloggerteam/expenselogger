﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   Denotes a data summary for a month
  /// </summary>
  public class MonthSummary
  {
    /// <summary>
    ///   The day of the month and year for this summary
    /// </summary>
    public DateTime Day { get; set; }

    /// <summary>
    /// Daily average
    /// </summary>
    public double DailyAverage { get; set; }

    /// <summary>
    ///   Total amount spent in the month upto the day specified by <see cref="Day"/>
    /// </summary>
    public double TotalAmountTillEndOfDay { get; set; }

    /// <summary>
    ///   An end of month total amount projected value
    /// </summary>
    public double EndOfMonthProjection { get; set; }
  }
}