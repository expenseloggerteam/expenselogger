﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using ExpenseLogger.Core;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   A fake <see cref="IDbSet{TEntity}" />
  /// </summary>
  /// <typeparam name="TEntity">Entity type"/></typeparam>
  public class InMemoryDbSet<TEntity> : IDbSet<TEntity> where TEntity : AbstractEntity
  {
    private static int CURRENT_ID;
    private static readonly ObservableCollection<TEntity> Data = new ObservableCollection<TEntity>();
    private readonly IQueryable m_query;

    /// <summary>
    ///   Internal static constructor
    /// </summary>
    static InMemoryDbSet()
    {
      CURRENT_ID = 1;
    }

    /// <summary>
    ///   Constructor
    /// </summary>
    public InMemoryDbSet()
    {
      m_query = Data.AsQueryable();
    }

    /// <summary>
    ///   Event raised when an entity is added/removed
    /// </summary>
    public EventHandler OnSetChanged;

    #region Implementation of IEnumerable

    /// <summary>Returns an enumerator that iterates through the collection.</summary>
    /// <returns>
    ///   A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the
    ///   collection.
    /// </returns>
    public IEnumerator<TEntity> GetEnumerator()
    {
      return Data.GetEnumerator();
    }

    /// <summary>Returns an enumerator that iterates through a collection.</summary>
    /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    #endregion Implementation of IEnumerable

    #region Implementation of IQueryable

    /// <summary>Gets the expression tree that is associated with the instance of <see cref="T:System.Linq.IQueryable" />.</summary>
    /// <returns>
    ///   The <see cref="T:System.Linq.Expressions.Expression" /> that is associated with this instance of
    ///   <see cref="T:System.Linq.IQueryable" />.
    /// </returns>
    public Expression Expression
    {
      get { return m_query.Expression; }
    }

    /// <summary>
    ///   Gets the type of the element(s) that are returned when the expression tree associated with this instance of
    ///   <see cref="T:System.Linq.IQueryable" /> is executed.
    /// </summary>
    /// <returns>
    ///   A <see cref="T:System.Type" /> that represents the type of the element(s) that are returned when the
    ///   expression tree associated with this object is executed.
    /// </returns>
    public Type ElementType
    {
      get { return m_query.ElementType; }
    }

    /// <summary>Gets the query provider that is associated with this data source.</summary>
    /// <returns>The <see cref="T:System.Linq.IQueryProvider" /> that is associated with this data source.</returns>
    public IQueryProvider Provider
    {
      get { return m_query.Provider; }
    }

    #endregion  Implementation of IQueryable

    #region Implementation of IDbSet<TEntity>

    /// <summary>
    ///   Finds an entity with the given primary key values.
    ///   If an entity with the given primary key values exists in the context, then it is
    ///   returned immediately without making a request to the store.  Otherwise, a request
    ///   is made to the store for an entity with the given primary key values and this entity,
    ///   if found, is attached to the context and returned.  If no entity is found in the
    ///   context or the store, then null is returned.
    /// </summary>
    /// <remarks>
    ///   The ordering of composite key values is as defined in the EDM, which is in turn as defined in
    ///   the designer, by the Code First fluent API, or by the DataMember attribute.
    /// </remarks>
    /// <param name="keyValues"> The values of the primary key for the entity to be found. </param>
    /// <returns> The entity found, or null. </returns>
    public TEntity Find(params object[] keyValues)
    {
      return this.SingleOrDefault(f => f.Id == (int)keyValues.Single());
    }

    /// <summary>
    ///   Adds the given entity to the context underlying the set in the Added state such that it will
    ///   be inserted into the database when SaveChanges is called.
    /// </summary>
    /// <param name="entity"> The entity to add. </param>
    /// <returns> The entity. </returns>
    /// <remarks>
    ///   Note that entities that are already in the context in some other state will have their state set
    ///   to Added.  Add is a no-op if the entity is already in the context in the Added state.
    /// </remarks>
    public TEntity Add(TEntity entity)
    {
      if (entity.Id < 1)
        entity.Id = CURRENT_ID++;

      Data.Add(entity);

      if (OnSetChanged != null)
        OnSetChanged(this, new EventArgs());

      return entity;
    }

    /// <summary>
    ///   Marks the given entity as Deleted such that it will be deleted from the database when SaveChanges
    ///   is called.  Note that the entity must exist in the context in some other state before this method
    ///   is called.
    /// </summary>
    /// <param name="entity"> The entity to remove. </param>
    /// <returns> The entity. </returns>
    /// <remarks>
    ///   Note that if the entity exists in the context in the Added state, then this method
    ///   will cause it to be detached from the context.  This is because an Added entity is assumed not to
    ///   exist in the database such that trying to delete it does not make sense.
    /// </remarks>
    public TEntity Remove(TEntity entity)
    {
      Data.Remove(entity);

      if (OnSetChanged != null)
        OnSetChanged(this, new EventArgs());

      return entity;
    }

    /// <summary>
    ///   Attaches the given entity to the context underlying the set.  That is, the entity is placed
    ///   into the context in the Unchanged state, just as if it had been read from the database.
    /// </summary>
    /// <param name="entity"> The entity to attach. </param>
    /// <returns> The entity. </returns>
    /// <remarks>
    ///   Attach is used to repopulate a context with an entity that is known to already exist in the database.
    ///   SaveChanges will therefore not attempt to insert an attached entity into the database because
    ///   it is assumed to already be there.
    ///   Note that entities that are already in the context in some other state will have their state set
    ///   to Unchanged.  Attach is a no-op if the entity is already in the context in the Unchanged state.
    /// </remarks>
    public TEntity Attach(TEntity entity)
    {
      Data.Add(entity);
      return entity;
    }

    /// <summary>
    ///   Creates a new instance of an entity for the type of this set.
    ///   Note that this instance is NOT added or attached to the set.
    ///   The instance returned will be a proxy if the underlying context is configured to create
    ///   proxies and the entity type meets the requirements for creating a proxy.
    /// </summary>
    /// <returns> The entity instance, which may be a proxy. </returns>
    public TEntity Create()
    {
      return Activator.CreateInstance<TEntity>();
    }

    /// <summary>
    ///   Creates a new instance of an entity for the type of this set or for a type derived
    ///   from the type of this set.
    ///   Note that this instance is NOT added or attached to the set.
    ///   The instance returned will be a proxy if the underlying context is configured to create
    ///   proxies and the entity type meets the requirements for creating a proxy.
    /// </summary>
    /// <typeparam name="TDerivedEntity"> The type of entity to create. </typeparam>
    /// <returns> The entity instance, which may be a proxy. </returns>
    public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, TEntity
    {
      return Activator.CreateInstance<TDerivedEntity>();
    }

    /// <summary>
    ///   Gets an <see cref="T:System.Collections.ObjectModel.ObservableCollection`1" /> that represents a local view of all
    ///   Added, Unchanged,
    ///   and Modified entities in this set.  This local view will stay in sync as entities are added or
    ///   removed from the context.  Likewise, entities added to or removed from the local view will automatically
    ///   be added to or removed from the context.
    /// </summary>
    /// <remarks>
    ///   This property can be used for data binding by populating the set with data, for example by using the Load
    ///   extension method, and then binding to the local data through this property.  For WPF bind to this property
    ///   directly.  For Windows Forms bind to the result of calling ToBindingList on this property
    /// </remarks>
    /// <value> The local view. </value>
    public ObservableCollection<TEntity> Local
    {
      get { return Data; }
    }

    #endregion Implementation of IDbSet<TEntity>
  }
}