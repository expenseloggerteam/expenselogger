﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   All available payment types
  /// </summary>
  public enum EPayment
  {
#pragma warning disable 1591
    Unknown = 0,
    Cash = 1,
    CreditCard = 2,
    DebitCard = 3,
    BankTransfer = 4
#pragma warning restore 1591
  }
}