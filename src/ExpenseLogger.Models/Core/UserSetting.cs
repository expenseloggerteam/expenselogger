﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ExpenseLogger.Core;
using FluentValidation;
using FluentValidation.Attributes;

namespace ExpenseLogger.Models.Core
{
  /// <summary>
  ///   Denotes user settings
  /// </summary>
  [Validator(typeof(UserSettingValidator))]
  public class UserSetting
  {
    /// <summary>
    ///   Default user settings
    /// </summary>
    public static readonly UserSetting DefaultUserSettings = new UserSetting
    {
      Currency = ECurrency.Money,
      PaymentType = EPayment.Unknown,
      ReminderDays = EReminderDays.SameDay
    };

#pragma warning disable 1591
    [Required]
    public ECurrency Currency { get; set; }

    public int MonthlyBudget { get; set; }

    public EPayment PaymentType { get; set; }

    public EReminderDays ReminderDays { get; set; }
#pragma warning restore 1591
  }

  /// <summary>
  ///   A <see cref="UserSetting" /> fluent validator
  /// </summary>
  public class UserSettingValidator : AbstractValidator<UserSetting>
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    public UserSettingValidator()
    {
      RuleFor(f => f.MonthlyBudget)
        .Cascade(CascadeMode.StopOnFirstFailure)
        .NotEmpty()
        .WithMessage("Monthly Budget must be greater than 0")
        .GreaterThan(0)
        .WithMessage("Monthly Budget must be greater than 0");

      RuleFor(f => f.PaymentType)
        .NotEqual(EPayment.Unknown)
        .WithMessage("You can't set unknown as your default payment type");
    }
  }
}