﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ExpenseLogger.Models.Mail;
using FluentValidation;
using FluentValidation.Attributes;
using WebExtras.Core;

namespace ExpenseLogger.Models.Mail
{
  /// <summary>
  ///   Email service settings
  /// </summary>
  [Validator(typeof(EmailSettingsValidator))]
  public sealed class EmailSettings
  {
#pragma warning disable 1591
    [Required]
    public string Username { get; set; }

    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; }

    [Required]
    [DisplayName("Server")]
    public string Host { get; set; }

    [Required]
    [DataType(DataType.PhoneNumber)]
    public int Port { get; set; }

    public bool UseSsl { get; set; }

    [Required]
    [DataType(DataType.EmailAddress)]
    [DisplayName("System admin email")]
    public string SystemEmail { get; set; }

    [Required]
    [DisplayName("Webmaster name")]
    public string WebmasterName { get; set; }
#pragma warning restore 1591

    /// <summary>
    /// Converts current email settings to a dictionary
    /// </summary>
    /// <returns>Current email settings as a dictionary</returns>
    public IDictionary<string, string> ToDictionary()
    {
      Dictionary<string, string> dict = new Dictionary<string, string>();
      dict["Server/Host"] = Host + ":" + Port;
      dict["User"] = Username;
      dict["Password"] = Password;
      dict["Use SSL"] = UseSsl ? "Yes" : "No";
      dict["System email"] = SystemEmail;
      dict["Webmaster name"] = WebmasterName;

      return dict;
    }

    /// <summary>
    /// Dummy email settings
    /// </summary>
    public static readonly EmailSettings DummySettings = new EmailSettings
    {
      Host = "localhost",
      Port = 21,
      Username = "test",
      Password = "test",
      UseSsl = true,
      SystemEmail = "admin@example.com",
      WebmasterName = "ExpenseLogger Administrator"
    };
  }

  /// <summary>
  ///   A fluent validator for <see cref="EmailSettings" />
  /// </summary>
  public class EmailSettingsValidator : AbstractValidator<EmailSettings>
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    public EmailSettingsValidator()
    {
      RuleFor(f => f.Port)
        .GreaterThan(0)
        .WithMessage("Email server's port number must be greater than 0.");

      RuleFor(f => f.SystemEmail)
        .EmailAddress();

    }
  }
}