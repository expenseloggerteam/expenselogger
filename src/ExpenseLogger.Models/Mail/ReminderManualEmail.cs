﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;

namespace ExpenseLogger.Models.Mail
{
  /// <summary>
  ///   An email template for manually sending a reminder email
  /// </summary>
  public class ReminderManualEmail : AbstractEmail
  {
    private readonly Reminder m_reminder;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="reminder">Reminder for which to generate email</param>
    public ReminderManualEmail(Reminder reminder)
    {
      m_reminder = reminder;
    }

    #region Overrides of AbstractEmail

    /// <inheritdoc />
    public override string Subject
    {
      get { return "ExpenseLogger System: Reminder for " + m_reminder.Date.ToString(AppConstants.DisplayDateTimeFormat); }
    }

    /// <inheritdoc />
    protected override string GetBody()
    {
      return string.Format("Dear {0}" +
                           "<br>" +
                           "<br>You asked us to send you an email for a reminder" +
                           "<br>Reminder details:" +
                           "<br>{1}" +
                           "<br>" +
                           "<br>Regards," +
                           "<br>ExpenseLogger Webmaster", m_reminder.User.FullName, m_reminder.Description);
    }

    #endregion Overrides of AbstractEmail
  }
}