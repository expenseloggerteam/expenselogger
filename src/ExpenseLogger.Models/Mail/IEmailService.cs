﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using ExpenseLogger.Core;

namespace ExpenseLogger.Models.Mail
{
  /// <summary>
  ///   A default interface for all email services
  /// </summary>
  public interface IEmailService : IDisposable
  {
    /// <summary>
    ///   Send email
    /// </summary>
    /// <param name="toAddress">Address to which to send email</param>
    /// <param name="email">Email to send</param>
    /// <returns>Process result</returns>
    ProcessResult SendMail(string toAddress, AbstractEmail email);

    /// <summary>
    ///   Send email (asynchronous)
    /// </summary>
    /// <param name="toAddress">Address to which to send email</param>
    /// <param name="email">Email to send</param>
    /// <returns>Process result</returns>
    Task<ProcessResult> SendMailAsync(string toAddress, AbstractEmail email);
  }
}