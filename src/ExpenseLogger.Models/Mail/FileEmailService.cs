﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using ExpenseLogger.Core;
using Newtonsoft.Json;

namespace ExpenseLogger.Models.Mail
{
  /// <summary>
  ///   A raw file based email service which writes the email content to disk
  /// </summary>
  public class FileEmailService : AbstractEmailService
  {
    private static readonly string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="settings">Email settings</param>
    public FileEmailService(EmailSettings settings)
      : base(settings)
    {
      // nothing to do here
    }

    #region Implementation of IEmailService

    /// <inheritdoc />
    public override ProcessResult SendMail(string toAddress, AbstractEmail email)
    {
      string[] parts =
      {
        AppDataPath,
        DateTime.Now.ToString(AppConstants.DisplayDateTimeFormat),
        toAddress + DateTime.Now.ToString(AppConstants.IsoDateTimeFormat) + ".json"
      };

      string file = string.Join("\\", parts);

      File.WriteAllText(file, JsonConvert.SerializeObject(new {toAddress, email}));

      return new ProcessResult();
    }

    /// <inheritdoc />
    public override void Dispose()
    {
      // nothing to do
    }

    #endregion
  }
}