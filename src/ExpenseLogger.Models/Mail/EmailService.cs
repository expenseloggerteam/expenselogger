﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Net;
using ExpenseLogger.Core;
using MailKit.Net.Smtp;
using MimeKit;

namespace ExpenseLogger.Models.Mail
{
  /// <summary>
  ///   A generic email service
  /// </summary>
  public class EmailService : AbstractEmailService
  {
    private static readonly string[] RandomConfigMessages =
    {
      "May be the email service is configured wrong?",
      "Are you sure the email server is online?",
      "I got kicked even before I could start :("
    };

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="settings">Email settings</param>
    public EmailService(EmailSettings settings)
      : base(settings)
    {
      // nothing to do here
    }

    /// <summary>
    ///   Create a <see cref="MimeMessage" /> for delivery
    /// </summary>
    /// <param name="toAddress">Address to which to send email</param>
    /// <param name="email">Email to send</param>
    /// <returns>Created message</returns>
    private MimeMessage CreateMessage(string toAddress, AbstractEmail email)
    {
      BodyBuilder builder = new BodyBuilder {HtmlBody = email.Body};

      MimeMessage message = new MimeMessage();

      var fromAddress = new MailboxAddress(Settings.WebmasterName, Settings.SystemEmail);

      message.From.Add(fromAddress);
      message.To.Add(new MailboxAddress(string.Empty, toAddress));
      message.Bcc.Add(fromAddress);
      message.Subject = email.Subject;
      message.Body = builder.ToMessageBody();

      return message;
    }

    #region Implementation of IEmailService

    /// <inheritdoc />
    public override ProcessResult SendMail(string toAddress, AbstractEmail email)
    {
      ProcessResult result = new ProcessResult();
      MimeMessage message = CreateMessage(toAddress, email);

      try
      {
        using (var client = new SmtpClient())
        {
          client.Connect(Settings.Host, Settings.Port);
          client.Authenticate(new NetworkCredential(Settings.Username, Settings.Password));
          client.Send(message);
          client.Disconnect(true);
        }

        Log.Info("Email sent to: " + toAddress);
      }
      catch (Exception ex)
      {
        string msg = "Failed to send email to: " + toAddress + ". " + RandomConfigMessages[new Random(DateTime.Now.Millisecond).Next(0, RandomConfigMessages.Length)];
        Log.Error(msg, ex);
        result = new ProcessResult(ECode.Error, msg);
      }

      return result;
    }

    /// <inheritdoc />
    public override void Dispose()
    {
      // nothing to do
    }

    #endregion Implementation of IEmailService
  }
}