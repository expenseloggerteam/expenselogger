﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using log4net;

namespace ExpenseLogger.Models.Mail
{
  /// <summary>
  /// A hub email service which can send email via multiple email services
  /// </summary>
  public class HubEmailService : IEmailService
  {
    /// <summary>
    /// log4net logger
    /// </summary>
    private static readonly ILog Log = Log4NetHelper.GetLogger<HubEmailService>();

    private readonly IEnumerable<IEmailService> m_emailers;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="emailers">A collection of email services</param>
    public HubEmailService(IEnumerable<IEmailService> emailers)
    {
      if (emailers == null)
        throw new ArgumentNullException(nameof(emailers));

      m_emailers = emailers;
    }

    #region Implementation of IEmailService

    /// <inheritdoc />
    public ProcessResult SendMail(string toAddress, AbstractEmail email)
    {
      ProcessResult result = new ProcessResult();

      foreach (var svc in m_emailers)
      {
        try
        {
          svc.SendMail(toAddress, email);
        }
        catch (Exception e)
        {
          Log.Error(svc.GetType().Name + " failed.", e);
          result = new ProcessResult(ECode.Warning, "Atleast one configured email service failed. See logs for more details");
        }
      }

      return result;
    }

    /// <inheritdoc />
    public Task<ProcessResult> SendMailAsync(string toAddress, AbstractEmail email)
    {
      return Task.Run(() => SendMail(toAddress, email));
    }

    #endregion Implementation of IEmailService

    #region Implementation of IDisposable

    /// <inheritdoc />
    public void Dispose()
    {
      if (!m_emailers.Any())
        return;

      Array.ForEach(m_emailers.ToArray(), f => f.Dispose());
    }

    #endregion
  }
}
