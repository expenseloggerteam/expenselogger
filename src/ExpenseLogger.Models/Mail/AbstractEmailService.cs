﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using log4net;

namespace ExpenseLogger.Models.Mail
{
  /// <summary>
  ///   An abstract <see cref="IEmailService" />
  /// </summary>
  public abstract class AbstractEmailService : IEmailService
  {
    /// <summary>
    /// log4net logger
    /// </summary>
    protected static readonly ILog Log = Log4NetHelper.GetLogger<AbstractEmailService>();

    /// <summary>
    ///   Email settings
    /// </summary>
    public EmailSettings Settings { get; private set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="settings">Email settings</param>
    protected AbstractEmailService(EmailSettings settings)
    {
      Settings = settings;
    }

    #region Implementation of IEmailService

    /// <inheritdoc />
    public abstract ProcessResult SendMail(string toAddress, AbstractEmail email);

    /// <inheritdoc />
    public Task<ProcessResult> SendMailAsync(string toAddress, AbstractEmail email)
    {
      return Task.Run(() => SendMail(toAddress, email));
    }

    #endregion Implementation of IEmailService

    #region Implementation of IDisposable

    /// <inheritdoc />
    public abstract void Dispose();

    #endregion
  }
}