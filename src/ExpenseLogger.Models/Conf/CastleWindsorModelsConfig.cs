﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Extensions.Registration;
using ExpenseLogger.Core;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Jobs;
using ExpenseLogger.Models.Mail;
using ExpenseLogger.Models.Security;
using Quartz.Job;

namespace ExpenseLogger.Models.Conf
{
  /// <summary>
  ///   Castle windsor configuration
  /// </summary>
  public static class CastleWindsorModelsConfig
  {
    /// <summary>
    ///   Registers components with the castle windsor container
    /// </summary>
    /// <param name="container">Windsor container to register components with</param>
    /// <returns>Configuration properties</returns>
    public static ConfigProperty Configure(IWindsorContainer container)
    {
      container.Kernel.AddHandlerSelector(new EmailServiceHandlerSelector());

      container.Register(Component.For<IKernel>().Named("kernel").Instance(container.Kernel).OnlyNewServices());

      container.Register(PropertyResolvingComponent.For<ConfigProperty>()
        .DependsOnProperties(ResolvablePropertyUtil.From<ConfigProperty>()));

      container.Register(Component.For<IDataContextFactory>()
        .ImplementedBy<DataContextFactory>()
        .DependsOn(Dependency.OnComponent<ConfigProperty, ConfigProperty>()));

      container.Register(Component.For<IDataRepository>()
        .ImplementedBy<DefaultDataRepository>()
        .LifeStyle.Transient);

      container.Register(Component.For<IUserRepository>()
        .ImplementedBy<DefaultUserRepository>()
        .LifeStyle.Transient);

      container.Register(Component.For<EmailSettings>().UsingFactoryMethod(GetEmailSettings, true).LifeStyle.Transient);

      container.Register(Component.For<IEmailService>()
        .ImplementedBy<EmailService>()
        .LifeStyle.Transient);

      container.Register(Component.For<IEmailService>()
        .ImplementedBy<FileEmailService>()
        .LifeStyle.Transient);

      // register all quartz jobs
      container.Register(Component.For<FileScanJob>().LifeStyle.Transient);

      Property jobEnabledDependency = Dependency.OnValue("isEnabled", true);

      container.Register(Component.For<ReminderEmailerJob>()
        .DependsOn(jobEnabledDependency)
        .LifeStyle
        .Transient);

      container.Register(Component.For<CurrentMonthForecasterJob>()
        .DependsOn(jobEnabledDependency)
        .LifeStyle
        .Transient);

      container.Register(Component.For<ExpensesForecasterJob>()
        .DependsOn(jobEnabledDependency)
        .LifeStyle
        .Transient);

      container.Register(Component.For<IExcelFileProcessorFactory>().ImplementedBy<DefaultExcelFileProcessorFactory>().LifeStyle.Transient);

      ConfigProperty configProps = container.Resolve<ConfigProperty>();

      return configProps;
    }

    /// <summary>
    ///   Retrieves the email settings from the underlying data repository if available, else return the default dummy settings
    /// </summary>
    /// <param name="kernel">Current castle windsor kernel</param>
    /// <param name="model">Current component model</param>
    /// <param name="context">Current creation context</param>
    /// <returns>Email settings</returns>
    private static EmailSettings GetEmailSettings(IKernel kernel, ComponentModel model, CreationContext context)
    {
      var repo = kernel.Resolve<IDataRepository>();

      if (repo == null)
        return EmailSettings.DummySettings;

      EmailSettings settings = repo.GetSetting<EmailSettings>(ESystemSetting.Smtp);

      return settings ?? EmailSettings.DummySettings;
    }
  }
}