﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Net.Http;
using System.Reflection;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Castle.MicroKernel;
using Castle.MicroKernel.ComponentActivator;

namespace ExpenseLogger.Api.Models.Controllers
{
  /// <summary>
  ///   Castle Windsor controller factory which resolved api requests
  ///   to pertinent controllers
  /// </summary>
  public class WindsorApiControllerFactory : IHttpControllerActivator
  {
    /// <summary>
    ///   Windsor kernel object
    /// </summary>
    private readonly IKernel m_kernel;

    /// <summary>
    ///   Default constructor
    /// </summary>
    /// <param name="kernel">Castle Windsor kernel to be used to initialize</param>
    public WindsorApiControllerFactory(IKernel kernel)
    {
      m_kernel = kernel;
    }


    /// <summary>
    ///   Get an instance for the given controller
    /// </summary>
    /// <param name="request">Current HTTP request context</param>
    /// <param name="controllerDescriptor">controllerDescriptor</param>
    /// <param name="controllerType">
    ///   Type of controller object whose
    ///   instance is to be fetched
    /// </param>
    /// <returns>An instance of expected controller</returns>
    public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
    {
      if (controllerType == null)
      {
        string msg = string.Format("The api controller for url '{0}' could not be found", request.RequestUri.OriginalString);

        // Throw an HTTP exception rather than showing a Code exception
        throw new Exception(msg);
      }

      // resolve controller
      IHttpController controller;
      try
      {
        controller = (IHttpController)m_kernel.Resolve(controllerType);
      }
      catch (Exception ex)
      {
        Exception meaningfulException = ex;
        if (meaningfulException is ComponentActivatorException && meaningfulException.InnerException != null)
          meaningfulException = meaningfulException.InnerException;
        if (meaningfulException is TargetInvocationException && meaningfulException.InnerException != null)
          meaningfulException = meaningfulException.InnerException;
        throw new Exception(
          string.Format("Unable to process the request '{0} {1}' using controller '{2}'. Details: {3}", request.Method, request.RequestUri, controllerType.Name, meaningfulException.Message), ex);
      }

      // hook up mechanism to release the component from the kernel
      request.RegisterForDispose(
        new Release(() => m_kernel.ReleaseComponent(controller))
      );

      return controller;
    }


    /// <summary>
    ///   private class used to provide an action to run on dispose
    /// </summary>
    private class Release : IDisposable
    {
      /// <summary>
      ///   the action to run on dispose
      /// </summary>
      private readonly Action m_release;

      /// <summary>
      ///   ctor
      /// </summary>
      /// <param name="release">the action to run on dispose</param>
      public Release(Action release)
      {
        m_release = release;
      }

      /// <summary>
      ///   dispose the object and run the release action
      /// </summary>
      public void Dispose()
      {
        m_release();
      }
    }
  }
}