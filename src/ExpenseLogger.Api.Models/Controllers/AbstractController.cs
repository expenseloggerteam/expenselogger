﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using ExpenseLogger.Api.Models.Helpers;
using ExpenseLogger.Api.Models.Results;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Security;

namespace ExpenseLogger.Api.Models.Controllers
{
  /// <summary>
  ///   Abstract controller providing some basic utility methods
  /// </summary>
  public abstract class AbstractController : ApiController
  {
    /// <summary>
    ///   User repository
    /// </summary>
    protected IUserRepository UserRepo { get; private set; }

    /// <summary>
    ///   Data repository
    /// </summary>
    protected IDataRepository DataRepo { get; private set; }

    /// <summary>
    ///   Username of current request
    /// </summary>
    protected string Username { get; private set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="userRepo">User repository</param>
    /// <param name="dataRepo">Data repository</param>
    protected AbstractController(IUserRepository userRepo, IDataRepository dataRepo)
    {
      UserRepo = userRepo;
      DataRepo = dataRepo;
    }


    /// <summary>
    ///   Creates a http 'OK' success result for a get action.
    /// </summary>
    /// <param name="data">the returned MODEL</param>
    /// <returns>Success result</returns>
    protected IHttpActionResult Success<T>(T data)
    {
      return Content(
        HttpStatusCode.OK,
        new SuccessResult<T>
        {
          code = HttpStatusCode.OK,
          request = string.Format("{0} {1}", Request.Method, Request.RequestUri),
          result = data
        });
    }

    /// <summary>
    ///   Creates a http 'OK' success result for a get action.
    /// </summary>
    /// <param name="data">the returned results</param>
    /// <returns>Success result</returns>
    /// <typeparam name="T">the result data type</typeparam>
    protected IHttpActionResult Success<T>(IEnumerable<T> data)
    {
      return Content(
        HttpStatusCode.OK,
        new SuccessEnumerableResult<T>
        {
          code = HttpStatusCode.OK,
          request = string.Format("{0} {1}", Request.Method, Request.RequestUri),
          result = data
        });
    }

    /// <summary>
    ///   Creates a http 'NotFound' result.
    /// </summary>
    /// <param name="message">the error message</param>
    /// <param name="values">format arguments</param>
    /// <returns>NotFoundResult</returns>
    protected IHttpActionResult NotFound(string message, params object[] values)
    {
      message = string.Format(message, values);
      return Content(
          HttpStatusCode.NotFound,
          new NotFoundResult
          {
            error = new HttpErrorModel(message),
            request = string.Format("{0} {1}", Request.Method, Request.RequestUri)
          })
        .With(r => r.ReasonPhrase = message);
    }

    /// <summary>
    ///   Creates a http 'NoContent' result.
    /// </summary>
    /// <param name="message">the error message</param>
    /// <param name="values">format arguments</param>
    /// <returns>NoContentResult</returns>
    protected IHttpActionResult NoContent(string message, params object[] values)
    {
      message = string.Format(message, values);
      return Content(
          HttpStatusCode.NoContent,
          new NoContentResult
          {
            error = new HttpErrorModel(message),
            request = string.Format("{0} {1}", Request.Method, Request.RequestUri)
          })
        .With(r => r.ReasonPhrase = message);
    }


    /// <summary>
    ///   Creates a http 'Forbidden' result.
    /// </summary>
    /// <param name="message">the error message</param>
    /// <param name="values">format arguments</param>
    /// <returns>ForbiddenResult</returns>
    protected IHttpActionResult Forbidden(string message, params object[] values)
    {
      message = string.Format(message, values);
      return Content(
          HttpStatusCode.Forbidden,
          new ForbiddenResult
          {
            error = new HttpErrorModel(message),
            request = string.Format("{0} {1}", Request.Method, Request.RequestUri)
          })
        .With(r => r.ReasonPhrase = message);
    }

    /// <summary>
    ///   Populates the ModelState dictionary with a validation error and creates a http 'BadRequest' result.
    /// </summary>
    /// <param name="errorField">the field associated with the error</param>
    /// <param name="errorMessage">the error message</param>
    /// <param name="values">format arguments</param>
    /// <returns>BadRequestResult</returns>
    protected IHttpActionResult BadRequest(string errorField, string errorMessage, params object[] values)
    {
      ModelState.AddModelError(errorField, string.Format(errorMessage, values));
      return BadRequest();
    }

    /// <summary>
    ///   Populates the ModelState dictionary with a validation error and creates a http 'BadRequest' result.
    /// </summary>
    /// <param name="errorField">the field associated with the error</param>
    /// <param name="errorMessages">the error messages</param>
    /// <returns>BadRequestResult</returns>
    protected IHttpActionResult BadRequest(string errorField, IEnumerable<string> errorMessages)
    {
      ModelState modelState = new ModelState();
      foreach (string errorMessage in errorMessages)
      {
        modelState.Errors.Add(errorMessage);
      }
      ModelState.Add(errorField, modelState);
      return BadRequest();
    }

    /// <summary>
    ///   Creates a http 'BadRequest' result. Used for model validation errors.
    ///   Is populated from the ModelState dictionary.
    /// </summary>
    /// <returns>BadRequestResult</returns>
    protected new IHttpActionResult BadRequest()
    {
      HttpErrorModel error = new HttpErrorModel(ModelState);
      return Content(
          HttpStatusCode.BadRequest,
          new BadRequestResult
          {
            request = string.Format("{0} {1}", Request.Method, Request.RequestUri),
            error = error
          })
        .With(r => r.ReasonPhrase = error.message);
    }

    /// <summary>
    ///   Initializes the <see cref="T:System.Web.Http.ApiController" /> instance with the specified controllerContext.
    /// </summary>
    /// <param name="controllerContext">
    ///   The <see cref="T:System.Web.Http.Controllers.HttpControllerContext" /> object that is
    ///   used for the initialization.
    /// </param>
    protected override void Initialize(HttpControllerContext controllerContext)
    {
      base.Initialize(controllerContext);

      IEnumerable<string> values;
      if (controllerContext.Request.Headers.TryGetValues(ApiConstants.UsernameHeader.Name, out values))
        Username = values.FirstOrDefault();

      if (string.IsNullOrWhiteSpace(Username))
        throw new WebException("Required header: " + ApiConstants.UsernameHeader.Name + " not found in request");
    }
  }
}