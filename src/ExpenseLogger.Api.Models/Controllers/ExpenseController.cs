﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Net;
using System.Web.Http;
using ExpenseLogger.Api.Models.Results;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Security;
using Swashbuckle.Swagger.Annotations;

namespace ExpenseLogger.Api.Models.Controllers
{
  /// <summary>
  ///   Expenses controller
  /// </summary>
  [RoutePrefix("expenses")]
  [SwaggerResponse(HttpStatusCode.InternalServerError, Description = "Unhandled exception", Type = typeof(ErrorResult))]
  public class ExpenseController : AbstractController
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="userRepo">User repository</param>
    /// <param name="dataRepo">Data repository</param>
    public ExpenseController(IUserRepository userRepo, IDataRepository dataRepo)
      : base(userRepo, dataRepo)
    {
      // nothing to do here
    }

    /// <summary>
    ///   Get the summary for current month
    /// </summary>
    /// <returns>Current month summary data</returns>
    [Route("getSummary")]
    [SwaggerResponse(HttpStatusCode.OK, "Summary of current month")]
    public IHttpActionResult GetSummary()
    {
      return null;
    }
  }
}