﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using ExpenseLogger.Api.Models.Results;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Security;
using Swashbuckle.Swagger.Annotations;

namespace ExpenseLogger.Api.Models.Controllers
{
  /// <summary>
  ///   Categories controller
  /// </summary>
  [RoutePrefix("categories")]
  [SwaggerResponse(HttpStatusCode.InternalServerError, Description = "Unhandled exception", Type = typeof(ErrorResult))]
  public class CategoryController : AbstractController
  {
    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="userRepo">User repository</param>
    /// <param name="dataRepo">Data repository</param>
    public CategoryController(IUserRepository userRepo, IDataRepository dataRepo)
      : base(userRepo, dataRepo)
    {
      // nothing to do here
    }

    /// <summary>
    ///   Get categories
    /// </summary>
    /// <returns>A collection of categories</returns>
    [Route("")]
    [SwaggerResponse(HttpStatusCode.OK, Description = "Success", Type = typeof(IEnumerable<Category>))]
    [SwaggerResponse(HttpStatusCode.BadRequest, Description = "If the user is not found")]
    public IHttpActionResult GetCategories()
    {
      try
      {
        IEnumerable<Category> categories = DataRepo.GetCategoriesForUser(Username);

        return Success(categories);
      }
      catch (InvalidOperationException e)
      {
        return BadRequest(e.Message);
      }
    }
  }
}