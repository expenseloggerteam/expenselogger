﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using ExpenseLogger.Api.Models.Results;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using log4net;

namespace ExpenseLogger.Api.Models.Handlers
{
  /// <summary>
  ///   A exception handler used to return unhandled exceptions as a UnhandledExceptionResult object
  /// </summary>
  public class UnhandledExceptionHandler : IExceptionHandler
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof(UnhandledExceptionHandler));

    /// <summary>
    ///   Handle the exception, which will set the context.result as a UnhandledExceptionResult.
    /// </summary>
    /// <param name="context">exception context</param>
    /// <param name="cancellationToken">threading cancellationToken</param>
    public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
    {
      return Task.Run(() =>
      {
        Exception ex = context.Exception is AggregateException ? context.Exception.InnerException : context.Exception;

        Log.ToLoggerAndConsole(ELogLevel.Error, "An unhandled exception occurred", ex);

        context.Result = new UnhandledExceptionResult(context.Request, ex);
      }, cancellationToken);
    }
  }
}