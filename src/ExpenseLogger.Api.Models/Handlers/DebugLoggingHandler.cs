﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ExpenseLogger.Api.Models.Handlers
{
  /// <summary>
  ///   A handler used to display api requests in the console window
  /// </summary>
  public class DebugLoggingHandler : DelegatingHandler
  {
    /// <summary>
    ///   Execute the action
    /// </summary>
    /// <param name="request">the http request</param>
    /// <param name="cancellationToken">the cancellation token</param>
    /// <returns>a response message</returns>
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
      string requestUri = request.RequestUri.ToString();
      if (requestUri.IndexOf("/api/", StringComparison.OrdinalIgnoreCase) < 0) // only handle api requests (not swagger or nancy)
        return await base.SendAsync(request, cancellationToken);

      string requestInfo = string.Format("{0} {1}", request.Method, request.RequestUri);

      // write request debug string
      Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss} - Request: {1}", DateTime.Now, requestInfo);

      // get response
      HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

      // write response debug string
      Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss} - Response: {1} - {2} {3}", DateTime.Now, requestInfo, (int)response.StatusCode, response.StatusCode);
      return response;
    }
  }
}