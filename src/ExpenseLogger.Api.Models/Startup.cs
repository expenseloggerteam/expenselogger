﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.ExceptionHandling;
using Castle.Windsor;
using ExpenseLogger.Api.Models.Config;
using ExpenseLogger.Api.Models.Controllers;
using ExpenseLogger.Api.Models.Handlers;
using ExpenseLogger.Core.Json;
using Microsoft.Owin.Extensions;
using Newtonsoft.Json;
using Owin;
using Swashbuckle.Application;

namespace ExpenseLogger.Api.Models
{
  /// <summary>
  ///   Main startup class for the utility
  /// </summary>
  public class Startup
  {
    private readonly IWindsorContainer m_container;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="container">Castle windsor container</param>
    public Startup(IWindsorContainer container)
    {
      m_container = container;
    }

    /// <summary>
    ///   Configures the web server and api services
    /// </summary>
    /// <param name="appBuilder">Application builder configuration</param>
    public void Configuration(IAppBuilder appBuilder)
    {
      CastleWindsorConfig.Register(m_container);

      HttpConfiguration config = new HttpConfiguration();

      // use attribute routing (uses [Route] and [RoutePrefix] attributes on actions/controllers).
      config.MapHttpAttributeRoutes();

      // add unhandled exception handler
      config.Services.Replace(typeof(IExceptionHandler), new UnhandledExceptionHandler());

      // adds a debug message handler which writes requests received to console
      config.MessageHandlers.Add(new DebugLoggingHandler());

      // replace the default controller activator with windsor
      config.Services.Replace(typeof(IHttpControllerActivator), new WindsorApiControllerFactory(m_container.Kernel));

      // remove the XML formatter to support only JSON responses
      config.Formatters.Remove(config.Formatters.XmlFormatter);

      // use custom serialiser settings
      config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
      {
        ContractResolver = new LowerFirstCharacterContractResolver()
      };

      AbstractSwaggerPathProvider pathProvider = m_container.Resolve<AbstractSwaggerPathProvider>();

      // configure swagger documentation generation
      SwaggerConfig.Register(config, pathProvider);

      // Redirect root to Swagger UI
      config.Routes.MapHttpRoute(
        name: "Swagger UI",
        routeTemplate: "",
        defaults: null,
        constraints: null,
        handler: new RedirectHandler(message => message.RequestUri.ToString().TrimEnd('/'), "swagger/ui/index"));

      // configured web api requests
      appBuilder.UseWebApi(config);

      // let Nancy handle static requests (not owin)
      // https://github.com/NancyFx/Nancy/wiki/Managing-static-content#extra-steps-required-when-using-microsoftowinhostsystemweb
      appBuilder.UseStageMarker(PipelineStage.MapHandler);

      config.EnsureInitialized();
    }
  }
}