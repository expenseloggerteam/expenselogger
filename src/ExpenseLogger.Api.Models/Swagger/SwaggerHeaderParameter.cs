﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Web.Http.Description;
using Swashbuckle.Application;
using Swashbuckle.Swagger;

namespace ExpenseLogger.Api.Models.Swagger
{
  /// <summary>
  ///   An <see cref="Swashbuckle.Swagger.IOperationFilter" /> to apply a header parameter for all operations of the Api
  /// </summary>
  public class SwaggerHeaderParameter : IOperationFilter
  {
    /// <summary>
    ///   Security token key name
    /// </summary>
    public string Key { get; set; }

    /// <summary>
    ///   Header parameter name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    ///   Header parameter description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    ///   Default value of parameter if none specified in request
    /// </summary>
    public string DefaultValue { get; set; }

    /// <summary>
    ///   Apply filter to the swagger documentation config
    /// </summary>
    /// <param name="c"></param>
    public void Apply(SwaggerDocsConfig c)
    {
      c.ApiKey(Key).Name(Name).Description(Description).In("header");
      c.OperationFilter(() => this);
    }

    /// <summary>
    ///   Apply filter to swagger document
    /// </summary>
    /// <param name="swaggerDoc">Swagger document</param>
    /// <param name="schemaRegistry">Schema registry</param>
    /// <param name="apiExplorer">Api explorer</param>
    public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
    {
      var security = new List<IDictionary<string, IEnumerable<string>>>
      {
        new Dictionary<string, IEnumerable<string>> {{Key, new string[0]}}
      };
      swaggerDoc.security = security;
    }

    #region Implementation of IOperationFilter

    /// <summary>
    ///   Apply filter
    /// </summary>
    /// <param name="operation">Current operation being performed</param>
    /// <param name="schemaRegistry">Current schema registry</param>
    /// <param name="apiDescription">Api description</param>
    public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
    {
      operation.parameters = operation.parameters ?? new List<Parameter>();
      operation.parameters.Add(new Parameter
      {
        name = Name,
        description = Description,
        @in = "header",
        required = true,
        type = "string",
        @default = DefaultValue
      });
    }

    #endregion Implementation of IOperationFilter
  }
}