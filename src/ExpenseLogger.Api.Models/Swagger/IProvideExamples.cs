﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace ExpenseLogger.Api.Models.Swagger
{
  /// <summary>
  ///   An interface to allow an object to provide examples for the Swagger help page
  /// </summary>
  public interface IProvideExamples
  {
    /// <summary>
    ///   Get an example for the Swagger help page
    /// </summary>
    /// <returns>an example object</returns>
    object GetExamples();
  }
}