﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using Swashbuckle.Swagger;

namespace ExpenseLogger.Api.Models.Swagger
{
  /// <summary>
  ///   A class to modify the swagger schema to set examples.
  /// </summary>
  public class ExamplesSchemaFilter : ISchemaFilter
  {
    /// <summary>
    ///   apply a filter on the schema to set the default property to provide
    ///   a example of the object based on the IProvideExamples interface.
    /// </summary>
    /// <param name="schema">the current schema</param>
    /// <param name="schemaRegistry">the schema registry</param>
    /// <param name="type">the type being created</param>
    public void Apply(Schema schema, SchemaRegistry schemaRegistry, Type type)
    {
      object example = GetExample(type);
      if (example != null)
      {
        schema.@default = example;
      }
    }

    /// <summary>
    ///   Helper method to get the example object for the type
    /// </summary>
    /// <param name="type">the type being created</param>
    /// <returns>the example object or null if not found</returns>
    public static object GetExample(Type type)
    {
      if (type.IsPrimitive)
        return null;

      Type provideExamplesType = null;
      if (typeof(IProvideExamples).IsAssignableFrom(type))
      {
        provideExamplesType = type;
      }
      else
      {
        // get the type from the attribute
        SwaggerExampleAttribute attr = (SwaggerExampleAttribute)type.GetCustomAttributes(typeof(SwaggerExampleAttribute), true).FirstOrDefault();
        if (attr != null)
          provideExamplesType = attr.ExamplesType;
      }

      if (provideExamplesType != null)
      {
        // get the example object from the object
        IProvideExamples provideExamples = (IProvideExamples)Activator.CreateInstance(provideExamplesType);
        return provideExamples.GetExamples();
      }

      return null;
    }
  }
}