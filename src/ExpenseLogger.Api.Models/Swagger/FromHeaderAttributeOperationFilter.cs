﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Linq;
using System.Web.Http.Description;
using ExpenseLogger.Api.Models.Http;
using Swashbuckle.Swagger;

namespace ExpenseLogger.Api.Models.Swagger
{
  /// <summary>
  ///   An <see cref="IOperationFilter" /> to handle parameters decorated with the
  ///   <see cref="ExpenseLogger.Api.Models.Http.FromHeaderAttribute" />
  /// </summary>
  public class FromHeaderAttributeOperationFilter : IOperationFilter
  {
    /// <summary>
    ///   Apply filter
    /// </summary>
    /// <param name="operation">Current operation being performed</param>
    /// <param name="schemaRegistry">Current schema registry</param>
    /// <param name="apiDescription">Api description</param>
    public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
    {
      var fromHeaderAttributes = apiDescription.ActionDescriptor.GetParameters()
        .Where(param => param.GetCustomAttributes<FromHeaderAttribute>().Any())
        .ToArray();

      foreach (var headerParam in fromHeaderAttributes)
      {
        var operationParameter = operation.parameters.First(p => p.name == headerParam.ParameterName);

        operationParameter.name = headerParam.ParameterName;
        operationParameter.@in = "header";
        operationParameter.type = "string";
      }
    }
  }
}