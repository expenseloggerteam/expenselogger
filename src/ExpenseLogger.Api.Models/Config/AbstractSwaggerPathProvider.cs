﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace ExpenseLogger.Api.Models.Config
{
  /// <summary>
  ///   A generic interface that can provide the XML file path of the comments file to create swagger api docs
  /// </summary>
  public abstract class AbstractSwaggerPathProvider
  {
    /// <summary>
    ///   The XML documentation file which swagger should use to generate docs
    /// </summary>
    public string SwaggerDocsFileName => "ExpenseLogger.Api.Models.xml";

    /// <summary>
    ///   Get the XML comments file path to generate swagger api docs
    /// </summary>
    /// <returns>Path to XML file</returns>
    public abstract string GetXmlCommentsPath();
  }
}