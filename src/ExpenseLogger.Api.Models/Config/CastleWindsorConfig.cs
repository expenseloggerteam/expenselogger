﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Web.Http;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using ExpenseLogger.Core.Castle;

namespace ExpenseLogger.Api.Models.Config
{
  /// <summary>
  ///   Castle windsor configurations
  /// </summary>
  public static class CastleWindsorConfig
  {
    /// <summary>
    ///   Registers all castle windsor components required to run the utility
    /// </summary>
    /// <param name="container">Current windsor container</param>
    public static void Register(IWindsorContainer container)
    {
      container.AddFacility(new AppConstantsInitFacility());
      container.Register(Component.For<IKernel>().Named("kernel").Instance(container.Kernel).OnlyNewServices());
      container.Register(Classes.FromThisAssembly().BasedOn<ApiController>().LifestyleTransient());

      // config registration
      //var configRegistration = PropertyResolvingComponent
    }
  }
}