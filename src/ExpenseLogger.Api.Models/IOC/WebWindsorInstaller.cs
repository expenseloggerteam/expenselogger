﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.Extensions.Registration;
using ExpenseLogger.Models.Conf;

namespace ExpenseLogger.Api.Models.IOC
{
  /// <summary>
  ///   A <see cref="Castle.MicroKernel.Registration.IWindsorInstaller" /> for registering components for the
  ///   ExpenseLogger.Web project
  /// </summary>
  public class WebWindsorInstaller : IWindsorInstaller
  {
    #region Implementation of IWindsorInstaller

    /// <summary>
    ///   Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
    /// </summary>
    /// <param name="container">The container.</param>
    /// <param name="store">The configuration store.</param>
    public void Install(IWindsorContainer container, IConfigurationStore store)
    {
      container.Register(PropertyResolvingComponent.For<ApiConfigProperty>()
        .DependsOnProperties(ResolvablePropertyUtil.From<ApiConfigProperty>()));

      CastleWindsorModelsConfig.Configure(container);
    }

    #endregion
  }
}