﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A action result used to return an unhandled exception result
  /// </summary>
  public class UnhandledExceptionResult : IHttpActionResult
  {
    /// <summary>
    ///   The request that caused the unhandled exception
    /// </summary>
    public HttpRequestMessage Request { get; private set; }

    /// <summary>
    ///   The exception
    /// </summary>
    public Exception Exception { get; private set; }

    /// <summary>
    ///   ctor
    /// </summary>
    /// <param name="request">The request that caused the unhandled exception</param>
    /// <param name="exception">The exception</param>
    public UnhandledExceptionResult(HttpRequestMessage request, Exception exception)
    {
      Request = request;
      Exception = exception;
    }

    /// <summary>
    ///   Execute the action
    /// </summary>
    /// <param name="cancellationToken">cancellation token</param>
    /// <returns>a unhandled exception response</returns>
    public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
    {
      return Task.Run(() => CreateErrorResponse(Request, Exception), cancellationToken);
    }

    /// <summary>
    ///   Hellper method to create a HttpResponseMessage with a ErrorResult representing a
    ///   http error InternalServerError 500.
    /// </summary>
    /// <param name="request">The request that caused the unhandled exception</param>
    /// <param name="exception">The exception</param>
    /// <returns>a unhandled exception response</returns>
    public static HttpResponseMessage CreateErrorResponse(HttpRequestMessage request, Exception exception)
    {
      HttpResponseMessage response = request.CreateResponse(
        HttpStatusCode.InternalServerError,
        new ErrorResult
        {
          code = HttpStatusCode.InternalServerError,
          request = string.Format("{0} {1}", request.Method, request.RequestUri),
          error = new HttpErrorModel(exception)
        });
      response.ReasonPhrase = "Error";
      return response;
    }
  }
}