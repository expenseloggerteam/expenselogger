﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Net;
using System.Web.Http.ModelBinding;
using ExpenseLogger.Api.Models.Swagger;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A class representing an invalid model or arguments result (HTTP BadRequest (400))
  /// </summary>
  public class BadRequestResult : ErrorResult, IProvideExamples
  {
    /// <summary>
    ///   ctor. Sets the HTTP status code to BadRequest (400).
    /// </summary>
    public BadRequestResult()
    {
      code = HttpStatusCode.BadRequest;
    }

    /// <summary>
    ///   Get an example of this class for the Swagger help page
    /// </summary>
    /// <returns>an example of this class</returns>
    public override object GetExamples()
    {
      ModelStateDictionary modelState = new ModelStateDictionary();
      modelState.AddModelError("field", "The field is invalid");
      return new BadRequestResult
      {
        request = "POST /api/x",
        error = new HttpErrorModel(modelState)
      };
    }
  }
}