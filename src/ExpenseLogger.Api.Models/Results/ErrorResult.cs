﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Net;
using ExpenseLogger.Api.Models.Swagger;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A class representing an error result
  /// </summary>
  public class ErrorResult : IProvideExamples
  {
    /// <summary>
    ///   whether the result was a success. RETURNS FALSE.
    /// </summary>
    public bool success
    {
      get { return false; }
    }

    /// <summary>
    ///   the request URL that produced the result
    /// </summary>
    public string request { get; set; }

    /// <summary>
    ///   the HTTP status code.
    /// </summary>
    public HttpStatusCode code { get; set; }

    /// <summary>
    ///   the HTTP status string.
    /// </summary>
    public string status
    {
      get { return code.ToString(); }
    }

    /// <summary>
    ///   The error details of the result.
    /// </summary>
    public HttpErrorModel error { get; set; }

    /// <summary>
    ///   Get an example of this class for the Swagger help page
    /// </summary>
    /// <returns>an example of this class</returns>
    public virtual object GetExamples()
    {
      return new ErrorResult
      {
        code = HttpStatusCode.InternalServerError,
        request = "GET /api/x",
        error = new HttpErrorModel("Object ref not set")
      };
    }
  }
}