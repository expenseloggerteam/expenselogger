﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Linq;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A class representing an success result that returns a data collection
  /// </summary>
  /// <typeparam name="T">the type of data to return</typeparam>
  public class SuccessEnumerableResult<T> : SuccessResult<IEnumerable<T>>
  {
    /// <summary>
    ///   The number of items returned
    /// </summary>
    public int count
    {
      get { return result != null ? result.Count() : 0; }
    }
  }
}