﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using ExpenseLogger.Api.Models.Swagger;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A class representing an success result that returns data
  /// </summary>
  /// <typeparam name="T">the type of data to return</typeparam>
  public class SuccessResult<T> : IProvideExamples
  {
    /// <summary>
    ///   whether the result was a success
    /// </summary>
    public bool success
    {
      get { return true; }
    }

    /// <summary>
    ///   the request URL that produced the result
    /// </summary>
    public string request { get; set; }

    /// <summary>
    ///   the HTTP status code
    /// </summary>
    public HttpStatusCode code { get; set; }

    /// <summary>
    ///   the HTTP status string
    /// </summary>
    public string status
    {
      get { return code.ToString(); }
    }

    /// <summary>
    ///   The response data
    /// </summary>
    public T result { get; set; }

    /// <summary>
    ///   Get an example of this class for the Swagger help page
    /// </summary>
    /// <returns>an example of this class</returns>
    public virtual object GetExamples()
    {
      T data;

      Type tType = typeof(T);
      bool isArray = tType.IsArray;
      bool isEnumerable = tType.IsGenericType && tType.GetGenericTypeDefinition() == typeof(IEnumerable<>);

      // get the model type
      Type modelType = tType;
      if (isArray)
        modelType = tType.GetElementType();
      else if (isEnumerable)
        modelType = tType.GetGenericArguments()[0];

      object example = ExamplesSchemaFilter.GetExample(modelType);
      if (isArray)
      {
        Array array = Array.CreateInstance(modelType, 1);
        array.SetValue(example, 0);
        data = (T)((object)array);
      }
      else if (isEnumerable)
      {
        Type generic = typeof(List<>);
        Type constucted = generic.MakeGenericType(modelType);
        IList list = (IList)Activator.CreateInstance(constucted);
        list.Add(example);
        data = (T)list;
      }
      else
        data = (T)example;

      return new SuccessResult<T>
      {
        code = HttpStatusCode.OK,
        request = "GET /api/data/",
        result = data
      };
    }
  }
}