﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A Http Action to wrap another action so that add additional processing can be done on the result.
  ///   For example additional response headers or a reason phrase can be added after the action has executed.
  /// </summary>
  public class WrappedHttpActionResult : IHttpActionResult
  {
    /// <summary>
    ///   the IHttpActionResult result
    /// </summary>
    public IHttpActionResult InnerResult { get; private set; }

    /// <summary>
    ///   the action to perform on the response
    /// </summary>
    private readonly Action<HttpResponseMessage> m_responseAction;

    /// <summary>
    ///   ctor
    /// </summary>
    /// <param name="inner">the IHttpActionResult result</param>
    /// <param name="responseAction">the action to perform on the response</param>
    public WrappedHttpActionResult(IHttpActionResult inner, Action<HttpResponseMessage> responseAction)
    {
      InnerResult = inner;
      m_responseAction = responseAction;
    }

    /// <summary>
    ///   Execute the action
    /// </summary>
    /// <param name="cancellationToken">the cancellation token</param>
    /// <returns>a response message</returns>
    public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
    {
      // execute inner action
      HttpResponseMessage response = await InnerResult.ExecuteAsync(cancellationToken);

      // process the response using the supplied function
      m_responseAction(response);

      return response;
    }
  }
}