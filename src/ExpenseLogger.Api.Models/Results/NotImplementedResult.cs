﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Net;
using ExpenseLogger.Api.Models.Swagger;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A class representing an obsolete method request.
  ///   Returns NotImplemented error code (501).
  /// </summary>
  public class NotImplementedResult : IProvideExamples
  {
    /// <summary>
    ///   whether the result was a success
    /// </summary>
    public bool success
    {
      get { return false; }
    }

    /// <summary>
    ///   the request URL that produced the result
    /// </summary>
    public string request { get; set; }

    /// <summary>
    ///   the HTTP status code. Returns NotImplemented error code (501).
    /// </summary>
    public HttpStatusCode code
    {
      get { return HttpStatusCode.NotImplemented; }
    }

    /// <summary>
    ///   the HTTP status string. Returns NotImplemented error code (501).
    /// </summary>
    public string status
    {
      get { return code.ToString(); }
    }

    /// <summary>
    ///   the request message
    /// </summary>
    public string message { get; set; }

    /// <summary>
    ///   Get an example of this class for the Swagger help page
    /// </summary>
    /// <returns>an example of this class</returns>
    public object GetExamples()
    {
      return new NotImplementedResult
      {
        request = "GET /api/myFunction",
        message = "The 'myFunction' method has been deprecated and is now obsolete."
      };
    }
  }
}