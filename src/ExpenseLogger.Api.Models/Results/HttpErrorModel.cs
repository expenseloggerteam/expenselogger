﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;

namespace ExpenseLogger.Api.Models.Results
{
  /// <summary>
  ///   A class containing the details of an Error Result.
  ///   Loosely based on the .net HttpError class.
  /// </summary>
  public class HttpErrorModel
  {
    /// <summary>
    ///   The error message
    /// </summary>
    public string message { get; set; }

    /// <summary>
    ///   The exception that caused the error result
    /// </summary>
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string exception { get; set; }

    /// <summary>
    ///   The validation errors that caused the error result
    /// </summary>
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public Dictionary<string, string[]> validationErrors { get; set; }

    /// <summary>
    ///   ctor
    /// </summary>
    /// <param name="message">the error message</param>
    public HttpErrorModel(string message)
    {
      this.message = message;
    }

    /// <summary>
    ///   ctor used to construct with an exception
    /// </summary>
    /// <param name="exception">the exception that caused the error result</param>
    public HttpErrorModel(Exception exception)
    {
      message = exception.Message;
      this.exception = exception.ToString();
    }

    /// <summary>
    ///   ctor used to construct with validation errors
    /// </summary>
    /// <param name="modelState">the model state validation errors that caused the error result</param>
    public HttpErrorModel(ModelStateDictionary modelState)
    {
      message = "The request is invalid";

      validationErrors = new Dictionary<string, string[]>();
      // lifted from https://github.com/ASP-NET-MVC/aspnetwebstack/blob/master/src/System.Web.Http/HttpError.cs
      foreach (KeyValuePair<string, ModelState> keyModelStatePair in modelState)
      {
        string key = keyModelStatePair.Key;
        ModelErrorCollection errors = keyModelStatePair.Value.Errors;
        if (errors != null && errors.Count > 0)
        {
          string[] errorMessages = errors.Select(error =>
          {
            if (error.Exception != null)
            {
              return error.Exception.Message;
            }
            return string.IsNullOrEmpty(error.ErrorMessage) ? "An error occurred" : error.ErrorMessage;
          }).ToArray();
          validationErrors.Add(key, errorMessages);
        }
      }
    }
  }
}