﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.IO;
using System.Reflection;
using ExpenseLogger.Core;

namespace ExpenseLogger.Api.Models
{
  /// <summary>
  ///   Application root path provider
  /// </summary>
  public class AppRootPathProvider
  {
    #region Implementation of IRootPathProvider

    /// <summary>
    ///   Returns the root folder path of the current Nancy application.
    /// </summary>
    /// <returns>
    ///   A <see cref="T:System.String" /> containing the path of the root folder.
    /// </returns>
    public string GetRootPath()
    {
      string path = Path.GetFullPath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + AppConstants.PathSeparationChar + ".." + AppConstants.PathSeparationChar);

      return path;
    }

    #endregion
  }
}