﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;

namespace ExpenseLogger.Api.Models.Http
{
  /// <summary>
  ///   A <see cref="System.Web.Http.Controllers.HttpParameterBinding" /> to parse parameters from request header
  /// </summary>
  public class FromHeaderBinding : HttpParameterBinding
  {
    private readonly string m_name;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="parameter">Parameter descriptor</param>
    /// <param name="headerName">Header name</param>
    public FromHeaderBinding(HttpParameterDescriptor parameter, string headerName)
      : base(parameter)
    {
      if (string.IsNullOrEmpty(headerName))
      {
        throw new ArgumentNullException(nameof(headerName));
      }

      m_name = headerName;
    }

    /// <summary>
    ///   Asynchronously executes the binding for the given request.
    /// </summary>
    /// <param name="metadataProvider">Metadata provider to use for validation.</param>
    /// <param name="actionContext">
    ///   The action context for the binding. The action context contains the parameter dictionary
    ///   that will get populated with the parameter.
    /// </param>
    /// <param name="cancellationToken">Cancellation token for cancelling the binding operation.</param>
    /// <returns>
    ///   A task object representing the asynchronous operation.
    /// </returns>
    public override Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider, HttpActionContext actionContext, CancellationToken cancellationToken)
    {
      IEnumerable<string> values;
      if (actionContext.Request.Headers.TryGetValues(m_name, out values))
      {
        var tempVal = values.FirstOrDefault();
        if (tempVal != null)
        {
          var actionValue = Convert.ChangeType(tempVal, Descriptor.ParameterType);
          actionContext.ActionArguments[Descriptor.ParameterName] = actionValue;
        }
      }

      var taskSource = new TaskCompletionSource<object>();
      taskSource.SetResult(null);
      return taskSource.Task;
    }
  }
}