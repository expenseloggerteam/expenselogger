﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Web.Http;
using System.Web.Http.Controllers;

namespace ExpenseLogger.Api.Models.Http
{
  /// <summary>
  ///   An attribute to specify that a parameter should be auto-bound from request header
  /// </summary>
  public class FromHeaderAttribute : ParameterBindingAttribute
  {
    private readonly string m_name;

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="headerName">[Optional] Header parameter name. Defaults to the name of parameter that it decorates</param>
    public FromHeaderAttribute(string headerName = null)
    {
      m_name = headerName;
    }

    /// <summary>
    ///   Gets the parameter binding.
    /// </summary>
    /// <param name="parameter">The parameter description.</param>
    /// <returns>The parameter binding.</returns>
    public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
    {
      return new FromHeaderBinding(parameter, m_name ?? parameter.ParameterName);
    }
  }
}