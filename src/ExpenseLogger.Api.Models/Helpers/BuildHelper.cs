﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Reflection;

namespace ExpenseLogger.Api.Models.Helpers
{
  /// <summary>
  ///   Application build helpers
  /// </summary>
  public static class BuildHelper
  {
    /// <summary>
    ///   Version info of current assembly
    /// </summary>
    private static Version VERSION;

    /// <summary>
    ///   Version of the current assembly
    /// </summary>
    public static Version AppVersion
    {
      get { return VERSION ?? (VERSION = Assembly.GetExecutingAssembly().GetName().Version); }
    }

    ///// <summary>
    /////   Retrieves system version number
    ///// </summary>
    ///// <param name="html">Current html helper</param>
    ///// <returns>System version number</returns>
    //public static IHtmlString GetVersion(this HtmlHelpers html)
    //{
    //  string version = string.Format("v{0}.{1}.{2} Rev {3}", AppVersion.Major, AppVersion.Minor, AppVersion.Build, AppVersion.Revision);

    //  return new NonEncodedHtmlString(version);
    //}
  }
}