﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Net.Http;
using System.Web.Http;
using ExpenseLogger.Api.Models.Results;

namespace ExpenseLogger.Api.Models.Helpers
{
  /// <summary>
  ///   A class containing extensions for IHttpActionResult objects
  /// </summary>
  public static class IHttpActionResultExtensions
  {
    /// <summary>
    ///   An extension method to process the response of an action to add additional data such as response headers or a reason
    ///   phrase.
    /// </summary>
    /// <param name="inner">the IHttpActionResult</param>
    /// <param name="responseAction">the action to perform on the response</param>
    /// <returns>the IHttpActionResult</returns>
    public static IHttpActionResult With(this IHttpActionResult inner, Action<HttpResponseMessage> responseAction)
    {
      return new WrappedHttpActionResult(inner, responseAction);
    }
  }
}