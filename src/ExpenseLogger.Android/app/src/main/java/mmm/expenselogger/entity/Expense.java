package mmm.expenselogger.entity;

import android.content.ContentValues;
import android.database.Cursor;

import org.joda.time.DateTime;

import java.io.Serializable;

import mmm.expenselogger.core.Constants;
import mmm.expenselogger.data.IAsContentValues;
import mmm.expenselogger.data.ExpenseContract;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 21/02/2017.
 */

/**
 * Denotes an expense entity
 */
public final class Expense implements Serializable, IAsContentValues {
  public int _id;
  public DateTime date;
  public double amount;
  public String description;

  /**
   * Constructor
   */
  public Expense() {
  }

  /**
   * Constructor
   *
   * @param cursor Database cursor to read an expense from
   */
  public Expense(Cursor cursor) {
    _id = cursor.getInt(cursor.getColumnIndex(ExpenseContract.ID));

    String str = cursor.getString(cursor.getColumnIndex(ExpenseContract.DATE));
    date = DateTime.parse(str);

    amount = cursor.getDouble(cursor.getColumnIndex(ExpenseContract.ID));
    description = cursor.getString(cursor.getColumnIndex(ExpenseContract.ID));
  }

  /**
   * @return Get current object as content values
   */
  @Override
  public ContentValues getContentValues() {

    ContentValues values = new ContentValues();
    values.put(ExpenseContract.ID, _id);
    values.put(ExpenseContract.DATE, date.toString(Constants.DATEFORMAT));
    values.put(ExpenseContract.AMOUNT, amount);
    values.put(ExpenseContract.DESCRIPTION, description);

    return values;
  }
}
