package mmm.expenselogger.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import mmm.expenselogger.R;
import mmm.expenselogger.core.Constants;
import mmm.expenselogger.entity.Expense;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Expense} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class ExpenseRecyclerViewAdapter extends RecyclerView.Adapter<ExpenseItemViewHolder> {

  private final List<Expense> mValues;
  private final ExpenseFragment mContext;

  public ExpenseRecyclerViewAdapter(ExpenseFragment fragment, List<Expense> items) {
    mValues = items;
    mContext = fragment;
  }

  @Override
  public ExpenseItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.fragment_expense, parent, false);
    return new ExpenseItemViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ExpenseItemViewHolder holder, int position) {

    Expense e = mValues.get(position);

    holder.mItem = e;
    holder.mDateView.setText(e.date.toString(Constants.DATEFORMAT));
    holder.mDescriptionView.setText(e.description);

    holder.mView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(v.getContext(), "Clicked on expense with id: " + holder.mIdView.getText(), Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public int getItemCount() {
    return mValues.size();
  }
}
