package mmm.expenselogger.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import static mmm.expenselogger.data.AppDataContract.AUTHORITY;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 22/02/2017.
 */

/**
 * Data content provider for the expenses database
 */
public final class ExpensesContentProvider extends ContentProvider {

  public static final UriMatcher URI_MATCHER = buildUriMatcher();
//  public static final String[] VALID_PATHS = {
//      "expenses",
//      "expenses/*"
//  };

  public static final String EXPENSES_PATH = "expenses";
  public static final int EXPENSES_PATH_TOKEN = 100;
  public static final String EXPENSES_PATH_FOR_ID = "expenses/*";
  public static final int EXPENSES_PATH_FOR_ID_TOKEN = 101;

  private AppDb mDb;

  /**
   * @return Uri Matcher for the content provider
   */
  private static UriMatcher buildUriMatcher() {
    final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
    final String authority = AUTHORITY;
    matcher.addURI(authority, EXPENSES_PATH, EXPENSES_PATH_TOKEN);
    matcher.addURI(authority, EXPENSES_PATH_FOR_ID, EXPENSES_PATH_FOR_ID_TOKEN);
    return matcher;
  }

  @Override
  public boolean onCreate() {
    mDb = new AppDb(getContext());
    return true;
  }

  @Nullable
  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
    SQLiteDatabase db = mDb.getReadableDatabase();
    final int match = URI_MATCHER.match(uri);
    switch (match) {
      // retrieve tv shows list
      case EXPENSES_PATH_TOKEN: {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(ExpenseContract.NAME);
        return builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
      }
      case EXPENSES_PATH_FOR_ID_TOKEN: {
        int tvShowId = (int) ContentUris.parseId(uri);
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(ExpenseContract.NAME);
        builder.appendWhere(ExpenseContract.ID + "=" + tvShowId);
        return builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
      }
      default:
        return null;
    }
  }

  @Nullable
  @Override
  public String getType(Uri uri) {
    final int match = URI_MATCHER.match(uri);
    switch (match) {
      case EXPENSES_PATH_TOKEN:
        return ExpenseContract.CONTENT_TYPE_LIST;
      case EXPENSES_PATH_FOR_ID_TOKEN:
        return ExpenseContract.CONTENT_TYPE_ITEM;
      default:
        throw new UnsupportedOperationException("URI " + uri + " is not supported.");
    }
  }

  @Nullable
  @Override
  public Uri insert(Uri uri, ContentValues contentValues) {
    SQLiteDatabase db = mDb.getWritableDatabase();
    int token = URI_MATCHER.match(uri);
    switch (token) {
      case EXPENSES_PATH_TOKEN: {
        long id = db.insert(ExpenseContract.NAME, null, contentValues);
        if (id != -1)
          getContext().getContentResolver().notifyChange(uri, null);
        return ExpenseContract.CONTENT_URI.buildUpon().appendPath(String.valueOf(id)).build();
      }
      default: {
        throw new UnsupportedOperationException("URI: " + uri + " not supported.");
      }
    }
  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs) {
    SQLiteDatabase db = mDb.getWritableDatabase();
    int token = URI_MATCHER.match(uri);
    int rowsDeleted = -1;
    switch (token) {
      case (EXPENSES_PATH_TOKEN):
        rowsDeleted = db.delete(ExpenseContract.NAME, selection, selectionArgs);
        break;
      case (EXPENSES_PATH_FOR_ID_TOKEN):
        String whereClause = ExpenseContract.ID + "=" + uri.getLastPathSegment();
        if (!TextUtils.isEmpty(selection))
          whereClause += " AND " + selection;
        rowsDeleted = db.delete(ExpenseContract.NAME, whereClause, selectionArgs);
        break;
      default:
        throw new IllegalArgumentException("Unsupported URI: " + uri);
    }

    // Notifying the changes, if there are any
    if (rowsDeleted != -1)
      getContext().getContentResolver().notifyChange(uri, null);

    return rowsDeleted;
  }

  @Override
  public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
    SQLiteDatabase db = mDb.getWritableDatabase();
    int token = URI_MATCHER.match(uri);
    int rowsUpdated = -1;
    switch (token) {
      case (EXPENSES_PATH_FOR_ID_TOKEN):
        String whereClause = ExpenseContract.ID + "=" + uri.getLastPathSegment();
        if (!TextUtils.isEmpty(selection))
          whereClause += " AND " + selection;
        rowsUpdated = db.update(ExpenseContract.NAME, contentValues, whereClause, selectionArgs);
        break;
      default:
        throw new IllegalArgumentException("Unsupported URI: " + uri);
    }

    // Notifying the changes, if there are any
    if (rowsUpdated != -1)
      getContext().getContentResolver().notifyChange(uri, null);

    return rowsUpdated;
  }
}
