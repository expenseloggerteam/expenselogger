package mmm.expenselogger.data;

import org.joda.time.DateTime;

import java.util.List;

import mmm.expenselogger.entity.ChangeSet;
import mmm.expenselogger.entity.Expense;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 24/02/2017.
 */

/**
 * A generic data service to provide all required data from the ExpenseLogger Data Api
 */
public interface IDataService {
  List<Expense> getExpenses(String authToken);

  Expense getExpenseById(String authToken, int id);

  List<Expense> getExpensesForMonth(String authToken, DateTime date);

  List<Expense> getExpensesForYear(String authToken, int year);

  List<Expense> getExpensesForDay(String authToken, DateTime date);

  ChangeSet<Expense> getUpdatedExpenses(String authToken, DateTime updatedSince);

  void saveExpense(String authToken, String userId, Expense expense);
}
