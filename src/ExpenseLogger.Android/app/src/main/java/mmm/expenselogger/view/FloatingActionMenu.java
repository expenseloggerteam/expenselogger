package mmm.expenselogger.view;

import android.app.Activity;
import android.graphics.Rect;
import android.support.design.widget.FloatingActionButton;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.common.collect.FluentIterable;

import org.apache.commons.lang.NullArgumentException;

import java.util.List;

import mmm.expenselogger.R;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 04/03/2017.
 */

public class FloatingActionMenu {

  private Activity mActivity;
  private Animation mFabOpen, mFabClose, mFabRotateClockwise, mFabRotateAntiClockwise;
  private FloatingActionButton mBtnMenuLauncher;
  private List<FloatingActionButton> mButtons;
  private boolean mIsMenuOpen;

  public FloatingActionMenu(Activity activity, FloatingActionButton launcher, List<FloatingActionButton> buttons) {
    mActivity = activity;
    mBtnMenuLauncher = launcher;
    mBtnMenuLauncher.setOnClickListener(view -> toggleMenu());

    if (buttons == null)
      throw new NullArgumentException("buttons");

    mButtons = buttons;
    //mButtons.forEach(btn -> btn.setOnClickListener(view -> Toast.makeText(activity, "Clicked on: " + btn.getId(), Toast.LENGTH_LONG)));

    mFabOpen = AnimationUtils.loadAnimation(activity, R.anim.fab_open);
    mFabClose = AnimationUtils.loadAnimation(activity, R.anim.fab_close);
    mFabRotateClockwise = AnimationUtils.loadAnimation(activity, R.anim.fab_rotate_clockwise_45);
    mFabRotateAntiClockwise = AnimationUtils.loadAnimation(activity, R.anim.fab_rotate_anticlockwise_45);
  }

  public void addButton(FloatingActionButton button) {
    mButtons.add(button);
    //button.setOnClickListener(view -> Toast.makeText(mActivity, "Clicked on: " + button.getId(), Toast.LENGTH_LONG));
  }

  public void getGlobalVisibleRect(Rect rect) {

    mButtons.forEach(btn -> {
      Rect btnRect = new Rect();
      btn.getGlobalVisibleRect(btnRect);

      rect.union(btnRect);
    });

  }

  public void showMenu() {

    if (mIsMenuOpen)
      return;

    mButtons.forEach(btn -> {
      btn.startAnimation(mFabOpen);
      btn.setClickable(true);
    });

    mBtnMenuLauncher.startAnimation(mFabRotateClockwise);
    mIsMenuOpen = true;
  }

  public void closeMenu() {

    if (!mIsMenuOpen)
      return;

    mButtons.forEach(btn -> {
      btn.startAnimation(mFabClose);
      btn.setClickable(false);
    });

    mBtnMenuLauncher.startAnimation(mFabRotateAntiClockwise);
    mIsMenuOpen = false;
  }

  public void toggleMenu() {
    if (mIsMenuOpen)
      closeMenu();
    else
      showMenu();
  }
}
