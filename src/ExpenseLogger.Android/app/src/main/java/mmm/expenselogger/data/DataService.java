package mmm.expenselogger.data;

import org.joda.time.DateTime;

import java.util.List;

import mmm.expenselogger.entity.ChangeSet;
import mmm.expenselogger.entity.Expense;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 22/02/2017.
 */
public class DataService implements IDataService {

  /**
   * @param authToken User auth token
   * @return User's expenses
   */
  @Override
  public List<Expense> getExpenses(String authToken) {
    throw new UnsupportedOperationException("operation not implemented");
  }

  @Override
  public Expense getExpenseById(String authToken, int id) {
    throw new UnsupportedOperationException("operation not implemented");
  }

  @Override
  public List<Expense> getExpensesForMonth(String authToken, DateTime date) {
    throw new UnsupportedOperationException("operation not implemented");
  }

  @Override
  public List<Expense> getExpensesForYear(String authToken, int year) {
    throw new UnsupportedOperationException("operation not implemented");
  }

  @Override
  public List<Expense> getExpensesForDay(String authToken, DateTime date) {
    throw new UnsupportedOperationException("operation not implemented");
  }

  @Override
  public ChangeSet<Expense> getUpdatedExpenses(String authToken, DateTime updatedSince) {
    throw new UnsupportedOperationException("operation not implemented");
  }

  /**
   * @param authToken User auth token
   * @param userId    User unique id
   * @param expense   Expense to be posted
   */
  @Override
  public void saveExpense(String authToken, String userId, Expense expense) {
    // nothing to do here
  }
}
