package mmm.expenselogger.auth;

import mmm.expenselogger.core.Constants;
import mmm.expenselogger.entity.User;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 22/02/2017.
 */

/**
 * ExpenseLogger authentication api service
 */
public final class AuthApiService implements IAuthenticationService {
  @Override
  public User signUp(User user, String authType) {

    user.id = 1;

    return user;
  }

  @Override
  public User logIn(String username, String password, String authType) {

    User user = new User();
    user.id = 1;
    user.fullName = "Mihir Mone";
    user.email = "monemihir@gmail.com";
    user.username = "mihir";

    // send device id as well
    String deviceId = Constants.DEVICE_ID;

    return user;
  }
}
