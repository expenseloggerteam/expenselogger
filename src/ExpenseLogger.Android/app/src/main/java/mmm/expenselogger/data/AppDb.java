package mmm.expenselogger.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 21/02/2017.
 */

/**
 * Denotes expenses database
 */
public final class AppDb extends SQLiteOpenHelper {

  private final static String DB_NAME = "mmm.expenselogger.db";
  private final static int DB_VERSION = 1;

  public final static String DB_CREATE_CMD = "create table "
      + ExpenseContract.NAME + "("
      + ExpenseContract.ID + " integer primary key autoincrement, "
      + ExpenseContract.DATE + " date not null, "
      + ExpenseContract.AMOUNT + " double not null, "
      + ExpenseContract.DESCRIPTION + " text not null"
      + ");";


  /**
   * Constructor
   *
   * @param context Database context
   */
  public AppDb(Context context) {
    super(context, DB_NAME, null, DB_VERSION);
  }


  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase) {
    sqLiteDatabase.execSQL(DB_CREATE_CMD);
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
    Log.w(AppDb.class.getSimpleName(), "Upgrading database from v" + oldVersion + " to v" + newVersion + ", which will destroy all old data");
    sqLiteDatabase.execSQL("drop table if exists " + ExpenseContract.NAME);
    onCreate(sqLiteDatabase);
  }
}
