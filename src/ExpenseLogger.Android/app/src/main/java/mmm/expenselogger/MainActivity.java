package mmm.expenselogger;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.google.common.collect.Lists;

import mmm.expenselogger.core.Constants;
import mmm.expenselogger.view.FloatingActionMenu;

import static android.content.Context.TELEPHONY_SERVICE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

  private FloatingActionMenu mFabMenu;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton mFabPlus = (FloatingActionButton) findViewById(R.id.fab_plus);
    FloatingActionButton mFabExpense = (FloatingActionButton) findViewById(R.id.fab_expense);
    FloatingActionButton mFabReminder = (FloatingActionButton) findViewById(R.id.fab_reminder);

    mFabMenu = new FloatingActionMenu(this, mFabPlus, Lists.newArrayList(mFabExpense, mFabReminder));

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);

    Constants.DEVICE_ID = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getDeviceId();
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @SuppressWarnings("StatementWithEmptyBody")
  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.nav_expenses) {
      // Handle the expenses action
    } else if (id == R.id.nav_reminders) {
      // Handle the reminders action
    } else if (id == R.id.nav_licenses) {
      // Handle the licenses action
    } else if (id == R.id.nav_about) {
      // Handle the about action
    }

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

//  @Override
//  public boolean dispatchTouchEvent(MotionEvent event) {
//    if (event.getAction() == MotionEvent.ACTION_DOWN) {
//      Rect outRect = new Rect();
//      mFabMenu.getGlobalVisibleRect(outRect);
//      if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
//        mFabMenu.closeMenu();
//      }
//      else
//        return false;
//    }
//    return super.dispatchTouchEvent(event);
//  }
}
