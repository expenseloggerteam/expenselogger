package mmm.expenselogger.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mmm.expenselogger.R;
import mmm.expenselogger.auth.AccountGeneral;
import mmm.expenselogger.data.ExpenseContract;
import mmm.expenselogger.data.FakeDataService;
import mmm.expenselogger.data.IDataService;
import mmm.expenselogger.entity.Expense;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 22/02/2017.
 */

public final class ExpensesSyncAdapter extends AbstractThreadedSyncAdapter {
  private static final String TAG = ExpensesSyncAdapter.class.getSimpleName();

  private final AccountManager mAccountManager;

  public ExpensesSyncAdapter(Context context, boolean autoInitialize) {
    super(context, autoInitialize);
    mAccountManager = AccountManager.get(context);
  }

  @Override
  public void onPerformSync(Account account, Bundle extras, String authority,
                            ContentProviderClient provider, SyncResult syncResult) {

    // Building a print of the extras we got
    StringBuilder sb = new StringBuilder();
    if (extras != null) {
      for (String key : extras.keySet()) {
        sb.append(key + "[" + extras.get(key) + "] ");
      }
    }

    Log.d(getContext().getString(R.string.app_name), TAG + "#onPerformSync for account[" + account.name + "]. Extras: " + sb.toString());

    try {
      // Get the auth token for the current account and
      // the userObjectId, needed for creating items on Parse.com account
      String authToken = mAccountManager.blockingGetAuthToken(account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, true);
      String userObjectId = mAccountManager.getUserData(account, AccountGeneral.USERDATA_USER_OBJ_ID);

      IDataService parseComService = new FakeDataService();

      Log.d("udinic", TAG + "> Get remote TV Shows");
      // Get shows from remote
      List<Expense> remoteTvShows = parseComService.getExpenses(authToken);

      Log.d("udinic", TAG + "> Get local TV Shows");
      // Get shows from local
      ArrayList<Expense> localTvShows = new ArrayList<Expense>();
      Cursor curTvShows = provider.query(ExpenseContract.CONTENT_URI, null, null, null, null);
      if (curTvShows != null) {
        while (curTvShows.moveToNext()) {
          localTvShows.add(new Expense(curTvShows));
        }
        curTvShows.close();
      }

      // See what Local shows are missing on Remote
      ArrayList<Expense> showsToRemote = new ArrayList<Expense>();
      for (Expense localTvShow : localTvShows) {
        if (!remoteTvShows.contains(localTvShow))
          showsToRemote.add(localTvShow);
      }

      // See what Remote shows are missing on Local
      ArrayList<Expense> showsToLocal = new ArrayList<Expense>();
      for (Expense remoteTvShow : remoteTvShows) {
        if (!localTvShows.contains(remoteTvShow) && remoteTvShow._id != 1) // TODO REMOVE THIS
          showsToLocal.add(remoteTvShow);
      }

      if (showsToRemote.size() == 0) {
        Log.d("udinic", TAG + "> No local changes to update server");
      } else {
        Log.d("udinic", TAG + "> Updating remote server with local changes");

        // Updating remote tv shows
        for (Expense remoteTvShow : showsToRemote) {
          Log.d("udinic", TAG + "> Local -> Remote [" + remoteTvShow._id + "]");
          parseComService.saveExpense(authToken, userObjectId, remoteTvShow);
        }
      }

      if (showsToLocal.size() == 0) {
        Log.d("udinic", TAG + "> No server changes to update local database");
      } else {
        Log.d("udinic", TAG + "> Updating local database with remote changes");

        // Updating local tv shows
        int i = 0;
        ContentValues showsToLocalValues[] = new ContentValues[showsToLocal.size()];
        for (Expense localTvShow : showsToLocal) {
          Log.d("udinic", TAG + "> Remote -> Local [" + localTvShow._id + "]");
          showsToLocalValues[i++] = localTvShow.getContentValues();
        }
        provider.bulkInsert(ExpenseContract.CONTENT_URI, showsToLocalValues);
      }

      Log.d("udinic", TAG + "> Finished.");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
