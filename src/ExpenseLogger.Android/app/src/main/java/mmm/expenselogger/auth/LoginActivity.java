package mmm.expenselogger.auth;

import android.accounts.AccountAuthenticatorActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import mmm.expenselogger.R;

public class LoginActivity extends AccountAuthenticatorActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_login);
  }

}
