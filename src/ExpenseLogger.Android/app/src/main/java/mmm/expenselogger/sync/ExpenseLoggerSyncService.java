package mmm.expenselogger.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 22/02/2017.
 */

/**
 * Service to handle the ExpenseLogger account sync. It instantiates {@link ExpensesSyncAdapter} and returns its {@link IBinder}
 */
public final class ExpenseLoggerSyncService extends Service {

  private static final Object mSyncLock = new Object();
  private static ExpensesSyncAdapter mSyncAdapter = null;

  @Override
  public void onCreate() {
    synchronized (mSyncLock) {
      if (mSyncAdapter == null)
        mSyncAdapter = new ExpensesSyncAdapter(getApplicationContext(), true);
    }
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }
}
