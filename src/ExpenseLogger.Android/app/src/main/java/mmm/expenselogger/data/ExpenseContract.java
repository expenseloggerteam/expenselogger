package mmm.expenselogger.data;

import android.net.Uri;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 22/02/2017.
 */

/**
 * Expenses table api contract
 */
public final class ExpenseContract {
  public static final Uri CONTENT_URI = Uri.parse("content://" + AppDataContract.AUTHORITY + "/expense");
  public static final String CONTENT_TYPE_ITEM = "mmm.expenselogger.cursor.item/mmm.expenselogger.expense";
  public static final String CONTENT_TYPE_LIST = "mmm.expenselogger.cursor.list/mmm.expenselogger.expense";
  public static final String NAME = "expenses";
  public static final String ID = "_id";
  public static final String DATE = "date";
  public static final String AMOUNT = "amount";
  public static final String DESCRIPTION = "description";
}
