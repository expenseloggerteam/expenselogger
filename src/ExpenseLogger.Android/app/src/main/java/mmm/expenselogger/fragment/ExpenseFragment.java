package mmm.expenselogger.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import mmm.expenselogger.R;
import mmm.expenselogger.core.Constants;
import mmm.expenselogger.data.FakeDataService;
import mmm.expenselogger.data.IDataService;
import mmm.expenselogger.entity.Expense;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ExpenseFragment extends Fragment {

  private OnListFragmentInteractionListener mListener;
  private RecyclerView mRecyclerView;
  private List<Expense> mExpenses;
  public IDataService mDataSvc;

  /**
   * Mandatory empty constructor for the fragment manager to instantiate the
   * fragment (e.g. upon screen orientation changes).
   */
  public ExpenseFragment() {
    mDataSvc = new FakeDataService();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    mExpenses = mDataSvc.getExpenses(Constants.TEST_AUTH_TOKEN);

    mRecyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_expense_list, container, false);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    mRecyclerView.setAdapter(new ExpenseRecyclerViewAdapter(this, mExpenses));

    return mRecyclerView;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
//    if (context instanceof OnListFragmentInteractionListener) {
//      mListener = (OnListFragmentInteractionListener) context;
//    } else {
//      throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
//    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
//    mListener = null;
  }
}
