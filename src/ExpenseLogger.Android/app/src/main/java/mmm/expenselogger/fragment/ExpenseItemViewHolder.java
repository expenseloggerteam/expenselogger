package mmm.expenselogger.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import mmm.expenselogger.R;
import mmm.expenselogger.entity.Expense;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 24/02/2017.
 */

public final class ExpenseItemViewHolder extends RecyclerView.ViewHolder {
  public final View mView;
  public final TextView mIdView;
  public final TextView mDateView;
  public final TextView mDescriptionView;
  public Expense mItem;

  public ExpenseItemViewHolder(View view) {
    super(view);
    mView = view;

    mIdView = (TextView) view.findViewById(R.id.expense_id);
    mDateView = (TextView) view.findViewById(R.id.expense_date);
    mDescriptionView = (TextView) view.findViewById(R.id.expense_description);
  }

  @Override
  public String toString() {
    return super.toString() + " '" + mDescriptionView.getText() + "'";
  }

}
