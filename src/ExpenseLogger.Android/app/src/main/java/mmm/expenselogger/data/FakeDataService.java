package mmm.expenselogger.data;

import com.google.common.collect.FluentIterable;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import mmm.expenselogger.core.Constants;
import mmm.expenselogger.entity.ChangeSet;
import mmm.expenselogger.entity.Expense;

/**
 * This file is part of - ExpenseLogger application
 * Copyright (C) 2017 Mihir Mone
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p>
 * Created on 24/02/2017.
 */

public final class FakeDataService implements IDataService {

  private static int mLastId;
  private static FluentIterable<Expense> mExpenseFluentIterable;
  private static List<Expense> mExpensesList;
  private static DateTime mLastUpdateUtc;

  /**
   * Constructor
   */
  public FakeDataService() {
    mExpensesList = new ArrayList<>();

    Random rand = new Random(DateTime.now().getMillis());
    int limit = 25;

    for (int i = 1; i <= limit; i++) {
      Expense e = new Expense();
      e._id = i;
      e.date = DateTime.now().plusDays(i);
      e.description = "fake expense " + i;
      e.amount = rand.nextDouble();

      mExpensesList.add(e);
    }

    mExpenseFluentIterable = FluentIterable.from(mExpensesList);
    mLastId = limit;
    mLastUpdateUtc = DateTime.now(DateTimeZone.UTC);
  }

  @Override
  public List<Expense> getExpenses(String authToken) {

    if (authToken != Constants.TEST_AUTH_TOKEN)
      throw new RuntimeException("Invalid auth token");

    return mExpensesList;
  }

  @Override
  public Expense getExpenseById(String authToken, int id) {
    if (authToken != Constants.TEST_AUTH_TOKEN)
      throw new RuntimeException("Invalid auth token");

    return mExpenseFluentIterable.firstMatch(e -> e._id == id).orNull();
  }

  @Override
  public List<Expense> getExpensesForMonth(String authToken, DateTime date) {
    if (authToken != Constants.TEST_AUTH_TOKEN)
      throw new RuntimeException("Invalid auth token");

    return mExpenseFluentIterable.filter(e -> e.date.toDateTimeISO() == date.toDateTimeISO()).toList();
  }

  @Override
  public List<Expense> getExpensesForYear(String authToken, int year) {
    if (authToken != Constants.TEST_AUTH_TOKEN)
      throw new RuntimeException("Invalid auth token");

    return mExpenseFluentIterable.filter(e -> e.date.getYear() == year).toList();
  }

  @Override
  public List<Expense> getExpensesForDay(String authToken, DateTime date) {
    if (authToken != Constants.TEST_AUTH_TOKEN)
      throw new RuntimeException("Invalid auth token");

    return mExpenseFluentIterable.filter(e -> e.date.toDate() == date.toDate()).toList();
  }

  @Override
  public ChangeSet<Expense> getUpdatedExpenses(String authToken, DateTime updatedSince) {
    return null;
  }

  @Override
  public void saveExpense(String authToken, String userId, Expense expense) {
    if (authToken != Constants.TEST_AUTH_TOKEN)
      throw new RuntimeException("Invalid auth token");

    if (expense._id != 0) {
      Expense existing = getExpenseById(authToken, expense._id);
      if (existing != null)
        mExpensesList.remove(existing);
    } else {
      expense._id = ++mLastId;
      mExpensesList.add(expense);
    }

    mExpenseFluentIterable = FluentIterable.from(mExpensesList);
  }
}
