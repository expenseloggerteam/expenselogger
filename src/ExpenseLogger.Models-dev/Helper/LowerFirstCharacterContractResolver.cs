﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Newtonsoft.Json.Serialization;

namespace ExpenseLogger.Models.Dev.Helper
{
  /// <summary>
  ///   JSON contract resolver to change the first character of
  ///   a property to lower case making it lower camel case.
  ///   <br></br>
  ///   For eg: PropertyName => propertyName
  /// </summary>
  public class LowerFirstCharacterContractResolver : DefaultContractResolver
  {
    /// <summary>
    ///   Resolves the property name
    /// </summary>
    /// <param name="propertyName">Property name to be resolved</param>
    /// <returns>Resolved property name</returns>
    protected override string ResolvePropertyName(string propertyName)
    {
      if (string.IsNullOrEmpty(propertyName))
        throw new ArgumentException("Property name can not be null or empty");

      string str = char.ToLowerInvariant(propertyName[0]) + propertyName.Substring(1);

      return str;
    }
  }
}