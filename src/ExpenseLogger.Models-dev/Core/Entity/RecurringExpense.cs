﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExpenseLogger.Models.Dev.Core.Entity
{
  /// <summary>
  ///   A recurring expense
  /// </summary>
  [Table("RecurringExpenses")]
  public class RecurringExpense
  {
    /// <summary>
    ///   Expense id
    /// </summary>
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    /// <summary>
    ///   Date of expense
    /// </summary>
    public DateTime ExpenseDate { get; set; }

    /// <summary>
    ///   Description of expense
    /// </summary>
    [Required(AllowEmptyStrings = false)]
    public string Description { get; set; }

    /// <summary>
    ///   Amount spent
    /// </summary>
    [Range(0.01, double.MaxValue)]
    public double Amount { get; set; }

    /// <summary>
    ///   User id associated with this expense
    /// </summary>
    [ForeignKey("User")]
    public string UserId { get; set; }

    /// <summary>
    ///   Expense category
    /// </summary>
    [ForeignKey("Category")]
    public int CategoryId { get; set; }

    /// <summary>
    ///   Expense recurrance type
    /// </summary>
    public ERecurType RecurType { get; set; }

    /// <summary>
    ///   Whether this recurring expense is enabled
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    ///   Associated user profile
    /// </summary>
    public virtual User User { get; set; }

    /// <summary>
    ///   Expense category. Note. This property is lazy loaded.
    /// </summary>
    public virtual Category Category { get; set; }
  }
}