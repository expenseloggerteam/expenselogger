﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Nancy.Security;

namespace ExpenseLogger.Models.Dev.Core.Entity
{
  /// <summary>
  ///   Represents a user profile
  /// </summary>
  public class User : IUserIdentity
  {
    /// <summary>
    ///   User id
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    ///   User identifier
    /// </summary>
    public Guid Identifier { get; private set; }

    /// <summary>
    ///   User full name
    /// </summary>
    [Required]
    public string FullName { get; set; }

    /// <summary>
    ///   Password hash
    /// </summary>
    public string PasswordHash { get; private set; }

    /// <summary>
    ///   User's country
    /// </summary>
    [Required]
    public string Country { get; set; }

    /// <summary>
    ///   User secret question
    /// </summary>
    [Required]
    [DisplayName("Secret Question")]
    public string SecretQuestion { get; set; }

    /// <summary>
    ///   User secret answer
    /// </summary>
    [Required]
    [DisplayName("Secret Answer")]
    public string SecretAnswer { get; set; }

    /// <summary>
    ///   Member since/user registration date
    /// </summary>
    [Required]
    public DateTime MemberSince { get; set; }

    /// <summary>
    ///   Last activity date
    /// </summary>
    [DisplayName("Last Logged In")]
    public DateTime LastLoginDate { get; set; }

    /// <summary>
    ///   User settings as JSON
    /// </summary>
    [Required]
    public string SettingsJson { get; set; }

    #region Implementation of IUserIdentity

    /// <summary>
    ///   The username of the authenticated user.
    /// </summary>
    /// <value>
    ///   A <see cref="T:System.String" /> containing the username.
    /// </value>
    public string UserName { get; private set; }

    /// <summary>
    ///   The claims of the authenticated user.
    /// </summary>
    /// <value>
    ///   An <see cref="T:System.Collections.Generic.IEnumerable`1" />, containing the claims.
    /// </value>
    public IEnumerable<string> Claims { get; private set; }

    #endregion

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="identifier">User identifier</param>
    /// <param name="userName">Username</param>
    /// <param name="passwordHash">Password hash</param>
    /// <param name="claims">User claims</param>
    public User(Guid identifier, string userName, string passwordHash, IEnumerable<string> claims = null)
    {
      Identifier = identifier;
      UserName = userName;
      PasswordHash = passwordHash;
      Claims = claims == null ? new string[0] : claims;
      LastLoginDate = DateTime.MinValue;
    }
  }
}