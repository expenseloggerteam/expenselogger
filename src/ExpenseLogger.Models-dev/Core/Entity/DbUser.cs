﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using ExpenseLogger.Models.Dev.Data;
using Nancy.Security;

namespace ExpenseLogger.Models.Dev.Core.Entity
{
  internal class DbUser : IUserIdentity
  {
    /// <summary>
    ///   User id
    /// </summary>
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }

    /// <summary>
    ///   User identifier
    /// </summary>
    public Guid Identifier { get; set; }

    /// <summary>
    ///   User full name
    /// </summary>
    public string FullName { get; set; }

    /// <summary>
    ///   Password hash
    /// </summary>
    public string PasswordHash { get; set; }

    /// <summary>
    ///   User's country
    /// </summary>
    public string Country { get; set; }

    /// <summary>
    ///   User secret question
    /// </summary>
    public string SecretQuestion { get; set; }

    /// <summary>
    ///   User secret answer
    /// </summary>
    public string SecretAnswer { get; set; }

    /// <summary>
    ///   Member since/user registration date
    /// </summary>
    public DateTime MemberSince { get; set; }

    /// <summary>
    ///   Last activity date
    /// </summary>
    public DateTime LastLoginDate { get; set; }

    /// <summary>
    ///   User settings as JSON
    /// </summary>
    public string SettingsJson { get; set; }

    #region Implementation of IUserIdentity

    /// <summary>
    ///   The username of the authenticated user.
    /// </summary>
    /// <value>
    ///   A <see cref="T:System.String" /> containing the username.
    /// </value>
    public string UserName { get; set; }

    /// <summary>
    ///   The claims of the authenticated user.
    /// </summary>
    /// <value>
    ///   An <see cref="T:System.Collections.Generic.IEnumerable`1" />, containing the claims.
    /// </value>
    public IEnumerable<string> Claims { get; set; }

    #endregion

    /// <summary>
    ///   Converts the current <see cref="DbUser" /> to an <see cref="User" />
    /// </summary>
    /// <returns>A <see cref="User" /></returns>
    public User ToUser()
    {
      User u = new User(Identifier, UserName, PasswordHash, Claims)
      {
        Id = Id,
        FullName = FullName,
        Country = Country,
        SecretQuestion = SecretAnswer,
        SecretAnswer = SecretAnswer,
        SettingsJson = SettingsJson,
        LastLoginDate = LastLoginDate,
        MemberSince = MemberSince
      };

      return u;
    }

    /// <summary>
    ///   Create the current <see cref="DbUser" /> from an <see cref="User" />
    /// </summary>
    /// <param name="user">The <see cref="User" /> to convert from</param>
    /// <returns>A <see cref="DbUser" /></returns>
    public DbUser FromUser(User user)
    {
      Id = user.Id;
      Identifier = user.Identifier;
      UserName = user.UserName;
      PasswordHash = user.PasswordHash;
      Claims = user.Claims;
      FullName = user.FullName;
      Country = user.Country;
      SecretQuestion = user.SecretQuestion;
      SecretAnswer = user.SecretAnswer;
      SettingsJson = user.SettingsJson;
      LastLoginDate = user.LastLoginDate;
      MemberSince = user.MemberSince;

      return this;
    }
  }
}