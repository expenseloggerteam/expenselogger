﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel.DataAnnotations;

namespace ExpenseLogger.Models.Dev.Core.Entity
{
  /// <summary>
  ///   Denotes an expense
  /// </summary>
  public class Expense : AbstractEntity
  {
    /// <summary>
    ///   Date of expense
    /// </summary>
    public DateTime ExpenseDate { get; set; }

    /// <summary>
    ///   Description of expense
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    ///   Amount spent
    /// </summary>
    [Range(0.01, double.MaxValue)]
    public double Amount { get; set; }

    /// <summary>
    ///   Expense category
    /// </summary>
    public int CategoryId { get; set; }
  }
}