﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ExpenseLogger.Models.Dev.Data;

namespace ExpenseLogger.Models.Dev.Core.Entity
{
  /// <summary>
  ///   Denotes a database entity class
  /// </summary>
  public abstract class AbstractEntity
  {
    /// <summary>
    ///   Expense id
    /// </summary>
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }

    /// <summary>
    ///   User id associated with this expense
    /// </summary>
    public int UserId { get; set; }
  }
}