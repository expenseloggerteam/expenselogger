﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace ExpenseLogger.Models.Dev.Core
{
  /// <summary>
  ///   Global application wide constants
  /// </summary>
  public static class Constants
  {
    /// <summary>
    ///   Path separation character based on the environment Windows/Linux
    /// </summary>
    public static readonly string PathSeparationChar = "" + Path.DirectorySeparatorChar;

    /// <summary>
    ///   Default display date time format
    /// </summary>
    public const string DisplayDateTimeFormat = "dd MMM yyyy";

    /// <summary>
    ///   ISO date time format string
    /// </summary>
    public const string IsoDateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    /// <summary>
    ///   Min date in system
    /// </summary>
    public static readonly DateTime MinDate = new DateTime(2010, 1, 1, 0, 0, 0, DateTimeKind.Local);

    /// <summary>
    ///   Administrator user's id
    /// </summary>
    public static readonly string AdminUserId = "";

    /// <summary>
    ///   Initialisation of constants
    /// </summary>
    public static void Init()
    {
    }
  }
}