﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Models.Dev.Abstract;
using ExpenseLogger.Models.Dev.Core;
using ExpenseLogger.Models.Dev.Core.Entity;
using ExpenseLogger.Models.Dev.Helper;
using Nancy;
using Nancy.Security;

namespace ExpenseLogger.Models.Dev.Concrete
{
  /// <summary>
  ///   Fake user repository
  /// </summary>
  public class FakeUserRepository : Disposable, IUserRepository
  {
    private static readonly List<User> m_users;

    #region Implementation of IUserMapper

    /// <summary>
    ///   Constructor
    /// </summary>
    static FakeUserRepository()
    {
      m_users = new List<User>();

      User admin = new User(Guid.NewGuid(), "admin", MD5Helper.CreateHash("admin123"))
      {
        Id = 1,
        FullName = "System Administrator",
        Country = "India",
        SecretQuestion = "No question",
        SecretAnswer = "No Answer",
        MemberSince = new DateTime(2016, 5, 4, 0, 0, 0, DateTimeKind.Local),
        SettingsJson = string.Empty
      };

      m_users.Add(admin);
    }

    /// <summary>
    ///   Get the real username from an identifier
    /// </summary>
    /// <param name="identifier">User identifier</param>
    /// <param name="context">The current NancyFx context</param>
    /// <returns>
    ///   Matching populated IUserIdentity object, or empty
    /// </returns>
    public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context)
    {
      IUserIdentity identity = m_users.FirstOrDefault(f => f.Identifier == identifier);

      return identity;
    }

    #endregion Implementation of IUserMapper

    #region Implementation of IUserProfileRepository

    /// <summary>
    ///   Data persistence type of the repository
    /// </summary>
    public EPersistenceType RepoStore
    {
      get { return EPersistenceType.SQLite; }
    }

    /// <summary>
    ///   Whether the given user is valid
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <param name="password">Password of user</param>
    /// <returns>A valid <see cref="User" /> if found, else null</returns>
    public User IsValidUser(string username, string password)
    {
      string passwordHash = MD5Helper.CreateHash(password);

      User user = m_users.FirstOrDefault(u => u.UserName == username && u.PasswordHash == passwordHash);

      return user;
    }

    /// <summary>
    ///   Get a user based on username
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <returns>User if found, else null</returns>
    public User GetUser(string username)
    {
      User user = m_users.FirstOrDefault(u => u.UserName == username);

      return user;
    }

    #endregion Implementation of IUserProfileRepository
  }
}