﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Models.Dev.Abstract;
using ExpenseLogger.Models.Dev.Core;
using ExpenseLogger.Models.Dev.Core.Entity;

namespace ExpenseLogger.Models.Dev.Concrete
{
  /// <summary>
  ///   Fake data repository
  /// </summary>
  public class FakeDataRepository : AbstractDataRepository
  {
    private static List<Category> m_categories;
    private static List<Expense> m_expenses;

    /// <summary>
    ///   Constructor
    /// </summary>
    static FakeDataRepository()
    {
      m_categories = new List<Category>
      {
        new Category {Id = 1, UserId = 1, Description = "Food"},
        new Category {Id = 2, UserId = 1, Description = "Rent/Mortgage"},
        new Category {Id = 3, UserId = 1, Description = "Transport"},
        new Category {Id = 4, UserId = 1, Description = "Bills"},
        new Category {Id = 5, UserId = 1, Description = "Misc"}
      };

      Random rand = new Random(DateTime.Now.Millisecond);

      m_expenses = Enumerable.Range(0, 60)
        .Select(f => new Expense
        {
          Id = f + 1,
          CategoryId = rand.Next(1, 5),
          UserId = 1,
          ExpenseDate = DateTime.Now.Date.AddDays(f),
          Amount = rand.Next(1, 1000)/0.1,
          Description = "fake expense " + f
        })
        .ToList();

      // TODO:
      // probably setup a timer here so if we get close to running out of data
      // it'll re-insert new data
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="userRepo">User repository</param>
    public FakeDataRepository(IUserRepository userRepo)
      : base(new FakeUserRepository())
    {
    }

    #region Implementation of IDataRepository

    /// <summary>
    /// All expenses
    /// </summary>
    protected override IEnumerable<Expense> Expenses
    {
      get { return m_expenses; }
    }

    /// <summary>
    /// All categories
    /// </summary>
    protected override IEnumerable<Category> Categories
    {
      get { return m_categories; }
    }

    /// <summary>
    ///   Save an expense
    /// </summary>
    /// <param name="expense">Expense to be saved</param>
    /// <returns>True if successful, else false</returns>
    public override bool SaveExpense(Expense expense)
    {
      Expense existing = m_expenses.FirstOrDefault(f => f.Id == expense.Id);

      if (existing == null)
      {
        expense.Id = m_expenses.Count + 1;
        m_expenses.Add(expense);
      }
      else
      {
        m_expenses.Remove(existing);
        m_expenses.Add(expense);

        m_expenses = m_expenses.OrderBy(f => f.Id).ToList();
      }

      return true;
    }

    /// <summary>
    ///   Get expenses
    /// </summary>
    /// <param name="filter">Filter to be applied</param>
    /// <returns>List of expenses</returns>
    public override List<Expense> GetExpenses(EFilter filter)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}