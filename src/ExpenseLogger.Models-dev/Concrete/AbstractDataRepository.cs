﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseLogger.Models.Dev.Abstract;
using ExpenseLogger.Models.Dev.Core;
using ExpenseLogger.Models.Dev.Core.Entity;

namespace ExpenseLogger.Models.Dev.Concrete
{
  /// <summary>
  /// A generic abstract data repository
  /// </summary>
  public abstract class AbstractDataRepository : Disposable, IDataRepository
  {
    /// <summary>
    /// User repository
    /// </summary>
    protected IUserRepository UserRepo { get; private set; }

    /// <summary>
    /// All expenses
    /// </summary>
    protected abstract IEnumerable<Expense> Expenses { get; }

    /// <summary>
    /// All categories
    /// </summary>
    protected abstract IEnumerable<Category> Categories { get; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="userRepo">User repository</param>
    protected AbstractDataRepository(IUserRepository userRepo)
    {
      UserRepo = userRepo;
    }

    #region Implementation of IDataRepository

    /// <summary>
    ///   Save an expense
    /// </summary>
    /// <param name="expense">Expense to be saved</param>
    /// <returns>True if successful, else false</returns>
    public abstract bool SaveExpense(Expense expense);

    /// <summary>
    ///   Get expenses
    /// </summary>
    /// <param name="filter">Filter to be applied</param>
    /// <returns>List of expenses</returns>
    public abstract List<Expense> GetExpenses(EFilter filter);

    #endregion

    /// <summary>
    ///   Get data
    /// </summary>
    /// <typeparam name="T">Type to get data for</typeparam>
    /// <param name="id">Id of the data</param>
    /// <returns>Data if found, else null</returns>
    public virtual T Get<T>(int id) where T : AbstractEntity
    {
      Type ttype = typeof(T);

      object val = null;
      switch (ttype.FullName)
      {
        case EntityName.ExpenseDataType:
          val = Expenses.FirstOrDefault(f => f.Id == id);
          break;

        case EntityName.CategoryDataType:
          val = Categories.FirstOrDefault(f => f.Id == id);
          break;
      }

      return val as T;
    }

    /// <summary>
    ///   Get all data for given user
    /// </summary>
    /// <typeparam name="T">Type to get data for</typeparam>
    /// <param name="username">Username for which to retrieve data</param>
    /// <returns>Data is found, else empty list</returns>
    public virtual List<T> GetAll<T>(string username) where T : AbstractEntity
    {
      List<T> result = new List<T>();

      User u = UserRepo.GetUser(username);

      Func<IEnumerable<AbstractEntity>, List<T>> runFn = data =>
      {
        var retVal = string.IsNullOrWhiteSpace(username) ? data : data.Where(f => f.UserId == u.Id).Cast<T>().ToList();

        return (List<T>) retVal;
      };

      Type ttype = typeof(T);

      switch (ttype.FullName)
      {
        case EntityName.ExpenseDataType:
          result = runFn(Expenses);
          break;

        case EntityName.CategoryDataType:
          result = runFn(Categories);
          break;
      }

      return result;
    }
  }
}