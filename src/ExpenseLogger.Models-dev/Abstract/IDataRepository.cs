﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using ExpenseLogger.Models.Dev.Core;
using ExpenseLogger.Models.Dev.Core.Entity;

namespace ExpenseLogger.Models.Dev.Abstract
{
  /// <summary>
  ///   Expense Logger repository
  /// </summary>
  public interface IDataRepository : IDisposable
  {
    /// <summary>
    ///   Get data
    /// </summary>
    /// <typeparam name="T">Type to get data for</typeparam>
    /// <param name="id">Id of the data</param>
    /// <returns>Data if found, else null</returns>
    T Get<T>(int id) where T : AbstractEntity;

    /// <summary>
    ///   Get all data
    /// </summary>
    /// <typeparam name="T">Type to get data for</typeparam>
    /// <param name="username">Username for which to retrieve data</param>
    /// <returns>Data is found, else empty list</returns>
    List<T> GetAll<T>(string username) where T : AbstractEntity;

    /// <summary>
    ///   Save an expense
    /// </summary>
    /// <param name="expense">Expense to be saved</param>
    /// <returns>True if successful, else false</returns>
    bool SaveExpense(Expense expense);

    /// <summary>
    ///   Get expenses
    /// </summary>
    /// <param name="filter">Filter to be applied</param>
    /// <returns>List of expenses</returns>
    List<Expense> GetExpenses(EFilter filter);
  }
}