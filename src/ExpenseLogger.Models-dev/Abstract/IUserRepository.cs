﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ExpenseLogger.Models.Dev.Core;
using ExpenseLogger.Models.Dev.Core.Entity;
using Nancy.Authentication.Forms;

namespace ExpenseLogger.Models.Dev.Abstract
{
  /// <summary>
  ///   User profile repository
  /// </summary>
  public interface IUserRepository : IUserMapper, IDisposable
  {
    /// <summary>
    ///   Data persistence type of the repository
    /// </summary>
    EPersistenceType RepoStore { get; }

    /// <summary>
    ///   Whether the given user is valid
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <param name="password">Password of user</param>
    /// <returns>A valid <see cref="User" /> if found, else null</returns>
    User IsValidUser(string username, string password);

    /// <summary>
    ///   Get a user based on username
    /// </summary>
    /// <param name="username">Username of user</param>
    /// <returns>User if found, else null</returns>
    User GetUser(string username);
  }
}