﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Linq;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ExpenseLogger.Models.Dev.Abstract;
using ExpenseLogger.Models.Dev.Concrete;

namespace ExpenseLogger.Models.Dev.IOC
{
  /// <summary>
  ///   A <see cref="IWindsorInstaller" /> for all services/repositories/factories contained in
  ///   <see cref="AFLThingi.Models" /> namespace
  /// </summary>
  public class ModelsWindsorInstaller : IWindsorInstaller
  {
    /// <summary>
    ///   Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
    /// </summary>
    /// <param name="container">The container.</param>
    /// <param name="store">The configuration store.</param>
    public void Install(IWindsorContainer container, IConfigurationStore store)
    {
      container.Register(Component.For<IKernel>().Named("kernel").Instance(container.Kernel).OnlyNewServices());

      if (container.Kernel.GetFacilities().Count(f => f.GetType() == typeof (ConstantsInitFacility)) == 0)
        container.AddFacility(new ConstantsInitFacility());

      container.Register(Component.For<IUserRepository>().ImplementedBy<FakeUserRepository>().LifeStyle.Singleton);

      container.Register(Component.For<IDataRepository>().ImplementedBy<FakeDataRepository>().LifeStyle.Singleton);

    }
  }
}