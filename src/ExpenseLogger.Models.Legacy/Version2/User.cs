﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ExpenseLogger.Core;

namespace ExpenseLogger.Models.Legacy.Version2
{
  /// <summary>
  ///   Represents a user profile
  /// </summary>
  [Table("Users")]
  public class User : AbstractEntity<short>
  {
#pragma warning disable 1591
    public string Name { get; set; }

    public string Username { get; set; }

    [DataType(DataType.Password)]
    public string Password { get; set; }

    public string Email { get; set; }

    public DateTime MemberSince { get; set; }

    [DisplayName("Secret Question")]
    public string SecretQuestion { get; set; }

    [DisplayName("Secret Answer")]
    public string SecretAnswer { get; set; }

    public bool Verified { get; set; }

    public string VerifyGuid { get; set; }
#pragma warning restore 1591
  }
}