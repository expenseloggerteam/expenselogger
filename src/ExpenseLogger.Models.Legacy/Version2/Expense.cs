﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ExpenseLogger.Core;
using Newtonsoft.Json;

namespace ExpenseLogger.Models.Legacy.Version2
{
  /// <summary>
  ///   Denotes an expense
  /// </summary>
  [Table("Expenses")]
  public class Expense : AbstractEntity
  {
#pragma warning disable 1591
    public short UserId { get; set; }

    [Required(ErrorMessage = "The Category field is required")]
    public short CategoryId { get; set; }

    [Required]
    public DateTime ExpenseDate { get; set; }

    [Required(AllowEmptyStrings = false)]
    public string Description { get; set; }

    [Required]
    public double Amount { get; set; }

    [JsonIgnore]
    [ForeignKey("CategoryId")]
    public virtual Category Category { get; set; }

    [JsonIgnore]
    [ForeignKey("UserId")]
    public virtual User User { get; set; }
#pragma warning restore 1591
  }
}