﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Data.Entity;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Data;

namespace ExpenseLogger.Models.Legacy.Version2
{
  /// <summary>
  ///   A <see cref="DbContext" /> for ExpenseLogger v2 database tables
  /// </summary>
  [DbConfigurationType(typeof(DatabaseConfiguration))]
  public class SQLServerDataContextV2 : DbContext
  {
    public DbSet<Expense> Expenses { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Reminder> Reminders { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<UserSetting> UserSettings { get; set; }
    public DbSet<RecurringExpense> RecurringExpenses { get; set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="connString">Connection string to the database</param>
    public SQLServerDataContextV2(string connString)
      : base(DatabaseConnectionFactory.CreateConnection(EPersistenceType.SqlServer, connString), true)
    {
    }
  }
}