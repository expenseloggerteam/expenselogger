﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Diagnostics;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Extensions.SubSystems;
using ExpenseLogger.Api.IIS;
using ExpenseLogger.Api.Models;
using ExpenseLogger.Api.Models.Config;
using ExpenseLogger.Api.Models.IOC;
using ExpenseLogger.Core.Helpers;
using log4net;
using log4net.Appender;
using log4net.Config;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(IISOwinStartup))]

namespace ExpenseLogger.Api.IIS
{
  /// <summary>
  ///   An OWIN startup implementation which actually internally calls to <see cref="Startup" />
  /// </summary>
  public class IISOwinStartup
  {
    public void Configuration(IAppBuilder app)
    {
      GlobalContext.Properties["pid"] = Process.GetCurrentProcess().Id;

      IAppender appender = Log4NetHelper.GetRollingFileAppender();
      BasicConfigurator.Configure(appender);

      ILog logger = LogManager.GetLogger("IISOwinStartup");
      logger.EmptyLine();

      logger.Info("Initializing components");
      //IWindsorInstaller defaultConf = Configuration.FromAppConfig();
      IWindsorInstaller propertiesSubSystemConf = PropertiesSubSystem.FromAppConfig();
      IWindsorContainer container = new WindsorContainer();
      container.Install(propertiesSubSystemConf);
      container.Install(new WebWindsorInstaller());

      container.Register(Component.For<AbstractSwaggerPathProvider>().ImplementedBy<IISSwaggerPathProvider>().LifeStyle.Transient);

      ApiConfigProperty cfg = container.Resolve<ApiConfigProperty>();

      logger.Info("Starting ExpenseLogger Http Server");

      // delegate initialisation to the internal class
      new Startup(container).Configuration(app);

      logger.Info("Server started");
    }
  }
}