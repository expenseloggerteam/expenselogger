﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.IO;
using System.Web.Hosting;
using ExpenseLogger.Api.Models.Config;

namespace ExpenseLogger.Api.IIS
{
  /// <summary>
  ///   An <see cref="AbstractSwaggerPathProvider" /> for providing swagger docs path for a IIS hosted app
  /// </summary>
  public class IISSwaggerPathProvider : AbstractSwaggerPathProvider
  {
    #region Overrides of AbstractSwaggerPathProvider

    /// <inheritdoc />
    public override string GetXmlCommentsPath()
    {
      return Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "bin", SwaggerDocsFileName);
    }

    #endregion Overrides of AbstractSwaggerPathProvider
  }
}