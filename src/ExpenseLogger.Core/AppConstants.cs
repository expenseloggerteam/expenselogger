﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Linq;

namespace ExpenseLogger.Core
{
  /// <summary>
  ///   Application wide constants
  /// </summary>
  public static class AppConstants
  {
    /// <summary>
    ///   Flag indicating whether the application is running in demo mode. Defaults to false
    /// </summary>
    public static bool DemoFlag = false;

    /// <summary>
    ///   Flag indicating whether the database should be seeded with demo data. Defaults to true
    /// </summary>
    public static bool SeedDataFlag = true;

    /// <summary>
    ///   Application name
    /// </summary>
    public const string ApplicationName = "ExpenseLogger";

    /// <summary>
    ///   Expenses database connection string name
    /// </summary>
    public const string ExpensesDbConnStringName = "ExpensesDbConnString";

    /// <summary>
    ///   Username of the super admin
    /// </summary>
    public const string AdminUsername = "admin";

    /// <summary>
    ///   Default value set for <see cref="int" /> fields set by all constructors
    /// </summary>
    public const int DefaultInt32NotSetValue = -99;

    /// <summary>
    ///   Path separation character based on the environment Windows/Linux
    /// </summary>
    public static readonly string PathSeparationChar = "" + Path.DirectorySeparatorChar;

    /// <summary>
    ///   Default display date time format
    /// </summary>
    public const string DisplayDateTimeFormat = "dd MMM yyyy";

    /// <summary>
    ///   ISO date time format string
    /// </summary>
    public const string IsoDateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    /// <summary>
    /// No. of days in a quarter
    /// </summary>
    public const int NumDaysInQuarter = 120;

    /// <summary>
    ///   Min date in system
    /// </summary>
    public static readonly DateTime MinDate = new DateTime(2010, 1, 1, 0, 0, 0, DateTimeKind.Local);

    /// <summary>
    ///   Administrator user's id
    /// </summary>
    public static readonly string AdminUserId = "";

    /// <summary>
    ///   Cache directory path
    /// </summary>
    public static string CacheDirectory;

    /// <summary>
    ///   Returns a pretty formatted line break string to
    ///   be printed on the console
    /// </summary>
    /// <param name="c">[Optional] The character to be printed. Defaults to -</param>
    public static string LineBreak(char c = '-')
    {
      string linebreak = string.Join("" + c, Enumerable.Range(1, 40).Select(f => "" + c));
      return linebreak;
    }

    /// <summary>
    ///   Initialisation of constants
    /// </summary>
    public static void Init()
    {
      if (string.IsNullOrWhiteSpace(CacheDirectory))
        return;

      if (!Directory.Exists(CacheDirectory))
        Directory.CreateDirectory(CacheDirectory);

    }
  }
}