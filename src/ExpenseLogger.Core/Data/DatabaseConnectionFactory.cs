﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using MySql.Data.MySqlClient;
using Npgsql;

namespace ExpenseLogger.Core.Data
{
  /// <summary>
  ///   Custom database connection factory
  /// </summary>
  public static class DatabaseConnectionFactory
  {
    /// <summary>
    ///   Creates a connection
    /// </summary>
    /// <param name="type">Persistence type</param>
    /// <param name="connString">Connection string</param>
    /// <returns>Database connection</returns>
    public static DbConnection CreateConnection(EPersistenceType type, string connString)
    {
      const int maxPoolSize = 50;
      DbConnection conn = null;

      switch (type)
      {
        case EPersistenceType.Postgresql:
          var npgsqlBuilder = new NpgsqlConnectionStringBuilder(connString)
          {
            ApplicationName = "ExpenseLogger",
            MaxPoolSize = maxPoolSize,
            SslMode = SslMode.Prefer
          };

          conn = new NpgsqlConnection(npgsqlBuilder);
          break;

        case EPersistenceType.SQLite:
          var sqLiteBuilder = new SQLiteConnectionStringBuilder(connString)
          {
            {"pooling", true},
            {"max pool size", maxPoolSize},
            {"auto vaccuum", "incremental"}
          };

          conn = new SQLiteConnection(sqLiteBuilder.ToString(), true);
          break;

        case EPersistenceType.SqlServer:
          var sqlServerBuilder = new SqlConnectionStringBuilder(connString)
          {
            MultipleActiveResultSets = true,
            Pooling = true,
            MaxPoolSize = maxPoolSize
          };

          conn = new SqlConnection(sqlServerBuilder.ToString());
          break;

        case EPersistenceType.MySql:
          var mysqlBuilder = new MySqlConnectionStringBuilder(connString)
          {
            SslMode = MySqlSslMode.Preferred,
            AllowBatch = true,
            AllowZeroDateTime = false,
            MaximumPoolSize = maxPoolSize,
            Pooling = true
          };

          conn = new MySqlConnection(mysqlBuilder.ToString());
          break;
      }

      if (conn == null)
        throw new NullReferenceException("Database connection could not be created for persistence type: " + type);

      if (string.IsNullOrWhiteSpace(conn.Database))
        throw new ConfigurationErrorsException("You must specify a database in the connection string");

      return conn;
    }
  }
}