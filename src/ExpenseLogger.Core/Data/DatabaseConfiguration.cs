﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Data.Entity.Migrations.History;
using System.Data.Entity.SqlServer;
using System.Data.SQLite;
using System.Data.SQLite.EF6;
using MySql.Data.MySqlClient;
using Npgsql;

namespace ExpenseLogger.Core.Data
{
  /// <summary>
  ///   Code based database configuration
  /// </summary>
  public class DatabaseConfiguration : DbConfiguration
  {
    /// <summary>
    ///   SQLite EntityFramework 6 powered provider invariant name
    /// </summary>
    public static readonly string SQLiteEF6ProviderInvariantName = "System.Data.SQLite.EF6";

    /// <summary>
    ///   SQLite powered provider invariant name
    /// </summary>
    public static readonly string SQLiteProviderInvariantName = "System.Data.SQLite";

    /// <summary>
    ///   Postgresql powered provider invariant name
    /// </summary>
    public static readonly string NpgsqlProviderInvariantName = "Npgsql";

    /// <summary>
    ///   MySQL powered provider invariant name
    /// </summary>
    public static readonly string MySqlProviderInvariantName = MySql.Data.Entity.MySqlProviderInvariantName.ProviderName;

    /// <summary>
    ///   Constructor
    /// </summary>
    public DatabaseConfiguration()
    {
      // SqlServer
      SetProviderServices(SqlProviderServices.ProviderInvariantName, SqlProviderServices.Instance);

      // SQLite
      SetProviderFactory(SQLiteProviderInvariantName, SQLiteFactory.Instance);
      SetProviderFactory(SQLiteEF6ProviderInvariantName, SQLiteProviderFactory.Instance);
      SetProviderServices(SQLiteProviderInvariantName, (DbProviderServices)SQLiteProviderFactory.Instance.GetService(typeof(DbProviderServices)));
      SetProviderServices(SQLiteEF6ProviderInvariantName, (DbProviderServices)SQLiteProviderFactory.Instance.GetService(typeof(DbProviderServices)));

      // Npgsql (Postgresql)
      SetProviderFactory(NpgsqlProviderInvariantName, NpgsqlFactory.Instance);
      SetProviderServices(NpgsqlProviderInvariantName, NpgsqlServices.Instance);

      // MySql
      SetProviderFactory(MySqlProviderInvariantName, MySqlClientFactory.Instance);
      SetProviderServices(MySqlProviderInvariantName, new MySqlProviderServices());
      SetHistoryContext(MySqlProviderInvariantName, (existingConnection, defaultSchema) => (HistoryContext)new DefaultMySqlHistoryContext(existingConnection, defaultSchema));
    }
  }
}