﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations.History;

namespace ExpenseLogger.Core.Data
{
  /// <summary>
  ///   A default history context
  /// </summary>
  public class DefaultMySqlHistoryContext : HistoryContext
  {
    /// <inheritdoc />
    public DefaultMySqlHistoryContext(DbConnection existingConnection, string defaultSchema)
      : base(existingConnection, defaultSchema)
    {
      // nothing to do here
    }

    /// <inheritdoc />
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);
      modelBuilder.Entity<HistoryRow>().HasKey(h => new
      {
        h.MigrationId
      });
    }
  }
}