// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace ExpenseLogger.Core
{
  /// <summary>
  ///   A generic process result
  /// </summary>
  public class ProcessResult<TData> where TData : class
  {
    /// <summary>
    ///   Whether the result was a success
    /// </summary>
    public bool Success => Code == ECode.Ok;

    /// <summary>
    ///   Result code
    /// </summary>
    public ECode Code { get; private set; }

    /// <summary>
    ///   Result Message Summary
    /// </summary>
    public string Summary { get; private set; }

    /// <summary>
    ///   Result Message details
    /// </summary>
    public string Details { get; private set; }

    /// <summary>
    ///   Any data to be returned back to calling method
    /// </summary>
    public TData Data { get; private set; }

    /// <summary>
    ///   Default constructor. Creates a success result
    /// </summary>
    /// <param name="code">[Optional] Result code</param>
    /// <param name="summary">[Optional] Result message summary</param>
    /// <param name="details">[Optional] Result message details</param>
    /// <param name="data">[Optional] Any data to be returned back to calling method</param>
    public ProcessResult(ECode code = ECode.Ok, string summary = "", string details = "", TData data = null)
    {
      Details = details;
      Summary = string.IsNullOrEmpty(summary) ? "All OK" : summary;
      Code = code;
      Data = data;
    }

    /// <summary>
    ///   Constructor to create an error result
    /// </summary>
    /// <param name="code">Result code</param>
    /// <param name="ex">Associated result exception</param>
    /// <param name="data">[Optional] Any data to be returned back to calling method</param>
    public ProcessResult(ECode code, Exception ex, TData data = null)
    {
      Code = code;
      Summary = ex.Message;
      Details = ex.StackTrace;
      Data = data;
    }
  }

  /// <summary>
  ///   A process result
  /// </summary>
  public class ProcessResult : ProcessResult<object>
  {
    /// <summary>
    ///   Default constructor. Creates a success result
    /// </summary>
    /// <param name="code">[Optional] Result code</param>
    /// <param name="summary">[Optional] Result message summary</param>
    /// <param name="details">[Optional] Result message details</param>
    /// <param name="data">[Optional] Any data to be returned back to calling method</param>
    public ProcessResult(ECode code = ECode.Ok, string summary = "", string details = "", object data = null)
      : base(code, summary, details, data)
    {
      // nothing to do here
    }

    /// <summary>
    ///   Constructor to create an error result
    /// </summary>
    /// <param name="code">Result code</param>
    /// <param name="ex">Associated result exception</param>
    /// <param name="data">[Optional] Any data to be returned back to calling method</param>
    public ProcessResult(ECode code, Exception ex, object data = null)
      : base(code, ex, data)
    {
      // nothing to do here
    }
  }
}