﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseLogger.Core
{
  /// <summary>
  /// Run mode of the application
  /// </summary>
  public enum ERunMode
  {
    /// <summary>
    /// The default run mode i.e operation normal
    /// </summary>
    Default,

    /// <summary>
    /// Demo run mode. In this mode the database in re-initialised everytime the server is restarted
    /// </summary>
    Demo,

    
  }
}
