﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExpenseLogger.Core
{
  /// <summary>
  ///   An abstract entity inherited by all entity classes
  /// </summary>
  /// <typeparam name="TIdType">
  ///   The type of the ID field. Valid values are <see cref="Guid" />, <see cref="int" />,
  ///   <see cref="short" />
  /// </typeparam>
  public abstract class AbstractEntity<TIdType>
  {
    /// <summary>
    ///   Id column of table
    /// </summary>
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public TIdType Id { get; set; }
  }

  /// <summary>
  ///   An abstract entity inherited by all entity classes
  /// </summary>
  public abstract class AbstractEntity : AbstractEntity<int>
  {
    ///// <summary>
    /////   Id column of table
    ///// </summary>
    //[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //public int Id { get; set; }
  }
}