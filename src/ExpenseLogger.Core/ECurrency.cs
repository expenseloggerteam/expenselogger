﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using WebExtras.Core;

namespace ExpenseLogger.Core
{
  /// <summary>
  ///   All available currencies notations
  /// </summary>
  public enum ECurrency
  {
#pragma warning disable 1591
    [StringValue("fa-money")]
    Money,

    [StringValue("fa-jpy")]
    Yen,

    [StringValue("fa-inr")]
    Rupees,

    [StringValue("fa-usd")]
    Dollar,

    [StringValue("fa-gbp")]
    Pound,

    [StringValue("fa-eur")]
    Euro
#pragma warning restore 1591
  }

  /// <summary>
  ///   <see cref="ECurrency" /> extension methods
  /// </summary>
  public static class ECurrencyExtension
  {
    /// <summary>
    ///   Gets the unicode hex value representing this currency
    /// </summary>
    /// <param name="currency">Current currency</param>
    /// <returns>Unicode hex value of currency</returns>
    public static string GetUnicodeValue(this ECurrency currency)
    {
      switch (currency)
      {
        case ECurrency.Money:
          return "\u00a4";

        case ECurrency.Dollar:
          return "\u0024";

        case ECurrency.Euro:
          return "\u20ac";

        case ECurrency.Pound:
          return "\u00a3";

        case ECurrency.Rupees:
          return "\u20b9";

        case ECurrency.Yen:
          return "\u00a5";
      }

      return string.Empty;
    }
  }
}