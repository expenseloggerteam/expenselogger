// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma warning disable 1591

using WebExtras.Core;

namespace ExpenseLogger.Core
{
  /// <summary>
  ///   Predefined Result codes
  /// </summary>
  public enum ECode
  {
    [StringValue("No error")]
    Ok = 0,

    [StringValue("Generic error")]
    Error = 1,

    [StringValue("Cookies not supported")]
    CookiesNoSupported = 2,

    [StringValue("Invalid username or password")]
    InvalidUidPwd = 3,

    [StringValue("Account not verified")]
    NotVerified = 4,

    [StringValue("Username already exists")]
    UniqueAlias = 5,

    [StringValue("Email already exists")]
    UniqueEmail = 6,

    [StringValue("This operation is not allowed")]
    NotAllowed = 7,

    [StringValue("Generic warning")]
    Warning = 8
  }
}