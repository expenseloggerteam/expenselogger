// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace ExpenseLogger.Core.Helpers
{
  /// <summary>
  ///   Useful static testing methods.
  /// </summary>
  [EditorBrowsable(EditorBrowsableState.Never)]
  public sealed class IOResourceHelper
  {
    /// <summary>
    ///   Saves a resource from an assembly to a file.
    ///   Note. The location the file is saved to is relative to the executing directory (eg bin).
    ///   Note. The file is created in a subdirectory based on the resourceNameSpace (with "." replaced by "\").
    ///   NOTE: if the resource is in a subdirectory where the directory name contains only numbers
    ///   (eg. file rao in directory 94) this method will throw an "unable to locate resource" exception.
    ///   For some reason C# doesn't like the directory name to consist of only numbers...
    ///   We have no idea why this happens. To fix it, rename resource directories like "94" to "test94", etc.
    /// </summary>
    /// <param name="source">The assembly containing the resource.</param>
    /// <param name="resourceNameSpace">The namespace of the resource within the assembly and basis for destination directory.</param>
    /// <param name="resourceFileName">The name of the resource file to create.</param>
    /// <returns>The relative path to the saved file.</returns>
    public static string CreateResourceOnDisk(Assembly source, string resourceNameSpace, string resourceFileName)
    {
      // Destination directory is based on the resourceNameSpace.
      string destinationDir = resourceNameSpace.Replace(".", "\\");

      // Create the resource file and return its path to the caller
      return CreateResourceAtPath(source, resourceNameSpace, resourceFileName, destinationDir);
    }

    /// <summary>
    ///   Saves the specified resource from the source assembly to the path specified.
    ///   Note. Within the assembly the fully qualified name of the resource is expected to be resourcePath + "." +
    ///   resourceName.
    ///   NOTE: if the resource is in a subdirectory where the directory name contains only numbers
    ///   (eg. file rao in directory 94) this method will throw an "unable to locate resource" exception.
    ///   For some reason C# doesn't like the directory name to consist of only numbers...
    ///   We have no idea why this happens. To fix it, rename resource directories like "94" to "test94", etc.
    /// </summary>
    /// <param name="source">The assembly containing the resource.</param>
    /// <param name="resourcePath">
    ///   The fully qualified (ie namespace + subdirectory) resource path (excluding the
    ///   resourceName).
    /// </param>
    /// <param name="resourceName">The resource name (also becomes the file name on disk).</param>
    /// <param name="path">The path to save to (will be created if it doesn't exist).</param>
    /// <returns>The relative path to the saved file.</returns>
    public static string CreateResourceAtPath(Assembly source, string resourcePath, string resourceName, string path)
    {
      // Work out the fully qualified resource name in the assembly
      string fullResourceName = resourcePath + "." + resourceName;

      // Look for resource inside assembly
      Stream resourceStream = source.GetManifestResourceStream(fullResourceName);

      // Test that the resource was accessed correctly.
      if (resourceStream == null)
      {
        string message = string.Format(
          "ExpenseLogger.Core.IOResourceHelper.CreateResourceAtPath: Unable to locate resource {0} in assembly {1}",
          fullResourceName,
          source.Location);
        throw new Exception(message);
      }

      // Create directory
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);

      // Work out the file path
      string filePath = path + "\\" + resourceName;

      // Write resource to file.
      // Create a binary reader and writer
      BinaryReader br = new BinaryReader(resourceStream);
      BinaryWriter bw = new BinaryWriter(new FileStream(filePath, FileMode.Create));

      // Read a block of data at a time
      byte[] buffer = br.ReadBytes(4096);
      while (buffer.Length > 0)
      {
        // Write block of data
        bw.Write(buffer);
        buffer = br.ReadBytes(4096);
      }

      // Close the writer and reader
      bw.Close();
      br.Close();

      // Return the path to the resource file to the caller.
      return filePath;
    }

    /// <summary>
    ///   Deletes a temporary file which had been used to store a resource.
    ///   Any file created using IOResourceHelper.CreateResourceOnDisk(resourceNameSpace, resourceFileName)
    ///   can be deleted by IOResourceHelper.DeleteResourceFromDisk(resourceNameSpace, resourceFileName).
    ///   NOTE: This should be used with caution to avoid deleting necessary files!
    /// </summary>
    /// <param name="resourceNameSpace">The namespace of the resource whose temporary file is to be deleted.</param>
    /// <param name="resourceFileName">The file name of the resource.</param>
    public static void DeleteResourceFromDisk(string resourceNameSpace, string resourceFileName)
    {
      string destinationDir = resourceNameSpace.Replace(".", "\\");
      string destinationPath = destinationDir + "\\" + resourceFileName;

      // Delete the file if it exists.
      DeleteResourceFromDisk(destinationPath);
    }

    /// <summary>
    ///   Deletes a temporary file which had been used to store a resource.
    ///   Note. Any empty directories in the resourcePath will also be deleted.
    ///   NOTE: This should be used with caution to avoid deleting necessary files!
    /// </summary>
    /// <param name="resourcePath">The full path and file name of the temporary file to be deleted.</param>
    public static void DeleteResourceFromDisk(string resourcePath)
    {
      // Delete the file if it exists.
      if (File.Exists(resourcePath))
      {
        File.Delete(resourcePath);
      }

      // Delete any directories made empty by this file deletion.
      string currentDir = Directory.GetParent(resourcePath).FullName;
      if (!Directory.Exists(currentDir))
        return;

      while (Directory.GetFiles(currentDir).Length == 0 && Directory.GetDirectories(currentDir).Length == 0)
      {
        Directory.Delete(currentDir);
        currentDir = Directory.GetParent(currentDir).FullName;
      }
    }

    /// <summary>
    ///   Runs a command through the command line. This can be used to, for example, run a
    ///   batch file that copies data required for a test.
    /// </summary>
    /// <param name="workingDir">Working directory for the file to be run</param>
    /// <param name="fileName">File to be run on command line</param>
    /// <param name="args">Command to run through the command line.</param>
    public static int RunFromCommandLine(string workingDir, string fileName, string args)
    {
      Console.WriteLine("Starting command {0} with args {1}", fileName, args);
      Console.WriteLine("Working Directory: {0}", Directory.GetCurrentDirectory());

      // Run the given command using the command line in the given working directory.
      Process proc = new Process
      {
        EnableRaisingEvents = false,
        StartInfo =
        {
          WorkingDirectory = workingDir,
          FileName = fileName,
          Arguments = args,
          UseShellExecute = true
        }
      };
      proc.Start();

      // Make sure the process has completed and return the exit code.
      proc.WaitForExit();
      return proc.ExitCode;
    }

    /// <summary>
    ///   From http://www.codeproject.com/KB/cs/testnonpublicmembers.aspx?display=Print
    ///   Allows the running of private static methods via reflection.
    /// </summary>
    /// <param name="t">Type containing method to run.</param>
    /// <param name="strMethod">Name of method.</param>
    /// <param name="aobjParams">Parameters to pass to method.</param>
    /// <returns>Whatever method under test returns.</returns>
    public static object RunStaticMethod(Type t, string strMethod, object[] aobjParams)
    {
      const BindingFlags eFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
      return RunMethod(t, strMethod, null, aobjParams, eFlags);
    }

    /// <summary>
    ///   From http://www.codeproject.com/KB/cs/testnonpublicmembers.aspx?display=Print
    ///   Allows the running of private methods via reflection.
    /// </summary>
    /// <param name="t">Type containing method to run.</param>
    /// <param name="strMethod">Name of method.</param>
    /// <param name="objInstance">Instance of object containing method.</param>
    /// <param name="aobjParams">Parameters to pass to method.</param>
    /// <returns>Whatever method under test returns.</returns>
    public static object RunInstanceMethod(Type t, string strMethod, object objInstance, object[] aobjParams)
    {
      const BindingFlags eFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
      return RunMethod(t, strMethod, objInstance, aobjParams, eFlags);
    }

    /// <summary>
    ///   From http://www.codeproject.com/KB/cs/testnonpublicmembers.aspx?display=Print
    ///   Helper method to run private methods via reflection.
    /// </summary>
    /// <param name="t">Type containing method to run.</param>
    /// <param name="strMethod">Name of method.</param>
    /// <param name="objInstance">Instance of object containing method (null for static methods).</param>
    /// <param name="aobjParams">Parameters to pass to method.</param>
    /// <param name="eFlags">Binding flags (passed to GetMethod).</param>
    /// <returns>Whatever method under test returns.</returns>
    private static object RunMethod(Type t, string strMethod, object objInstance, object[] aobjParams, BindingFlags eFlags)
    {
      MethodInfo m = t.GetMethod(strMethod, eFlags);
      if (m == null)
        throw new ArgumentException("There is no method '" + strMethod + "' for type '" + t + "'.");

      object objRet = m.Invoke(objInstance, aobjParams);
      return objRet;
    }
  }
}