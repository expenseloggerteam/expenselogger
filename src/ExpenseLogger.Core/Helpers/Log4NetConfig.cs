﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Diagnostics;
using log4net;
using log4net.Appender;
using log4net.Config;

namespace ExpenseLogger.Core.Helpers
{
  /// <summary>
  ///   <see cref="log4net" /> configuration
  /// </summary>
  public static class Log4NetConfig
  {
    /// <summary>
    ///   Configures the <see cref="log4net" /> logging mechanism
    /// </summary>
    public static void Configure(string logFile)
    {
      GlobalContext.Properties["pid"] = Process.GetCurrentProcess().Id;

      IAppender appender = Log4NetHelper.GetRollingFileAppender(logFile);
      BasicConfigurator.Configure(appender);

      ILog logger = LogManager.GetLogger(typeof(Log4NetConfig));
      logger.EmptyLine();
    }
  }
}