﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Reflection;
using log4net;
using log4net.Appender;
using log4net.Layout;

namespace ExpenseLogger.Core.Helpers
{
  /// <summary>
  ///   Log4Net helper class to create default appenders
  /// </summary>
  public static class Log4NetHelper
  {
    /// <summary>
    /// Date time format string used to log date timestamps to log file/console
    /// </summary>
    public const string LogDateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    /// <summary>
    ///   Whether to use time stamps when logging to console
    /// </summary>
    public static bool UseTimestampOnConsole = true;

    /// <summary>
    ///   Overridable static action to facilitate testing of write to console
    /// </summary>
    public static readonly Action<string> WriteToConsole = str =>
    {
      string prefix = (UseTimestampOnConsole && !string.IsNullOrEmpty(str) ? DateTime.Now.ToString(LogDateTimeFormat) + " - " : string.Empty);

      Console.WriteLine(prefix + str);
    };

    /// <summary>
    ///   Get a rolling file appender
    /// </summary>
    /// <param name="logfile">[Optional] Log file to be used to log stuff. Defaults to "..\logs\application.log"</param>
    /// <returns>log4net rolling file appender loggin to the given file</returns>
    public static RollingFileAppender GetRollingFileAppender(string logfile = "")
    {
      // create a new log appender
      RollingFileAppender appender = new RollingFileAppender
      {
        AppendToFile = true,
        LockingModel = new FileAppender.MinimalLock(),
        RollingStyle = RollingFileAppender.RollingMode.Size,
        MaxSizeRollBackups = 10,
        MaximumFileSize = "4MB",
        StaticLogFileName = true
      };

      if (string.IsNullOrEmpty(logfile))
      {
        string[] defaultLogFileParts =
        {
          Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
          "..",
          "logs",
          "application.log"
        };
        appender.File = Path.GetFullPath(string.Join(AppConstants.PathSeparationChar, defaultLogFileParts));
      }
      else
        appender.File = Path.GetFullPath(logfile);

      PatternLayout pl = new PatternLayout
      {
        ConversionPattern = "%date{" + LogDateTimeFormat + "} %-5level \t [PID: %property{pid}] \t %message%newline"
      };
      pl.ActivateOptions();

      appender.Layout = pl;
      appender.ActivateOptions();

      return appender;
    }

    /// <summary>
    ///   Write empty line to console and log file
    /// </summary>
    /// <param name="logger">Current log4net logger</param>
    /// <param name="toConsole">[Optional] Whether to write empty line to console. Defaults to true</param>
    /// <param name="toLogfile">[Optional] Whether to write empty line to log file. Defaults to true</param>
    public static void EmptyLine(this ILog logger, bool toConsole = true, bool toLogfile = true)
    {
      if (toConsole)
        WriteToConsole(string.Empty);

      if (toLogfile)
        logger.Info(string.Empty);
    }

    /// <summary>
    ///   Log an INFO event to console
    /// </summary>
    /// <param name="logger">Current log4net logger</param>
    /// <param name="message">Log message</param>
    /// <param name="e">[Optional] Exception occurred</param>
    public static void ToConsole(this ILog logger, string message, Exception e = null)
    {
      ToConsole(logger, ELogLevel.Info, message, e);
    }

    /// <summary>
    ///   Log to console
    /// </summary>
    /// <param name="logger">Current log4net logger</param>
    /// <param name="lvl">Log level</param>
    /// <param name="message">Log message</param>
    /// <param name="e">[Optional] Exception occurred</param>
    public static void ToConsole(this ILog logger, ELogLevel lvl, string message, Exception e = null)
    {
      if (lvl != ELogLevel.Info)
        WriteToConsole(lvl.ToString().ToUpperInvariant() + ":");

      WriteToConsole(message);

      if (e == null)
        return;

      WriteToConsole(e.Message);
      WriteToConsole(e.StackTrace);
    }

    /// <summary>
    ///   Log an INFO event to the underlying logger as well as the console
    /// </summary>
    /// <param name="logger">Current log4net logger</param>
    /// <param name="message">Log message</param>
    /// <param name="e">[Optional] Exception occurred</param>
    public static void ToLoggerAndConsole(this ILog logger, string message, Exception e = null)
    {
      ToLoggerAndConsole(logger, ELogLevel.Info, message, e);
    }

    /// <summary>
    ///   Log to the underlying logger as well as the console
    /// </summary>
    /// <param name="logger">Current log4net logger</param>
    /// <param name="lvl">Log level</param>
    /// <param name="message">Log message</param>
    /// <param name="e">[Optional] Exception occurred</param>
    public static void ToLoggerAndConsole(this ILog logger, ELogLevel lvl, string message, Exception e = null)
    {
      ToConsole(logger, lvl, message, e);

      switch (lvl)
      {
        case ELogLevel.Fatal:
          logger.Fatal(message, e);
          break;

        case ELogLevel.Error:
          logger.Error(message, e);
          break;

        case ELogLevel.Info:
          logger.Info(message, e);
          break;

        case ELogLevel.Warning:
          logger.Warn(message, e);
          break;

        case ELogLevel.Debug:
          logger.Debug(message, e);
          break;
      }
    }

    /// <summary>
    /// Get logger of type
    /// </summary>
    /// <typeparam name="TType">Type of the logger</typeparam>
    /// <returns>Logger instance</returns>
    public static ILog GetLogger<TType>() where TType : class
    {
      return LogManager.GetLogger(typeof(TType));
    }
  }
}