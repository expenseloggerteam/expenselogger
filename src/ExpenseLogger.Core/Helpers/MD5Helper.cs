﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace ExpenseLogger.Core.Helpers
{
  /// <summary>
  ///   MD5 Helper
  /// </summary>
  public static class MD5Helper
  {
    private static readonly MD5 MD5Hash = MD5.Create();

    /// <summary>
    ///   Create a MD5 hash from given plain text
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string CreateHash(object obj)
    {
      return CreateHash(JsonConvert.SerializeObject(obj));
    }

    /// <summary>
    ///   Create a MD5 hash from given plain text
    /// </summary>
    /// <param name="plainText"></param>
    /// <returns></returns>
    public static string CreateHash(string plainText)
    {
      // Convert the input string to a byte array and compute the hash.
      byte[] data = MD5Hash.ComputeHash(Encoding.UTF8.GetBytes(plainText));

      // Create a new Stringbuilder to collect the bytes
      // and create a string.
      StringBuilder sBuilder = new StringBuilder();

      // Loop through each byte of the hashed data 
      // and format each one as a hexadecimal string.
      foreach (byte b in data)
        sBuilder.Append(b.ToString("x2"));

      // Return the hexadecimal string.
      return sBuilder.ToString();
    }
  }
}