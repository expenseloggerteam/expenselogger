/*
* This file is part of - ExpenseLogger application
* Copyright (C) 2014 Mihir Mone
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;

namespace ExpenseLogger.Core
{
  /// <summary>
  /// Represents a JSON process result
  /// </summary>
  [Serializable]
  public class JsonProcessResult
  {
    /// <summary>
    /// Process exit code
    /// </summary>
    public int code { get; private set; }

    /// <summary>
    /// Summary of error if exit code is not 0
    /// </summary>
    public string summary { get; private set; }

    /// <summary>
    /// Data payload for the result if any
    /// </summary>
    public object payload { get; private set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public JsonProcessResult()
    {
      code = (int)ECode.Ok;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Data payload</param>
    public JsonProcessResult(object data)
    {
      code = (int)ECode.Ok;
      payload = data;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="result">ProcessResult to initialize with</param>
    /// <param name="data">[Optional] Data payload if any. Defaults to null</param>
    public JsonProcessResult(ProcessResult result, object data = null)
    {
      code = (int)result.Code;
      summary = result.Summary;
      payload = data;
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="code">Exit code</param>
    /// <param name="summary">Summary of error if exit code is not 0</param>
    /// <param name="data">Data payload for the result if any</param>
    public JsonProcessResult(ECode code, string summary = "", object data = null)
    {
      this.code = (int)code;
      this.summary = summary;
      payload = data;
    }
  }
}
