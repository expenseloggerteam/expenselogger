﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.ServiceProcess;
using Castle.Facilities.QuartzIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Extensions;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Conf;
using ExpenseLogger.Models.Jobs;
using log4net;
using Quartz;
using Quartz.Job;

namespace ExpenseLogger.JobRunner
{
  /// <summary>
  ///   Main class which installs as a service
  /// </summary>
  class Program : ServiceBase
  {
    #region Attributes & Ctor

    private static ILog LOG;
    private IScheduler m_quartzScheduler;

    /// <summary>
    ///   Command line arguments
    /// </summary>
    public static CmdArgs Args { get; private set; }

    /// <summary>
    ///   Constructor
    /// </summary>
    /// <param name="args">Command line arguments</param>
    private Program(string[] args)
    {
      Args = new CmdArgs(args);
      ServiceName = WinSvcInstaller.GetServiceName();
    }

    #endregion Attributes & Ctor

    #region Init

    /// <summary>
    ///   Initialize
    /// </summary>
    private void Init()
    {
      LOG.ToLoggerAndConsole("Initialising");
      AppConstants.CacheDirectory = Path.GetFullPath(Assembly.GetExecutingAssembly().Location + @"\..\..\cache");

      // Initialize castle components
      try
      {
        if (string.IsNullOrEmpty(Args.ConfigFile))
        {
          LOG.ToLoggerAndConsole(ELogLevel.Error, "No configuration file specified. I do plan to become self-aware and take over the world, but not yet...");
          Args.ShowHelp();

          Environment.Exit(1);
        }

        LOG.ToLoggerAndConsole("Reading configuration from: " + Args.ConfigFile);
        IWindsorContainer container = new PropertyResolvingWindsorContainer(Args.ConfigFile);
        CastleWindsorModelsConfig.Configure(container);

        WindsorJobFactory factory = new WindsorJobFactory(container.Kernel);

        string quartzJobsFile = Path.GetDirectoryName(Args.ConfigFile) + "\\quartz-jobs.config";

        if (!File.Exists(quartzJobsFile))
          IOResourceHelper.CreateResourceAtPath(Assembly.GetExecutingAssembly(), "ExpenseLogger.JobRunner.etc", "quartz-jobs.config", Path.GetDirectoryName(quartzJobsFile));

        IDictionary<string, string> properties = new Dictionary<string, string>
        {
          {"quartz.scheduler.instanceName", "XmlConfiguredInstance"},
          {"quartz.threadPool.type", "Quartz.Simpl.SimpleThreadPool, Quartz"},
          {"quartz.threadPool.threadCount", "4"},
          {"quartz.threadPool.threadPriority", "Normal"},
          {"quartz.plugin.xml.type", "Quartz.Plugin.Xml.XMLSchedulingDataProcessorPlugin, Quartz"},
          {"quartz.plugin.xml.scanInterval", "10"},
          {"quartz.plugin.xml.fileNames", quartzJobsFile}
        };

        m_quartzScheduler = new QuartzNetScheduler(properties, factory, container.Kernel);

        JobsRegistrar.RegisterWith(m_quartzScheduler);
      }
      catch (Exception ex)
      {
        LOG.Error("MMM.ExpenseLogger.TaskHandler.Program.Init: Unable to process castle configuration", ex);

        Console.WriteLine("Unable to process configuration file. See log file for more details");
        Environment.Exit(1);
      }
    }

    #endregion Init

    #region IsAdmin

    /// <summary>
    ///   Whether the current process is running as administrator
    /// </summary>
    /// <returns>True if administrator or false</returns>
    private static bool IsAdmin()
    {
      WindowsIdentity identity = WindowsIdentity.GetCurrent();

      WindowsPrincipal principal = new WindowsPrincipal(identity);

      return principal.IsInRole(WindowsBuiltInRole.Administrator);
    }

    #endregion IsAdmin

    #region Main

    /// <summary>
    ///   Main function
    /// </summary>
    /// <param name="args">Command line arguments</param>
    private static void Main(string[] args)
    {
      //Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter();

      const string logFile = @"..\logs\jobrunner.log";
      Log4NetConfig.Configure(logFile);

      LOG = Log4NetHelper.GetLogger<Program>();
      LOG.ToLoggerAndConsole("Pre-processing");


      Program p = new Program(args);

      switch (Args.Mode)
      {
        case RunMode.Install:
          p.Init();
          p.InstallService();
          break;

        case RunMode.Uninstall:
          p.Init();
          p.UninstallService();
          break;

        case RunMode.Interactive:
          p.Init();
          p.WorkerFunction();
          break;

        case RunMode.Service:
          p.Init();
          Run(p);
          break;

        case RunMode.Help:
          Args.ShowHelp();
          break;

        default:
          Args.ShowHelp();
          break;
      }
    }

    #endregion Main

    #region InstallService

    /// <summary>
    ///   Installs the TaskHandler windows service
    /// </summary>
    protected void InstallService()
    {
      LOG.ToLoggerAndConsole("Installing windows service");

      if (!IsAdmin())
      {
        Console.WriteLine("You must run the install step from an Administrator command prompt");
        Environment.Exit(1);
      }

      IEnumerable<ServiceController> sc = ServiceController.GetServices().Where(f => f.ServiceName == WinSvcInstaller.GetServiceName());

      // if the service is already installed, uninstall it before proceeding
      if (sc.Count() != 0)
        UninstallService();

      ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
    }

    #endregion InstallService

    #region UninstallService

    /// <summary>
    ///   Uninstalls the TaskHandler windows service
    /// </summary>
    protected void UninstallService()
    {
      LOG.ToLoggerAndConsole("Uninstalling windows service");

      if (!IsAdmin())
      {
        Console.WriteLine("You must run the uninstall step from an Administrator command prompt");
        Environment.Exit(1);
      }

      try
      {
        ServiceController sc = new ServiceController(WinSvcInstaller.GetServiceName());

        switch (sc.Status)
        {
          case ServiceControllerStatus.Stopped:
            break;

          case ServiceControllerStatus.StopPending:
            sc.WaitForStatus(ServiceControllerStatus.Stopped);
            break;

          case ServiceControllerStatus.StartPending:
            sc.WaitForStatus(ServiceControllerStatus.Running);
            sc.Stop();
            sc.WaitForStatus(ServiceControllerStatus.Stopped);
            break;

          default:
            sc.Stop();
            sc.WaitForStatus(ServiceControllerStatus.Stopped);
            break;
        }

        ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
      }
      catch (InvalidOperationException ex)
      {
        if (!ex.Message.Contains("was not found on computer"))
          return;

        Console.WriteLine(WinSvcInstaller.GetServiceName() + " does not exist on this computer");
        LOG.Error(WinSvcInstaller.GetServiceName() + " does not exist on this computer");
      }
    }

    #endregion UninstallService

    #region ServiceBase overrides

    /// <summary>
    ///   On start event of the service. This method is fired when
    ///   the service is started via the MMC
    /// </summary>
    /// <param name="args">
    ///   Command line arguments automatically passed through
    ///   from the Main function
    /// </param>
    protected override void OnStart(string[] args)
    {
      LOG.ToLoggerAndConsole("Starting windows service");

      base.OnStart(args);

      WorkerFunction();
    }

    /// <summary>
    ///   On stop event of the service. This method is fired when the
    ///   service is stopped via the MMC
    /// </summary>
    protected override void OnStop()
    {
      LOG.ToLoggerAndConsole("Stopping windows service");

      base.OnStop();

      // stop the quartz scheduler
      m_quartzScheduler.Shutdown(true);
    }

    #endregion ServiceBase overrides

    #region WorkerFunction

    /// <summary>
    ///   Worker function that runs the tasks
    /// </summary>
    private void WorkerFunction()
    {
      // start the quartz scheduler
      m_quartzScheduler.Start();

      LOG.ToLoggerAndConsole("Quartz scheduler started");
    }

    #endregion WorkerFunction
  }
}