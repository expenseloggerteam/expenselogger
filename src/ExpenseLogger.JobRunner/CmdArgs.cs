﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace ExpenseLogger.JobRunner
{
  /// <summary>
  ///   Command line arguments
  /// </summary>
  public class CmdArgs
  {
    /// <summary>
    ///   Custom castle config file
    /// </summary>
    public string ConfigFile { get; private set; }

    /// <summary>
    ///   Run mode for the task handler
    /// </summary>
    public RunMode Mode { get; private set; }

    /// <summary>
    ///   Default constructor
    /// </summary>
    /// <param name="args">Command line arguments</param>
    public CmdArgs(string[] args)
    {
      Mode = RunMode.Interactive;
      ConfigFile = string.Empty;

      for (int i = 0; i < args.Length; i++)
      {
        string arg = args[i];
        switch (arg.ToLower())
        {
          case "-c":
          case "--configfile":
            ConfigFile = Path.GetFullPath(args[++i]);
            break;

          case "-i":
          case "--install":
            Mode = RunMode.Install;
            break;

          case "-u":
          case "--uninstall":
            Mode = RunMode.Uninstall;
            break;

          case "-s":
          case "--service":
            Mode = RunMode.Service;
            break;

          case "-h":
          case "--help":
            Mode = RunMode.Help;
            break;

          default:
            Console.WriteLine("\nUnknown switch: " + arg);
            Mode = RunMode.Unknown;
            break;
        }
      }

      //// Retrieve ExpenseLogger installation folder
      //RegistryKey rKey = Registry.LocalMachine.OpenSubKey(string.Format("SOFTWARE"));
      //try
      //{
      //  if (rKey != null) AppConstants.RegistryRoot = rKey.OpenSubKey("ExpenseLogger");
      //}
      //catch (Exception)
      //{
      //  try
      //  {
      //    if (rKey != null) AppConstants.RegistryRoot = rKey.OpenSubKey("Wow6432Node\\ExpenseLogger");
      //  }
      //  catch (Exception)
      //  {
      //    throw new Exception("ExpenseLogger installation has been tampered or corrupt. Please reinstall and try again. If this does not solve the issue, please contact Mihir Mone (monemihir@gmail.com)");
      //  }
      //}

      //// set config file to default if none was given
      //ConfigFile = string.IsNullOrEmpty(ConfigFile) ? AppConstants.InstallPath + @"\Etc\castle.config" : ConfigFile;
    }

    /// <summary>
    ///   Shows the utility help
    /// </summary>
    public void ShowHelp()
    {
      Console.WriteLine("\nUsage:");
      Console.WriteLine("--configfile or -c\tCastle Windsor configuration file to use. Normally in \n\t\t\tEtc folder of ExpenseLogger");
      Console.WriteLine("--service or -s\t\tRun as a windows service");
      Console.WriteLine("--install or -i\t\tInstall this utility as a windows service");
      Console.WriteLine("--uninstall or -u\tUninstall this utility as a windows service");
      //Console.WriteLine("--runlocal or -r\tRun this utility from command line");
      Console.WriteLine("--help or -h or /?\tDisplay this help\n");
    }
  }
}