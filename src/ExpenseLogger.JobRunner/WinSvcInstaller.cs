﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;
using System.Text;

namespace ExpenseLogger.JobRunner
{
  /// <summary>
  ///   Reminders service installer class
  /// </summary>
  [RunInstaller(true)]
  public class WinSvcInstaller : Installer
  {
    #region Attributes and Ctor

    /// <summary>
    ///   Task Handler service name prefix. See WinSvcInstaller.GetServiceName() to
    ///   retrieve full service name
    /// </summary>
    const string ServiceNamePrefix = "ExpenseLogger JobRunner";

    /// <summary>
    ///   Default constructor which installs and registers the service with Windows
    /// </summary>
    public WinSvcInstaller()
    {
      ServiceProcessInstaller procInstaller = new ServiceProcessInstaller();
      ServiceInstaller svcInstaller = new ServiceInstaller();

      // Set priviledges
      procInstaller.Account = ServiceAccount.LocalSystem;

      svcInstaller.DisplayName = GetServiceName();
      svcInstaller.StartType = ServiceStartMode.Automatic;
      svcInstaller.DelayedAutoStart = true;
      svcInstaller.Description = "Handles jobs for ExpenseLogger";

      // This name MUST be the same as the one set in Program.Ctor()
      svcInstaller.ServiceName = GetServiceName();

      Installers.Add(procInstaller);
      Installers.Add(svcInstaller);

      AfterInstall += TaskHandlerServiceInstaller_AfterInstall;
    }

    #endregion Attributes and Ctor

    #region GetServiceName method

    /// <summary>
    ///   Gets the windows service name
    /// </summary>
    /// <returns>Task Handler's windows service name</returns>
    public static string GetServiceName()
    {
      Version v = Assembly.GetExecutingAssembly().GetName().Version;

      return string.Format("{0} {1}.{2}", ServiceNamePrefix, v.Major, v.Minor);
    }

    #endregion GetServiceName method

    #region Override for Install method

    /// <summary>
    ///   Overrides the default Install method to add startup parameters
    /// </summary>
    /// <param name="stateSaver">Current context state</param>
    public override void Install(IDictionary stateSaver)
    {
      StringBuilder path = new StringBuilder(Context.Parameters["assemblypath"]);
      if (path[0] != '"')
      {
        path.Insert(0, '"');
        path.Append('"');
      }

      path.Append(" --service");
      path.Append(" --configfile");
      path.Append(" \"" + Program.Args.ConfigFile + "\"");

      Context.Parameters["assemblypath"] = path.ToString();

      base.Install(stateSaver);
    }

    #endregion Override for Install method

    /// <summary>
    ///   Automatically start the service that was installed
    /// </summary>
    /// <param name="sender">Sender object parameters</param>
    /// <param name="e">Result of installation</param>
    private void TaskHandlerServiceInstaller_AfterInstall(object sender, InstallEventArgs e)
    {
      Console.WriteLine("\nService " + GetServiceName() + " is being started");

      using (ServiceController sc = new ServiceController(GetServiceName()))
      {
        sc.Start();
      }
    }
  }
}