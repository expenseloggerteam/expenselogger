﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace ExpenseLogger.JobRunner
{
  /// <summary>
  ///   All available run modes for TaskHandler
  /// </summary>
  public enum RunMode
  {
    /// <summary>
    ///   Run from command line
    /// </summary>
    Interactive,

    /// <summary>
    ///   Run as windows service
    /// </summary>
    Service,

    /// <summary>
    ///   Install windows service
    /// </summary>
    Install,

    /// <summary>
    ///   Uninstall windows service
    /// </summary>
    Uninstall,

    /// <summary>
    ///   Show utility help
    /// </summary>
    Help,

    /// <summary>
    ///   Unknown run mode. Occurs when user specifies an invalid command line argument
    /// </summary>
    Unknown
  }
}