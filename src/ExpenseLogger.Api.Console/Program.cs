﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2017 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Diagnostics;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Extensions.SubSystems;
using ExpenseLogger.Api.Models;
using ExpenseLogger.Api.Models.Config;
using ExpenseLogger.Api.Models.IOC;
using ExpenseLogger.Core;
using ExpenseLogger.Core.Helpers;
using log4net;
using log4net.Appender;
using log4net.Config;
using Microsoft.Owin.Hosting;

namespace ExpenseLogger.Api.Console
{
  /// <summary>
  ///   Main runner program
  /// </summary>
  internal static class Program
  {
    private static ILog LOGGER;

    static int Main(string[] args)
    {
      int exitCode = 0;
      GlobalContext.Properties["pid"] = Process.GetCurrentProcess().Id;

      IAppender appender = Log4NetHelper.GetRollingFileAppender();
      BasicConfigurator.Configure(appender);

      LOGGER = LogManager.GetLogger("Program");
      LOGGER.EmptyLine();

      try
      {
        LOGGER.ToLoggerAndConsole("Initializing components");
        //IWindsorInstaller defaultConf = Configuration.FromAppConfig();
        IWindsorInstaller propertiesSubSystemConf = PropertiesSubSystem.FromAppConfig();
        IWindsorContainer container = new WindsorContainer();
        container.Install(propertiesSubSystemConf);
        container.Install(new WebWindsorInstaller());

        container.Register(Component.For<AbstractSwaggerPathProvider>().ImplementedBy<ConsoleSwaggerPathProvider>().LifeStyle.Transient);

        ApiConfigProperty cfg = container.Resolve<ApiConfigProperty>();

        LOGGER.ToLoggerAndConsole("Starting ExpenseLogger Http Server");

        using (WebApp.Start(cfg.BaseAddress, appBuilder => new Startup(container).Configuration(appBuilder)))
        {
          LOGGER.ToLoggerAndConsole(string.Format("Server running at {0}", cfg.BaseAddress));
          LOGGER.ToLoggerAndConsole("Press ENTER to exit");
          LOGGER.EmptyLine();

          System.Console.ReadLine();
        }
      }
      catch (Exception ex)
      {
        LOGGER.ToLoggerAndConsole(ELogLevel.Error, "Unable to start server", ex);

        exitCode = 1;
      }

      return exitCode;
    }
  }
}