﻿// 
// This file is part of - ExpenseLogger application
// Copyright (C) 2016 Mihir Mone
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ExpenseLogger.Core.Helpers;
using ExpenseLogger.Models.Conf;
using ExpenseLogger.Models.Core;
using ExpenseLogger.Models.Data;
using ExpenseLogger.Models.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace ExpenseLogger.Models.Test.Security
{
  /// <summary>
  ///   <see cref="DefaultUserRepository" /> unit tests
  /// </summary>
  [TestClass]
  public class DefaultUserRepositoryTest
  {
    private ConfigProperty m_conf;
    private DefaultUserRepository m_repo;
    private string m_defaultPassword;
    private User m_defaultUser;

    /// <summary>
    ///   Test setup
    /// </summary>
    [TestInitialize]
    public void Initialise()
    {
      m_conf = new ConfigProperty {
        DbType = ExpenseLogger.Core.EPersistenceType.InMemory
      };

      m_repo = new DefaultUserRepository(new DataContextFactory(m_conf));

      m_defaultPassword = "test@123";
      m_defaultUser = new User
      {
        Username = "test",
        PasswordHash = MD5Helper.CreateHash(m_defaultPassword),
        FullName = "Test User",
        Country = "India",
        Email = "test@test.com",
        IsAdmin = false,
        IsVerified = true,
        SecretQuestion = "no question",
        SecretAnswer = MD5Helper.CreateHash("no answer"),
        LastLoginDate = DateTime.Now,
        MemberSince = DateTime.Now,
        SettingsJson = JsonConvert.SerializeObject(new UserSetting())
      };
    }

    /// <summary>
    ///   Test that <see cref="DefaultUserRepository.SaveUser(Core.User)" /> and
    ///   <see cref="DefaultUserRepository.GetUser(string)" /> works as expected
    /// </summary>
    [TestMethod]
    public void SaveUser_And_GetUser_Test()
    {
      // act
      m_repo.SaveUser(m_defaultUser);

      // assert
      User savedUser = m_repo.GetUser(m_defaultUser.Username);

      Assert.IsNotNull(savedUser);
      Assert.AreNotEqual(0, savedUser.Id);
    }

    /// <summary>
    ///   Test that <see cref="DefaultUserRepository.ValidateUser(string, string)" /> works as expected
    /// </summary>
    [TestMethod]
    public void ValidateUser_Test()
    {
      // test non existent user
      // act
      bool isValid = m_repo.ValidateUser("random user", "jdlkslkaoipqowe");

      // assert
      Assert.IsFalse(isValid);

      // test existing user
      // arrange
      m_repo.SaveUser(m_defaultUser);

      // act
      isValid = m_repo.ValidateUser(m_defaultUser.Username, m_defaultPassword);

      // assert
      Assert.IsTrue(isValid);
    }
  }
}