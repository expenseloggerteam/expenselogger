REM ~ go one level up
cd..

REM ~ clone WebExtras at this level
git clone --branch=master https://github.com/monemihir/webextras WebExtras

REM ~ restore nuget packages for WebExtras
cd WebExtras
nuget restore trunk\WebExtras.sln

REM ~ go one level up
REM ~ cd..

REM ~ clone CastleWindsorExtensions at this level
REM ~ git clone --branch=master https://github.com/monemihir/castle-windsor-extensions.git CastleWindsorExtensions

REM ~ restore nuget packages for CastleWindsorExtensions
REM ~ cd CastleWindsorExtensions
REM ~ nuget restore src\CastleWindsorExtensions.sln

REM ~ go back to ExpenseLogger and restore nuget packages for ExpenseLogger
cd ..\ExpenseLogger
nuget restore src\ExpenseLogger.sln

