rmdir /S /Q artifacts
rmdir /S /Q dist

mkdir dist

REM ~ Making ExpenseLogger deploy package
xcopy /I /Y /S /E "src\ExpenseLogger.Web\bin" "dist\bin"
xcopy /I /Y /S /E "src\ExpenseLogger.Web\Content" "dist\Content"
xcopy /I /Y "src\ExpenseLogger.Web\Etc\expenselogger.config" "dist\Etc\"
xcopy /I /Y /S /E "src\ExpenseLogger.Web\Scripts" "dist\Scripts"
xcopy /I /Y /S /E "src\ExpenseLogger.Web\Views" "dist\Views"
xcopy /I /Y "src\ExpenseLogger.Web\Web.config" "dist\"
xcopy /I /Y "src\ExpenseLogger.Web\packages.config" "dist\"
xcopy /I /Y "src\ExpenseLogger.Web\Global.asax" "dist\"
xcopy /I /Y "src\ExpenseLogger.Web\Project_Readme.html" "dist\"

REM ~ Making DbMigrator deploy package
xcopy /I /Y /S /E "src\ExpenseLogger.DbMigrator\bin" "dist\bin"
xcopy /I /Y "src\ExpenseLogger.DbMigrator\Etc\dbmigrator.config" "dist\Etc\"

REM ~ Making JobRunner deploy package
xcopy /I /Y /S /E "src\ExpenseLogger.JobRunner\bin" "dist\bin"

REM ~ Making Api deploy package
REM ~ xcopy /I /Y /S /E "src\ExpenseLogger.Api.Console\bin" "dist\bin"
REM ~ xcopy /I /Y /S /E "src\ExpenseLogger.Api.Console\Etc" "dist\Etc"
REM ~ xcopy /I /Y /S /E "src\ExpenseLogger.Api.IIS\bin" "dist\bin"

REM ~ Cleaning up unnecessary files
del "dist\bin\*.pdb"
del "dist\bin\*.xml"
del "dist\bin\*vshost.*"

REM ~ Creating 7zip archive
7z a artifacts\ExpenseLoggerDebug-v%APPVEYOR_BUILD_VERSION%-x86.zip dist