# ExpenseLogger

As the name suggests, this is a expenses logging and monitoring tool. Using ExpenseLogger you can log your daily expenses and get a brief statistical analysis. ExpenseLogger also makes a low level trend analysis giving you estimations for your monthly and yearly projected spend amounts. You can get statistics in a tabular format as well as a graphical format. You also get average and total spend amounts for the ongoing month and year. 

For convenience, your expenses can be divided into categories like Food, Grocery, Transport etc. which come prebuilt with the system. What's good is that you are not limited to these categories alone and you can always add new categories of your own. You can also rename and delete categories as per your convenience. 

ExpenseLogger also facilitates generation of tabular and graphical reports.

## Build Status

[![AppVeyor](https://ci.appveyor.com/api/projects/status/aaqftacs4je5l96v?svg=true)](https://ci.appveyor.com/project/monemihir/expenselogger)

## License

ExpenseLogger is licensed under [AGPLv3.0](https://bitbucket.org/expenseloggerteam/expenselogger/raw/894452b1681fd0c558f3e3d4efc1d87c84302d7a/LICENSE)

## Shout out

Here's a shout out to the cool guys at

* BitBucket for code hosting
* [JetBrains](https://www.jetbrains.com) for development tools and goodies
* [AppVeyor](https://www.appveyor.com) for continuous integration (Windows)